from typing import List, Tuple, Dict, Any, Optional, Union
import pandas as pd
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.io.shapereader as shpreader
import folium
import geopandas as gpd
import matplotlib.pyplot as plt
import numpy as np
import xarray as xr


def add_wms_layer(ax, region):
    wms_url = {
        "Bay of Brest": "https://services.data.shom.fr/INSPIRE/wms/r?service=WMS&version=1.3.0&request=GetCapabilities",
        "Another Region": "https://another.url/for/region",
    }
    layers = {"Bay of Brest": ["LITTO3D_FINISTR_2014_PYR_3857_WMSR"], "Another Region": ["AnotherLayer"]}
    ax.add_wms(
        wms=wms_url.get(region),
        layers=layers.get(region),
        alpha=0.4,
    )


def add_shapefile_layer(ax, region):

    coastFile = "/Users/spetton/DATA/SIG/FondDeCarte/TEMP/Finistere.shp"
    tch = shpreader.Reader(coastFile)
    tmp = [rcd.geometry for rcd in tch.records() if rcd.attributes["Feature"] == "TERRE"]
    shape_feature = cfeature.ShapelyFeature(tmp, ccrs.PlateCarree(), facecolor=None)
    shapefile_path = {
        "Bay of Brest": "/Users/spetton/DATA/SIG/FondDeCarte/TEMP/Finistere.shp",
        "Another Region": "/path/to/another_region_shapefile.shp",
    }
    # shape_feature = gpd.read_file(shapefile_path.get(region))
    # shape_feature.plot(ax=ax, facecolor="lightgrey", edgecolor="k", linewidth=0.1, transform=ccrs.PlateCarree())
    ax.add_feature(shape_feature, facecolor="lightgrey", edgecolor="k", linewidths=0.1, zorder=-15)


def add_netcdf_layer(ax, region):
    netcdf_path = {
        "Bay of Brest": "/path/to/bay_of_brest_shapefile.shp",
        "Another Region": "/path/to/another_region_shapefile.shp",
    }
    bathy_data = xr.open_dataset(netcdf_path.get(region))
    H0 = bathy_data["H0"].values
    lon = bathy_data["longitude"].values
    lat = bathy_data["latitude"].values

    ax.contourf(lon, lat, H0, transform=ccrs.PlateCarree(), cmap="Blues", alpha=0.5)


_all__ = ["plot_geographic_position_static", "plot_geographic_position_interactive"]


def plot_geographic_position_static(
    df: Union[pd.DataFrame, List],
    ax: Optional = None,
    region: str = "Bay of Brest",
    background: List[str] = [],
    projection=ccrs.PlateCarree(),
):
    """
    Plots the geographic position of the data points on a given axis with the option to add either a WMS layer, a shapefile, or a bathymetry layer.

    Parameters:
    - df: DataFrame containing the data with columns 'Longitude', 'Latitude', and 'num'.
    - ax: The matplotlib axis to plot on. If None, a new figure and axis will be created.
    - region: The geographic region for the WMS layer or shapefile.
    - background: List of background layers to add ("wms", "shapefile", "netcdf").
    - projection: The projection for the map. Default is ccrs.PlateCarree().
    """
    plt.rcParams.update(
        {
            "font.size": 7,
            "font.family": "serif",
            "font.serif": ["Latin Modern Roman"],
            "font.weight": "bold",
            "axes.labelsize": 9,
        }
    )

    if ax is None:
        fig, ax = plt.subplots(subplot_kw={"projection": projection}, figsize=(8.27, 11.69), constrained_layout=True)

    # Plot geographic position
    if isinstance(df, pd.DataFrame):
        ax.scatter(df["Longitude"], df["Latitude"], s=1, c=df["num"], marker=",", transform=ccrs.PlateCarree())
    else:
        for i, yuco in enumerate(df):
            df = yuco.dfMis
            ax.plot(
                df["Longitude"],
                df["Latitude"],
                #s=(i + 0.5) * 2,
                #c=df["num"],
                marker=",",
                transform=ccrs.PlateCarree(),
                label=f"Yuco {i+1}",
            )
        ax.legend()

    def add_layer(layer_type):
        if layer_type == "wms":
            add_wms_layer(ax, region)
        elif layer_type == "shapefile":
            add_shapefile_layer(ax, region)
        elif layer_type == "netcdf":
            add_netcdf_layer(ax, region)

    for layer in background:
        add_layer(layer)

    xmin, xmax = df["Longitude"].min(), df["Longitude"].max()
    ymin, ymax = df["Latitude"].min(), df["Latitude"].max()
    dx = (xmax - xmin) * 0.5
    dy = (ymax - ymin) * 0.5
    dd = max(dx, dy)
    ax.set_extent([xmin - dd, xmax + dd, ymin - dd, ymax + dd], crs=ccrs.PlateCarree())

    gl = ax.gridlines(draw_labels=True)
    gl.top_labels = True
    gl.right_labels = True
    gl.bottom_labels = False
    gl.left_labels = False

    if ax is None:
        plt.show()


color_mapping = {
    1: "blue",
    2: "green",
    3: "red",
    4: "purple",
    5: "orange",
    6: "yellow",
    7: "pink",
    8: "brown",
    9: "gray",
    10: "black",
    11: "cyan",
    12: "magenta",
    13: "lime",
    14: "maroon",
    15: "navy",
    16: "olive",
    17: "teal",
    18: "violet",
    19: "gold",
    20: "indigo",
}


def plot_geographic_position_interactive(df, region="Bay of Brest", geographic_data=None):
    """
    Creates an interactive map using Folium.

    Parameters:
    - df: DataFrame containing the data with columns 'Longitude', 'Latitude', and 'num'.
    - region: The geographic region for the WMS layer or shapefile.
    - geographic_data: Type of geographic data to add ("wms", "shapefile", or None).
    """
    folium_map = folium.Map(location=[df.Latitude.mean(), df.Longitude.mean()], zoom_start=12)

    for _, row in df.iterrows():
        folium.CircleMarker(
            location=[row["Latitude"], row["Longitude"]],
            # location=[row['CorrectedLatitude'], row['CorrectedLongitude']],
            radius=1,
            color=color_mapping.get(row["num"], "white"),
            fill=True,
            fill_opacity=0.6,
        ).add_to(folium_map)

    if geographic_data == "wms":
        # Add WMS Layer to Folium map (Example for Bay of Brest)
        wms_layer = folium.raster_layers.WmsTileLayer(
            url="https://services.data.shom.fr/INSPIRE/wms/r",
            name="LITTO3D_FINISTR_2014_PYR_3857_WMSR",
            fmt="image/png",
            layers="LITTO3D_FINISTR_2014_PYR_3857_WMSR",
            transparent=True,
            version="1.3.0",
        )
        folium_map.add_child(wms_layer)

    elif geographic_data == "shapefile":
        # Add shapefile to Folium map
        shapefile_path = {
            "Bay of Brest": "/path/to/bay_of_brest_shapefile.shp",
            "Another Region": "/path/to/another_region_shapefile.shp",
        }
        gdf = gpd.read_file(shapefile_path.get(region))
        folium.GeoJson(gdf).add_to(folium_map)

    return folium_map
    # folium_map.save("geographic_position_map.html")
    # print("Interactive map saved as 'geographic_position_map.html'")


# Example usage:
# plot_geographic_position_interactive(df, region="Bay of Brest", geographic_data="wms")
# plot_geographic_position_interactive(df, region="Bay of Brest", geographic_data="shapefile")

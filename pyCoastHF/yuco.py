# -*- coding: utf-8 -*-
import glob
import os
import re
import json
import platform
import numpy as np
import pandas as pd
import datetime as dt
from collections import defaultdict
from typing import List, Tuple, Dict, Any, Optional, Union

from matplotlib import rc
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import cmocean
import seaborn as sns

# Need for CTD
import gsw
from . import rbr_odo

import cartopy.crs as ccrs

from . import common, carto

cfg = common.data_config("yucos")


# - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * #
# - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * #


class Mission:
    def __init__(self, num=0):
        self.num = num
        self.name = None
        self.dfRaw = pd.DataFrame()
        self.dfMis = pd.DataFrame()

    def reader(self, csvPath="default.csv"):
        # TODO : ne lire que les données de navigation de l'AUV ?
        dfRaw = pd.read_csv(csvPath)
        self.name = os.path.basename(os.path.dirname(csvPath))[16:]

        # Replace header names with python compatible names
        dfRaw.columns = (
            dfRaw.columns.str.replace(" ", "", regex=True)
            .str.replace(r"\(.*\)", "", regex=True)
            .str.replace("+0", "", regex=False)
            .str.replace("INX", "", regex=False)
        )

        # Creating a dataframe that only contains the data when the auv is in mission status
        dfMis = dfRaw[dfRaw.loc[:, "AUVStatus"] == "MISSION"]
        dfMis = dfMis[(dfMis["Latitude"] != 0) & (dfMis["Longitude"] != 0)]

        # Calculating the absolute distance travelled (m) by the AUV with Haversine function
        latRad = np.deg2rad(dfMis["CorrectedLatitude"])
        longRad = np.deg2rad(dfMis["CorrectedLongitude"])

        # Absolute distance travelled in meters
        deltaLatRad = latRad.diff()
        deltaLongRad = longRad.diff()
        a = np.sin(deltaLatRad / 2) ** 2 + np.cos(latRad.shift(1)) * np.cos(latRad) * np.sin(deltaLongRad / 2) ** 2
        c = 2 * np.arcsin(np.sqrt(a))
        m = 6378.1e3 * c
        dfMis["absDistance"] = m.cumsum()

        # Calculating the bathymetry from altitude and depth
        dfMis["Bathymetry"] = dfMis["Altitude"] + dfMis["Depth"]

        dfRaw["num"] = self.num
        dfMis["num"] = self.num

        self.dfRaw = dfRaw
        self.dfMis = dfMis


# - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * #
# - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * #

def detect_name_from_path(path: str):
    match = re.search(r"YUCO-\w{8}", path)
    name = match.group(0) if match else None
    if name not in cfg:
        raise ValueError(f"Name '{name}' not found in cfg/config_yucos.cfg")
    return name


def get_yuconames(cruisePath: str) -> List[str]:
    """
    Retrieve the names of YUCO directories from the given cruise path.

    Args:
        cruisePath (str): The path to the cruise directory.

    Returns:
        List[str]: A list of YUCO directory names.
    """
    exports_path = os.path.join(cruisePath, "exports")
    yuco_names = [x for x in os.listdir(exports_path) if "YUCO" in x and os.path.isdir(os.path.join(exports_path, x))]

    print("YUCO deployed:", yuco_names)
    return yuco_names


def get_filenames(cruisePath: str, yucoNames: List[str]) -> Tuple[List[str], List[str]]:
    """
    Retrieve the names of CSV files and mission files from a given directory path.

    Args:
        cruisePath (str): The path to the cruise directory.
        yucoNames (List[str]): A list of YUCO names.

    Returns:
        Tuple[List[str], List[str]]: A tuple containing a sorted list of CSV file names and a sorted list of mission file names.
    """
    csvNames = []
    missions = []
    sep = os.path.sep
    cruiseName = cruisePath.split(sep)[-2]

    for root, dirs, files in os.walk(cruisePath):
        for file in files:
            if file.endswith(".json"):
                missions.append(f"{root}/{file}")

    for yucoName in yucoNames:
        folderPath = os.path.join(cruisePath, "exports", yucoName)
        if not os.path.isdir(folderPath):
            continue

        for root, dirs, files in os.walk(folderPath):
            for file in files:
                if file.startswith(".") or file.endswith((".html", ".pdf")):
                    continue

                if file.startswith("dataset") and file.endswith(".csv") and not "_alt_" in file:
                    relative_path = os.path.relpath(root, cruisePath + "/exports/")
                    csvNames.append(f"{relative_path}/{file}")

    csvNames.sort()
    missions.sort()

    return csvNames, missions

def read_all_mission(cruisePath, csvNames):
    yucos = []
    yuco_files = defaultdict(list)

    for csv in csvNames:
        csvfile = cruisePath + '/exports/' + csv
        name = detect_name_from_path(csvfile)
        if name is None:
            print("Unable to detect YUCO id for file:", csvfile)
            continue
        yuco_files[name].append(csvfile)

    for name, files in yuco_files.items():
        y = YUCO(name)
        y.read_mission(files)
        y.filter_air_data()
        yucos.append(y)
        
    return yucos


class YUCO:
    def __init__(self, name: str = None):
        """
        Initializes the YUCO object with the given name and sets the type based on the configuration dictionary.

        Args:
            name: The serial number of the YUCO AUV.

        Returns:
            None
        """
        self.name = name
        self.type = cfg.get(name, {}).get("type", None)
        self.dfMis = pd.DataFrame()
        self.dataset = []
        self.num_mis = 0

    def detect_name_from_path(self, path: str):
        match = re.search(r"YUCO-\w{8}", path)
        name = match.group(0) if match else None
        if name not in cfg:
            raise ValueError(f"Name '{name}' not found in cfg/config_yucos.cfg")
        self.name = name
        self.type = cfg[name]["type"]
        print("I'm Yuco : %s" % name)

    def concat_missions(self, missions: List[Mission]) -> Mission:
        """
         Concatenates multiple Mission objects into one, updating `absDistance` and other properties.

         Args:
             missions: A list of Mission objects to concatenate.

        Returns:
             A single Mission object representing the combined missions.
        """
        combined = Mission()
        cumulative_distance = 0
        for m in missions:
            m.dfMis["absDistance"] += cumulative_distance  # Mettre à jour la distance
            cumulative_distance = m.dfMis["absDistance"].iloc[-1]  # Distance cumulative après cette mission
            combined.dfMis = pd.concat([combined.dfMis, m.dfMis], axis=0)
            combined.dfRaw = pd.concat([combined.dfRaw, m.dfRaw], axis=0)
        combined.dfMis.reset_index(drop=True, inplace=True)
        combined.dfRaw.reset_index(drop=True, inplace=True)
        return combined

    def read_mission(self, csvfile: List[str], concat=False):
        """
        Reads and concatenates mission data from CSV files, sets the index to the timestamp, and reduces the data to CTD measurements if applicable.
        If `concat` is True, combines the missions into one.

        Args:
            csvfile: A list of CSV file paths for the mission data.

        Returns:
            None
        """
        if not isinstance(csvfile, list):
            csvfile = [csvfile]

        missions = []
        for i, csv in enumerate(csvfile):
            if self.name is None:
                self.detect_name_from_path(csv)
            m = Mission(i + 1)
            m.reader(csv)
            missions.append(m)

        # Concatenate missions if required
        if concat:
            combined_mission = self.concat_missions(missions)
            self.dfMis = combined_mission.dfMis
            self.dfMis["num"] = 1
            self.num_mis = 1
            self.dataset.append(csv)
        else:
            self.dfMis = pd.concat([m.dfMis for m in missions], axis=0)
            self.num_mis = len(missions)
            self.dataset.append(csv)
        self.dfMis.reset_index(drop=True, inplace=True)

        if self.dfMis.empty:
            raise ValueError("No mission found")

        self.dfMis["time"] = pd.to_datetime(self.dfMis["TimestampUTC"], unit="s")
        self.dfMis.set_index("time", inplace=True)

        if any(self.type.startswith(prefix) for prefix in ["CTD", "PHYSICO"]):
            self.reduce_to_ctd()

    def reduce_to_ctd(self):
        """
        Drops rows with missing CTD measurements and calculates additional variables such as absolute salinity, conservative temperature, and density.

        Args:
            None

        Returns:
            None
        """
        if self.type.startswith("CTD"):
            ctd = "Legato3"
            name_temp = "Legato3Temperature"
            name_psal = "Legato3Salinity"
            name_pres = "Legato3SeaPressure"
        else:
            ctd = "AML"
            self.dfMis.rename(columns={"AMLConductivityTemperature": "AMLTemperature"}, inplace=True)
            name_temp = "AMLTemperature"
            name_psal = "AMLSalinity"
            name_pres = "AMLPressure"

        self.dfMis.dropna(subset=[name_temp], inplace=True)

        if self.type.startswith("PHYSICO"):
            self.dfMis = self.dfMis[(self.dfMis["AMLTemperature"] != 0.0) & (self.dfMis["AMLConductivity"] != 0.0)]
            self.dfMis[name_pres] = 0.0
            self.dfMis["AMLSalinity"] = gsw.SP_from_C(
                self.dfMis["AMLConductivity"],
                self.dfMis[name_temp],
                self.dfMis[name_pres],
            )

        # Calculate Absolute Salinity
        SA = gsw.SA_from_SP(
            self.dfMis[name_psal],
            self.dfMis[name_pres],
            self.dfMis["Longitude"],
            self.dfMis["Latitude"],
        )
        self.dfMis[ctd + "AbsoluteSalinity"] = SA

        # Calculate Conservative Temperature
        CT = gsw.CT_from_t(self.dfMis[ctd + "AbsoluteSalinity"], self.dfMis[name_temp], self.dfMis[name_pres])
        self.dfMis[ctd + "ConservativeTemperature"] = CT

        # Calculate Density
        rho = gsw.rho(
            self.dfMis[ctd + "AbsoluteSalinity"],
            self.dfMis[ctd + "ConservativeTemperature"],
            self.dfMis[name_pres],
        )
        self.dfMis[ctd + "Density"] = rho

        if self.type.startswith("PHYSICO"):
            self.dfMis[ctd + "Depth"] = - self.dfMis["Depth"]
        else:
            self.dfMis[ctd + "Depth"] = gsw.z_from_p(self.dfMis[name_pres], self.dfMis["Latitude"])

        # Correct oxygen data (no salinity taken into account)
        if "OXYG" in self.type:
            (
                self.dfMis["Legato3OxygenConcentration"],
                self.dfMis["Legato3OxygenSaturation"],
            ) = rbr_odo.correctOxyg(
                self.dfMis["Legato3ODOPhase"],
                self.dfMis["Legato3Temperature"],
                self.dfMis["Legato3Salinity"],
                self.dfMis["Legato3Depth"],
            )

    def filter_air_data(self, depth=1.0):
        if not any(keyword in self.type for keyword in ["CTD", "PHYSICO"]):
            return

        if "CTD" in self.type:
            ctd = "Legato3"
        else:
            ctd = "AML"

        # Identify columns that start with "Legato3"
        ctd_columns = [col for col in self.dfMis.columns if col.startswith(ctd)]

        # Apply the filter condition to the identified columns
        mask = self.dfMis[ctd + "Depth"] > - depth
        self.dfMis.loc[mask, ctd_columns] = np.NaN

    def extract_altitude(self):
        """
        Apply to YUCO SCAN only.

        Extracts and saves altitude data for each scan in separate CSV files.
        """
        if "SCAN" not in self.type:
            return

        fileout = "temp"
        # Create a new column called "ScanId" in the dfMis DataFrame
        self.dfMis["ScanId"] = (
            (self.dfMis["ScanRecording"] == "Y") & (self.dfMis["ScanRecording"].shift(1) == "N")
        ).cumsum()
        self.dfMis.loc[self.dfMis["ScanRecording"] == "N", "ScanId"] = np.NaN

        for n in range(self.num_mis):
            tmp = self.dfMis[self.dfMis.num == (n + 1)]
            path, name = os.path.split(self.dataset[n])
            fileout = os.path.join(path, "scans", f"scan_corrected{name[7:-4]}")

            for j, i in enumerate(range(int(np.nanmin(tmp["ScanId"])), int(np.nanmax(tmp["ScanId"])) + 1)):
                os.makedirs(os.path.join(path, "scans"), exist_ok=True)
                # Get the first and last indices where ScanId is equal to i
                ind_s = tmp.loc[tmp["ScanId"] == i].index[0]
                ind_e = tmp.loc[tmp["ScanId"] == i].index[-1]

                # Convert timestamp to datetime
                d_str = dt.datetime.fromtimestamp(tmp.loc[ind_s, "TimestampUTC"])
                d_end = dt.datetime.fromtimestamp(tmp.loc[ind_e, "TimestampUTC"])

                # Save altitude data for each scan in separate CSV files
                tmp.loc[ind_s:ind_e, ["TimestampUTC", "Altitude"]].to_csv(
                    "%s_%d_alt.csv" % (fileout, j + 1), index=False
                )

    # - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * #
    # TODO: the two next function must be improved
    # - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * #

    def plot_report_alongtrack(self, projection=ccrs.PlateCarree(), background=["wms"]):
        if not any(keyword in self.type for keyword in ["CTD", "PHYSICO"]):
            return
        ctd = "Legato3" if self.type.startswith("CTD") else "AML"

        rc("font", **{"size": "7", "family": "serif", "serif": ["Latin Modern Roman"], "weight": "bold"})
        rc("axes", **{"labelsize": "9"})
        cmap_vars = common.cmap_vars

        fig = plt.figure(figsize=(8.27, 11.69), constrained_layout=True)
        fig.set_constrained_layout_pads(h_pad=0, hspace=0, w_pad=0, wspace=0)
        gs = fig.add_gridspec(5, 2)

        ax0 = fig.add_subplot(gs[0, 0])
        common.plot_ts_diagram(ax0, self.dfMis[[ctd + "ConservativeTemperature", ctd + "AbsoluteSalinity", "Depth"]])
        ax0.tick_params(which="major", top=True, labeltop=True, bottom=True, labelbottom=False, direction="inout")
        ax0.xaxis.set_label_position("top")

        ax01 = fig.add_subplot(gs[0, 1], projection=projection)
        carto.plot_geographic_position_static(self.dfMis, ax=ax01, background=background)

        ax1 = fig.add_subplot(gs[1, :])
        ax1.plot(self.dfMis["Bathymetry"], "k", lw=0.5, label="Bathymetry")
        ax1.plot(self.dfMis["Depth"], lw=0.8)
        ax1.set_ylabel("Depth (m)")
        ax1.invert_yaxis()
        ax1.tick_params(which="major", top=True, labeltop=True, bottom=True, labelbottom=False, direction="inout")

        ax2 = fig.add_subplot(gs[2, :], sharex=ax1)
        ax2.plot(self.dfMis[ctd + "Temperature"], color=cmap_vars["TEMP"])
        ax2.set_ylabel("Temperature (°C)", color=cmap_vars["TEMP"])
        ax2.tick_params(axis="y", colors=cmap_vars["TEMP"])

        ax3 = fig.add_subplot(gs[3, :], sharex=ax1)
        ax3.plot(self.dfMis[ctd + "Salinity"], color=cmap_vars["PSAL"])
        ax3.set_ylabel("Salinity (PSU)", color=cmap_vars["PSAL"])
        ax3.tick_params(axis="y", colors=cmap_vars["PSAL"])
        ax3.tick_params(which="major", top=True, labeltop=False, bottom=True, labelbottom=True, direction="inout")

        if "OXYG" in self.type:
            ax4 = fig.add_subplot(gs[4, :], sharex=ax1)
            ax4.plot(self.dfMis["Legato3OxygenSaturation"], color=cmap_vars["OSAT"])
            ax4.set_ylabel("Oxygen saturation (%)", color=cmap_vars["OSAT"])
            ax4.tick_params(axis="y", colors=cmap_vars["OSAT"])

        if "TURB" in self.type:
            ax4 = fig.add_subplot(gs[4, :], sharex=ax1)
            ax4.plot(self.dfMis["Legato3Turbidity00"], color=cmap_vars["TURB"])
            ax4.set_ylabel("Turbidity (NTU)", color=cmap_vars["TURB"])
            ax4.tick_params(axis="y", colors=cmap_vars["TURB"])

        ax1.set_xlim(self.dfMis.index[0], self.dfMis.index[-1])

        date_str = self.dfMis.index[0].strftime("%Y-%m-%d")
        plt.savefig(f"{date_str}_{self.name}_{self.type}_alongtrack.png", dpi=300, bbox_inches="tight")
        plt.show()

    def plot_report_depth(
        self,
        variables=None,
        bathy=True,
        separate_missions=True,
        projection=ccrs.PlateCarree(),
        background=[],
        savepath=None,
        x_index="absDistance",
        secondary_x_axis="time",
    ):
        if not any(keyword in self.type for keyword in ["CTD", "PHYSICO"]):
            return

        rc("font", **{"size": "7", "family": "serif", "serif": ["Latin Modern Roman"], "weight": "bold"})
        rc("axes", **{"labelsize": "9"})

        ctd = "Legato3" if self.type.startswith("CTD") else "AML"
        var_info = {
            "temperature": {"name": ctd + "Temperature", "unit": "°C", "cmap": cmocean.cm.thermal},
            "salinity": {"name": ctd + "Salinity", "unit": "PSU", "cmap": cmocean.cm.haline},
            "density": {"name": ctd + "Density", "unit": "kg.m$^{-3}$", "cmap": cmocean.cm.dense},
            "oxygensaturation": {"name": "Legato3OxygenSaturation", "unit": "%", "cmap": cmocean.cm.oxy},
            "turbidity": {"name": "Legato3Turbidity00", "unit": "NTU", "cmap": cmocean.cm.turbid},
        }

        if self.type == "CTD":
            valid_vars = {"temperature", "salinity", "density"}
        elif self.type == "CTD-OXYG":
            valid_vars = {"temperature", "salinity", "density", "oxygensaturation"}
        elif self.type == "CTD-TURB":
            valid_vars = {"temperature", "salinity", "density", "turbidity"}
        elif self.type == "PHYSICO":
            valid_vars = {"temperature", "salinity", "density"}

        available_vars = [var for var in var_info if var_info[var]["name"] in self.dfMis.columns and var in valid_vars]

        if variables is None:
            variables = available_vars

        if savepath is None:
            savepath = "."
        else:
            savepath = os.path.join(savepath, "figures")
            os.makedirs(savepath, exist_ok=True)

        def create_figure(
            df,
            mission_num=None,
            projection=ccrs.PlateCarree(),
            background=[],
            x_index="time",
            secondary_x_axis="time",
        ):
            fig = plt.figure(figsize=(8.27, 11.69), constrained_layout=True)
            fig.set_constrained_layout_pads(h_pad=0, hspace=0, w_pad=0, wspace=0)
            gs = fig.add_gridspec(len(variables) + 1, 2)

            # T-S diagram
            ax0 = fig.add_subplot(gs[0, 0])
            common.plot_ts_diagram(ax0, df[[ctd + "ConservativeTemperature", ctd + "AbsoluteSalinity", "Depth"]])
            ax0.tick_params(which="major", top=True, labeltop=True, bottom=True, labelbottom=False, direction="inout")
            ax0.xaxis.set_label_position("top")

            # TODO : add elapsed time and absolute distance information
            ax01 = fig.add_subplot(gs[0, 1], projection=projection)
            carto.plot_geographic_position_static(df, ax=ax01, background=background)

            axes = [fig.add_subplot(gs[1, :])]
            axes.extend([fig.add_subplot(gs[i, :], sharex=axes[0]) for i in range(2, len(variables) + 1)])

            if x_index not in df.columns and x_index != "time":
                raise ValueError(f"x_index '{x_index}' is not a valid column or 'time'.")
            x_values = df.index if x_index == "time" else df[x_index]

            for ax, var in zip(axes, variables):
                column_info = var_info[var]
                if bathy:
                    ax.plot(x_values, df["Bathymetry"], "k", lw=0.5, label="Bathymetry")
                sc = ax.scatter(x_values, df["Depth"], s=0.7, c=df[column_info["name"]], cmap=column_info["cmap"])
                ax.set_ylabel("Depth (m)")
                ax.invert_yaxis()
                ax.tick_params(
                    which="major", top=True, labeltop=False, bottom=True, labelbottom=False, direction="inout"
                )
                cbar = fig.colorbar(sc, ax=ax, orientation="vertical", pad=2e-3)
                cbar.ax.tick_params(colors="k")
                cbar.set_label(f"{var.capitalize()} ({column_info['unit']})", color="k", fontsize=8)
                cbar.outline.set_alpha(0.5)

            axes[-1].tick_params(
                which="major", top=True, labeltop=False, bottom=True, labelbottom=True, direction="inout"
            )
            if x_index == "time":
                axes[0].set_xlim(x_values[0], x_values[-1])
            else:
                axes[0].set_xlim(x_values.iloc[0], x_values.iloc[-1])

            if secondary_x_axis is None:
                secondary_x_axis = "time"
            secondary_values = df.index if secondary_x_axis == "time" else df[secondary_x_axis]
            secax = axes[0].secondary_xaxis("top")
            ticks = axes[0].get_xticks()
            if secondary_x_axis == "time":
                if x_index == "time":
                    labels = axes[-1].get_xticklabels()
                else:
                    closest_indices = [np.abs(x_values - tick).argmin() for tick in ticks]
                    labels = secondary_values[closest_indices]
                    labels = [label.strftime("%H:%M") for label in labels]
            else:
                if x_index == "time":
                    ticks = [np.datetime64(mdates.num2date(tick)) for tick in ticks]
                closest_indices = [np.abs(x_values - tick).argmin() for tick in ticks]
                labels = secondary_values.iloc[closest_indices]
                if secondary_x_axis == "absDistance":
                    labels = [int(label) for label in labels]
                if "itude" in secondary_x_axis:
                    labels = ["%.3f" % label for label in labels]
            secax.set_xticks(axes[0].get_xticks(), labels=labels)

            if x_index == "absDistance":
                axes[-1].set_xlabel("Absolute distance (m)")

            date_str = df.index[0].strftime("%Y-%m-%d")
            if mission_num is not None:
                fig.suptitle(f"Mission {mission_num}: {self.name} {self.type}", fontsize=12, fontweight="bold")
                plt.savefig(
                    f"{savepath}/{date_str}_{self.name}_{self.type}_depth_{mission_num}.png",
                    dpi=300,
                    bbox_inches="tight",
                )
            else:
                plt.savefig(f"{savepath}/{date_str}_{self.name}_{self.type}_depth.png", dpi=300, bbox_inches="tight")
            plt.show()

        if separate_missions:
            mission_numbers = self.dfMis["num"].unique()
            for mission_num in mission_numbers:
                df_mission = self.dfMis[self.dfMis["num"] == mission_num]
                create_figure(
                    df_mission,
                    mission_num,
                    projection=projection,
                    background=background,
                    x_index=x_index,
                    secondary_x_axis=secondary_x_axis,
                )
        else:
            df_combined = self.dfMis
            cumulative_distance = [
                0,
            ]
            num_mis = df_combined.num.iloc[0]
            for i in range(1, len(df_combined)):
                if df_combined["num"].iloc[i] != num_mis:
                    cumulative_distance.append(cumulative_distance[-1] + df_combined["absDistance"].iloc[i - 1])
                    num_mis = df_combined.num.iloc[i]
                else:
                    cumulative_distance.append(cumulative_distance[-1])
            df_combined["absDistance"] += cumulative_distance
            create_figure(
                df_combined,
                projection=projection,
                background=background,
                x_index=x_index,
                secondary_x_axis=secondary_x_axis,
            )


# - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * #
# - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * - * #

#   Next functions were designed to read *json mission file created with Seaplan to convert it to GPX format for
#   verification in QGIS. Not needed anymore with recent version of Seaplan

def process_starting_point(step, mission_name, df_steps):
    """
    Process the starting point step and append it to the DataFrame.

    Args:
        step (dict): The starting point step information.
        mission_name (str): The name of the mission.
        df_steps (pd.DataFrame): The DataFrame to append the step information to.
    """
    df_steps = df_steps.append(
        {
            "JsonName": step["JsonName"],
            "Latitude": step["Latitude"],
            "Longitude": step["Longitude"],
            "StepType": "startingPoint",
            "Speed": None,
            "Distance": None,
            "Bearing": None,
            "Duration": None,
        },
        ignore_index=True,
    )


def process_waypoint(step, mission_name, df_steps):
    """
    Process the waypoint step and append it to the DataFrame.

    Args:
        step (dict): The waypoint step information.
        mission_name (str): The name of the mission.
        df_steps (pd.DataFrame): The DataFrame to append the step information to.
    """
    df_steps = df_steps.append(
        {
            "JsonName": step["JsonName"],
            "Latitude": step["Latitude"],
            "Longitude": step["Longitude"],
            "StepType": "waypoint",
            "Speed": step["Speed"],
            "Distance": None,
            "Bearing": None,
            "Duration": None,
        },
        ignore_index=True,
    )


def process_rail(step, mission_name, df_steps):
    """
    Process the rail step and append it to the DataFrame.

    Args:
        step (dict): The rail step information.
        mission_name (str): The name of the mission.
        df_steps (pd.DataFrame): The DataFrame to append the step information to.
    """
    df_steps = df_steps.append(
        {
            "JsonName": step["JsonName"],
            "Latitude": None,
            "Longitude": None,
            "StepType": "rail",
            "Speed": step["Speed"],
            "Distance": step["Distance"],
            "Bearing": step["Bearing"],
            "Duration": None,
        },
        ignore_index=True,
    )


def process_segment(step, mission_name, df_steps):
    """
    Process the segment step and append it to the DataFrame.

    Args:
        step (dict): The segment step information.
        mission_name (str): The name of the mission.
        df_steps (pd.DataFrame): The DataFrame to append the step information to.
    """
    df_steps = df_steps.append(
        {
            "JsonName": step["JsonName"],
            "Latitude": None,
            "Longitude": None,
            "StepType": "segment",
            "Speed": step["Speed"],
            "Distance": None,
            "Bearing": step["Bearing"],
            "Duration": step["Duration"],
        },
        ignore_index=True,
    )


def process_starting_point(step: Dict[str, Any], mission_name: str, df_steps: pd.DataFrame) -> None:
    """
    Process the starting point step and update the DataFrame.

    Args:
        step (Dict[str, Any]): The step information.
        mission_name (str): The name of the mission.
        df_steps (pd.DataFrame): The DataFrame to update.
    """
    df_steps.loc[len(df_steps)] = [
        mission_name,
        step["latitude"],
        step["longitude"],
        "startingPoint",
        None,
        None,
        None,
        None,
    ]


def process_waypoint(step: Dict[str, Any], mission_name: str, df_steps: pd.DataFrame) -> None:
    """
    Process the waypoint step and update the DataFrame.

    Args:
        step (Dict[str, Any]): The step information.
        mission_name (str): The name of the mission.
        df_steps (pd.DataFrame): The DataFrame to update.
    """
    df_steps.loc[len(df_steps)] = [
        mission_name,
        step["latitude"],
        step["longitude"],
        "waypoint",
        step["speed"],
        None,
        None,
        None,
    ]


def process_rail(step: Dict[str, Any], mission_name: str, df_steps: pd.DataFrame) -> None:
    """
    Process the rail step and update the DataFrame.

    Args:
        step (Dict[str, Any]): The step information.
        mission_name (str): The name of the mission.
        df_steps (pd.DataFrame): The DataFrame to update.
    """
    df_steps.loc[len(df_steps)] = [
        mission_name,
        None,
        None,
        "rail",
        step["speed"],
        step["distance"],
        step["bearing"],
        None,
    ]


def process_segment(step: Dict[str, Any], mission_name: str, df_steps: pd.DataFrame) -> None:
    """
    Process the segment step and update the DataFrame.

    Args:
        step (Dict[str, Any]): The step information.
        mission_name (str): The name of the mission.
        df_steps (pd.DataFrame): The DataFrame to update.
    """
    df_steps.loc[len(df_steps)] = [
        mission_name,
        None,
        None,
        "segment",
        step["speed"],
        None,
        step["bearing"],
        step["duration"],
    ]


def missionReader(path: str) -> pd.DataFrame:
    """
    Reads a JSON file containing navigation steps and creates a pandas DataFrame with information about each step.

    Args:
        path (str): The path to the JSON file containing the mission information.

    Returns:
        pd.DataFrame: A DataFrame containing information about each navigation step.

    from geopy import distancs
    from geopy.point import Point
    from geographiclib.geodesic import Geodesic
    """

    with open(path) as file:
        data = json.load(file)

    mission_name = data["mission"]["settings"]["name"]
    steps = data["mission"]["steps"]

    df_steps = pd.DataFrame(
        columns=["JsonName", "Latitude", "Longitude", "StepType", "Speed", "Distance", "Bearing", "Duration"]
    )

    step_type_mapping = {
        "startingPoint": process_starting_point,
        "waypoint": process_waypoint,
        "rail": process_rail,
        "segment": process_segment,
    }

    for step in steps:
        step_type = next(iter(step))
        if step_type in step_type_mapping:
            step_type_mapping[step_type](step, mission_name, df_steps)

    prev_step = None
    for idx, step in df_steps.iterrows():
        if prev_step is not None:
            prev_point = Point(prev_step["Latitude"], prev_step["Longitude"])
            bearing = step["Bearing"]
            speed = step["Speed"]
            duration = step["Duration"]

            if step["StepType"] == "waypoint":
                curr_point = Point(step["Latitude"], step["Longitude"])
                distance_meters = distance.distance(prev_point, curr_point).meters
                bearing = Geodesic.WGS84.Inverse(prev_point, curr_point).azi1
                duration = distance_meters / speed

                df_steps.at[idx, "Distance"] = distance_meters
                df_steps.at[idx, "Bearing"] = bearing
                df_steps.at[idx, "Duration"] = duration

            elif step["StepType"] == "rail":
                distance_meters = step["Distance"]
                duration = distance_meters / speed

                end_point = distance.distance(meters=distance_meters).destination(prev_point, bearing=bearing)
                df_steps.at[idx, "Latitude"] = end_point.latitude
                df_steps.at[idx, "Longitude"] = end_point.longitude
                df_steps.at[idx, "Duration"] = duration

            elif step["StepType"] == "segment":
                distance_meters = speed * duration

                end_point = distance.distance(meters=distance_meters).destination(prev_point, bearing=bearing)
                df_steps.at[idx, "Latitude"] = end_point.latitude
                df_steps.at[idx, "Longitude"] = end_point.longitude
                df_steps.at[idx, "Distance"] = distance_meters

        prev_step = step

    return df_steps


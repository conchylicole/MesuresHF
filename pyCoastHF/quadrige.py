"""
    QUADRIGE
"""
import datetime as dt
import glob
import logging as log
import os
import shutil

import pandas as pd

from . import common, reader, sonde

config = common.data_config('sondes')
conf_sites = common.data_config('sites')
conf_sondes = config['SONDES']


def download_from_q2(path, liste_excel, liste_manuel, datarmor, verbose):
    """Télécharge les fichiers de sondes extraits de Quadrige 2 sur un des répertoires
    du réseau Ifremer. Convertit les fichiers de liste au format utf-8"""

    log.info('-' * 60)
    log.info('=> Récupération des fichiers extraits de Quadrige')
    log.info('-' * 60)

    if not datarmor:
        if os.path.exists('./tmp_SONDES/'):
            log.debug('Suppression du dossier local tmp_SONDES')
            shutil.rmtree('./tmp_SONDES/')

        log.debug('Récupération du dossier "%s" en tmp_SONDES par scp' % path.split('/')[-1])
        cmd = 'scp -r spetton@argenton.ifremer.fr:"%s" tmp_SONDES' % (path)
        common.call(cmd, verbose=verbose)

    # Q2 ne marche que sous Windows, donc conversion au format utf-8
    log.debug('Conversion des fichiers Q2*.csv en liste_*.csv')
    cmd = 'iconv -f ISO-8859-1 -t UTF-8 ./tmp_SONDES/%s > liste_sondes.csv' % (liste_excel)
    common.call(cmd, verbose=verbose)
    cmd = 'iconv -f ISO-8859-1 -t UTF-8 ./tmp_SONDES/%s > liste_manuel.csv' % (liste_manuel)
    common.call(cmd, verbose=verbose)


def detect_type_sonde(fic, fic_type):
    """Retourne la première case du tableau le type de sonde"""
    import re

    from xlrd import open_workbook

    if fic_type == 'XLS':
        try:
            wb = open_workbook(fic)
            sh = wb.sheet_by_index(0)
            val = sh.cell(0, 0).value
            val = ' '.join(val.split())  # Remplace les doubles espaces par des simples
            return val.replace(' ', '_')  # Remplace les espaces par un _
        except Exception:
            log.error('Not a valid file %s' % fic)
    elif fic_type == 'TXT':
        f = open(fic, 'r')
        lines = f.readlines()
        f.close()
        num = re.findall(r'\d{5}', lines[0])
        if len(num) >= 1:
            return 'STPS_' + num[0]


def test_excel_file(filename):
    from xlrd import XLRDError, open_workbook

    try:
        open_workbook(filename)
    except XLRDError:
        return False
    else:
        return True


def read_liste_q2_sondes(code_quadrige, fileIn='liste_sonde.csv', OTT=False):
    """Extraction Quadrige 2 des Fichiers de mesures"""

    # Read first line to detect columns
    with open(fileIn, 'r') as f:
        line = f.readline()
    li = line.strip().split(';')

    try:
        indexId = li.index('Identifiant')
        indexQ2 = li.index('Lieu de surveillance : Mnémonique')
        indexLi = li.index('Libellé')
    except IndexError:
        log.critical("Impossible de trouver les noms de colones dans l'extraction Quadrige")
        raise IndexError('Problème extraction Quadrige')

    names = ['Id', 'Q2', 'Li']
    cols = [indexId, indexQ2, indexLi]
    names = [names[i] for i in sorted(range(len(cols)), key=lambda k: cols[k])]
    data = pd.read_csv(fileIn, skiprows=1, delimiter=';', header=None, names=names, usecols=cols)

    if OTT:
        data = data[(data.Q2 == code_quadrige) & (data.Li.str.startswith('OTT'))]
    else:
        data = data[(data.Q2 == code_quadrige) & (~data.Li.str.startswith('OTT'))]

    liste_id = data.Id.apply(str).tolist()

    liste_file = []
    for fic in liste_id:
        tmp = glob.glob('./tmp_SONDES/*' + fic + '*.XLS') + glob.glob('./tmp_SONDES/*' + fic + '*.TXT')
        if len(tmp) > 0:
            liste_file.append(tmp[0])

    return liste_file


def read_liste_q2_manuel(site, fileIn='liste_manuel.csv'):
    """Read discrete manual data from Quadrige 2"""
    log.info(' -> Mesures manuelles')
    name = conf_sites[site]['name']
    code_quadrige = conf_sites[site]['code_quadrige']
    S = sonde.SondeData(name, 'MANUEL', site)

    # Read first line to detect columns
    log.debug('Détection de %s dans le fichier liste_manuel.csv' % code_quadrige)
    with open(fileIn, 'r') as f:
        line = f.readline()
    li = line.strip().split(';')

    try:
        indexDa = li.index('Passage : Date')
        indexSi = li.index('Lieu de surveillance : Mnémonique')
        indexVa = li.index('Résultat : Code paramètre')
        indexMe = li.index('Résultat : Valeur de la mesure')
    except IndexError:
        log.critical("Impossible de trouver les noms de colones dans l'extraction Quadrige")
        return pd.DataFrame(), pd.DataFrame()

    # Read table
    names = ['Date', 'Site', 'Type', 'Mesu']
    cols = [indexDa, indexSi, indexVa, indexMe]
    names = [names[i] for i in sorted(range(len(cols)), key=lambda k: cols[k])]
    data = pd.read_csv(fileIn, skiprows=1, delimiter=';', header=None, names=names, usecols=cols)

    # Conversion des dates (WARNING : pas d'heure dans Quadrige 2)
    data['Date'] = pd.to_datetime(data['Date'], format='%d/%m/%Y', errors='ignore') + dt.timedelta(hours=10)
    data = data.set_index(data['Date']).drop(['Date'], axis=1)

    # Detect Temperature & Salinity
    temp = data[(data.Site == code_quadrige) & (data.Type == 'TEMP')].copy()
    temp.rename(columns={'Mesu': 'TEMP'}, inplace=True)
    psal = data[(data.Site == code_quadrige) & (data.Type == 'SALI')].copy()
    psal.rename(columns={'Mesu': 'PSAL'}, inplace=True)

    # Concaténation des valeurs par Date
    if not temp.empty:
        data = pd.concat([temp, psal], axis=1)
        S.raws = data[['TEMP', 'PSAL']]
    else:
        S.raws = psal[['PSAL']]
    return [S]


def read_local_dir(Type, site, unique_sonde=False):
    """
    Read data from an Extraction from Q2
      - 1st generate liste_file
      - 2nd call read_data_files
    """
    log.info(' -> Sonde type %s' % Type)
    name = conf_sites[site]['name']
    try:
        code_quadrige = conf_sites[site]['code_quadrige']
    except KeyError:
        code_quadrige = None

    # Détection des fichiers à lire
    log.debug('Détection de %s dans le fichier liste_sonde.csv' % code_quadrige)
    if Type == 'OTT':
        liste_file = read_liste_q2_sondes(code_quadrige, OTT=True, fileIn='liste_sondes.csv')
    else:
        liste_file = read_liste_q2_sondes(code_quadrige, OTT=False, fileIn='liste_sondes.csv')

    # Lecture des fichiers
    SONDE = []
    if len(liste_file) == 0:
        log.warning('Pas de fichier %s pour %s : %s' % (Type, code_quadrige, name))
    else:
        SONDE = read_data_files(Type, site, liste_file, unique_sonde=unique_sonde)
    return SONDE


def read_data_files(type_sonde, site, liste_file, unique_sonde=False):
    """
    Iterate over liste_file to read raw data
    Arguments:
           unique_sonde = True -> ne cherche pas le numéro de série et renvoi une seule sonde
    """
    S = []
    if unique_sonde:
        name = conf_sites[site]['name']
        S.append(sonde.SondeData(name, type_sonde, site))
    else:
        n_sonde = []

    for fic in liste_file:
        # Cas Extraction Q2 (fichier STPS au format Excel "logiquement")
        try:
            ext = fic.split('.')[-1]
            if ext == 'XLS' and test_excel_file(fic):
                probe, data = reader.read_excel(
                    fic, variables=conf_sondes[type_sonde]['variables'], unique_sonde=unique_sonde
                )
            else:
                log.error('Fichier %s pas au bon format' % fic)
        except IndexError:
            log.error('Impossible de lire le fichier %s' % fic)
            data = pd.DataFrame()

        # Vérification du bon format des dates
        if not isinstance(data.index, pd.DatetimeIndex):
            log.error('Impossible de lire les dates du fichier %s' % fic)
            print(data.index)
            data = pd.DataFrame()

        # Concaténation des données lues
        if unique_sonde:
            s = 0
        else:
            if probe not in n_sonde:
                n_sonde.append(probe)
                S.append(sonde.SondeData(probe.split('_')[1].upper(), type_sonde, site))
            s = n_sonde.index(probe)
        S[s].raws = pd.concat([S[s].raws, data], axis=0, sort=False)

    return S

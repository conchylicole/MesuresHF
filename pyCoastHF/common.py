# -*- coding: utf-8 -*-
"""

Created on Wed Jul 31 14:07:56 2013
@author: S. Petton
"""

import logging
import os
import pathlib
import shutil
import subprocess
import sys
import datetime as dt
import locale
import importlib

import configobj
import statsmodels.api as sm
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import matplotlib.ticker as tick
from matplotlib.ticker import MaxNLocator
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

import gsw
import seaborn as sns
import numpy as np
import pandas as pd
from pandas.plotting import register_matplotlib_converters
import cmocean

register_matplotlib_converters()

rep_lib = pathlib.Path(__file__).parent


def call(cmd, verbose=False, stdin=None, out=None, check=True):
    """
    Exécute cmd sur la ligne de commande
      verbose : paramètre logique pour l'affichage à l'écran de la commande executée
      stdin : entrée standard
      out : nom du fichier de sortie
    """
    if verbose:
        print(cmd)
    if out:
        with open(out, "w") as fileout:
            code = subprocess.run(cmd, shell=True, stdout=fileout, stderr=fileout, stdin=stdin, check=check)
    else:
        code = subprocess.run(cmd, shell=True, stdin=stdin, check=check)
    return code


def data_config(model="sondes", conf="config", name=None, force=False):
    """
    Récupération du fichier de configuration associé au type de données model
    Renvoie un dictionnaire python
    """
    cfgfile = f"cfg/config_{model}.cfg"
    if not os.path.exists(cfgfile) or force:
        cfgfile = f"{rep_lib}/cfg/config_{model}.cfg"
    _data_config = {}
    try:
        _data_config[model] = configobj.ConfigObj(cfgfile, unrepr=True, interpolation=True)[conf]
    except KeyError:
        raise KeyError(f"Error occured when loading config file {cfgfile}")
    if name is None:
        return _data_config[model]
    else:
        return _data_config[model][name]


def copy_data_config():
    """Create local configuration file to enable user modifications"""
    cfg_dir = pathlib.Path("cfg")
    if not cfg_dir.is_dir():
        cfg_dir.mkdir()
    logging.info("Get original configuration files")
    config_file = rep_lib / "cfg" / "config_sondes.cfg"
    shutil.copy(config_file, cfg_dir)


def load_periods(site: str) -> object:
    """Load deployement periods (which probe to keep) for a given site

    Args:
        site (str): The name of the site for which periods need to be loaded.

    Returns:
        object: The loaded periods specific to the given site.
    """
    periods = None

    if os.path.exists("periods"):
        sys.path.insert(0, os.path.join(os.getcwd(), "periods"))
        module_name = f"{site}_Param_Periods"
    else:
        module_name = f".periods.{site}_Param_Periods"

    try:
        periods_module = importlib.import_module(module_name)
        periods = periods_module.periods
    except ImportError as e:
        print(f"Error importing module: {e}")

    return periods


def copy_periods():
    """Create local period file to enable user modifications"""
    if not os.path.isdir("periods"):
        os.makedirs("periods")
    logging.info("Get original period files")
    shutil.copy(f"{rep_lib}/periods/MAREL_Param_Periods.py", "periods")


def supprime_accent(text):
    """Supprime les accents du texte source"""
    accent = ["é", "è", "ê", "à", "ù", "û", "ç", "ô", "î", "ï", "â"]  # noqa E221
    sans_accent = ["e", "e", "e", "a", "u", "u", "c", "o", "i", "i", "a"]
    for c, s in zip(accent, sans_accent):
        text = text.replace(c, s)
    return text


def init_workbook(out="./sum-up.xlsx"):
    writer = pd.ExcelWriter(out, engine="xlsxwriter", datetime_format="dd mmm yyyy")
    return writer


cmap_vars = {
    "TEMP": (0.0, 0.35, 0.85, 1),
    "PSAL": (0.85, 0.0, 0.0, 1),
    "CHLO": (0.0, 0.5, 0.0, 1),
    "FLUO": (0.0, 0.5, 0.2, 0.9),
    "TURB": (0.47, 0.21, 0.07, 1),
    "PROF": (0.4, 0.4, 0.4, 1),
    "OSAT": (0.0, 0.55, 0.85, 1),
    "DOX1": (0.0, 0.55, 0.85, 1),
    "PH": (0.85, 0.0, 0.4, 1),
    "PH_INT": (0.32, 0.55, 0.55, 1),
    "PH_EXT": (1.0, 0.5, 0.0, 1),
    "A_T": (0.3, 0.5, 0.7, 1),
    "C_T": (0.7, 0.2, 0.2, 1),
}


def plot_var(
    S,
    variables,
    i,
    j,
    date_start,
    date_end,
    raw=False,
    Dmean=False,
    x=None,
    y=None,
    colors=None,
    name_var="",
    units="",
    uniqueCol=False,
    sub=None,
    rasterized=False,
    legend=True,
    format_xaxis=False,
    markersize=1,
):
    """
    Trace les variables 'var' des sondes 'S' pour une comparaison de sonde
    Arguments:
        i : numéro du subplot
        j : nombre total de subplot
        raw = True -> trace les valeurs non filtrées
        Dmean = True -> trace les valeurs journalières
        colors = Couleurs définies pour chaque sonde
        niqueCol = 1 couleur par variable
    Renvoie les subplots
    """
    nb_plot = j
    if sub is None:
        sub = [[[None] * 3] for _ in range(nb_plot)]

    if i == 0:
        sub[i][0][0] = plt.subplot(nb_plot, 1, i + 1)
    else:
        sub[i][0][0] = plt.subplot(nb_plot, 1, i + 1, sharex=sub[0][0][0])

    if colors is None:
        cmap = plt.cm.get_cmap("Set1", len(S) + 1)
        # cmap = plt.cm.get_cmap("gist_rainbow", len(S) + 1)
        cmaplist = [cmap(i) for i in range(cmap.N)]

    if not isinstance(variables, list):
        variables = [variables]
    else:
        uniqueCol = True
        [sub[i].append([None] * 3) for _ in variables]

    list_legend = []
    for v, var in enumerate(variables):
        if v == 0:
            ax = sub[i][v][0]
        else:
            ax = sub[i][0][0].twinx()
            ax.spines["right"].set_position(("outward", 35 * (v - 1)))
        y = sub[i][v][2]

        for k, s in enumerate(S):
            if colors is None:
                colorVal = cmaplist[k]
            else:
                colorVal = colors[k]

            if uniqueCol:
                colorVal = list(cmap_vars[var])
                colorVal[-1] = 1 - 0.1 * v

            if var not in list(s.data):
                logging.debug("Pas de variables %s - %s %s" % (var, s.Type, s.Name))
                continue
            else:
                name_var = s.conf_vars[var]["longname"]
                units = s.conf_vars[var]["units"]

            if s.Type.startswith("MANUEL"):
                l = ax.plot(
                    s.data[var],
                    linewidth=0,
                    label=s.Type + " " + s.Name,
                    marker="*",
                    markersize=7,
                    markeredgewidth=0.3,
                    markeredgecolor="k",
                    markerfacecolor=colorVal,
                )[0]
            else:
                l = ax.plot(
                    s.data[var],
                    color=colorVal,
                    linewidth=0.0,
                    label=s.Type + " " + s.Name,
                    marker=".",
                    markersize=markersize,
                    rasterized=rasterized,
                )[0]
            if v == 0:
                list_legend.append(l)

            if y is not None:
                y = (min(y + (s.data[var].min(),)), max(y + (s.data[var].max(),)))
                ax.set_ylim(y)
            if raw:
                x, y = ax.get_xlim(), ax.get_ylim()
                ax.plot(s.raws[var], color=colorVal, linewidth=0.0, alpha=0.2, marker=".", markersize=1)
                ax.set_xlim(x)
                ax.set_ylim(y)
            if Dmean and s.Type != "MANUEL":
                y = ax.get_ylim()
                ax.plot(s.data[var].rolling("D", center=True).mean(), color=colorVal, linewidth=1, alpha=0.8)
                ax.set_ylim(y)

            sub[i][v][1], sub[i][v][2] = ax.get_xlim(), ax.get_ylim()

        if len(variables) > 1:
            ylab = name_var
            if len(units) > 0:
                ylab += " (%s)" % units
            ax.set_ylabel(ylab, color=colorVal)
            ax.tick_params(axis="y", colors=colorVal)
        else:
            ax.set_ylabel("%s (%s)" % (name_var, units), color="k")
        ax.set_xlim(date_start, date_end)
        if format_xaxis:
            format_date_xaxis(ax)

    if not (len(list_legend) == 1 and s.Type == "GLOBAL") and legend:
        ax.legend(handles=list_legend, loc="best", fontsize="small", markerscale=2)

    return sub


def add_handling_operations(ax: plt.Axes, periods: pd.DataFrame) -> None:
    """
    Adds visual operations to a given matplotlib axis based on a pandas DataFrame called `periods`.

    Args:
        ax (plt.Axes): The axis to which the visual operations will be added.
        periods (pd.DataFrame): A DataFrame containing the periods with 'IN', 'OUT', and 'Deployments' columns.

    Returns:
        None. The function modifies the given matplotlib axis by adding visual operations based on the periods defined in the `periods` DataFrame.
    """
    n_deploy = 0
    ylim = ax.get_ylim()

    for i, row in periods.iterrows():
        in_date = row["IN"]
        out_date = row["OUT"]
        deployments = row["Deployments"]

        if pd.notnull(out_date) and i % 2 == 0:
            ax.axvspan(in_date, out_date, color="grey", alpha=0.1)

        if n_deploy != deployments:
            ax.vlines(in_date, ylim[0], ylim[1], color="black", lw=1)
            n_deploy = deployments

    ax.set_ylim(ylim)


def plot_seafet_diag(SeaFET, Probe, Samples, Periods, d_str, d_end, conf_site, DuraFET_Temp=True, rasterized=False):
    from matplotlib import rc

    # TODO title font /= label font
    rc("font", **{"size": "7", "family": "serif", "serif": ["Latin Modern Roman"], "weight": "bold"})
    rc("axes", **{"labelsize": "9"})

    fig = plt.figure(figsize=(10, 6), constrained_layout=True)
    fig.set_constrained_layout_pads(h_pad=0, hspace=0)
    gs = fig.add_gridspec(3, 1, height_ratios=[1.5, 3.5, 1.5])

    # * - * - * - * - * - * - *
    # Temperature and Salinity
    # * - * - * - * - * - * - *
    ax1 = fig.add_subplot(gs[0])
    ax1.plot(Probe["TEMP"], color=cmap_vars["TEMP"], linewidth=0, marker=".", markersize=1, rasterized=rasterized)
    ax1.plot(
        Samples["Temp"],
        color=cmap_vars["TEMP"],
        linewidth=0,
        marker="D",
        markersize=4,
        markeredgecolor="k",
        rasterized=rasterized,
    )
    if DuraFET_Temp:
        ax1.plot(SeaFET["Temp"], color="green", linewidth=0, marker=".", markersize=1, rasterized=rasterized)

    ax1.set_ylabel("Temperature (°C)", color=cmap_vars["TEMP"])
    ax1.tick_params(axis="y", colors=cmap_vars["TEMP"])

    ax11 = ax1.twinx()
    ax11.plot(Probe["PSAL"], color=cmap_vars["PSAL"], linewidth=0, marker=".", markersize=1, rasterized=rasterized)
    ax11.plot(
        Samples["Psal"],
        color=cmap_vars["PSAL"],
        linewidth=0,
        marker="D",
        markersize=4,
        markeredgecolor="k",
        rasterized=rasterized,
    )
    ax11.set_ylabel("Salinity (psu)", color=cmap_vars["PSAL"], rotation=270, labelpad=10)
    ax11.tick_params(axis="y", colors=cmap_vars["PSAL"])
    ax11.text(
        0.05,
        0.1,
        conf_site["name"],
        transform=ax11.transAxes,
        color="green",
        bbox=dict(boxstyle="round", facecolor="white"),
    )

    # * - * - * - * - * - * - * - * - * - * - * - * - * - * -
    # Corrected Internal and External pH & Delta between them
    # * - * - * - * - * - * - * - * - * - * - * - * - * - * -
    ax2 = fig.add_subplot(gs[1], sharex=ax1)
    ax2.plot(
        SeaFET["pH_EXT_Cor"],
        color=cmap_vars["PH_EXT"],
        linewidth=0,
        marker=".",
        markersize=1,
        label=r"$\mathregular{pH_{EXT}}$",
        rasterized=rasterized,
    )
    ax2.plot(
        SeaFET["pH_INT_Cor"],
        color=cmap_vars["PH_INT"],
        linewidth=0,
        marker=".",
        markersize=1,
        label=r"$\mathregular{pH_{INT}}$",
        rasterized=rasterized,
    )
    ax2.plot(
        Samples["pH_Spectro_InSitu"],
        color="k",
        linewidth=0,
        marker=".",
        markersize=3,
        label=r"$\mathregular{pH_{Spectro}}$",
        rasterized=rasterized,
    )
    if "Bouin" in conf_site["name"]:
        ax2.plot(Probe["PH"], color="pink", linewidth=0, marker=".", markersize=0.5, rasterized=rasterized)
        # ax2.plot(Probe['OSAT']/100+7, color='blue', linewidth=0, marker='.', markersize=.5)

    pH_SNAPO = True
    if pH_SNAPO:
        import PyCO2SYS as pyco2

        opt_pyco2_cst = {"opt_k_carbonic": 10, "opt_k_fluoride": 2, "opt_total_borate": 2}
        res = pyco2.sys(
            par1=Samples["A_T"],
            par1_type=1,
            par2=Samples["C_T"],
            par2_type=2,
            temperature=Samples["Temp_pH_Spectro"],
            temperature_out=Samples["Temp"],
            salinity=Samples["Psal"],
            **opt_pyco2_cst,
        )
        Samples["pH_SNAPO"] = res["pH_total_out"]
        Samples.loc[Samples["Status_SNAPO"] != "A", "pH_SNAPO"] = np.NaN
        ax2.plot(Samples["pH_SNAPO"], "*b", ms=3)

    ax2.set_ylabel(r"$\mathregular{pH_{Total}}$")
    add_handling_operations(ax2, Periods)
    ax2.yaxis.grid(True, lw=0.3)

    tmp = SeaFET[["pH_EXT_Cor", "pH_INT_Cor"]].to_numpy()
    ymin, ymax = np.nanmin(tmp), np.nanmax(tmp)
    ymin = max(ymin, 7.4)
    ymax = min(ymax, 8.5)
    ax2.set_ylim(ymin, ymax)
    dy = ymax - ymin
    a = (ymax + 0.2 * dy - (ymax - 0.2 * dy)) / (0.15 - -0.10)
    b = ymax + 0.2 * dy - a * 0.15
    x1 = (ymin - b) / a

    ax22 = ax2.twinx()
    ax22.hlines(0, d_str, d_end, color="grey", linestyles="dashed", linewidth=0.8)
    ax22.plot(
        SeaFET["pH_EXT_Cor"] - SeaFET["pH_INT_Cor"],
        color="lightgrey",
        linewidth=0,
        marker=".",
        markersize=1,
        rasterized=rasterized,
    )
    ax22.set_ylabel(r"$\mathregular{\Delta pH_{Total}}$", rotation=270, color="grey")
    ax22.set(ylim=[x1, 0.15], yticks=np.arange(-0.1, 0.15, 0.05))
    ax22.yaxis.set_label_coords(1.05, 0.85)
    ax22.tick_params(axis="y", colors="grey", labelsize=6)
    ax2.set_zorder(ax22.get_zorder() + 1)
    ax2.patch.set_visible(False)

    # ax22.text(0.55, 0.1, 'SeaBird coefficients', transform=ax22.transAxes, color='green')
    # , bbox=dict(boxstyle="round", facecolor='white'))

    lgnd = ax2.legend(loc="best", handletextpad=-0.25, fontsize=8, ncol=3)
    for handle in lgnd.legendHandles:
        handle.set_markersize(5)
    #     handle._sizes = [60]
    #     #handle._legmarker.set_markersize(5)

    # * - * - * - * - * - * - * - * - * - * - * - * - *
    # Difference between pH spectro and both SeaFET pH
    # * - * - * - * - * - * - * - * - * - * - * - * - *
    ax3 = fig.add_subplot(gs[2], sharex=ax1)
    ax3.hlines(0.02, d_str, d_end, color="k", linewidth=1)
    ax3.hlines(-0.02, d_str, d_end, color="k", linewidth=1)
    pH_Spectro_InSitu = Samples["pH_Spectro_InSitu"].copy()
    pH_Spectro_InSitu[Samples["Status"] != "A"] = np.NaN
    ax3.plot(Samples["pH_INT_Cor"] - pH_Spectro_InSitu, color=cmap_vars["PH_INT"], lw=0, marker=".", ms=3)
    ax3.plot(Samples["pH_EXT_Cor"] - pH_Spectro_InSitu, color=cmap_vars["PH_EXT"], lw=0, marker=".", ms=3)
    if pH_SNAPO:
        pH_SNAPO = Samples["pH_SNAPO"].copy()
        pH_SNAPO[(Samples["Status"] != "A") | (Samples["Status_SNAPO"] != "A")] = np.NaN
        ax3.plot(Samples["pH_INT_Cor"] - pH_SNAPO, color="honeydew", lw=0, marker="*", ms=3)
        ax3.plot(Samples["pH_EXT_Cor"] - pH_SNAPO, color="bisque", lw=0, marker="*", ms=3)
    ax3.set_ylabel(r"$\mathregular{\Delta pH_{Spectro}}$", labelpad=-5)
    ylim = ax3.get_ylim()
    ax3.set_ylim(max(min(-0.04, ylim[0]), -0.5), min(max(0.04, ylim[1]), 0.5))
    add_handling_operations(ax3, Periods)

    ax1.set_xlim(d_str, d_end)

    # ax1.tick_params(which='major', bottom=True,  labelbottom=False, direction='inout')
    # ax1.tick_params(which='minor', bottom=False, labelbottom=False)

    # ax2.tick_params(which='major', bottom=True,  labelbottom=False, direction='inout')
    # ax2.tick_params(which='minor', bottom=False, labelbottom=True)

    ax3.tick_params(which="major", top=True, labeltop=False, direction="inout", bottom=False, labelbottom=False)
    ax3.tick_params(which="minor", bottom=False, labelbottom=False)

    return fig


def plot_samples_diag(Samples, conf_site, d_str=None, d_end=None):
    from matplotlib import rc

    rc("font", **{"size": "7", "family": "serif", "serif": ["Latin Modern Roman"], "weight": "bold"})
    rc("axes", **{"labelsize": "9"})

    locale.setlocale(locale.LC_ALL, "en_GB")
    import cmocean

    if d_str is None:
        d_str = Samples.index[0]
    if d_end is None:
        d_end = Samples.index[-1]
    Samples["DOY"] = Samples.index.dayofyear

    fig = plt.figure(figsize=(29 / 2.54, 21 / 2.54), constrained_layout=True)
    fig.set_constrained_layout_pads(h_pad=0.0, hspace=0.01)  # , w_pad=0., wspace=0.)
    gs = fig.add_gridspec(4, 3, height_ratios=[2.0, 0.75, 1.75, 1])

    ind = Samples["Status_SNAPO"] != "B"
    ax0 = fig.add_subplot(gs[0, :])
    ax0.plot(Samples.A_T[ind], ".", color=cmap_vars["A_T"])
    ax0.set_ylabel(r"$\mathregular{Total\ Alkalinity\ (µmol.kg^{-1})}$", color=cmap_vars["A_T"])
    ax0.tick_params(axis="y", colors=cmap_vars["A_T"])
    ax00 = ax0.twinx()
    ax00.plot(Samples.C_T[ind], ".", color=cmap_vars["C_T"])
    ax00.set_ylabel(
        r"$\mathregular{Dissolved\ Inorganic\ Carbon\ (µmol.kg^{-1})}$",
        rotation=270,
        labelpad=12,
        color=cmap_vars["C_T"],
    )
    ax00.tick_params(axis="y", colors=cmap_vars["C_T"])
    ax00.text(
        0.05,
        0.1,
        conf_site["name"],
        transform=ax00.transAxes,
        color="green",
        bbox=dict(boxstyle="round", facecolor="white"),
    )

    ax1 = fig.add_subplot(gs[1, :], sharex=ax0)
    ax1.hlines(0, d_str, d_end, color="grey", linestyles="dashed", linewidth=0.8)
    # ax1.plot(Samples.pH_Spectro - Samples.pH_SNAPO, '.')
    cs = ax1.scatter(
        Samples.index,
        Samples.pH_Spectro - Samples.pH_SNAPO,
        c=Samples["DOY"],
        s=5,
        cmap=cmocean.cm.phase,
        vmin=1,
        vmax=366,
    )
    ax1.set_ylabel(r"$\mathregular{\Delta pH_{Total}}$")
    ax1.tick_params(which="major", top=True, bottom=False, labelbottom=False)
    ax1.text(
        0.05,
        0.1,
        r"$\mathregular{pH_{Spectro} - pH_{SNAPO}}$",
        transform=ax1.transAxes,
        color="k",
        bbox=dict(boxstyle="round", facecolor="white"),
    )

    ax2 = fig.add_subplot(gs[2, 0])

    xmin, xmax = Samples[["pH_Spectro", "pH_SNAPO"]].min().min(), Samples[["pH_Spectro", "pH_SNAPO"]].max().max()
    xmin, xmax = min(7.65, xmin), max(8.35, xmax)
    ax2.plot([xmin, xmax], [xmin, xmax], "--", color="grey", lw=0.8)
    # ax2.plot(Samples.pH_Spectro, Samples.pH_SNAPO, '.')
    ax2.scatter(Samples.pH_Spectro, Samples.pH_SNAPO, c=Samples["DOY"], s=5, cmap=cmocean.cm.phase, vmin=1, vmax=366)
    ax2.set_xlabel(r"$\mathregular{pH_{Spectro}}$")
    ax2.set_ylabel(r"$\mathregular{pH_{SNAPO}}$")

    ax3 = fig.add_subplot(gs[2, 1])
    # ax3.plot(Samples.Psal, Samples.A_T, '.')
    ax3.scatter(Samples.Psal, Samples.A_T, c=Samples["DOY"], s=5, cmap=cmocean.cm.phase, vmin=1, vmax=366)
    ax3.set_xlabel(r"Salinity (psu)")
    ax3.set_ylabel(r"$\mathregular{Alkalinity\ (µmol.kg^{-1})}$")

    ax4 = fig.add_subplot(gs[2, 2])
    # ax4.plot(Samples.C_T, Samples.A_T, '.')
    ax4.scatter(Samples.C_T, Samples.A_T, c=Samples["DOY"], s=5, cmap=cmocean.cm.phase, vmin=1, vmax=366)
    ax4.set_ylabel(r"$\mathregular{Alkalinity\ (µmol.kg^{-1})}$")
    ax4.set_xlabel(r"$\mathregular{DIC \ (µmol.kg^{-1})}$")

    ax5 = fig.add_subplot(gs[3, :], sharex=ax0)
    ax5.hlines(1, d_str, d_end, color="grey", linestyles="dashed", linewidth=0.8)
    ax5.plot(Samples.OmegaAragonite, ".", color=cmap_vars["A_T"], label=r"$\mathregular{\Omega\ Aragonite}$")
    ax5.plot(Samples.OmegaCalcite, ".", color=cmap_vars["C_T"], label=r"$\mathregular{\Omega\ Calcite}$")
    ax5.plot(Samples.OmegaAragoniteSNAPO, ".k", markersize=2)
    ax5.plot(Samples.OmegaCalciteSNAPO, ".k", markersize=2)
    ax5.legend(loc="best", handletextpad=-0.25, fontsize=7)
    ax5.set_ylabel(r"$\mathregular{\Omega\ at\ T^{°C}_{in-situ}}$")

    majors = [d.dayofyear for d in pd.date_range(start="1/1/2012", periods=12, freq="MS")]
    minors = np.array(majors) + 15
    minorslab = [d.strftime("%b").capitalize() for d in pd.date_range(start="1/1/2012", periods=12, freq="MS")]
    majors.append(366)
    cbaxes = fig.add_axes([0.35, 0.55, 0.3, 0.014])
    c = plt.colorbar(cs, orientation="horizontal", cax=cbaxes, ticks=majors)
    # c.solids.set(alpha=.4)
    c.set_label("Day of year", labelpad=-19, size=7)
    c.ax.xaxis.set_ticks(minors, minor=True)
    c.ax.xaxis.set_ticklabels(minorslab, minor=True, backgroundcolor="white")
    c.ax.tick_params(which="minor", labelsize=7, bottom=False, labelbottom=True)
    c.ax.tick_params(which="major", bottom=True, labelbottom=False)

    return fig


french_names = {
    "TEMP": "Température",
    "PSAL": "Salinité",
    "FLUO": "Fluorescence",
    "TURB": "Turbidité",
    "DOX1": "Oxygène",
}


def plot_yearly_variable(
    s, variable, daily=False, d_str=None, d_end=None, french=False, fig_width=8.27, fig_height=11.69
):
    if d_str and d_end:
        df = s.data.loc[d_str:d_end, [variable]]
    elif d_str:
        df = s.data.loc[d_str:, [variable]]
    elif d_end:
        df = s.data.loc[:d_end, [variable]]
    else:
        df = s.data.loc[:, [variable]]

    lw = 0.7
    if daily:
        df = df.resample("D").mean()
        df = df.asfreq("D", fill_value=np.nan)
        lw *= 2

    df["Decimal_Day_of_Year"] = df.index.dayofyear + (df.index.hour * 3600 + df.index.minute * 60 + df.index.second) / (
        24 * 3600
    )

    # Plot each year's time series in its own facet
    n_years = len(df.index.year.unique())
    height = min(4, fig_height / n_years)  # Max height of 4 inches per facet
    aspect = fig_width / height

    pal_len = sum(df[variable].isna())
    if not df[variable].isna().iloc[0]:
        pal_len += 1
    g = sns.relplot(
        data=df,
        x="Decimal_Day_of_Year",
        y=variable,
        col=df.index.year,
        color=cmap_vars[variable],
        kind="line",
        linewidth=lw,
        zorder=5,
        col_wrap=1,
        height=height,
        aspect=aspect,
        legend=False,
        hue=df[variable].isna().cumsum(),
        palette=[cmap_vars[variable]] * pal_len,
    )

    for year, ax in zip(df.index.year.unique(), g.axes.flat):
        # Plot every year's time series in the background
        other_years_data = df
        sns.lineplot(
            data=other_years_data,
            x="Decimal_Day_of_Year",
            y=variable,
            estimator=None,
            color="lightgrey",
            linewidth=lw,
            alpha=0.6,
            ax=ax,
        )
        ax.text(0.5, 0.5, year, transform=ax.transAxes, fontsize=10, fontweight="bold", ha="center")
        if french:
            months = ["Jan", "Fév", "Mar", "Avr", "Mai", "Juin", "Juil", "Août", "Sep", "Oct", "Nov", "Déc"]
        else:
            months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

        days_per_month = [0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        ticks = np.cumsum(days_per_month)
        ax.set_xticks(np.cumsum(days_per_month))
        ax.set_xticklabels([])
        ax.tick_params(which="major", bottom=True, labelbottom=False, direction="inout", colors="k")
        ax.set_xticks(np.cumsum(days_per_month[:-1]) + 15, minor=True)
        ax.set_xticklabels(months, minor=True)
        ax.tick_params(which="minor", bottom=False, labelbottom=True, colors="grey")
        ax.set_xlim(0, 366)

    name_vars = s.conf_vars[variable]["longname"]
    units = s.conf_vars[variable]["units"]
    if french:
        name_vars = french_names[variable]
    plt.figtext(
        -0.01,
        0.5,
        f"{name_vars} ({units})",
        va="center",
        ha="center",
        rotation="vertical",
        fontsize=12,
        fontweight="bold",
    )

    # Tweak the supporting aspects of the plot
    g.set_titles("")
    g.set_axis_labels("", "")
    g.tight_layout()
    plt.subplots_adjust(hspace=0.0)

    fileout = f"comparaison_annuelle_{s.site}_{variable}.png"
    if daily:
        fileout = fileout[:-4] + "_daily.png"

    plt.savefig(fileout, bbox_inches="tight", dpi=300)


def get_month_change_indices(index):
    month_changes = []
    year_changes = []
    last_month = index[0].month
    last_year = index[0].year
    for i, date in enumerate(index[1:], start=1):  # Start from the second entry
        if date.month != last_month:  # If the month changes
            month_changes.append(i)  # Record the index
            last_month = date.month  # Update the last month
            if date.year != last_year:
                year_changes.append("%d" % date.year)
                last_year = date.year
            else:
                year_changes.append("")
    return month_changes, year_changes


def plot_last_year(s, variables, daily=False, d_str=None, d_end=None, french=False, fig_width=8.27, fig_height=11.69):
    if d_str and d_end:
        df = s.data.loc[d_str:d_end, variables]
    elif d_str:
        df = s.data.loc[d_str:, variables]
    elif d_end:
        df = s.data.loc[:d_end, variables]
    else:
        df = s.data.loc[:, variables]

    lw = 0.7
    if daily:
        df = df.resample("D").mean()
        df = df.asfreq("D", fill_value=np.nan)
        lw *= 2

    df["Decimal_Day_of_Year"] = df.index.dayofyear + (df.index.hour * 3600 + df.index.minute * 60 + df.index.second) / (
        24 * 3600
    )

    delay = df["Decimal_Day_of_Year"].iloc[-1] + 1
    df["Decimal_Day_of_Year"] = (df["Decimal_Day_of_Year"] - delay) % 366

    d_tmp = df.index[-1] - dt.timedelta(days=364)
    last_data = df[d_tmp:]

    n_vars = len(variables)
    height = min(4, fig_height / n_vars)
    aspect = fig_width / height

    fig = plt.figure(figsize=(fig_width, fig_height))

    for i, variable in enumerate(variables):
        ax = plt.subplot(n_vars, 1, i + 1)

        sns.lineplot(
            data=df,
            x="Decimal_Day_of_Year",
            y=variable,
            estimator=None,
            color="lightgrey",
            linewidth=lw,
            alpha=0.6,
            ax=ax,
        )

        pal_len = sum(df[variable].isna())
        if not df[variable].isna().iloc[0]:
            pal_len += 1
        sns.lineplot(
            data=last_data,
            x="Decimal_Day_of_Year",
            y=variable,
            color=cmap_vars[variable],
            ax=ax,
            linewidth=lw,
            zorder=5,
            legend=False,
            hue=df[variable].isna().cumsum(),
            palette=[cmap_vars[variable]] * pal_len,
        )

        if i == 0:
            prefix = "Last data : "
            if french:
                locale.setlocale(locale.LC_ALL, "fr_FR")
                prefix = "Dernières données du site :"
            ax.set_title(f'{prefix} {s.site} {s.conf_site["name"]}')
        ax.set_xlabel("")

        name_vars = s.conf_vars[variable]["longname"]
        if french:
            name_vars = french_names[variable]
        units = s.conf_vars[variable]["units"]
        ax.set_ylabel(f"{name_vars} ({units})")

        mo, yy = get_month_change_indices(last_data.index)
        ax.set_xlim(0, 366)

        xticks = last_data.iloc[mo, -1]
        ax.set_xticks(xticks)
        ax.set_xticklabels(yy)
        ax.tick_params(which="major", bottom=True, labelbottom=True, direction="inout", colors="k")

        if i == n_vars - 1:
            xticksminor = [i + 15 for i in xticks]
            ax.set_xticks(xticksminor, minor=True)
            ax.set_xticklabels(last_data.index[mo].strftime("%b"), minor=True)
            ax.tick_params(which="minor", bottom=False, labelbottom=True, colors="grey")
        ax.set_xlim(0, 366)

    plt.subplots_adjust(hspace=0.0)

    fileout = f"comparaison_annuelle_{s.site}.png"
    if daily:
        fileout = fileout[:-4] + "_daily.png"
    plt.savefig(fileout, bbox_inches="tight", dpi=300)


def format_date_xaxis(ax):
    """
    Reset the axis parameters to look nice!
    """
    # Get dt in days
    dt = ax.get_xlim()[-1] - ax.get_xlim()[0]

    if dt <= 1.0 / 24.0:  # less than one hour
        pass
    # elif dt <= 1.2:     # less than one day
    #    ax.xaxis.set_minor_locator( mdates.HourLocator() )
    #    ax.xaxis.set_minor_formatter( mdates.DateFormatter(""))

    #    ax.xaxis.set_major_locator( mdates.HourLocator( interval=3))
    #    ax.xaxis.set_major_formatter( mdates.DateFormatter("%-I %p"))
    elif dt <= 7.0:  # less than one week
        if dt <= 1.5:
            ax.xaxis.set_minor_locator(mdates.HourLocator())
        elif dt <= 3:
            ax.xaxis.set_minor_locator(mdates.HourLocator(byhour=[3, 6, 9, 12, 15, 18, 21]))
        else:
            ax.xaxis.set_minor_locator(mdates.HourLocator(byhour=[6, 12, 18]))
        ax.xaxis.set_minor_formatter(mdates.DateFormatter("%Hh"))
        ax.tick_params(which="minor", colors="grey")

        ax.xaxis.set_major_locator(mdates.DayLocator())
        ax.xaxis.set_major_formatter(mdates.DateFormatter("%d %b"))
    elif dt <= 14.0:  # less than two weeks
        ax.xaxis.set_minor_locator(mdates.DayLocator())
        ax.xaxis.set_minor_formatter(mdates.DateFormatter("%d"))

        ax.xaxis.set_major_locator(mdates.DayLocator(bymonthday=[1, 15]))
        ax.xaxis.set_major_formatter(mdates.DateFormatter("%b %Y"))
        ax.tick_params(which="minor", colors="grey")
    elif dt <= 7 * 30.0:  # less than seven months
        if dt <= 28:
            ax.xaxis.set_minor_locator(mdates.DayLocator())
        elif dt <= 75:
            ax.xaxis.set_minor_locator(mdates.DayLocator(bymonthday=range(0, 31, 5)))
        else:
            ax.xaxis.set_minor_locator(mdates.DayLocator(bymonthday=[1, 8, 15, 22]))
        ax.xaxis.set_minor_formatter(mdates.DateFormatter("%d"))
        ax.tick_params(which="minor", colors="grey")

        ax.xaxis.set_major_locator(mdates.MonthLocator())
        ax.xaxis.set_major_formatter(mdates.DateFormatter("%b %y"))
    elif dt <= 2 * 365:
        if dt <= 500:
            ax.xaxis.set_minor_locator(mdates.MonthLocator(bymonthday=15))
            ax.xaxis.set_minor_formatter(mdates.DateFormatter("%b"))
            ax.tick_params(which="minor", bottom=False, labelbottom=True, colors="grey")
        else:
            ax.xaxis.set_minor_locator(mdates.MonthLocator(interval=6))
            ax.xaxis.set_minor_formatter(mdates.DateFormatter("%b"))

        ax.xaxis.set_major_locator(mdates.MonthLocator())

        def y_fmt(x, y):
            if mdates.num2date(x).timetuple().tm_yday == 1:
                return "%s" % mdates.num2date(x).year
            else:
                return ""

        ax.xaxis.set_major_formatter(tick.FuncFormatter(y_fmt))
        # ax.xaxis.set_major_formatter( mdates.DateFormatter("\n%Y"))
        # majorLabel = []
        # for a in ax.get_xticks():
        #    if mdates.num2date(a).timetuple().tm_yday == 1 or mdates.num2date(a).timetuple().tm_mon == 6:
        #       majorLabel.append('%s'%mdates.num2date(a).year)
        #    else:
        #       majorLabel.append("")
    else:
        ax.xaxis.set_major_locator(mdates.YearLocator())
    return ax


def time_interpolation(df: pd.DataFrame, index: pd.Index, limit: pd.Timedelta) -> pd.DataFrame:
    """
    Interpolate missing values in a DataFrame based on time index and mask values with a specified limit.

    Args:
        df (pd.DataFrame): The DataFrame containing the data to be interpolated.
        index (pd.Index): The desired time index for the interpolated data.
        limit (pd.Timedelta): The maximum allowed time gap between consecutive data points.

    Returns:
        pd.DataFrame: The DataFrame with missing values interpolated based on time index and values masked where the time gap exceeds the specified limit.
    """
    if isinstance(df, pd.Series):
        df = pd.DataFrame(df)

    mask = pd.DataFrame({col: df[col].dropna().index.to_series().diff() for col in df})

    union_index = df.index.union(index)
    mask = mask.reindex(union_index).bfill().reindex(index)
    df = df.reindex(union_index).interpolate(method="time", limit_area="inside").reindex(index)

    df = df.where(mask > limit, np.NaN)

    return df


def linear_regression(X: pd.Series, Y: pd.Series, ax=None) -> sm.regression.linear_model.RegressionResultsWrapper:
    """
    Performs a linear regression analysis on two variables, X and Y.

    Args:
        X (pd.Series): The independent variable.
        Y (pd.Series): The dependent variable.
        ax (Optional[matplotlib.axes.Axes]): The Axes object to plot the regression results.

    Returns:
        sm.regression.linear_model.RegressionResultsWrapper: The regression results.

    """
    # Remove rows with missing values in either X or Y
    X = X.dropna()
    Y = Y.dropna()

    # Align indices between X and Y
    common_index = X.index.intersection(Y.index)
    X = X.loc[common_index]
    Y = Y.loc[common_index]

    # Add a constant term to X
    X = sm.add_constant(X)

    # Fit an OLS regression model
    model = sm.OLS(Y, X)
    res = model.fit()

    # Retrieve the p-values from the regression results
    p_values = res.pvalues

    # Determine the sign of the intercept term
    sign = "+" if res.params.iloc[0] >= 0 else "-"

    # Generate the regression equation, R-squared value, and p-value
    # equation = f"$\mathregular\{y = {res.params.iloc[1]:.3f} ± {res.bse.iloc[1]:.3f}x {sign} {abs(res.params.iloc[0]):.3f} ± {res.bse.iloc[0]:.3f}\}$"
    equation = f"\mathregular{{y = {res.params.iloc[1]:.3f} ± {res.bse.iloc[1]:.3f}x {sign} {abs(res.params.iloc[0]):.3f} ± {res.bse.iloc[0]:.3f}}}"
    r_squared = f"r^2 = {res.rsquared:.3f}"
    p_value = "p < 0.001"

    # Generate the formatted string
    mess = f"{equation}\n${r_squared}$\n${p_value}$"

    mess = f"$\mathregular{{y = {res.params.iloc[1]:.3f} ± {res.bse.iloc[1]:.3f}x {sign} {abs(res.params.iloc[0]):.3f} ± {res.bse.iloc[0]:.3f}}}$"
    equation = f"$\mathregular{{y = {res.params.iloc[1]:.3f} ± {res.bse.iloc[1]:.3f}x {sign} {abs(res.params.iloc[0]):.3f} ± {res.bse.iloc[0]:.3f}}}$"
    r_squared = f"$\mathregular{{r^2 = {res.rsquared:.3f}}}$"
    p_value = "$\mathregular{p < 0.001}$"
    mess = f"{equation}\n{r_squared}\n{p_value}"
    # Plot the regression results if an Axes object is provided
    if ax is not None:
        ax.text(0.05, 0.95, mess, va="top", transform=ax.transAxes, weight="normal")

    return res


def vector2speeddir(u10: float, v10: float, convention: str = "meteo") -> tuple[float, float]:
    """
    Calculates the speed and direction of a vector given its components u10 and v10.

    Args:
        u10 (float): The x-component of the vector.
        v10 (float): The y-component of the vector.
        convention (str, optional): The convention used for the calculation. Default is 'meteo'.

    Returns:
        tuple[float, float]: The calculated speed and direction as a tuple (ws, wd).


    ws, wd = vector2speeddir(np.array([-1, -1, +1, +1, -1, 1], [ 0, -1, -1, +1, +1, 0], "meteo")
    print(wd)
    vector2speeddir(np.array([-1, -1, +1, +1, -1, 1], [ 0, -1, -1, +1, +1, 0], "oceano")
    # for i in range(6):
    #  print('%3.0f %3.0f %3.0f'%(u10m[i],v10m[i],wind_direction[i]))

    """
    ws = np.sqrt(u10**2 + v10**2)
    if convention == "meteo":
        wd = np.arctan2(-v10, -u10) * 180 / np.pi
    elif convention == "oceano":
        wd = np.arctan2(v10, u10) * 180 / np.pi
    wd = np.mod((-wd + 90.0), 360.0)
    return ws, wd


def plot_wind_data(ax, time, ws, wd, rose=True, inset_position=[0.6, 0.69, 0.2, 0.2]):
    ax.plot(time, ws, color="k", lw=0.5)

    # Bar width calculation
    bar_width = np.diff(time)
    bar_width = np.concatenate((bar_width, [bar_width[-1]]))
    c = ax.bar(time, height=ws, width=bar_width, color=cmocean.cm.phase(wd / 360))

    # Inset Axes
    if rose:
        axin = ax.inset_axes(inset_position, polar=True)
        axin.set_theta_zero_location("N")
        axin.set_theta_direction(-1)
        axin.set_yticks([])
        axin.set_xticks(np.radians(np.arange(0, 360, 45)))
        axin.tick_params(axis="x", which="major", pad=-3)
        axin.set_xticklabels(["N", "NE", "E", "SE", "S", "SW", "W", "NW"], fontsize=6)
        azimuths = np.arange(0, 361, 1)
        zeniths = np.arange(10, 40, 1)
        values = azimuths * np.ones((30, 361))
        axin.pcolormesh(azimuths * np.pi / 180.0, zeniths, values, cmap=cmocean.cm.phase)

    ax.set_xlim(time[0], time[-1])


def plot_ts_diagram(ax, df, extension_percentage=0.05):
    """
    Plot a T-S (Temperature-Salinity) diagram on the given axis.

    Parameters:
    - ax: matplotlib axis object where the plot will be drawn.
    - df: pd.DataFrame containing 'temperature' and 'salinity' columns.
                       optional 'depth' column for a scattered plot
    """
    df = df.dropna()
    if len(df.columns) == 2:
        df.columns = ["temperature", "salinity"]
    else:
        df.columns = ["temperature", "salinity", "depth"]
        df = df.sort_values("depth", ascending=True)

    mint = np.nanmin(df["temperature"])
    maxt = np.nanmax(df["temperature"])
    temp_range = maxt - mint
    temp_extension = temp_range * extension_percentage

    mins = np.nanmin(df["salinity"])
    maxs = np.nanmax(df["salinity"])
    sal_range = maxs - mins
    sal_extension = sal_range * extension_percentage

    tempL = np.linspace(mint - temp_extension, maxt + temp_extension, 156)
    salL = np.linspace(mins - sal_extension, maxs + sal_extension, 156)

    Tg, Sg = np.meshgrid(tempL, salL)
    sigma_theta = gsw.sigma0(Sg, Tg)
    cnt = np.linspace(sigma_theta.min(), sigma_theta.max(), 156)

    cs = ax.contour(Sg, Tg, sigma_theta, linestyles="dashed", colors="grey", linewidths=0.7, zorder=1)
    ax.clabel(cs, fontsize=10, inline=1, fmt="%.1f")

    if len(df.columns) == 2:
        ax.plot(df["salinity"], df["temperature"], ".", ms=2)
    else:
        vmin = min(df["depth"].min(), 0)
        sc = ax.scatter(df["salinity"], df["temperature"], c=df["depth"], s=1, vmin=vmin)

        axins = inset_axes(
            ax,
            width="70%",  # width of the colorbar relative to the plot
            height="6%",  # height of the colorbar relative to the plot
            loc="upper right",
            bbox_to_anchor=(0.1, 0.1, 0.8, 0.9),
            bbox_transform=ax.transAxes,
            borderpad=0.5,
        )
        cb = plt.colorbar(sc, cax=axins, orientation="horizontal")
        cb.ax.tick_params(colors="m")
        cb.set_label("Depth (m)", color="w", labelpad=-23, fontsize=8, weight="bold")

    ax.set_xlabel("Absolute salinity")
    ax.set_ylabel("Conservative temperature (°C)")
    ax.xaxis.set_major_locator(MaxNLocator(nbins=6))
    ax.yaxis.set_major_locator(MaxNLocator(nbins=8))


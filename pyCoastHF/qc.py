import logging
import datetime as dt

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

from . import climate

"""
This code snippet contains several functions related to data filtering and flagging. 

The functions included are:
- reset_flags: Resets the flag values of a given variable to a specified flag value.
- flag_missing_values: Flags missing values in a given variable.
- threshold_min: Filters a variable based on a minimum threshold value. The filtering is done in a specific order of priority.
- threshold_max: Filters a variable based on a maximum threshold value. The filtering is done in a specific order of priority.
- rate_of_change: Evaluates the rate of change of a variable with respect to the previous measurement.
- gradient: Evaluates the rate of change of a variable with respect to the neighboring points.
- spike: Evaluates the rate of change of a variable with respect to the neighboring points and the gradient.
- tukey53H: Evaluates how different a measurement is compared to the low-frequency tendency of the data series.
- climato: Evaluates the bias between the observed measurement and a climatology, normalized by the expected variability.
- center_time_window_rolling: Computes a center rolling mean with a time offset.
- forward_rolling: Computes a forward rolling mean with a time offset.
- forward_rolling_sum: Computes a forward rolling sum with a time offset.
- forward_rolling_nbr: Computes a forward rolling count with a time offset.
- mask_spikes: Masks spikes in a variable based on the gradient and a specified limit.

Please refer to the individual function docstrings for more details on their usage and parameters.
"""


class FilterManager:
    def __init__(self, config_site, config_vars):
        self.config_site = config_site
        self.config_vars = config_vars

    def get_filter_value(self, var, filter_type):
        # Priority 1 : site specific value
        value = self.config_site.get(var, {}).get(filter_type, None)
        if value is None:
            # Priority 2 : general value
            value = self.config_vars.get(var, {}).get(filter_type, None)
        return value  # Priorité 3 : No filter will be applied

    def apply_filters(self, var):
        return {
            "threshold_min": self.get_filter_value(var, "threshold_min"),
            "threshold_max": self.get_filter_value(var, "threshold_max"),
            "tukey53H": self.get_filter_value(var, "tukey53H"),
            "climatology": self.get_filter_value(var, "climatology"),
        }

    def generate_qc(self, all_vars):
        return {var: self.apply_filters(var) for var in all_vars}


def reset_flags(s, flag=1):
    s.flag = s.raws.copy() * 0 + flag


def flag_missing_values(s):
    s.flag[s.raws.isnull()] = 9


def threshold_min(s, var, value=None, local=True):
    """
    Filtering of a variable with min threshold by following order of priority:
        1. Site value
        2. General variable value
        3. No filter
    if not local: flag all variables
    """
    if value is None:
        value = s.conf_qc["threshold_min"]
        if value is None:
            return
    logging.debug(f"Min threshold for {s.conf_vars[var]['longname']}: {value:.2f}")
    if local:
        s.flag.loc[s.raws[var] < value, var] = 4
        s.flag.loc[s.raws[var].isnull(), var] = 9
    else:
        s.flag[s.raws[var] < value] = 4
        s.flag[s.raws.isnull()] = 9


def threshold_max(s, var, value=None, local=True):
    """
    Filtering of a variable with max threshold by following order of priority:
        1. Site value
        2. General variable value
        3. No filter
    if not local: flag all variables
    """
    if value is None:
        value = s.conf_qc["threshold_max"]
        if value is None:
            return

    logging.debug(f"Max threshold for {s.conf_vars[var]['longname']}: {value:.2f}")
    if local:
        s.flag.loc[s.raws[var] > value, var] = 4
        s.flag.loc[s.raws[var].isnull(), var] = 9
    else:
        s.flag[s.raws[var] > value] = 4
        s.flag[s.raws.isnull()] = 9


def rate_of_change(s, var, value=None):
    """
    Evaluates the change in respect to the previous measurement [x(i) - x(i-1)] / [t(i) - t(i-1)]
    Original RoC(t) = x(t) - x(t-1)
    PROBLEM: as it is coded, it flags 2 data (the bad one & the next correct one)
    TO SOLVE : make an iterative function with a rolling window (10 measurements)
     -> assume the first signal is stable
     -> loop over raw data & flag only next value upon RoC
     -> avoid false negative in case of noised signal
    """
    if value is None:
        value = s.conf_qc["rate_of_change"]
        if value is None:
            return

    logging.debug("Max rate of change for %s: %.2f / hr" % (s.conf_vars[var]["longname"], value))
    value /= 3600.0
    data = s.raws.loc[s.flag[var] <= 2, var]
    dnum = data.index.to_series().diff().dt.total_seconds().values
    roc = data.diff().div(dnum, fill_value=0).shift(periods=-1).abs()
    ind = s.flag.index.isin(roc.index[roc > value])
    s.flag.loc[ind, var] = 3
    s.flag.loc[s.raws[var].isnull(), var] = 9
    return roc


def gradient(s, var, value=None):
    """
    Evaluates the rate of change in respect to the neighboring points
    gradient = | x(i) - [x(i+1) + x(i-1)] / 2) |

    (dt(i+1) * x(i+1) + dt(i+1) * x(i+1)) / (t(i+1) - t(i-1))
    """
    if value is None:
        value = s.conf_qc["gradient"]
        if value is None:
            return

    data = s.raws.loc[s.flag[var] <= 2, var]
    dnum_g = data.index.to_series().diff().dt.total_seconds()
    dnum_d = dnum_g.shift(periods=-1)
    data_g = data.shift(periods=1)
    data_d = data.shift(periods=-1)
    grad = data - (data_g * dnum_d + data_d * dnum_g) / (dnum_d + dnum_g)

    ind = s.flag.index.isin(grad.index[grad.abs() > value])
    s.flag.loc[ind, var] = 3
    s.flag.loc[s.raws[var].isnull(), var] = 9
    
    s.grad = grad.abs()

    return grad.abs()


def spike(s, var, value=None):
    """
    Evaluates the rate of change in respect to the neighboring points
    spikes = | x(i) - [x(i+1) + x(i-1)] / 2) |

    grad - | [x(i+1) - x(i+1)] / [t(i+1) - t(i-1)] |
    """
    if value is None:
        value = s.conf_qc["spike"]
        if value is None:
            return

    data = s.raws.loc[s.flag[var] <= 2, var]
    dnum_g = data.index.to_series().diff().dt.total_seconds()
    dnum_d = dnum_g.shift(periods=-1)
    data_g = data.shift(periods=1)
    data_d = data.shift(periods=-1)
    spik = s.grad - ((data_d - data_g) / (dnum_d + dnum_g)).abs()
    return spik.abs()


def tukey53H(s, var, value=None, freq=pd.Timedelta(minutes=15), normalize=True):
    """
    Evaluates how different a measurement is in comparison to the low-frequency tendency of the data series.
    Goring, D.G., Nikora, V.I., 2002. Despiking acoustic Doppler velocimeter data. J. Hydraul. Eng. 128 (1), 117–126.
    """
    if value is None:
        value = s.conf_qc["tukey53H"]
        if value is None:
            return
    logging.debug("Tukey 53H value for %s: %.2f " % (s.conf_vars[var]["longname"], value))

    x = s.raws.loc[s.flag[var] <= 2, var]

    u1 = x.rolling(freq * 5.1, center=True).median()  # 5 * freq
    u2 = u1.rolling(freq * 3.1, center=True).median()  # 3 * freq
    u3 = 0.25 * (u2.shift(-1) + 2 * u2 + u2.shift(1))
    delta = (x - u3).abs()
    if normalize:
        sigma = u1.dropna().std(ddof=1)
        delta /= sigma

    ind = s.flag.index.isin(delta.index[delta > value])
    s.flag.loc[ind, var] = 3
    s.flag.loc[s.raws[var].isnull(), var] = 9
    return delta


def climatology(s, var, value=None):
    """
    Evaluates the bias between the observed measurement and a climatologgingy,
    normalized by the expected variability in that point and time
      𝑦𝑐𝑖= |𝑥𝑖 −⟨𝑥𝑖⟩| / 𝜎𝑖
     * Needs at least daily observations of 5 different years for each day
    """
    clim = climate.read_climatology(s.site)
    if clim is None:
        return
    if var not in list(clim.data):
        logging.debug(f'No climatological data found for {s.conf_vars[var]["longname"]} found')
        return

    if value is None:
        value = s.conf_qc["climatology"]
        if value is None:
            return
    logging.debug("Climatologic value for %s: %.2f " % (s.conf_vars[var]["longname"], value))

    x = pd.DataFrame(s.raws.loc[s.flag[var] <= 2, var])
    x["DOY"] = x.index.dayofyear
    x.loc[(x.DOY >= 60) & (~x.index.is_leap_year), "DOY"] += 1
    x["clim"] = clim.data.loc[x.DOY.values.astype(int), var].values
    x["nstd"] = clim.nstd.loc[x.DOY.values.astype(int), var].values
    x["nobs"] = clim.nobs.loc[x.DOY.values.astype(int), var].values

    delta = (x[var] - x["clim"]).abs() / x["nstd"]

    ind = s.flag.index.isin(delta.index[(delta > value) & (x["nobs"] >= 5)])
    s.flag.loc[ind, var] = 2
    s.flag.loc[s.raws[var].isnull(), var] = 9
    return delta


####################################################
#     Filtre temporel
####################################################
def center_time_window_rolling(data, window):
    """Function that computes a center rolling mean with a time
    offset. Not possible to center in pd.rolling 0.20.3
    """
    delta = pd.tseries.frequencies.to_offset(window).delta / 2.0

    def f(x):
        """Function to apply that actually computes the rolling operators"""
        dslice = data[x - delta : x + delta]  # noqa: E203
        return dslice.mean()

    idx = pd.Series(data.index.to_pydatetime(), index=data.index)
    dfout = idx.apply(f)
    return dfout


def forward_rolling(data, window):
    """Function that computes a forward rolling mean with a time
    offset. Not possible to center in pd.rolling 0.20.3
    """
    df = data[::-1]
    df.index = dt.datetime(2050, 1, 1) - df.index
    df = df.rolling(window).mean()
    df = df[::-1]
    df.index = data.index
    return df


def forward_rolling_sum(data, window):
    """Function that computes a forward rolling sum with a time
    offset. Not possible to center in pd.rolling 0.20.3
    """
    df = data[::-1]
    df.index = dt.datetime(2050, 1, 1) - df.index
    df = df.rolling(window).sum()
    df = df[::-1]
    df.index = data.index
    return df


def forward_rolling_nbr(data, window):
    """Function that computes a forward rolling count with a time
    offset. Not possible to center in pd.rolling 0.20.3
    """
    df = data[::-1]
    df.index = dt.datetime(2050, 1, 1) - df.index
    df = df.rolling(window).count()
    df = df[::-1]
    df.index = data.index
    return df


def mask_spikes(grad, mask, lim=1):
    """Se base sur le gradient vers la gauche : var[1:] - var[:-1]"""
    # To avoid RuntimeWarning: invalid value encountered in greater
    grad[np.isnan(grad)] = 0.0

    maskp = grad[:-1] > lim
    maskm = grad[:-1] < -lim
    # Downwards spike
    maskp_dw = np.append(maskp, False)
    maskm_dw = np.insert(maskm, 0, False)
    # Upwards spike
    maskp_up = np.insert(maskp, 0, False)
    maskm_up = np.append(maskm, False)
    # Masked neighbours
    masked_left = np.insert(mask[:-1], 0, False)
    masked_right = np.append(mask[1:], False)
    return (
        mask
        | (maskm_dw & maskp_dw)
        | (maskp_up & maskm_up)
        | (masked_left & (maskp_dw | maskm_up))
        | (masked_right & (maskm_dw | maskp_up))
    )


#################
# Développement #
#################
"""
# def mask_spikes(grad, mask, lim=1):  # noqa
#      10.   10.   10.   20.   10.   10.   20.   NaN    10.   0.   NaN   10.    0.  10.  NaN  20.  10.  10.  NaN   0.  10.  10. <== varf
#       0.    0.   10.  -10.    0.   10.   NaN   NaN   -10.  NaN   NaN  -10.   10.  NaN  NaN -10.   0.  NaN  NaN  10.   0.      <== grad
#       F     F     F     F     F     F     F     T     F     F     T     F     F    F    T    F    F    F    T    F    F    F  <== mask (T si NaN)
#       F     F     T     F     F     T     F     F     F     F     F     F     T    F    F    F    F    F    F    T    F       <== maskp
#       F     F     F     T     F     F     F     F     T     F     F     T     F    F    F    T    F    F    F    F    F       <== maskm

#       F     F     T     F     F     T     F     F     F     F     F     F     T    F    F    F    F    F    F    T    F    F  <== maskp_dw
#   --> F     F     F     T     F     F     T     F     F     F     F     F     F    T    F    F    F    F    F    F    T    F  <== maskp_up

#   --> F     F     F     T     F     F     F     F     T     F     F     T     F    F    F    T    F    F    F    F    F    F  <== maskm_up
#       F     F     F     F     T     F     F     F     F     T     F     F     T    F    F    F    T    F    F    F    F    F  <== maskm_dw

#   --> F     F     F     F     F     F     F     F     T     F     F     T     F    F    F    T    F    F    F    T    F    F  <== masked_left
#       F     F     F     F     F     F     T     F     F     T     F     F     F    T    F    F    F    T    F    F    F    F  <== masked_right
"""

#!/usr/bin/env python
# -*- coding: utf-8 -*-

periods = {}

import numpy as np

periods['PROF'] = ( \
        ['SMATCH_SURF' ,'2008/03/18 00:00','2008/07/08 00:00', .1, .3    ],\
        ['SMATCH_SURF' ,'2008/07/17 00:00','2008/09/17 12:00', .1, .3    ],\
        ['SMATCH_SURF' ,'2008/09/25 12:00','2009/01/01 00:00', .1, .3    ],\
        ['SMATCH_SURF' ,'2009/06/12 15:00','2009/07/08 00:00', .1, .3    ],\
        ['SMATCH_SURF' ,'2009/10/30 13:00','2010/10/23 00:00', .1, .3    ],\
        ['SP2T_29022'  ,'2010/10/23 00:00','2010/10/25 12:00', .1, np.nan],\
        ['SMATCH_SURF' ,'2011/01/06 12:00','2011/07/05 12:00', .1, .3    ],\
        ['SMATCH_SURF' ,'2011/07/19 12:00','2012/01/13 00:00', .1, .3    ],\
        ['STPS_29020'  ,'2012/01/13 12:00','2012/01/29 02:00', .1, .3    ],\
        ['SMATCH_SURF' ,'2012/01/29 02:00','2012/02/05 21:00', .1, .3    ],\
        ['STPS_29020'  ,'2012/02/05 21:00','2012/02/14 03:00', .1, .3    ],\
        ['STPS_29020'  ,'2012/02/23 11:00','2012/03/14 02:00', .1, .3    ],\
        ['SMATCH_FOND' ,'2012/03/14 02:00','2012/04/01 05:00', .1, .3    ],\
        ['STPS_29020'  ,'2012/04/01 05:00','2013/05/07 09:00', .1, .3    ],\
        ['SMATCH_FOND' ,'2013/05/07 09:00','2013/12/04 00:00', .1, .3    ],\
        ['SMATCH_SURF' ,'2013/12/04 00:00','2013/12/20 00:00', .1, .3    ],\
        ['SMATCH_FOND' ,'2013/12/20 00:00','2014/01/16 10:00', .1, .3    ],\
        ['STPS_29020'  ,'2014/01/16 10:00','2014/04/05 13:00', .1, .3    ],\
        ['SMATCH_FOND' ,'2014/04/05 13:00','2014/04/25 19:30', .1, .3    ],\
        ['STPS_29020'  ,'2014/05/13 09:00','2014/06/05 00:00', .1, .3    ],\
        ['SMATCH_SURF' ,'2014/06/05 00:00','2014/06/16 14:00', .1, .3    ],\
        ['STPS_29020'  ,'2014/06/16 14:00','2014/07/15 12:00', .1, .3    ],\
        ['STPS_32019'  ,'2014/07/15 12:00','2014/07/29 12:00', .1, .3    ],\
        ['STPS_29020'  ,'2014/07/29 12:00','2014/08/13 12:00', .1, .3    ],\
        ['STPS_32019'  ,'2014/08/13 12:00','2014/08/26 12:00', .1, .3    ],\
        ['STPS_29020'  ,'2014/08/26 12:00','2014/12/01 16:00', .1, .3    ],\
        ['STPS_29020'  ,'2014/12/04 12:00','2015/02/19 12:00', .1, .3    ],\
        ['STPS_34013'  ,'2015/02/19 12:00','2015/07/02 12:00', .1, .3    ],\
        ['SMATCH_FOND' ,'2015/07/02 12:00','2015/07/17 12:00', .1, .3    ],\
        ['STPS_34013'  ,'2015/07/17 12:00','2016/01/01 00:00', .1, .3    ],\
        ##                           2016                             ##
        ['STPS_34013'  ,'2016/01/01 00:00','2016/02/10 12:00', .1, .3    ],\
        ['STPS_32019'  ,'2016/02/10 12:00','2016/03/09 12:00', .1, .3    ],\
        ['STPS_29020'  ,'2016/03/09 12:00','2016/04/20 10:00', .1, .3    ],\
        ['STPS_34013'  ,'2016/04/20 10:00','2016/05/04 08:00', .1, .3    ],\
        ['STPS_29020'  ,'2016/05/04 08:00','2016/09/10 04:00', .1, .3    ],\
        ['SMATCH_30026','2016/09/10 04:00','2016/11/02 12:00', .1, .3    ],\
        ['STPS_32019'  ,'2016/11/02 12:00','2017/01/01 00:00', .1, .3    ],\
        ##                           2017                             ##
        ['STPS_32019'  ,'2017/01/01 00:00','2017/02/14 12:00', .1, .3    ],\
        ['STPS_34013'  ,'2017/02/14 12:00','2017/03/02 12:00', .1, .3    ],\
        ['STPS_32019'  ,'2017/03/02 12:00','2017/04/11 12:00', .1, .3    ],\
        ['STPS_29020'  ,'2017/04/11 12:00','2017/07/10 12:00', .1, .3    ],\
        ['STPS_32019'  ,'2017/07/10 12:00','2018/01/01 00:00', .1, .3    ],\
        ##                           2018                             ##
        ['STPS_32019'  ,'2018/01/01 00:00','2018/10/09 12:00', .1, .3    ],\
        ['STPS_29020'  ,'2018/10/09 12:00','2019/01/01 00:00', .1, .3    ],\
        ##                           2019                             ##
        ['STPS_29020'  ,'2019/01/01 00:00','2020/01/01 00:00', .1, .3    ],\
        ##                           2020                             ##
        ['STPS_29020'  ,'2020/01/01 00:00','2021/01/01 00:00', .1, .3    ],\
        ##                           2021                             ##
        ['STPS_29020'  ,'2021/01/01 00:00','2021/02/12 10:00', .1, .3    ],\
        ['STPS_31018'  ,'2021/02/12 10:00','2022/01/01 00:00', .1, .3    ],\
        ##                           2022                             ##
        ['STPS_31018'  ,'2022/01/01 00:00','2023/01/01 00:00', .1, .3    ],\
        ##                           2023                             ##
        ['STPS_31018'  ,'2023/01/01 00:00','2024/01/01 00:00', .1, .3    ],\
        ##                           2024                             ##
        ['STPS_31018'  ,'2024/01/01 00:00','2024/04/11 10:30', .1, .3    ],\
        ['STPS_36005'  ,'2024/04/11 10:30','2025/01/01 00:00', .1, .3    ],\
)

periods['TEMP'] = ( \
        ['EBI_IUEM'    ,'2006/01/01 00:00','2008/03/18 00:00', .3, np.nan],\
        ['SMATCH_SURF' ,'2008/03/18 00:00','2008/07/08 00:00', .1, .3    ],\
        ['OTT_43144'   ,'2008/07/08 00:00','2008/07/17 00:00', .2, .3    ],\
        ['SMATCH_SURF' ,'2008/07/17 00:00','2008/09/17 12:00', .1, .3    ],\
        ['OTT_43144'   ,'2008/09/17 12:00','2008/09/25 12:00', .2, .3    ],\
        ['SMATCH_SURF' ,'2008/09/25 12:00','2009/01/01 00:00', .1, .3    ],\
        ['OTT_43144'   ,'2009/01/01 00:00','2009/06/12 14:50', .2, .3    ],\
        ['SMATCH_SURF' ,'2009/06/12 15:00','2009/07/08 00:00', .1, .3    ],\
        ['OTT_46531'   ,'2009/07/08 00:00','2009/08/10 13:00', .2, .3    ],\
        ['OTT_43144'   ,'2009/08/10 13:00','2009/10/30 13:00', .2, .3    ],\
        ['SMATCH_SURF' ,'2009/10/30 13:00','2010/10/23 00:00', .1, .3    ],\
        ['SP2T_29022'  ,'2010/10/23 00:00','2010/10/25 12:00', .1, np.nan],\
        ['OTT_49226'   ,'2010/10/25 12:00','2011/01/06 12:00', .2, .3    ],\
        ['SMATCH_SURF' ,'2011/01/06 12:00','2011/07/05 12:00', .1, .3    ],\
        ['OTT_49226'   ,'2011/07/05 12:00','2011/07/19 12:00', .2, .3    ],\
        ['SMATCH_SURF' ,'2011/07/19 12:00','2012/01/13 00:00', .1, .3    ],\
        ['STPS_29020'  ,'2012/01/13 12:00','2012/01/29 02:00', .1, .3    ],\
        ['SMATCH_SURF' ,'2012/01/29 02:00','2012/02/05 21:00', .1, .3    ],\
        ['STPS_29020'  ,'2012/02/05 21:00','2012/02/14 03:00', .1, .3    ],\
        ['OTT_49226'   ,'2012/02/14 03:00','2012/02/23 11:00', .2, .3    ],\
        ['STPS_29020'  ,'2012/02/23 11:00','2012/03/14 02:00', .1, .3    ],\
        ['SMATCH_FOND' ,'2012/03/14 02:00','2012/04/01 05:00', .1, .3    ],\
        ['STPS_29020'  ,'2012/04/01 05:00','2013/05/07 09:00', .1, .3    ],\
        ['SMATCH_FOND' ,'2013/05/07 09:00','2013/12/04 00:00', .1, .3    ],\
        ['SMATCH_SURF' ,'2013/12/04 00:00','2013/12/20 00:00', .1, .3    ],\
        ['SMATCH_FOND' ,'2013/12/20 00:00','2014/01/16 10:00', .1, .3    ],\
        ['STPS_29020'  ,'2014/01/16 10:00','2014/04/05 13:00', .1, .3    ],\
        ['SMATCH_FOND' ,'2014/04/05 13:00','2014/04/25 19:30', .1, .3    ],\
        ['OTT_49226'   ,'2014/04/25 19:30','2014/05/13 09:00', .2, .3    ],\
        ['STPS_29020'  ,'2014/05/13 09:00','2014/06/05 00:00', .1, .3    ],\
        ['SMATCH_SURF' ,'2014/06/05 00:00','2014/06/16 14:00', .1, .3    ],\
        ['STPS_29020'  ,'2014/06/16 14:00','2014/07/15 12:00', .1, .3    ],\
        ['STPS_32019'  ,'2014/07/15 12:00','2014/07/29 12:00', .1, .3    ],\
        ['STPS_29020'  ,'2014/07/29 12:00','2014/08/13 12:00', .1, .3    ],\
        ['STPS_32019'  ,'2014/08/13 12:00','2014/08/26 12:00', .1, .3    ],\
        ['STPS_29020'  ,'2014/08/26 12:00','2014/12/01 16:00', .1, .3    ],\
        ['OTT_62596'   ,'2014/12/01 16:00','2014/12/04 12:00', .2, .3    ],\
        ['STPS_29020'  ,'2014/12/04 12:00','2015/02/19 12:00', .1, .3    ],\
        ['STPS_34013'  ,'2015/02/19 12:00','2015/07/02 12:00', .1, .3    ],\
        ['SMATCH_FOND' ,'2015/07/02 12:00','2015/07/17 12:00', .1, .3    ],\
        ['STPS_34013'  ,'2015/07/17 12:00','2016/01/01 00:00', .1, .3    ],\
        ##                           2016                             ##
        ['STPS_34013'  ,'2016/01/01 00:00','2016/02/10 12:00', .1, .3    ],\
        ['STPS_32019'  ,'2016/02/10 12:00','2016/03/09 12:00', .1, .3    ],\
        ['STPS_29020'  ,'2016/03/09 12:00','2016/04/20 10:00', .1, .3    ],\
        ['STPS_34013'  ,'2016/04/20 10:00','2016/05/04 08:00', .1, .3    ],\
        ['STPS_29020'  ,'2016/05/04 08:00','2016/09/10 04:00', .1, .3    ],\
        ['SMATCH_30026','2016/09/10 04:00','2016/11/02 12:00', .1, .3    ],\
        ['STPS_32019'  ,'2016/11/02 12:00','2017/01/01 00:00', .1, .3    ],\
        ##                           2017                             ##
        ['STPS_32019'  ,'2017/01/01 00:00','2017/02/14 12:00', .1, .3    ],\
        ['STPS_34013'  ,'2017/02/14 12:00','2017/03/02 12:00', .1, .3    ],\
        ['STPS_32019'  ,'2017/03/02 12:00','2017/04/11 12:00', .1, .3    ],\
        ['STPS_29020'  ,'2017/04/11 12:00','2017/07/10 12:00', .1, .3    ],\
        ['STPS_32019'  ,'2017/07/10 12:00','2018/01/01 00:00', .1, .3    ],\
        ##                           2018                             ##
        ['STPS_32019'  ,'2018/01/01 00:00','2018/10/09 12:00', .1, .3    ],\
        ['STPS_29020'  ,'2018/10/09 12:00','2019/01/01 00:00', .1, .3    ],\
        ##                           2019                             ##
        ['STPS_29020'  ,'2019/01/01 00:00','2020/01/01 00:00', .1, .3    ],\
        ##                           2020                             ##
        ['STPS_29020'  ,'2020/01/01 00:00','2021/01/01 00:00', .1, .3    ],\
        ##                           2021                             ##
        ['STPS_29020'  ,'2021/01/01 00:00','2021/02/12 10:00', .1, .3    ],\
        ['STPS_31018'  ,'2021/02/12 10:00','2022/01/01 00:00', .1, .3    ],\
        ##                           2022                             ##
        ['STPS_31018'  ,'2022/01/01 00:00','2023/01/01 00:00', .1, .3    ],\
        ##                           2023                             ##
        ['STPS_31018'  ,'2023/01/01 00:00','2024/01/01 00:00', .1, .3    ],\
        ##                           2024                             ##
        ['STPS_31018'  ,'2024/01/01 00:00','2024/04/11 10:30', .1, .3    ],\
        ['STPS_36005'  ,'2024/04/11 10:30','2025/01/01 00:00', .1, .3    ],\
)

periods['PSAL'] = ( \
        ['SMATCH_SURF' ,'2008/03/18 00:00','2008/07/08 00:00', .1, .3    ],\
        ['OTT_43144'   ,'2008/07/08 00:00','2008/07/17 00:00', .2, .3    ],\
        ['SMATCH_SURF' ,'2008/07/17 00:00','2008/09/17 12:00', .1, .3    ],\
        ['OTT_43144'   ,'2008/09/17 12:00','2008/09/25 12:00', .2, .3    ],\
        ['SMATCH_SURF' ,'2008/09/25 12:00','2009/01/01 00:00', .1, .3    ],\
        ['OTT_43144'   ,'2009/01/01 00:00','2009/06/12 14:50', .2, .3    ],\
        ['SMATCH_SURF' ,'2009/06/12 15:00','2009/07/08 00:00', .1, .3    ],\
        ['OTT_46531'   ,'2009/07/08 00:00','2009/08/10 13:00', .2, .3    ],\
        ['OTT_43144'   ,'2009/08/10 13:00','2009/10/30 13:00', .2, .3    ],\
        ['SMATCH_SURF' ,'2009/10/30 13:00','2010/10/23 00:00', .1, .3    ],\
        ['OTT_49226'   ,'2010/10/25 12:00','2011/01/06 12:00', .2, .3    ],\
        ['SMATCH_SURF' ,'2011/01/06 12:00','2011/07/05 12:00', .1, .3    ],\
        ['OTT_49226'   ,'2011/07/05 12:00','2011/07/19 12:00', .2, .3    ],\
        ['SMATCH_SURF' ,'2011/07/19 12:00','2012/01/13 00:00', .1, .3    ],\
        ['STPS_29020'  ,'2012/01/13 12:00','2012/01/29 02:00', .1, .3    ],\
        ['SMATCH_SURF' ,'2012/01/29 02:00','2012/02/05 21:00', .1, .3    ],\
        ['STPS_29020'  ,'2012/02/05 21:00','2012/02/14 03:00', .1, .3    ],\
        ['OTT_49226'   ,'2012/02/14 03:00','2012/02/23 11:00', .2, .3    ],\
        ['STPS_29020'  ,'2012/02/23 11:00','2012/03/14 02:00', .1, .3    ],\
        ['SMATCH_FOND' ,'2012/03/14 02:00','2012/04/01 05:00', .1, .3    ],\
        ['STPS_29020'  ,'2012/04/01 05:00','2013/05/07 09:00', .1, .3    ],\
        ['SMATCH_FOND' ,'2013/05/07 09:00','2013/12/04 00:00', .1, .3    ],\
        ['SMATCH_SURF' ,'2013/12/04 00:00','2013/12/20 00:00', .1, .3    ],\
        ['SMATCH_FOND' ,'2013/12/20 00:00','2014/01/16 10:00', .1, .3    ],\
        ['STPS_29020'  ,'2014/01/16 10:00','2014/04/05 13:00', .1, .3    ],\
        ['SMATCH_FOND' ,'2014/04/05 13:00','2014/04/25 19:30', .1, .3    ],\
        ['OTT_49226'   ,'2014/04/25 19:30','2014/05/13 09:00', .2, .3    ],\
        ['STPS_29020'  ,'2014/05/13 09:00','2014/06/05 00:00', .1, .3    ],\
        ['SMATCH_SURF' ,'2014/06/05 00:00','2014/06/16 14:00', .1, .3    ],\
        ['STPS_29020'  ,'2014/06/16 14:00','2014/07/15 12:00', .1, .3    ],\
        ['STPS_32019'  ,'2014/07/15 12:00','2014/07/29 12:00', .1, .3    ],\
        ['STPS_29020'  ,'2014/07/29 12:00','2014/08/13 12:00', .1, .3    ],\
        ['STPS_32019'  ,'2014/08/13 12:00','2014/08/26 12:00', .1, .3    ],\
        ['STPS_29020'  ,'2014/08/26 12:00','2014/12/01 16:00', .1, .3    ],\
        ['OTT_62596'   ,'2014/12/01 16:00','2014/12/04 12:00', .2, .3    ],\
        ['STPS_29020'  ,'2014/12/04 12:00','2015/02/19 12:00', .1, .3    ],\
        ['STPS_34013'  ,'2015/02/19 12:00','2015/07/02 12:00', .1, .3    ],\
        ['SMATCH_FOND' ,'2015/07/02 12:00','2015/07/17 12:00', .1, .3    ],\
        ['STPS_34013'  ,'2015/07/17 12:00','2016/01/01 00:00', .1, .3    ],\
        ##                           2016                             ##
        ['STPS_34013'  ,'2016/01/01 00:00','2016/02/10 12:00', .1, .3    ],\
        ['STPS_32019'  ,'2016/02/10 12:00','2016/03/09 12:00', .1, .3    ],\
        ['STPS_29020'  ,'2016/03/09 12:00','2016/04/20 10:00', .1, .3    ],\
        ['STPS_34013'  ,'2016/04/20 10:00','2016/05/04 08:00', .1, .3    ],\
        ['STPS_29020'  ,'2016/05/04 08:00','2016/09/10 04:00', .1, .3    ],\
        ['SMATCH_30026','2016/09/10 04:00','2016/11/02 12:00', .1, .3    ],\
        ['STPS_32019'  ,'2016/11/02 12:00','2017/01/01 00:00', .1, .3    ],\
        ##                           2017                             ##
        ['STPS_32019'  ,'2017/01/01 00:00','2017/02/14 12:00', .1, .3    ],\
        ['STPS_34013'  ,'2017/02/14 12:00','2017/03/02 12:00', .1, .3    ],\
        ['STPS_32019'  ,'2017/03/02 12:00','2017/04/11 12:00', .1, .3    ],\
        ['STPS_29020'  ,'2017/04/11 12:00','2017/07/10 12:00', .1, .3    ],\
        ['STPS_32019'  ,'2017/07/10 12:00','2018/01/01 00:00', .1, .3    ],\
        ##                           2018                             ##
        ['STPS_32019'  ,'2018/01/01 00:00','2018/06/28 12:00', .1, .3    ],\
        ['SMATCH_30026','2018/06/28 12:00','2018/07/13 08:00', .1, .3    ],\
        ['STPS_32019'  ,'2018/07/13 08:00','2018/10/09 12:00', .1, .3    ],\
        ['STPS_29020'  ,'2018/10/09 12:00','2019/01/01 00:00', .1, .3    ],\
        ##                           2019                             ##
        ['STPS_29020'  ,'2019/01/01 00:00','2020/01/01 00:00', .1, .3    ],\
        ##                           2020                             ##
        ['STPS_29020'  ,'2020/01/01 00:00','2021/01/01 00:00', .1, .3    ],\
        ##                           2021                             ##
        ['STPS_29020'  ,'2021/01/01 00:00','2021/02/12 10:00', .1, .3    ],\
        ['STPS_31018'  ,'2021/02/12 10:00','2022/01/01 00:00', .1, .3    ],\
        ##                           2022                             ##
        ['STPS_31018'  ,'2022/01/01 00:00','2023/01/01 00:00', .1, .3    ],\
        ##                           2023                             ##
        ['STPS_31018'  ,'2023/01/01 00:00','2024/01/01 00:00', .1, .3    ],\
        ##                           2024                             ##
        ['STPS_31018'  ,'2024/01/01 00:00','2024/04/11 10:30', .1, .3    ],\
        ['STPS_36005'  ,'2024/04/11 10:30','2025/01/01 00:00', .1, .3    ],\
)

periods['FLUO'] = ( \
        ['OTT_49226'   ,'2010/10/01 00:00','2012/04/23 08:30', 20, 1.],\
        ['OTT_62596'   ,'2012/04/23 08:30','2012/05/23 00:00', 20, 1.],\
        ['OTT_49226'   ,'2012/05/23 00:00','2013/02/12 11:00', 20, 1.],\
        ['OTT_62596'   ,'2013/02/12 11:00','2013/10/07 11:00', 20, 1.],\
        ['OTT_61086'   ,'2013/10/07 11:00','2013/10/21 12:00', 20, 1.],\
        ['OTT_62596'   ,'2013/10/21 12:00','2014/02/17 09:00', 20, 1.],\
        ['SMATCH_30008','2014/02/17 09:00','2014/03/04 06:00', 20, 1.],\
        ['OTT_62597'   ,'2014/03/04 06:00','2014/07/15 12:00', 20, 1.],\
        ['OTT_64693'   ,'2014/07/15 12:00','2014/07/29 11:00', 20, 1.],\
        ['OTT_62597'   ,'2014/07/29 11:00','2014/08/13 11:00', 20, 1.],\
        ['OTT_64693'   ,'2014/08/13 11:00','2014/08/26 11:00', 20, 1.],\
        ['OTT_62597'   ,'2014/08/26 11:00','2014/12/01 14:00', 20, 1.],\
        ['OTT_62596'   ,'2014/12/01 14:00','2015/02/18 22:00', 20, 1.],\
        ['OTT_62597'   ,'2015/02/18 22:00','2015/04/21 11:00', 20, 1.],\
        ['OTT_62596'   ,'2015/04/21 11:00','2015/05/20 11:00', 20, 1.],\
        ['OTT_62597'   ,'2015/05/20 11:00','2015/07/17 12:00', 20, 1.],\
        ['OTT_62596'   ,'2015/07/17 12:00','2015/08/18 12:00', 20, 1.],\
        ['OTT_62597'   ,'2015/08/18 12:00','2015/10/26 11:00', 20, 1.],\
        ['OTT_64693'   ,'2015/10/26 11:00','2016/01/01 00:00', 20, 1.],\
        ##                           2016                             ##
        ['OTT_64693'   ,'2016/01/01 00:00','2016/02/10 11:00', 20, 1.],\
        ['OTT_49226'   ,'2016/02/10 11:00','2016/04/20 11:00', 20, 1.],\
        ['OTT_64693'   ,'2016/04/20 11:00','2016/05/31 11:00', 20, 1.],\
        ['OTT_61086'   ,'2016/06/07 11:00','2016/11/15 11:00', 20, 1.],\
        ['SMATCH_30008','2016/11/15 21:00','2017/01/01 00:00', 20, 1.],\
        ##                           2017                             ##
        ['SMATCH_30008','2017/01/01 00:00','2017/01/13 12:00', 20, 1.],\
        ['OTT_61086'   ,'2017/01/13 12:00','2017/02/14 12:00', 20, 1.],\
        ['OTT_64693'   ,'2017/02/14 12:00','2017/02/24 20:00', 20, 1.],\
        ['SMATCH_30008','2017/02/24 20:00','2017/03/02 12:00', 20, 1.],\
        ['OTT_64693'   ,'2017/03/02 12:00','2017/04/11 10:00', 20, 1.],\
        ['OTT_67297'   ,'2017/04/11 10:00','2017/07/15 12:00', 20, 1.],\
        ['SMATCH_30008','2017/07/15 12:00','2017/07/24 12:00', 20, 1.],\
        ['OTT_67297'   ,'2017/07/24 12:00','2017/12/07 09:00', 20, 1.],\
        ['SMATCH_30026','2017/12/07 09:00','2018/01/01 00:00', 20, 1.],\
        ##                           2018                             ##
        ['SMATCH_30026','2018/01/01 00:00','2018/01/03 09:00', 20, 1.],\
        ['OTT_67297'   ,'2018/01/03 09:00','2018/02/28 12:00', 20, 1.],\
        ['SMATCH_30026','2018/02/28 12:00','2018/04/03 12:00', 20, 1.],\
        ['OTT_67297'   ,'2018/04/03 12:00','2018/04/19 12:00', 20, 1.],\
        ['FLNTU_4555'  ,'2018/04/19 12:00','2018/05/02 11:00', 20, 1.],\
        ['OTT_67297'   ,'2018/05/02 11:00','2018/09/10 11:00', 20, 1.],\
        ['FLNTU_5138'  ,'2018/09/10 11:00','2019/01/01 00:00', 10, 1.],\
        ##                           2019                             ##
        ['FLNTU_5138'  ,'2019/01/01 00:00','2020/01/01 00:00', 10, 1.],\
        ##                           2020                             ##
        ['FLNTU_5138'  ,'2020/01/01 00:00','2021/01/01 00:00', 10, 1.],\
        ##                           2021                             ##
        ['FLNTU_6602'  ,'2021/01/01 00:00','2022/01/01 00:00', 10, 1.],\
        ##                           2022                             ##
        ['FLNTU_6602'  ,'2022/01/01 00:00','2022/06/01 00:00', 10, 1.],\
        ['FLNTU_4771'  ,'2022/06/21 08:00','2023/01/01 00:00', 10, 1.],\
        ##                           2023                             ##
        ['FLNTU_4771'  ,'2023/01/01 00:00','2024/01/01 00:00', 10, 1.],\
        ##                           2024                             ##
        ['FLNTU_4771'  ,'2024/01/01 00:00','2024/01/24 09:30', 10, 1.],\
        ['FLNTU_8352'  ,'2024/01/24 09:30','2024/02/13 11:30', 10, 1.],\
        ['FLNTU_4771'  ,'2024/02/13 11:30','2025/01/01 00:00', 10, 1.],\
)    

periods['TURB'] = ( \
        ['FLNTU_4555'  ,'2018/04/19 12:00','2018/05/02 11:00', 20, 1.],\
        ['FLNTU_5138'  ,'2018/09/10 11:00','2019/01/01 00:00', 10, 1.],\
        ##                           2019                             ##
        ['FLNTU_5138'  ,'2019/01/01 00:00','2020/01/01 00:00', 10, 1.],\
        ##                           2020                             ##
        ['FLNTU_5138'  ,'2020/01/01 00:00','2021/01/01 00:00', 10, 1.],\
        ##                           2021                             ##
        ['FLNTU_6602'  ,'2021/01/01 00:00','2022/01/01 00:00', 10, 1.],\
        ##                           2022                             ##
        ['FLNTU_6602'  ,'2022/01/01 00:00','2022/06/01 00:00', 10, 1.],\
        ['FLNTU_4771'  ,'2022/06/21 08:00','2023/01/01 00:00', 10, 1.],\
        ##                           2023                             ##
        ['FLNTU_4771'  ,'2023/01/01 00:00','2024/01/01 00:00', 10, 1.],\
        ##                           2024                             ##
        ['FLNTU_4771'  ,'2024/01/01 00:00','2024/01/24 09:30', 10, 1.],\
        ['FLNTU_8352'  ,'2024/01/24 09:30','2024/02/13 11:30', 10, 1.],\
        ['FLNTU_4771'  ,'2024/02/13 11:30','2025/01/01 00:00', 10, 1.],\
)

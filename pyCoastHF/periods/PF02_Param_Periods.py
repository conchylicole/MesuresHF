#!/usr/bin/env python
# -*- coding: utf-8 -*-

periods = {}

periods['PROF'] = ( \
        ##                        2011                           ##
        ['SP2T_Pénerf'   ,'2011/01/01 00:00','2011/06/24 12:00', .1],\
        ['SMATCH_Penerf-Banastere' ,'2011/06/24 12:00','2012/01/01 00:00', .1],\
        ##                        2012                           ##
        ['SMATCH_Penerf-Banastere' ,'2012/01/01 00:00','2012/01/10 10:00', .1],\
        ['SP2T_Pénerf'   ,'2012/01/10 10:00','2012/01/26 12:00', .1],\
        ['STPS_Pénerf'   ,'2012/01/26 12:00','2013/01/01 00:00', .1],\
        ##                        2013                          ##
        ['STPS_Pénerf'   ,'2013/01/01 00:00','2013/01/16 00:00', .1],\
        ['SMATCH_Penerf-Banastere' ,'2013/01/16 00:00','2014/01/01 00:00', .1],\
        ##                        2014                          ##
        ['STPS_Pénerf'   ,'2014/01/01 00:00','2015/01/01 00:00', .1],\
        ##                        2015                          ##
        ['STPS_Pénerf'   ,'2015/01/01 00:00','2016/01/01 00:00', .1],\
        ##                        2016                          ##
        ['STPS_31020'    ,'2016/01/01 00:00','2016/01/12 10:00', .3],\
        ['STPS_32018'    ,'2016/01/12 13:00','2016/02/09 09:15', .3],\
        ['STPS_31020'    ,'2016/02/09 12:00','2016/03/10 08:45', .3],\
        ['STPS_32018'    ,'2016/03/10 12:45','2016/03/24 09:00', .3],\
        ['STPS_31020'    ,'2016/03/24 12:00','2016/04/06 07:15', .3],\
        ['STPS_32018'    ,'2016/04/06 10:15','2016/04/22 08:45', .3],\
        ['STPS_31020'    ,'2016/04/22 11:30','2016/05/09 10:15', .3],\
        ['STPS_32018'    ,'2016/05/09 13:00','2016/05/24 10:30', .3],\
        ['STPS_31020'    ,'2016/05/24 12:15','2016/06/08 10:45', .3],\
        ['STPS_32018'    ,'2016/06/08 13:30','2016/06/22 09:45', .3],\
        ['STPS_32018'    ,'2016/07/06 12:30','2016/07/21 09:15', .3],\
        ['STPS_31020'    ,'2016/07/21 12:00','2016/08/03 08:30', .3],\
        ['STPS_32018'    ,'2016/08/03 11:30','2016/08/23 12:00', .3],\
        ['STPS_31020'    ,'2016/08/23 14:30','2016/09/02 08:45', .3],\
        ['STPS_32018'    ,'2016/09/02 11:45','2016/09/20 10:30', .3],\
        ['STPS_31020'    ,'2016/09/20 13:45','2016/10/18 09:15', .3],\
        ['STPS_32018'    ,'2016/10/18 12:45','2016/11/16 09:00', .3],\
        ['STPS_31020'    ,'2016/11/16 12:30','2016/12/15 09:00', .3],\
        ['STPS_32018'    ,'2016/12/15 12:00','2017/01/27 09:00', .3],\
        ['STPS_31020'    ,'2017/01/27 10:30','2017/02/13 10:15', .3],\
        ['STPS_32018'    ,'2017/02/13 13:00','2017/03/15 10:00', .3],\
        ['STPS_31020'    ,'2017/03/15 13:15','2017/03/28 08:15', .3],\
        ['STPS_32018'    ,'2017/03/28 11:30','2017/04/12 09:15', .3],\
        ['STPS_31020'    ,'2017/04/12 12:15','2017/04/26 08:00', .3],\
        ['STPS_32018'    ,'2017/04/26 11:00','2017/05/11 09:15', .3],\
        ['STPS_31020'    ,'2017/05/11 11:15','2017/05/30 12:30', .3],\
        ['STPS_32018'    ,'2017/05/30 14:15','2017/06/09 09:00', .3],\
        ['STPS_31020'    ,'2017/06/09 10:45','2017/06/27 11:00', .3],\
        ['STPS_32018'    ,'2017/06/27 13:30','2017/07/13 12:00', .3],\
        ['STPS_31020'    ,'2017/07/13 13:15','2017/07/26 10:30', .3],\
        ['STPS_32018'    ,'2017/07/26 13:30','2017/08/08 09:15', .3],\
        ['STPS_31020'    ,'2017/08/08 11:30','2017/08/23 09:15', .3],\
        ['STPS_32018'    ,'2017/08/23 12:30','2017/09/07 09:00', .3],\
        ['STPS_31020'    ,'2017/09/07 11:45','2017/09/19 07:30', .3],\
        ['STPS_32018'    ,'2017/09/19 10:30','2017/10/09 10:45', .3],\
        ['STPS_31020'    ,'2017/10/09 13:30','2017/11/03 07:45', .3],\
        ['STPS_32018'    ,'2017/11/03 10:15','2017/12/06 10:15', .3],\
        ['STPS_31020'    ,'2017/12/06 13:30','2018/02/01 08:30', .3],\
        ['STPS_32018'    ,'2018/02/01 12:15','2018/03/01 08:00', .3],\
        ['STPS_31020'    ,'2018/03/01 11:00','2018/03/20 10:00', .3],\
        ['STPS_32018'    ,'2018/03/20 13:15','2018/04/03 11:00', .3],\
        ['STPS_32020'    ,'2018/04/03 14:30','2018/04/17 10:00', .3],\
        ['STPS_36003'    ,'2018/04/17 14:45','2018/05/02 12:00', .3],\
        ['STPS_32020'    ,'2018/05/02 14:00','2018/05/15 08:45', .3],\
        ['STPS_36003'    ,'2018/05/15 13:30','2018/05/29 10:00', .3],\
        ['STPS_32020'    ,'2018/05/29 12:30','2018/06/13 08:15', .3],\
        ['STPS_36003'    ,'2018/06/13 13:00','2018/06/27 09:45', .3],\
        ['STPS_32020'    ,'2018/06/27 12:00','2018/07/17 12:15', .3],\
        ['STPS_36003'    ,'2018/07/17 17:00','2018/07/27 10:00', .3],\
        ['STPS_32020'    ,'2018/07/27 12:15','2018/08/13 10:15', .3],\
        ['STPS_36003'    ,'2018/08/13 15:15','2018/08/29 11:45', .3],\
        ['STPS_32020'    ,'2018/08/29 14:15','2018/09/12 10:30', .3],\
        ['STPS_36003'    ,'2018/09/12 15:30','2018/09/24 09:45', .3],\
        ['STPS_32020'    ,'2018/09/24 12:00','2018/10/08 08:15', .3],\
        ['STPS_36003'    ,'2018/10/08 13:00','2018/11/09 11:00', .3],\
        ['STPS_32020'    ,'2018/11/09 13:15','2018/12/04 07:30', .3],\
        ['STPS_36003'    ,'2018/12/04 11:00','2019/01/23 11:30', .3],\
        ['STPS_32020'    ,'2019/01/23 14:30','2019/02/18 08:15', .3],\
        ['STPS_36003'    ,'2019/02/18 12:45','2019/03/05 10:00', .3],\
        ['STPS_32020'    ,'2019/03/05 11:30','2019/03/20 08:15', .3],\
        ['STPS_36003'    ,'2019/03/20 11:30','2019/04/03 07:30', .3],\
        ['STPS_32020'    ,'2019/04/03 10:30','2019/04/18 07:15', .3],\
        ['STPS_36003'    ,'2019/04/18 11:00','2019/05/07 09:30', .3],\
        ['STPS_32020'    ,'2019/05/07 12:45','2019/05/22 11:00', .3],\
        ['STPS_36003'    ,'2019/05/22 13:45','2019/06/03 08:00', .3],\
        ['STPS_32020'    ,'2019/06/03 11:15','2019/06/17 08:30', .3],\
        ['STPS_36003'    ,'2019/06/17 11:45','2019/07/02 07:30', .3],\
        ['STPS_32020'    ,'2019/07/16 11:00','2019/08/19 11:00', .3],\
        ['STPS_36003'    ,'2019/08/19 13:45','2019/09/18 10:30', .3],\
        ['STPS_32020'    ,'2019/09/18 13:30','2019/10/31 10:15', .3],\
        ['STPS_36003'    ,'2019/10/31 13:45','2019/11/27 08:30', .3],\
        ['STPS_32020'    ,'2019/11/27 11:30','2020/01/01 00:00', .3],\
        ##                        2020                          ##
        ['STPS_32020'    ,'2020/01/01 00:00','2020/01/13 10:45', .3],\
        ['STPS_36003'    ,'2020/01/13 13:30','2020/02/11 09:30', .3],\
        ['STPS_32020'    ,'2020/02/11 13:15','2020/05/11 11:30', .3],\
        ['STPS_36003'    ,'2020/05/11 14:30','2020/05/25 10:00', .3],\
        ['STPS_32020'    ,'2020/05/25 12:45','2020/06/08 10:30', .3],\
        ['STPS_36003'    ,'2020/06/08 13:15','2020/06/24 10:30', .3],\
        ['STPS_32020'    ,'2020/06/24 13:15','2020/07/07 10:15', .3],\
        ['STPS_36003'    ,'2020/08/04 12:15','2020/08/19 08:15', .3],\
        ['STPS_32020'    ,'2020/08/19 11:15','2020/08/31 08:15', .3],\
        ['STPS_36003'    ,'2020/08/31 10:15','2020/09/16 07:15', .3],\
        ['STPS_32020'    ,'2020/09/16 10:00','2020/10/14 07:15', .3],\
        ['STPS_36003'    ,'2020/10/14 09:00','2020/11/17 09:15', .3],\
        ['STPS_32020'    ,'2020/11/17 12:30','2020/12/15 08:45', .3],\
        ['STPS_36003'    ,'2020/12/15 09:00','2021/01/01 00:00', .3],\
        ##                        2021                          ##
        ['STPS_36003'    ,'2021/01/01 00:15','2021/01/11 06:30', .3],\
        ['STPS_32020'    ,'2021/01/11 09:15','2021/02/16 12:30', .3],\
        ['STPS_36003'    ,'2021/02/16 14:00','2021/03/30 09:00', .3],\
        ['STPS_31020'    ,'2021/03/30 14:00','2021/04/12 09:30', .3],\
        ['STPS_36003'    ,'2021/04/12 12:00','2021/04/26 07:15', .3],\
        ['STPS_31020'    ,'2021/04/26 12:15','2021/05/10 09:00', .3],\
        ['STPS_36003'    ,'2021/05/10 10:45','2021/05/25 06:45', .3],\
        ['STPS_31020'    ,'2021/05/25 11:45','2021/06/10 09:30', .3],\
        ['STPS_36003'    ,'2021/06/10 11:15','2021/06/24 07:30', .3],\
        ['STPS_31020'    ,'2021/06/24 11:00','2021/07/26 09:45', .3],\
        ['STPS_36003'    ,'2021/07/26 13:15','2021/08/10 09:30', .3],\
        ['STPS_31020'    ,'2021/08/10 12:30','2021/08/26 10:45', .3],\
        ['STPS_32020'    ,'2021/08/26 14:00','2021/09/10 10:15', .3],\
        ['STPS_31020'    ,'2021/09/10 13:45','2021/09/20 07:45', .3],\
        ['STPS_32020'    ,'2021/09/20 11:15','2021/10/18 07:15', .3],\
        ['STPS_31020'    ,'2021/10/18 09:30','2021/11/09 11:45', .3],\
        ['STPS_32020'    ,'2021/11/09 14:30','2021/12/03 07:00', .3],\
        ['STPS_31020'    ,'2021/12/03 10:15','2022/01/01 00:00', .3],\
        ##                        2022                          ##
        ['STPS_31020'    ,'2022/01/01 00:00','2022/01/18 09:15', .3],\
        ['STPS_32020'    ,'2022/01/18 11:30','2022/02/21 11:30', .3],\
        ['STPS_31020'    ,'2022/02/21 14:30','2022/03/17 08:00', .3],\
        ['STPS_32020'    ,'2022/03/17 11:15','2022/03/31 07:30', .3],\
        ['STPS_31020'    ,'2022/03/31 11:15','2022/04/19 09:45', .3],\
        ['STPS_32020'    ,'2022/04/19 13:15','2022/04/28 06:45', .3],\
        ['STPS_31020'    ,'2022/04/28 09:45','2022/05/16 08:00', .3],\
        ['STPS_32020'    ,'2022/05/16 11:30','2022/05/31 09:00', .3],\
        ['STPS_31020'    ,'2022/05/31 11:30','2022/06/14 07:30', .3],\
        ['STPS_32020'    ,'2022/06/14 11:00','2022/06/29 09:00', .3],\
        ['STPS_31020'    ,'2022/06/29 11:00','2022/07/12 06:45', .3],\
        ['STPS_32020'    ,'2022/07/12 09:30','2022/07/29 09:00', .3],\
        ['STPS_31020'    ,'2022/07/29 11:15','2022/08/16 11:15', .3],\
        ['STPS_36003'    ,'2022/08/16 14:45','2022/08/26 08:00', .3],\
        ['STPS_31020'    ,'2022/08/26 10:30','2022/09/09 07:15', .3],\
        ['STPS_36003'    ,'2022/09/09 10:45','2022/09/26 08:15', .3],\
        ['STPS_31020'    ,'2022/09/26 11:15','2022/10/07 06:30', .3],\
        ['STPS_36003'    ,'2022/10/07 09:15','2022/12/07 08:00', .3],\
        ['STPS_31020'    ,'2022/12/07 10:15','2023/01/01 00:00', .3],\
        ##                        2023                          ##
        ['STPS_31020'    ,'2023/01/01 00:00','2023/01/26 11:45', .3],\
        ['STPS_32020'    ,'2023/01/26 14:30','2023/02/21 08:45', .3],\
        ['STPS_36003'    ,'2023/02/21 12:30','2023/03/10 10:30', .3],\
        ['STPS_32020'    ,'2023/03/10 12:30','2023/04/06 08:30', .3],\
        ['STPS_36003'    ,'2023/04/06 11:30','2023/04/18 07:15', .3],\
        ['STPS_32020'    ,'2023/04/18 09:28','2023/05/09 11:15', .3],\
        ['STPS_36003'    ,'2023/05/09 13:00','2023/05/17 07:00', .3],\
        ['STPS_31020'    ,'2023/05/17 09:00','2023/06/05 09:15', .3],\
        ['STPS_36003'    ,'2023/06/05 11:45','2023/06/19 10:00', .3],\
        ['STPS_32020'    ,'2023/06/19 10:00','2023/07/04 09:00', .3],\
        ['STPS_36003'    ,'2023/07/04 11:39','2023/07/17 09:30', .3],\
        ['STPS_32020'    ,'2023/07/17 09:30','2023/07/31 08:15', .3],\
        ['STPS_36003'    ,'2023/07/31 08:51','2023/08/17 09:45', .3],\
        ['STPS_32020'    ,'2023/08/17 10:45','2023/08/29 07:15', .3],\
        ['STPS_36003'    ,'2023/08/29 09:00','2023/09/18 10:45', .3],\
        ['STPS_32020'    ,'2023/09/18 11:45','2023/10/02 10:00', .3],\
        ['STPS_36003'    ,'2023/10/02 13:10','2023/10/17 10:15', .3],\
        ['STPS_32020'    ,'2023/10/17 11:29','2023/11/16 10:30', .3],\
        ['STPS_36003'    ,'2023/11/16 13:00','2023/12/13 09:00', .3],\
        ['STPS_32020'    ,'2023/12/13 10:45','2024/01/01 00:00', .3],\
        ##                        2024                          ##

)


periods['TEMP'] = ( \
        ##                        2011                           ##
        ['SP2T_Pénerf'   ,'2011/01/01 00:00','2011/06/24 12:00', .1],\
        ['SMATCH_Penerf-Banastere' ,'2011/06/24 12:00','2012/01/01 00:00', .1],\
        ##                        2012                           ##
        ['SMATCH_Penerf-Banastere' ,'2012/01/01 00:00','2012/01/10 10:00', .1],\
        ['SP2T_Pénerf'   ,'2012/01/10 10:00','2012/01/26 12:00', .1],\
        ['STPS_Pénerf'   ,'2012/01/26 12:00','2013/01/01 00:00', .1],\
        ##                        2013                          ##
        ['STPS_Pénerf'   ,'2013/01/01 00:00','2013/01/16 00:00', .1],\
        ['SMATCH_Penerf-Banastere' ,'2013/01/16 00:00','2014/01/01 00:00', .1],\
        ##                        2014                          ##
        ['STPS_Pénerf'   ,'2014/01/01 00:00','2015/01/01 00:00', .1],\
        ##                        2015                          ##
        ['STPS_Pénerf'   ,'2015/01/01 00:00','2016/01/01 00:00', .1],\

        ##                        2016                          ##
        ['STPS_31020'    ,'2016/01/01 00:00','2016/01/12 10:00', .3],\
        ['STPS_32018'    ,'2016/01/12 13:00','2016/02/09 09:15', .3],\
        ['STPS_31020'    ,'2016/02/09 12:00','2016/03/10 08:45', .3],\
        ['STPS_32018'    ,'2016/03/10 12:45','2016/03/24 09:00', .3],\
        ['STPS_31020'    ,'2016/03/24 12:00','2016/04/06 07:15', .3],\
        ['STPS_32018'    ,'2016/04/06 10:15','2016/04/22 08:45', .3],\
        ['STPS_31020'    ,'2016/04/22 11:30','2016/05/09 10:15', .3],\
        ['STPS_32018'    ,'2016/05/09 13:00','2016/05/24 10:30', .3],\
        ['STPS_31020'    ,'2016/05/24 12:15','2016/06/08 10:45', .3],\
        ['STPS_32018'    ,'2016/06/08 13:30','2016/06/22 09:45', .3],\
        ['STPS_32018'    ,'2016/07/06 12:30','2016/07/21 09:15', .3],\
        ['STPS_31020'    ,'2016/07/21 12:00','2016/08/03 08:30', .3],\
        ['STPS_32018'    ,'2016/08/03 11:30','2016/08/23 12:00', .3],\
        ['STPS_31020'    ,'2016/08/23 14:30','2016/09/02 08:45', .3],\
        ['STPS_32018'    ,'2016/09/02 11:45','2016/09/20 10:30', .3],\
        ['STPS_31020'    ,'2016/09/20 13:45','2016/10/18 09:15', .3],\
        ['STPS_32018'    ,'2016/10/18 12:45','2016/11/16 09:00', .3],\
        ['STPS_31020'    ,'2016/11/16 12:30','2016/12/15 09:00', .3],\
        ['STPS_32018'    ,'2016/12/15 12:00','2017/01/27 09:00', .3],\
        ['STPS_31020'    ,'2017/01/27 10:30','2017/02/13 10:15', .3],\
        ['STPS_32018'    ,'2017/02/13 13:00','2017/03/15 10:00', .3],\
        ['STPS_31020'    ,'2017/03/15 13:15','2017/03/28 08:15', .3],\
        ['STPS_32018'    ,'2017/03/28 11:30','2017/04/12 09:15', .3],\
        ['STPS_31020'    ,'2017/04/12 12:15','2017/04/26 08:00', .3],\
        ['STPS_32018'    ,'2017/04/26 11:00','2017/05/11 09:15', .3],\
        ['STPS_31020'    ,'2017/05/11 11:15','2017/05/30 12:30', .3],\
        ['STPS_32018'    ,'2017/05/30 14:15','2017/06/09 09:00', .3],\
        ['STPS_31020'    ,'2017/06/09 10:45','2017/06/27 11:00', .3],\
        ['STPS_32018'    ,'2017/06/27 13:30','2017/07/13 12:00', .3],\
        ['STPS_31020'    ,'2017/07/13 13:15','2017/07/26 10:30', .3],\
        ['STPS_32018'    ,'2017/07/26 13:30','2017/08/08 09:15', .3],\
        ['STPS_31020'    ,'2017/08/08 11:30','2017/08/23 09:15', .3],\
        ['STPS_32018'    ,'2017/08/23 12:30','2017/09/07 09:00', .3],\
        ['STPS_31020'    ,'2017/09/07 11:45','2017/09/19 07:30', .3],\
        ['STPS_32018'    ,'2017/09/19 10:30','2017/10/09 10:45', .3],\
        ['STPS_31020'    ,'2017/10/09 13:30','2017/11/03 07:45', .3],\
        ['STPS_32018'    ,'2017/11/03 10:15','2017/12/06 10:15', .3],\
        ['STPS_31020'    ,'2017/12/06 13:30','2018/02/01 08:30', .3],\
        ['STPS_32018'    ,'2018/02/01 12:15','2018/03/01 08:00', .3],\
        ['STPS_31020'    ,'2018/03/01 11:00','2018/03/20 10:00', .3],\
        ['STPS_32018'    ,'2018/03/20 13:15','2018/04/03 11:00', .3],\
        ['STPS_32020'    ,'2018/04/03 14:30','2018/04/17 10:00', .3],\
        ['STPS_36003'    ,'2018/04/17 14:45','2018/05/02 12:00', .3],\
        ['STPS_32020'    ,'2018/05/02 14:00','2018/05/15 08:45', .3],\
        ['STPS_36003'    ,'2018/05/15 13:30','2018/05/29 10:00', .3],\
        ['STPS_32020'    ,'2018/05/29 12:30','2018/06/13 08:15', .3],\
        ['STPS_36003'    ,'2018/06/13 13:00','2018/06/27 09:45', .3],\
        ['STPS_32020'    ,'2018/06/27 12:00','2018/07/17 12:15', .3],\
        ['STPS_36003'    ,'2018/07/17 17:00','2018/07/27 10:00', .3],\
        ['STPS_32020'    ,'2018/07/27 12:15','2018/08/13 10:15', .3],\
        ['STPS_36003'    ,'2018/08/13 15:15','2018/08/29 11:45', .3],\
        ['STPS_32020'    ,'2018/08/29 14:15','2018/09/12 10:30', .3],\
        ['STPS_36003'    ,'2018/09/12 15:30','2018/09/24 09:45', .3],\
        ['STPS_32020'    ,'2018/09/24 12:00','2018/10/08 08:15', .3],\
        ['STPS_36003'    ,'2018/10/08 13:00','2018/11/09 11:00', .3],\
        ['STPS_32020'    ,'2018/11/09 13:15','2018/12/04 07:30', .3],\
        ['STPS_36003'    ,'2018/12/04 11:00','2019/01/23 11:30', .3],\
        ['STPS_32020'    ,'2019/01/23 14:30','2019/02/18 08:15', .3],\
        ['STPS_36003'    ,'2019/02/18 12:45','2019/03/05 10:00', .3],\
        ['STPS_32020'    ,'2019/03/05 11:30','2019/03/20 08:15', .3],\
        ['STPS_36003'    ,'2019/03/20 11:30','2019/04/03 07:30', .3],\
        ['STPS_32020'    ,'2019/04/03 10:30','2019/04/18 07:15', .3],\
        ['STPS_36003'    ,'2019/04/18 11:00','2019/05/07 09:30', .3],\
        ['STPS_32020'    ,'2019/05/07 12:45','2019/05/22 11:00', .3],\
        ['STPS_36003'    ,'2019/05/22 13:45','2019/06/03 08:00', .3],\
        ['STPS_32020'    ,'2019/06/03 11:15','2019/06/17 08:30', .3],\
        ['STPS_36003'    ,'2019/06/17 11:45','2019/07/02 07:30', .3],\
        ['STPS_32020'    ,'2019/07/16 11:00','2019/08/19 11:00', .3],\
        ['STPS_36003'    ,'2019/08/19 13:45','2019/09/18 10:30', .3],\
        ['STPS_32020'    ,'2019/09/18 13:30','2019/10/31 10:15', .3],\
        ['STPS_36003'    ,'2019/10/31 13:45','2019/11/27 08:30', .3],\
        ['STPS_32020'    ,'2019/11/27 11:30','2020/01/01 00:00', .3],\
        ##                        2020                          ##
        ['STPS_32020'    ,'2020/01/01 00:00','2020/01/13 10:45', .3],\
        ['STPS_36003'    ,'2020/01/13 13:30','2020/02/11 09:30', .3],\
        ['STPS_32020'    ,'2020/02/11 13:15','2020/05/11 11:30', .3],\
        ['STPS_36003'    ,'2020/05/11 14:30','2020/05/25 10:00', .3],\
        ['STPS_32020'    ,'2020/05/25 12:45','2020/06/08 10:30', .3],\
        ['STPS_36003'    ,'2020/06/08 13:15','2020/06/24 10:30', .3],\
        ['STPS_32020'    ,'2020/06/24 13:15','2020/07/07 10:15', .3],\
        ['STPS_36003'    ,'2020/08/04 12:15','2020/08/19 08:15', .3],\
        ['STPS_32020'    ,'2020/08/19 11:15','2020/08/31 08:15', .3],\
        ['STPS_36003'    ,'2020/08/31 10:15','2020/09/16 07:15', .3],\
        ['STPS_32020'    ,'2020/09/16 10:00','2020/10/14 07:15', .3],\
        ['STPS_36003'    ,'2020/10/14 09:00','2020/11/17 09:15', .3],\
        ['STPS_32020'    ,'2020/11/17 12:30','2020/12/15 08:45', .3],\
        ['STPS_36003'    ,'2020/12/15 09:00','2021/01/01 00:00', .3],\
        ##                        2021                          ##
        ['STPS_36003'    ,'2021/01/01 00:15','2021/01/11 06:30', .3],\
        ['STPS_32020'    ,'2021/01/11 09:15','2021/02/16 12:30', .3],\
        ['STPS_36003'    ,'2021/02/16 14:00','2021/03/30 09:00', .3],\
        ['STPS_31020'    ,'2021/03/30 14:00','2021/04/12 09:30', .3],\
        ['STPS_36003'    ,'2021/04/12 12:00','2021/04/26 07:15', .3],\
        ['STPS_31020'    ,'2021/04/26 12:15','2021/05/10 09:00', .3],\
        ['STPS_36003'    ,'2021/05/10 10:45','2021/05/25 06:45', .3],\
        ['STPS_31020'    ,'2021/05/25 11:45','2021/06/10 09:30', .3],\
        ['STPS_36003'    ,'2021/06/10 11:15','2021/06/24 07:30', .3],\
        ['STPS_31020'    ,'2021/06/24 11:00','2021/07/26 09:45', .3],\
        ['STPS_36003'    ,'2021/07/26 13:15','2021/08/10 09:30', .3],\
        ['STPS_31020'    ,'2021/08/10 12:30','2021/08/26 10:45', .3],\
        ['STPS_32020'    ,'2021/08/26 14:00','2021/09/10 10:15', .3],\
        ['STPS_31020'    ,'2021/09/10 13:45','2021/09/20 07:45', .3],\
        ['STPS_32020'    ,'2021/09/20 11:15','2021/10/18 07:15', .3],\
        ['STPS_31020'    ,'2021/10/18 09:30','2021/11/09 11:45', .3],\
        ['STPS_32020'    ,'2021/11/09 14:30','2021/12/03 07:00', .3],\
        ['STPS_31020'    ,'2021/12/03 10:15','2022/01/01 00:00', .3],\
        ##                        2022                          ##
        ['STPS_31020'    ,'2022/01/01 00:00','2022/01/18 09:15', .3],\
        ['STPS_32020'    ,'2022/01/18 11:30','2022/02/21 11:30', .3],\
        ['STPS_31020'    ,'2022/02/21 14:30','2022/03/17 08:00', .3],\
        ['STPS_32020'    ,'2022/03/17 11:15','2022/03/31 07:30', .3],\
        ['STPS_31020'    ,'2022/03/31 11:15','2022/04/19 09:45', .3],\
        ['STPS_32020'    ,'2022/04/19 13:15','2022/04/28 06:45', .3],\
        ['STPS_31020'    ,'2022/04/28 09:45','2022/05/16 08:00', .3],\
        ['STPS_32020'    ,'2022/05/16 11:30','2022/05/31 09:00', .3],\
        ['STPS_31020'    ,'2022/05/31 11:30','2022/06/14 07:30', .3],\
        ['STPS_32020'    ,'2022/06/14 11:00','2022/06/29 09:00', .3],\
        ['STPS_31020'    ,'2022/06/29 11:00','2022/07/12 06:45', .3],\
        ['STPS_32020'    ,'2022/07/12 09:30','2022/07/29 09:00', .3],\
        ['STPS_31020'    ,'2022/07/29 11:15','2022/08/16 11:15', .3],\
        ['STPS_36003'    ,'2022/08/16 14:45','2022/08/26 08:00', .3],\
        ['STPS_31020'    ,'2022/08/26 10:30','2022/09/09 07:15', .3],\
        ['STPS_36003'    ,'2022/09/09 10:45','2022/09/26 08:15', .3],\
        ['STPS_31020'    ,'2022/09/26 11:15','2022/10/07 06:30', .3],\
        ['STPS_36003'    ,'2022/10/07 09:15','2022/12/07 08:00', .3],\
        ['STPS_31020'    ,'2022/12/07 10:15','2023/01/01 00:00', .3],\
        ##                        2023                          ##
        ['STPS_31020'    ,'2023/01/01 00:00','2023/01/26 11:45', .3],\
        ['STPS_32020'    ,'2023/01/26 14:30','2023/02/21 08:45', .3],\
        ['STPS_36003'    ,'2023/02/21 12:30','2023/03/10 10:30', .3],\
        ['STPS_32020'    ,'2023/03/10 12:30','2023/04/06 08:30', .3],\
        ['STPS_36003'    ,'2023/04/06 11:30','2023/04/18 07:15', .3],\
        ['STPS_32020'    ,'2023/04/18 09:28','2023/05/09 11:15', .3],\
        ['STPS_36003'    ,'2023/05/09 13:00','2023/05/17 07:00', .3],\
        ['STPS_31020'    ,'2023/05/17 09:00','2023/06/05 09:15', .3],\
        ['STPS_36003'    ,'2023/06/05 11:45','2023/06/19 10:00', .3],\
        ['STPS_32020'    ,'2023/06/19 10:00','2023/07/04 09:00', .3],\
        ['STPS_36003'    ,'2023/07/04 11:39','2023/07/17 09:30', .3],\
        ['STPS_32020'    ,'2023/07/17 09:30','2023/07/31 08:15', .3],\
        ['STPS_36003'    ,'2023/07/31 08:51','2023/08/17 09:45', .3],\
        ['STPS_32020'    ,'2023/08/17 10:45','2023/08/29 07:15', .3],\
        ['STPS_36003'    ,'2023/08/29 09:00','2023/09/18 10:45', .3],\
        ['STPS_32020'    ,'2023/09/18 11:45','2023/10/02 10:00', .3],\
        ['STPS_36003'    ,'2023/10/02 13:10','2023/10/17 10:15', .3],\
        ['STPS_32020'    ,'2023/10/17 11:29','2023/11/16 10:30', .3],\
        ['STPS_36003'    ,'2023/11/16 13:00','2023/12/13 09:00', .3],\
        ['STPS_32020'    ,'2023/12/13 10:45','2024/01/01 00:00', .3],\
        ##                        2024                          ##


)



periods['PSAL'] = ( \
        ##                        2011                           ##
        ['SMATCH_Penerf-Banastere' ,'2011/01/01 00:00','2012/01/01 00:00', .1],\
        ##                        2012                           ##
        ['SMATCH_Penerf-Banastere' ,'2012/01/01 00:00','2012/01/15 00:00', .1],\
        ['STPS_Pénerf'   ,'2012/01/15 00:00','2013/01/01 00:00', .1],\
        ##                        2013                          ##
        ['STPS_Pénerf'   ,'2013/01/01 00:00','2013/01/16 00:00', .1],\
        ['SMATCH_Penerf-Banastere' ,'2013/01/16 00:00','2014/01/01 00:00', .1],\
        ##                        2014                          ##
        ['STPS_Pénerf'   ,'2014/01/01 00:00','2015/01/01 00:00', .1],\
        ##                        2015                          ##
        ['STPS_Pénerf'   ,'2015/01/01 00:00','2016/01/01 00:00', .1],\
        ##                        2016                          ##
        ['STPS_31020'    ,'2016/01/01 00:00','2016/01/12 10:00', .3],\
        ['STPS_32018'    ,'2016/01/12 13:00','2016/02/09 09:15', .3],\
        ['STPS_31020'    ,'2016/02/09 12:00','2016/03/10 08:45', .3],\
        ['STPS_32018'    ,'2016/03/10 12:45','2016/03/24 09:00', .3],\
        ['STPS_31020'    ,'2016/03/24 12:00','2016/04/06 07:15', .3],\
        ['STPS_32018'    ,'2016/04/06 10:15','2016/04/22 08:45', .3],\
        ['STPS_31020'    ,'2016/04/22 11:30','2016/05/09 10:15', .3],\
        ['STPS_32018'    ,'2016/05/09 13:00','2016/05/24 10:30', .3],\
        ['STPS_31020'    ,'2016/05/24 12:15','2016/06/08 10:45', .3],\
        ['STPS_32018'    ,'2016/06/08 13:30','2016/06/22 09:45', .3],\
        ['STPS_32018'    ,'2016/07/06 12:30','2016/07/21 09:15', .3],\
        ['STPS_31020'    ,'2016/07/21 12:00','2016/08/03 08:30', .3],\
        ['STPS_32018'    ,'2016/08/03 11:30','2016/08/23 12:00', .3],\
        ['STPS_31020'    ,'2016/08/23 14:30','2016/09/02 08:45', .3],\
        ['STPS_32018'    ,'2016/09/02 11:45','2016/09/20 10:30', .3],\
        ['STPS_31020'    ,'2016/09/20 13:45','2016/10/18 09:15', .3],\
        ['STPS_32018'    ,'2016/10/18 12:45','2016/11/16 09:00', .3],\
        ['STPS_31020'    ,'2016/11/16 12:30','2016/12/15 09:00', .3],\
        ['STPS_32018'    ,'2016/12/15 12:00','2017/01/27 09:00', .3],\
        ['STPS_31020'    ,'2017/01/27 10:30','2017/02/13 10:15', .3],\
        ['STPS_32018'    ,'2017/02/13 13:00','2017/03/15 10:00', .3],\
        ['STPS_31020'    ,'2017/03/15 13:15','2017/03/28 08:15', .3],\
        ['STPS_32018'    ,'2017/03/28 11:30','2017/04/12 09:15', .3],\
        ['STPS_31020'    ,'2017/04/12 12:15','2017/04/26 08:00', .3],\
        ['STPS_32018'    ,'2017/04/26 11:00','2017/05/11 09:15', .3],\
        ['STPS_31020'    ,'2017/05/11 11:15','2017/05/30 12:30', .3],\
        ['STPS_32018'    ,'2017/05/30 14:15','2017/06/09 09:00', .3],\
        ['STPS_31020'    ,'2017/06/09 10:45','2017/06/27 11:00', .3],\
        ['STPS_32018'    ,'2017/06/27 13:30','2017/07/13 12:00', .3],\
        ['STPS_31020'    ,'2017/07/13 13:15','2017/07/26 10:30', .3],\
        ['STPS_32018'    ,'2017/07/26 13:30','2017/08/08 09:15', .3],\
        ['STPS_31020'    ,'2017/08/08 11:30','2017/08/23 09:15', .3],\
        ['STPS_32018'    ,'2017/08/23 12:30','2017/09/07 09:00', .3],\
        ['STPS_31020'    ,'2017/09/07 11:45','2017/09/19 07:30', .3],\
        ['STPS_32018'    ,'2017/09/19 10:30','2017/10/09 10:45', .3],\
        ['STPS_31020'    ,'2017/10/09 13:30','2017/11/03 07:45', .3],\
        ['STPS_32018'    ,'2017/11/03 10:15','2017/12/06 10:15', .3],\
        ['STPS_31020'    ,'2017/12/06 13:30','2018/02/01 08:30', .3],\
        ['STPS_32018'    ,'2018/02/01 12:15','2018/03/01 08:00', .3],\
        ['STPS_31020'    ,'2018/03/01 11:00','2018/03/20 10:00', .3],\
        ['STPS_32018'    ,'2018/03/20 13:15','2018/04/03 11:00', .3],\
        ['STPS_32020'    ,'2018/04/03 14:30','2018/04/17 10:00', .3],\
        ['STPS_36003'    ,'2018/04/17 14:45','2018/05/02 12:00', .3],\
        ['STPS_32020'    ,'2018/05/02 14:00','2018/05/15 08:45', .3],\
        ['STPS_36003'    ,'2018/05/15 13:30','2018/05/29 10:00', .3],\
        ['STPS_32020'    ,'2018/05/29 12:30','2018/06/13 08:15', .3],\
        ['STPS_36003'    ,'2018/06/13 13:00','2018/06/27 09:45', .3],\
        ['STPS_32020'    ,'2018/06/27 12:00','2018/07/17 12:15', .3],\
        ['STPS_36003'    ,'2018/07/17 17:00','2018/07/27 10:00', .3],\
        ['STPS_32020'    ,'2018/07/27 12:15','2018/08/13 10:15', .3],\
        ['STPS_36003'    ,'2018/08/13 15:15','2018/08/29 11:45', .3],\
        ['STPS_32020'    ,'2018/08/29 14:15','2018/09/12 10:30', .3],\
        ['STPS_36003'    ,'2018/09/12 15:30','2018/09/24 09:45', .3],\
        ['STPS_32020'    ,'2018/09/24 12:00','2018/10/08 08:15', .3],\
        ['STPS_36003'    ,'2018/10/08 13:00','2018/11/09 11:00', .3],\
        ['STPS_32020'    ,'2018/11/09 13:15','2018/12/04 07:30', .3],\
        ['STPS_36003'    ,'2018/12/04 11:00','2019/01/23 11:30', .3],\
        ['STPS_32020'    ,'2019/01/23 14:30','2019/02/18 08:15', .3],\
        ['STPS_36003'    ,'2019/02/18 12:45','2019/03/05 10:00', .3],\
        ['STPS_32020'    ,'2019/03/05 11:30','2019/03/20 08:15', .3],\
        ['STPS_36003'    ,'2019/03/20 11:30','2019/04/03 07:30', .3],\
        ['STPS_32020'    ,'2019/04/03 10:30','2019/04/18 07:15', .3],\
        ['STPS_36003'    ,'2019/04/18 11:00','2019/05/07 09:30', .3],\
        ['STPS_32020'    ,'2019/05/07 12:45','2019/05/22 11:00', .3],\
        ['STPS_36003'    ,'2019/05/22 13:45','2019/06/03 08:00', .3],\
        ['STPS_32020'    ,'2019/06/03 11:15','2019/06/17 08:30', .3],\
        ['STPS_36003'    ,'2019/06/17 11:45','2019/07/02 07:30', .3],\
        ['STPS_32020'    ,'2019/07/16 11:00','2019/08/19 11:00', .3],\
        ['STPS_36003'    ,'2019/08/19 13:45','2019/09/18 10:30', .3],\
        ['STPS_32020'    ,'2019/09/18 13:30','2019/10/31 10:15', .3],\
        ['STPS_36003'    ,'2019/10/31 13:45','2019/11/27 08:30', .3],\
        ['STPS_32020'    ,'2019/11/27 11:30','2020/01/01 00:00', .3],\
        ##                        2020                          ##
        ['STPS_32020'    ,'2020/01/01 00:00','2020/01/13 10:45', .3],\
        ['STPS_36003'    ,'2020/01/13 13:30','2020/02/11 09:30', .3],\
        ['STPS_32020'    ,'2020/02/11 13:15','2020/05/11 11:30', .3],\
        ['STPS_36003'    ,'2020/05/11 14:30','2020/05/25 10:00', .3],\
        ['STPS_32020'    ,'2020/05/25 12:45','2020/06/08 10:30', .3],\
        ['STPS_36003'    ,'2020/06/08 13:15','2020/06/24 10:30', .3],\
        ['STPS_32020'    ,'2020/06/24 13:15','2020/07/07 10:15', .3],\
        ['STPS_36003'    ,'2020/08/04 12:15','2020/08/19 08:15', .3],\
        ['STPS_32020'    ,'2020/08/19 11:15','2020/08/31 08:15', .3],\
        ['STPS_36003'    ,'2020/08/31 10:15','2020/09/16 07:15', .3],\
        ['STPS_32020'    ,'2020/09/16 10:00','2020/10/14 07:15', .3],\
        ['STPS_36003'    ,'2020/10/14 09:00','2020/11/17 09:15', .3],\
        ['STPS_32020'    ,'2020/11/17 12:30','2020/12/15 08:45', .3],\
        ['STPS_36003'    ,'2020/12/15 09:00','2021/01/01 00:00', .3],\
        ##                        2021                          ##
        ['STPS_36003'    ,'2021/01/01 00:15','2021/01/11 06:30', .3],\
        ['STPS_32020'    ,'2021/01/11 09:15','2021/02/16 12:30', .3],\
        ['STPS_36003'    ,'2021/02/16 14:00','2021/03/30 09:00', .3],\
        ['STPS_31020'    ,'2021/03/30 14:00','2021/04/12 09:30', .3],\
        ['STPS_36003'    ,'2021/04/12 12:00','2021/04/26 07:15', .3],\
        ['STPS_31020'    ,'2021/04/26 12:15','2021/05/10 09:00', .3],\
        ['STPS_36003'    ,'2021/05/10 10:45','2021/05/25 06:45', .3],\
        ['STPS_31020'    ,'2021/05/25 11:45','2021/06/10 09:30', .3],\
        ['STPS_36003'    ,'2021/06/10 11:15','2021/06/24 07:30', .3],\
        ['STPS_31020'    ,'2021/06/24 11:00','2021/07/26 09:45', .3],\
        ['STPS_36003'    ,'2021/07/26 13:15','2021/08/10 09:30', .3],\
        ['STPS_31020'    ,'2021/08/10 12:30','2021/08/26 10:45', .3],\
        ['STPS_32020'    ,'2021/08/26 14:00','2021/09/10 10:15', .3],\
        ['STPS_31020'    ,'2021/09/10 13:45','2021/09/20 07:45', .3],\
        ['STPS_32020'    ,'2021/09/20 11:15','2021/10/18 07:15', .3],\
        ['STPS_31020'    ,'2021/10/18 09:30','2021/11/09 11:45', .3],\
        ['STPS_32020'    ,'2021/11/09 14:30','2021/12/03 07:00', .3],\
        ['STPS_31020'    ,'2021/12/03 10:15','2022/01/01 00:00', .3],\
        ##                        2022                          ##
        ['STPS_31020'    ,'2022/01/01 00:00','2022/01/18 09:15', .3],\
        ['STPS_32020'    ,'2022/01/18 11:30','2022/02/21 11:30', .3],\
        ['STPS_31020'    ,'2022/02/21 14:30','2022/03/17 08:00', .3],\
        ['STPS_32020'    ,'2022/03/17 11:15','2022/03/31 07:30', .3],\
        ['STPS_31020'    ,'2022/03/31 11:15','2022/04/19 09:45', .3],\
        ['STPS_32020'    ,'2022/04/19 13:15','2022/04/28 06:45', .3],\
        ['STPS_31020'    ,'2022/04/28 09:45','2022/05/16 08:00', .3],\
        ['STPS_32020'    ,'2022/05/16 11:30','2022/05/31 09:00', .3],\
        ['STPS_31020'    ,'2022/05/31 11:30','2022/06/14 07:30', .3],\
        ['STPS_32020'    ,'2022/06/14 11:00','2022/06/29 09:00', .3],\
        ['STPS_31020'    ,'2022/06/29 11:00','2022/07/12 06:45', .3],\
        ['STPS_32020'    ,'2022/07/12 09:30','2022/07/29 09:00', .3],\
        ['STPS_31020'    ,'2022/07/29 11:15','2022/08/16 11:15', .3],\
        ['STPS_36003'    ,'2022/08/16 14:45','2022/08/26 08:00', .3],\
        ['STPS_31020'    ,'2022/08/26 10:30','2022/09/09 07:15', .3],\
        ['STPS_36003'    ,'2022/09/09 10:45','2022/09/26 08:15', .3],\
        ['STPS_31020'    ,'2022/09/26 11:15','2022/10/07 06:30', .3],\
        ['STPS_36003'    ,'2022/10/07 09:15','2022/12/07 08:00', .3],\
        ['STPS_31020'    ,'2022/12/07 10:15','2023/01/01 00:00', .3],\
        ##                        2023                          ##
        ['STPS_31020'    ,'2023/01/01 00:00','2023/01/26 11:45', .3],\
        ['STPS_32020'    ,'2023/01/26 14:30','2023/02/21 08:45', .3],\
        ['STPS_36003'    ,'2023/02/21 12:30','2023/03/10 10:30', .3],\
        ['STPS_32020'    ,'2023/03/10 12:30','2023/04/06 08:30', .3],\
        ['STPS_36003'    ,'2023/04/06 11:30','2023/04/18 07:15', .3],\
        ['STPS_32020'    ,'2023/04/18 09:28','2023/05/09 11:15', .3],\
        ['STPS_36003'    ,'2023/05/09 13:00','2023/05/17 07:00', .3],\
        ['STPS_31020'    ,'2023/05/17 09:00','2023/06/05 09:15', .3],\
        ['STPS_36003'    ,'2023/06/05 11:45','2023/06/19 10:00', .3],\
        ['STPS_32020'    ,'2023/06/19 10:00','2023/07/04 09:00', .3],\
        ['STPS_36003'    ,'2023/07/04 11:39','2023/07/17 09:30', .3],\
        ['STPS_32020'    ,'2023/07/17 09:30','2023/07/31 08:15', .3],\
        ['STPS_36003'    ,'2023/07/31 08:51','2023/08/17 09:45', .3],\
        ['STPS_32020'    ,'2023/08/17 10:45','2023/08/29 07:15', .3],\
        ['STPS_36003'    ,'2023/08/29 09:00','2023/09/18 10:45', .3],\
        ['STPS_32020'    ,'2023/09/18 11:45','2023/10/02 10:00', .3],\
        ['STPS_36003'    ,'2023/10/02 13:10','2023/10/17 10:15', .3],\
        ['STPS_32020'    ,'2023/10/17 11:29','2023/11/16 10:30', .3],\
        ['STPS_36003'    ,'2023/11/16 13:00','2023/12/13 09:00', .3],\
        ['STPS_32020'    ,'2023/12/13 10:45','2024/01/01 00:00', .3],\
        ##                        2024                          ##


)


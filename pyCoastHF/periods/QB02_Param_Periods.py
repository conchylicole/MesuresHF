#!/usr/bin/env python
# -*- coding: utf-8 -*-

periods = {}

periods['PROF'] = ( \
        ['STPS_2000','2000/01/01 00:00','2002/06/21 11:00', .3],\
        ['YSI_2002' ,'2002/06/21 11:00','2002/08/13 09:00', .3],\
        ['STPS_2000','2002/08/13 09:00','2002/09/27 10:00', .3],\
        ['YSI_2002' ,'2002/09/27 10:00','2002/10/14 12:00', .3],\
        ['STPS_2000','2002/10/14 12:00','2003/03/01 00:00', .3],\
        ['YSI_2002' ,'2003/03/01 00:00','2004/01/01 00:00', .3],\
        ['STPS_2000','2004/01/01 00:00','2007/01/01 00:00', .3],\
        ['SP2T_2007','2007/01/01 00:00','2008/08/26 09:00', .3],\
        ['YSI_2002' ,'2008/08/26 09:00','2009/01/01 00:00', .3],\
        ['SP2T_2007','2009/01/01 00:00','2009/05/12 09:00', .3],\
        ['YSI_2002' ,'2009/05/12 09:00','2009/10/15 10:00', .3],\
        ['SP2T_2007','2009/10/15 10:00','2012/01/01 00:00', .3],\
        ['STPS_2000','2012/01/01 00:00','2015/01/01 00:00', .3],\
        ##                            2021                            ##
        ['SeapHox_2169', '2021/01/01 00:00','2021/08/11 00:00', .1],\
        ['SBE_03722471', '2021/09/01 00:00','2022/01/01 00:00', .1],\
        ##                            2022                            ##
        ['SBE_03722471', '2022/01/01 00:00','2023/01/01 00:00', .1],\
        ##                            2023                            ##
        ['SBE_03722471', '2023/01/01 00:00','2023/05/11 08:30', .1],\
        ['SBE_03725832', '2023/05/11 08:30','2024/01/01 00:00', .1],\
        ['SBE_03725832', '2024/01/01 00:00','2025/01/01 00:00', .1],\

)

periods['TEMP'] = ( \
        ['STPS_2000','2000/01/01 00:00','2003/02/01 00:00', .3],\
        ['YSI_2002' ,'2003/02/01 00:00','2004/01/01 00:00', .3],\
        ['STPS_2000','2004/01/01 00:00','2007/01/01 00:00', .3],\
        ['SP2T_2007','2007/01/01 00:00','2008/08/26 09:00', .3],\
        ['YSI_2002' ,'2008/08/26 09:00','2009/01/01 00:00', .3],\
        ['SP2T_2007','2009/01/01 00:00','2009/05/12 09:00', .3],\
        ['YSI_2002' ,'2009/05/12 09:00','2009/10/15 10:00', .3],\
        ['SP2T_2007','2009/10/15 10:00','2012/01/01 00:00', .3],\
        ['STPS_2000','2012/01/01 00:00','2015/01/01 00:00', .3],\
        ##                            2021                            ##
        ['SeapHox_2169', '2021/01/01 00:00','2021/08/11 00:00', .1],\
        ['SBE_03722471', '2021/09/01 00:00','2022/01/01 00:00', .1],\
        ##                            2022                            ##
        ['SBE_03722471', '2022/01/01 00:00','2023/01/01 00:00', .1],\
        ##                            2023                            ##
        ['SBE_03722471', '2023/01/01 00:00','2023/05/11 08:30', .1],\
        ['SBE_03725832', '2023/05/11 08:30','2024/01/01 00:00', .1],\
        ['SBE_03725832', '2024/01/01 00:00','2025/01/01 00:00', .1],\
)

periods['PSAL'] = ( \
        ['STPS_2000','2000/01/01 00:00','2007/01/01 00:00', .3],\
        ['YSI_2002' ,'2007/12/01 00:00','2010/01/01 00:00', .3],\
        ['STPS_2000','2012/01/01 00:00','2015/01/01 00:00', .3],\
        ##                            2021                            ##
        ['SeapHox_2169', '2021/01/01 00:00','2021/08/11 00:00', .1],\
        ['SBE_03722471', '2021/09/01 00:00','2022/01/01 00:00', .1],\
        ##                            2022                            ##
        ['SBE_03722471', '2022/01/01 00:00','2023/01/01 00:00', .1],\
        ##                            2023                            ##
        ['SBE_03722471', '2023/01/01 00:00','2023/05/11 08:30', .1],\
        ['SBE_03725832', '2023/05/11 08:30','2024/01/01 00:00', .1],\
        ['SBE_03725832', '2024/01/01 00:00','2025/01/01 00:00', .1],\
) 

periods['DOX1'] = ( \
        ##                            2021                            ##
        ['SeapHox_2169', '2021/01/01 00:00','2021/08/11 00:00', .1],\
        ['SBE_03722471', '2021/09/01 00:00','2022/01/01 00:00', .1],\
        ##                            2022                            ##
        ['SBE_03722471', '2022/01/01 00:00','2023/01/01 00:00', .1],\
        ##                            2023                            ##
        ['SBE_03722471', '2023/01/01 00:00','2023/05/11 08:30', .1],\
        ['SBE_03725832', '2023/05/11 08:30','2024/01/01 00:00', .1],\
        ['SBE_03725832', '2024/01/01 00:00','2025/01/01 00:00', .1],\
)


periods['OSAT'] = ( \
        ['YSI_2002' ,'2000/01/01 00:00','2020/01/01 00:00', .3],\
) 

periods['FLUO'] = ( \
        ##                            2021                            ##
        ['FLNTU_5151','2021/01/01 00:00','2022/01/01 00:00', .3],\
        ##                            2022                            ##
        ['FLNTU_5151','2022/01/01 00:00','2023/01/01 00:00', .3],\
        ##                            2023                            ##
        ['FLNTU_6602','2023/01/01 00:00','2024/01/01 00:00', .3],\
        ['FLNTU_6602','2024/01/01 00:00','2024/01/24 13:00', .3],\
        ['FLNTU_8392','2024/01/24 13:00','2024/04/12 09:00', .3],\
        ['FLNTU_6602','2024/04/12 09:00','2025/01/01 00:00', .3],\
) 

periods['TURB'] = ( \
        ##                            2021                            ##
        ['FLNTU_5151','2021/01/01 00:00','2022/01/01 00:00', .3],\
        ##                            2022                            ##
        ['FLNTU_5151','2022/01/01 00:00','2023/01/01 00:00', .3],\
        ##                            2023                            ##
        ['FLNTU_6602','2023/01/01 00:00','2024/01/01 00:00', .3],\
        ['FLNTU_6602','2024/01/01 00:00','2024/01/24 13:00', .3],\
        ['FLNTU_8392','2024/01/24 13:00','2024/04/12 09:00', .3],\
        ['FLNTU_6602','2024/04/12 09:00','2025/01/01 00:00', .3],\
) 

periods['PH'] = ( \
        ['pH_INT_Cor'    ,'2021/01/01 00:00','2021/02/15 00:00'],\
        ['pH_INT_Cor'    ,'2021/04/08 12:00','2021/09/15 00:00'],\
        ['pH_EXT_Cor'    ,'2021/09/15 00:00','2022/01/01 00:00'],\
        ##                        2022                          ##
        ['pH_EXT_Cor'    ,'2022/01/01 00:00','2022/05/21 00:00'],\
        ['pH_EXT_Cor'    ,'2022/05/24 10:00','2022/06/19 00:00'],\
        ['pH_EXT_Cor'    ,'2022/06/23 10:00','2023/01/01 00:00'],\
        ##                        2023                          ##
        ['pH_EXT_Cor'    ,'2023/01/01 00:00','2024/01/01 00:00'],\
        ['pH_EXT_Cor'    ,'2024/01/01 00:00','2024/06/27 15:00'],\
        ['pH_EXT_Cor'    ,'2024/07/03 11:00','2025/01/01 00:00'],\
)

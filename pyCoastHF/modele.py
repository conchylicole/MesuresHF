# -*- coding: utf-8 -*-
"""
Get data from model results

Created on Mon Jul 29 16:08:05 2013
@author: S. Petton
"""

import netCDF4
import numpy as np


def get_sst(model, date_start=None, date_end=None, moyenne=1):
    f = netCDF4.Dataset(model, 'r')
    lon = f.variables['longitude'][:]
    lat = f.variables['latitude'][:]
    nj, ni = lon.shape
    if date_start is not None and date_end is not None:
        ind_deb = netCDF4.date2index(date_start, f.variables['time'], select='nearest')
        ind_end = netCDF4.date2index(date_end, f.variables['time'], select='nearest')
        vec_time = np.arange(ind_deb, ind_end, moyenne)
    else:
        vec_time = np.arange(0, len(f.variables['time'][:]))
    if moyenne > 1:
        tps_mod = np.empty(len(vec_time))
        sst_mod = np.ma.MaskedArray(data=np.empty((len(vec_time), nj, ni)))
        for i in range(len(vec_time)):
            tps_mod[i] = f.variables['time'][int(vec_time[i] + np.floor(moyenne / 2))]
            sst_mod[i, :] = f.variables['TEMP'][vec_time[i] : vec_time[i] + moyenne - 1, -1, :, :]  # noqa : E203
            sst_mod[i, :] = sst_mod[i, :].mean(axis=0)
        tps_mod = netCDF4.num2date(tps_mod, f.variables['time'].units)
    else:
        tps_mod = f.variables['time'][vec_time, :]
        sst_mod = f.variables['TEMP'][vec_time, :]
    f.close()
    return tps_mod, sst_mod, lon, lat


def get_ij_from_xy(filename, x, y, check=False, var='H0'):
    """
    Donne les indices IJ à partir d'une position géographique

    """
    filein = netCDF4.Dataset(filename, 'r')
    lon = filein.variables['longitude'][:]
    lat = filein.variables['latitude'][:]

    if len(lon.shape) == 2:
        lon = lon[0, :]
        lat = lat[:, 0]

    i = np.argmin(abs(x - lon))
    j = np.argmin(abs(y - lat))

    if check:
        tmp = filein.variables[var][:]
        print(var, '\t', tmp[j, i], '\t', i, '\t', j)

    filein.close()
    return i, j


def check_r0_factor(h, rmask, visu=False):
    """
    Copyright (c) 2001 Rutgers University.
    This function computes the bathymetry slope R-factor
    rmask : if sea 1 else 0
    """
    import matplotlib.pyplot as plt

    [Lp, Mp] = h.shape
    L = Lp - 1
    M = Mp - 1

    umask = np.zeros(h.shape)
    vmask = np.zeros(h.shape)
    rx = np.zeros(h.shape)
    ry = np.zeros(h.shape)

    rx[:, 0:M] = abs(h[:, 0:M] - h[:, 1:Mp]) / (h[:, 0:M] + h[:, 1:Mp])
    umask[:, 0:M] = rmask[:, 0:M] * rmask[:, 1:Mp]
    rx = rx * umask
    ry[0:L, :] = abs(h[0:L, :] - h[1:Lp, :]) / (h[0:L, :] + h[1:Lp, :])
    vmask[0:L, :] = rmask[0:L, :] * rmask[1:Lp, :]
    ry = ry * vmask

    r = np.maximum(rx, ry)
    if visu:
        plt.imshow(np.flipud(r))
        plt.colorbar()
        plt.show()
    rmax = np.nanmax(r)

    print('r0 max : %f' % rmax)

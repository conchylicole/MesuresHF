# -*- coding: utf-8 -*-
"""
Module de traitement des bouées de type SMATCH / STPS / OTT

Created on Wed Feb 12 17:05:19 2014
@author: S. Petton

CLASSE SondeData()
 => Lecture sonde CDOCO
 => Lecture sonde Quadrige
 => Conversion de l'Oxygène % en mg/L
 convert_o2

 sort_data
 filtre_seuil_variable
 filtre_spike_variable
 filtre
 save_netcdf
 save_xls

# read_probe_cdoco
   # appel à read_cdoco_file
# read_tmp_SONDES_dir : Extraction Quadrige 2 et/ou tmp_SONDES/*
   # appel à read_data_xls

"""

import matplotlib.pyplot as plt
import datetime as dt
import logging
import os
import sys
from typing import List, Optional

import netCDF4
import numpy as np
import pandas as pd

from . import common, qc, quadrige, reader

# ---------------------------------------
#          Variables locales
# ---------------------------------------
config = common.data_config("sondes")
conf_sondes = config["SONDES"]
conf_cdoco = config["CDOCO"]
conf_sites = common.data_config("sites")
conf_vars = common.data_config("variables")
conf_eco = common.data_config("sondes", "EcoFLNTU")
CREATION = "Création du fichier : %s"


#############################################
#    Lecture et initialisation des sondes   #
#############################################
def read_local_dir(Type, site, directory="./tmp_SONDES/", extractionQ2=False, unique_sonde=False):
    """
    Read local directory and return SONDES data.

    Parameters:
    - Type: The type of data to read.
    - site: The site to read data from.
    - directory: The directory to read data from. Default is "./tmp_SONDES/".
    - extractionQ2: Flag indicating whether to perform Q2 extraction. Default is False.
    - unique_sonde: Flag indicating whether to return unique sonde data. Default is False.

    Returns:
    - SONDES: The data read from the local directory.
    """
    if extractionQ2:
        sondes = quadrige.read_local_dir(Type, site, unique_sonde)
        return sondes
    else:
        sondes = reader.read_local_dir(Type, site, directory, unique_sonde)
    return sondes


def read_local_manuel(site, fileIn="Mesures_MANUEL.xlsx", skiprows=0, variables=conf_vars.keys()):
    """Lecture des mesures manuelles pour chaque site"""
    logging.info(" -> Mesures manuelles")
    name = conf_sites[site]["name"]
    S = SondeData(name, "MANUEL", site)

    try:
        data = pd.read_excel(fileIn, sheet_name=site, skiprows=skiprows)
    except (IndexError, ValueError):
        return []

    data.dropna(subset=["Date"], inplace=True)
    S.raws = data.set_index("Date")

    # On ne conserve que les variables
    list_vars = []
    for v in list(S.raws):
        if v in variables:
            list_vars.append(v)
    S.raws = S.raws[list_vars]

    if "CHLO" in S.raws.columns and "FLUO" not in S.raws.columns:
        S.raws["FLUO"] = S.raws["CHLO"]

    S.data = S.raws.copy()
    return [S]


def read_local_rephy(site, fileIN="Mesures_REPHY.csv", date_str=None, date_end=None, prof=None, flore=False):
    """Lecture des données du réseau REPHY"""
    try:
        ref_q2 = conf_sites[site]["rephy"]
    except KeyError:
        return []

    if len(ref_q2) == 2:
        if prof is None:
            prof = ref_q2[1]
        ref_q2 = ref_q2[0]

    logging.info(" -> Mesures REPHY")
    S = SondeData("REPHY", "MANUEL", site)

    names = ("Lieu", "Mnemo", "Date", "Heure", "UT", "Prof", "Code", "Valeur", "VQ", "Taxon")
    df = pd.read_csv(fileIN, sep=";", header=0, names=names, encoding="ISO-8859-1")
    df = df[df.Mnemo == ref_q2]

    if not flore:
        df = df[df.Code != "FLORTOT"]

    df["Heure"].fillna("12:00:00", inplace=True)
    df["UT"].fillna(0, inplace=True)

    def parse_date(row):
        return pd.to_datetime(
            row["Date"] + " " + row["Heure"], format="%d/%m/%Y %H:%M:%S", errors="coerce"
        ) - dt.timedelta(hours=int(row["UT"]))

    df["Date"] = df[["Date", "Heure", "UT"]].apply(parse_date, axis=1)
    df = df.set_index("Date").drop(["Heure", "UT"], axis=1)

    if prof is not None:
        df = df[df.Prof.str.startswith(prof)]
    df = df.pivot_table(index=["Date"], columns="Code", values="Valeur", aggfunc="first")
    dict_var = {
        "SALI": "PSAL",
        "CHLOROA": "FLUO",
        "OXYGENE": "DOX1",
        "NH4": "AMON",
        "PO4": "PHOS",
        "SIOH": "SLCA",
        "NO3+NO2": "NTRA",
    }
    df.rename(columns=dict_var, inplace=True)
    df.columns = df.columns.rename(None)

    if "DOX1" in list(df):
        df["DOX1"] /= 1.429
    # Recopie des données brutes en données valides (cas simple de lecture)
    if date_str is not None:
        df = df[date_str:]
    if date_end is not None:
        df = df[:date_end]

    S.raws = df
    S.data = df
    return [S]


def read_local_somlit(
    site: str,
    fileIN: str = "Mesures_SOMLIT.csv",
    date_str: str = None,
    date_end: str = None,
    prof: str = None,
    ref_q2: str = None,
) -> List:
    """
    Lecture des données du réseau SOMLIT
    0 : Donnée en dessous de la limite de détection
    1 : Prélèvement effectué, mais mesure non réalisée
    2 : Mesure bonne, échantillon non répliqué
    3 : Mesure douteuse
    4 : Mesure mauvaise
    5 : Prélèvement effectué, mais valeur pas encore reportée
    6 : Mesure bonne ; moyenne de plusieurs réplicats
    7 : Mesure bonne ; valeur acquise hors protocoles SOMLIT
    8 : Donnée non qualifiée
    9 : Echantillon non prélevé
    """
    if ref_q2 is None:
        try:
            ref_q2 = conf_sites[site]["somlit"]
        except KeyError:
            return []

    logging.info(" -> Mesures SOMLIT")
    S = SondeData("SOMLIT", "MANUEL", site)

    df = pd.read_csv(fileIN, sep=";", skiprows=2)
    df = df.iloc[1:, :]

    df = df[df["nomSite*"] == ref_q2]

    df["Date"] = pd.to_datetime(df["DATE"] + " " + df["HEURE"], format="%Y-%m-%d %H:%M:%S")
    df = df.set_index("Date").drop(["DATE", "HEURE"], axis=1)

    if prof is not None:
        df = df[df.PROF_TEXT == prof]

    dict_var = {
        "TEMP": "T",
        "PSAL": "S",
        "DOX1": "O",
        "FLUO": "CHLA",
        "TURB": "MES",
        "PH25": "PH",
        "PHOS": "PO4",
        "SLCA": "SIOH4",
        "AMON": "NH4",
        "NTRA": "NO3",
        "NTRI": "NO2",
    }

    for var, short_var in dict_var.items():
        df[var] = pd.to_numeric(df[short_var])
        tmp = df["q" + short_var].astype(int)
        conditions = [(tmp == 0), (tmp == 1), (tmp == 3), (tmp == 4), (tmp == 5), (tmp == 9)]
        df.loc[np.any(conditions, axis=0), var] = np.NaN
    df.dropna(axis=0, how="all", inplace=True)

    df = df.sort_index()
    if date_str is not None:
        df = df[date_str:]
    if date_end is not None:
        df = df[:date_end]

    S.raws = df
    S.data = df
    return [S]


###################################################################################################
###################################################################################################


class SondeError(Exception):
    """Exception raised for errors in sonde processing."""

    def __init__(self, value: str):
        """
        Initialize SondeError object.

        Args:
            value: Represents the value associated with the exception.
        """
        super().__init__(value)
        self.value = value


class SondeData:
    """
    Representation of sonde data.

    Attributes:
        Name (str): The name of the sonde data.
        Type (str): The type of the probe.
        site (str): The site where the sonde data was collected.
        conf_site (dict): The configuration settings for the site.
        conf_vars (dict): The configuration settings for the variables.
        posi (pd.DataFrame): The position data.
        data (pd.DataFrame): The data.
        raws (pd.DataFrame): The raw data.
        flag (pd.DataFrame): The flag data.
        adjs (pd.DataFrame): The adjusted data.

    Methods:
        __init__(self, name: str, probe_type: str, site: str) -> None:
            Constructor. Initializes attribute types and reads configurations.

        get_writable_names(self) -> List[str]:
            Get writable name (unit) of the variables.

        get_cdoco_names(self) -> List[str]:
            Get CDOCO name (unit) of the variables.

        read_cdoco_file(self, fileIn: str, level: str = "0", variables: List[str] = conf_vars.keys(), flags: bool = False) -> None:
            Read file from Coriolis Database and extract specific columns based on the provided configuration.

        sort_index(self) -> None:
            Delete duplicate datetime indices and sort the dataframes in ascending order based on the datetime index.

        rename_pres(self) -> None:
            Renames the column "PRES" in the `raws` DataFrame to "PROF" if "PROF" does not already exist as a column.

        change_units_cond(self) -> None:
            Convert the units of conductivity data from Siemens per meter (S/m) to milliSiemens per centimeter (mS/cm).

        convert_cond_to_psal(self, force=False) -> None:
            Estimate salinity with temperature and conductivity data with EPS-78.

        convert_o2_saturation_to_concentration(self, force: bool = False) -> None:
            Convert oxygen data from saturation (%) to concentration (ml/L) with temperature and salinity data.

        convert_fluo_to_chloro(self, force=False) -> None:
            Convert fluorescence (FFU) signal to chlorophylle (µg/L) based on T-isochrysis standart range for ECO FLNTU.

        convert_variables(self) -> None:
            Convert units or types of variables.

        reduce_time(self, date_str: dt.datetime, date_end: dt.datetime) -> None:
            Select data within a specified time window.

        resample_data(self, freq=None) -> None:
            Resample data at a specified frequency using interpolation and nearest neighbor methods.

        filtre_spike_variable(self, var, diff, local=False) -> None:
            Filtrage d'une variable sur détection de pics.

        filtre(self, date_start: dt.datetime, date_end: dt.datetime, daily: bool = True) -> None:
            Filter and process the data in the `raws` attribute of the `SondeData` class.

        define_position(self, longitude=None, latitude=None) -> None:
            Set longitude and latitude data to arbitrary values for each time step.

        define_data(self) -> None:
            Define data DataFrame from raws & flags.

        init_read_netcdf(self, rep: str = ".", file_in: Optional[str] = None) -> None:
            Read a SondeData object from a NetCDF file and initialize its attributes.

        save_netcdf(self, rep: str = ".", name: Optional[str] = None) -> None:
            Save the data from the SondeData object into a NetCDF file.

        save_csv(self, rep: str = ".") -> None:
            Save data in csv file for French users.

        save_OCO_csv(self, rep: str = ".", name: Optional[str] = None) -> None:
            Save raws data in csv with CDOCO format.

        save_OCO_csv_new(self, folder: str = ".", filename: Optional[str] = None, ict: Optional[str] = None, platform: bool = True, names: Optional[List[str]] = None) -> None:
            Save raws data in csv with new CDOCO format.

        plot_raws_flag(self, variables: Optional[List[str]] = None) -> None:
            Plot the raw data with flags.
    """

    def __init__(self, name: str, probe_type: str, site: str) -> None:
        """
        Constructor.
        Initializes attribute types and reads configurations.

        Args:
            name (str): The name of the sonde data.
            probe_type (str): The type of the probe.
            site (str): The site where the sonde data was collected.
        """

        try:
            self.Name = name
        except Exception as e:
            logging.exception(f"Mauvais format de nom {type(name)} {name}")
            sys.exit()

        self.Type = probe_type
        self.site = site
        self.conf_site = conf_sites[site]
        self.conf_vars = conf_vars
        self.__dict__.update(
            {
                "posi": pd.DataFrame(),
                "data": pd.DataFrame(),
                "raws": pd.DataFrame(),
                "flag": pd.DataFrame(),
                "adjs": pd.DataFrame(),
            }
        )

    def get_writable_names(self) -> List[str]:
        """
        Get writable name (unit) of the variables

        Returns:
            List[str]: A list of strings containing the names and units of the variables in the `data` attribute
        """
        header = []
        for name in self.data.columns:
            if name in self.conf_vars:
                variable = self.conf_vars[name]
                header.append(f"{variable['longname']} ({variable['units']})")
            else:
                header.append(f"{name} ()")
        return header

    def get_cdoco_names(self) -> List[str]:
        """
        Get CDOCO name (unit) of the variables

        Returns:
            List[str]: A list of CDOCO names (units) corresponding to the variables in the `raws` attribute.
        """
        header = []
        for name in self.raws:
            header.append(self.conf_vars.get(name, {}).get("cdoco", name))
        return header

    def read_cdoco_file(
        self, fileIn: str, level: str = "0", variables: List[str] = conf_vars.keys(), flags: bool = False
    ) -> None:
        """
        Read file from Coriolis Database and extract specific columns based on the provided configuration.
        Perform data validation and quality checks, and store the extracted data in the 'raws' attribute of the 'SondeData' class.

        Args:
            fileIn (str): The path to the CDOCO file to be read.
            level (str, optional): The level of the data to be extracted. Defaults to "0".
            variables (List[str], optional): The list of variables to be extracted. Defaults to all variables defined in the 'conf_vars' dictionary.
            flags (bool, optional): Flag indicating whether to keep the flag values from the CDOCO file. Defaults to False.

        Returns:
            None. The extracted data is stored in the 'raws' attribute of the 'SondeData' object.
        """
        file_format = conf_cdoco["file_format"]
        sep = file_format["csv_separator"]

        # Read first line to detect columns
        with open(fileIn, "r") as f:
            line = f.readline()
        li = line.strip().split(sep)

        indexPlatform = li.index(file_format["header_platform"])
        if indexPlatform != 0:
            raise ValueError("PLATFORM must be the first column in CDOCO file")
        indexDate = li.index(file_format["header_date"])
        indexLat = li.index(file_format["header_latitude"])
        indexLon = li.index(file_format["header_longitude"])
        indexQC = li.index(file_format["header_qc"])

        # Select columns to be extracted
        names = ["Date", "LAT", "LON"]
        cols = [indexDate, indexLat, indexLon]
        level = "LEVEL" + level
        for i, y in enumerate(li):
            if level in y:
                try:
                    var = y.split(" ")[0]
                    uni = y.split("(")[1].split(")")[0].lower()
                    for v in variables:
                        if var in conf_vars[v]["list_header"] and uni in conf_vars[v]["list_units"]:
                            names.append(v)
                            cols.append(i)
                            break
                except (IndexError, KeyError):
                    pass

        names.append("QC")
        cols.append(indexQC)

        # Read CSV file and extract data
        names = [names[i] for i in sorted(range(len(cols)), key=lambda k: cols[k])]
        data = pd.read_csv(fileIn, skiprows=1, delimiter=sep, header=None, names=names, usecols=cols)

        # Convert dates to datetime format and set as index
        data["Date"] = pd.to_datetime(data["Date"], format="%Y-%m-%dT%H:%M:%SZ", errors="ignore")
        data = data.set_index(data["Date"]).drop(["Date"], axis=1)

        if flags:
            # Keep flag values from CDOCO (Scoop or Automatic QC)
            qc = data["QC"].apply(int)
            data.drop(["LON", "LAT", "QC"], axis=1, inplace=True)
            flag = data.copy() * 0 + 1
            for v, i in enumerate(cols[3:-1]):
                flag.iloc[:, v] = qc // 10 ** (cols[-1] - i - 1) % 10
            self.raws = pd.concat([self.raws, data], axis=0, sort=False)
            self.flag = pd.concat([self.flag, flag], axis=0, sort=False)
            self.flag[self.flag.isnull()] = 9

            self.data = self.raws.copy()
            self.data.where(self.flag == 1, np.NaN, inplace=True)
            self.data.dropna(axis=0, how="all", inplace=True)
        else:
            # Quality check
            if conf_cdoco["data_validation"]["qc_value_match"]:
                qc_value = conf_cdoco["data_validation"]["qc_value"]
                data["QC"] = data["QC"].apply(str)
                for v, i in enumerate(cols[1:-1]):
                    ind = [j[i - 1] not in qc_value for j in data["QC"][:]]
                    data.loc[ind, v] = np.NaN

                    # Remove Longitude, Latitude and QC data
                    data = data.drop(columns=["LON", "LAT", "QC"])

                    # Remove columns where all values are NaN
                    data = data.dropna(axis=1, how="all")

                    # Concatenate data
                    self.raws = self.raws.append(data, ignore_index=True)

    def sort_index(self):
        """
        Delete duplicate datetime indices and sort the dataframes in ascending order based on the datetime index.
        """
        # Remove duplicate rows in raws dataframe
        self.raws = self.raws[~self.raws.index.duplicated(keep="last")]
        self.raws.sort_index(inplace=True)

        # Sort flag dataframe if not empty
        if not self.flag.empty:
            self.flag = self.flag[~self.flag.index.duplicated(keep="last")]
            self.flag.sort_index(inplace=True)

        # Sort posi dataframe if not empty
        if not self.posi.empty:
            self.posi = self.posi[~self.posi.index.duplicated(keep="last")]
            self.posi.sort_index(inplace=True)

        # Sort data dataframe if not empty
        if not self.data.empty:
            self.data.sort_index(inplace=True)

    def rename_pres(self):
        """
        Renames the column "PRES" in the `raws` DataFrame to "PROF" if "PROF" does not already exist as a column.
        """
        if "PRES" in self.raws.columns and "PROF" not in self.raws.columns:
            self.raws.rename(columns={"PRES": "PROF"}, inplace=True)
            if not self.flag.empty:
                self.flag.rename(columns={"PRES": "PROF"}, inplace=True)
        elif "PRES" in self.raws.columns and "PROF" in self.raws.columns:
            self.raws.loc[:, "PROF"] = self.raws.loc[:, "PROF"].combine_first(self.raws.loc[:, "PRES"])

    def change_units_cond(self):
        """
        Convert the units of conductivity data from Siemens per meter (S/m) to milliSiemens per centimeter (mS/cm)
        """
        if "CNDC" in self.raws.columns:
            if "COND" in self.raws.columns:
                self.raws.loc[self.raws["COND"].isnull(), "COND"] = (
                    self.raws.loc[self.raws["COND"].isnull(), "CNDC"] * 10.0
                )
            else:
                self.raws["COND"] = self.raws["CNDC"] * 10.0
            self.raws.drop(columns="CNDC", inplace=True)
            if not self.flag.empty:
                self.flag["COND"] = self.flag["CNDC"]
                self.flag.drop(columns="CNDC", inplace=True)

    def convert_cond_to_psal(self, force=False):
        """Estimate salinity with temperature and conductivity data with EPS-78
            if pressure, take into account pressure effect otherwise P = 0 bar

        Réf : Aminot A., Kérouel R. (2004), Hydrologgingie des écosystèmes marins.
              Paramètres et analyses. Cf pages 74-75
              t[0] = 15. ;  p[0] =   0. ; R[0] = 1.   ; C[0] = 42.914
              t[1] = 20. ;  p[1] = 200. ; R[1] = 1.2  ; C[1] = 51.4968
              t[2] =  5. ;  p[2] = 150. ; R[2] = 0.65 ; C[2] = 27.8941
        """
        if "TEMP" in list(self.raws) and "COND" in list(self.raws):
            logging.debug("Conversion de la conductivité en salinité")
            a = [0.0080, -0.1692, 25.3851, 14.0941, -7.0261, 2.7081]
            b = [0.0005, -0.0056, -0.0066, -0.0375, 0.0636, -0.0144]
            c = [0.6766097, 2.00564e-2, 1.104259e-4, -6.9698e-7, 1.0031e-9]
            d = [0.0, 3.426e-2, 4.464e-4, 4.215e-1, -3.107e-3]
            e = [0.0, 2.070e-4, -6.370e-8, 3.989e-12]
            k = 0.0162
            C_ref = 42.914  # Conductivité en mS/cm à S = 35, T = 15°C et P = 0

            t = self.raws["TEMP"]  # en °C
            C = self.raws["COND"]  # en S/m
            if "PROF" in list(self.raws):
                p = self.raws["PROF"] / 10.0  # en bar

            R = C / C_ref
            r_t = c[0] + c[1] * t + c[2] * t**2 + c[3] * t**3 + c[4] * t**4
            if "PROF" in list(self.raws):
                R_p = 1.0 + (p * (e[1] + e[2] * p + e[3] * p**2)) / (1 + d[1] * t + d[2] * t**2 + (d[3] + d[4] * t) * R)
            else:
                R_p = 1.0
            R_t = R / (R_p * r_t)

            psal = (
                a[0]
                + a[1] * R_t**0.5
                + a[2] * R_t
                + a[3] * R_t**1.5
                + a[4] * R_t**2
                + a[5] * R_t**2.5
                + (t - 15)
                / (1 + k * (t - 15))
                * (b[0] + b[1] * R_t**0.5 + b[2] * R_t + b[3] * R_t**1.5 + b[4] * R_t**2 + b[5] * R_t**2.5)
            )
            if force or "PSAL" not in list(self.raws):
                self.raws["PSAL"] = psal
            else:
                self.raws.loc[self.raws["PSAL"].isnull(), "PSAL"] = psal[self.raws["PSAL"].isnull()]

    def convert_o2_saturation_to_concentration(self, force: bool = False):
        """
        Convert oxygen data from saturation (%) to concentration (ml/L) with temperature and salinity data.

        Reference: Aminot A., Kérouel R. (2004), Hydrologgingie des écosystèmes marins. Paramètres et analyses. Cf pages 110-118
        """

        if "OSAT" in self.raws.columns and "TEMP" in self.raws.columns and "PSAL" in self.raws.columns:
            logging.debug("Converting oxygen to ml/L")
            A21 = self.raws["TEMP"]
            B21 = self.raws["PSAL"]
            C21 = self.raws["OSAT"]

            oxyg = (
                0.0223916
                * C21
                * (
                    np.exp(
                        -135.90205
                        + (1.575701e5) / (A21 + 273.15)
                        - (6.642308e7) / (A21 + 273.15) ** 2
                        + (1.2438e10) / (A21 + 273.15) ** 3
                        - (8.621949e11) / (A21 + 273.15) ** 4
                        - B21 * (0.017674 - 10.754 / (A21 + 273.15) + 2140.7 / (A21 + 273.15) ** 2)
                    )
                )
                / 100.0
            )

            if force or "DOX1" not in self.raws.columns:
                self.raws["DOX1"] = oxyg
            else:
                self.raws.loc[self.raws["DOX1"].isnull(), "DOX1"] = oxyg[self.raws["DOX1"].isnull()]

        if "DOX1" in self.raws.columns and "TEMP" in self.raws.columns and "PSAL" in self.raws.columns:
            logging.debug("Converting oxygen to %")
            O21 = self.raws["TEMP"]
            P21 = self.raws["PSAL"]
            Q21 = self.raws["DOX1"]

            osat = (100 * Q21) / (
                0.0223916
                * (
                    np.exp(
                        -135.90205
                        + (1.575701e5) / (O21 + 273.15)
                        - (6.642308e7) / (O21 + 273.15) ** 2
                        + (1.2438e10) / (O21 + 273.15) ** 3
                        - (8.621949e11) / (O21 + 273.15) ** 4
                        - P21 * (0.017674 - 10.754 / (O21 + 273.15) + 2140.7 / (O21 + 273.15) ** 2)
                    )
                )
            )

            if force or "OSAT" not in self.raws.columns:
                self.raws["OSAT"] = osat
            else:
                self.raws.loc[self.raws["OSAT"].isnull(), "OSAT"] = osat[self.raws["OSAT"].isnull()]

    def convert_fluo_to_chloro(self, force=False):
        """Convert fluorescence (FFU) signal to chlorophylle (µg/L) based on
        T-isochrysis standart range for ECO FLNTU
        """
        if self.Type == "FLNTU" and "FLUO" in list(self.raws):
            chlo = self.raws["FLUO"].copy()
            for n, coef in conf_eco[self.Name].iteritems():
                dstr = dt.datetime.strptime(coef["str"], "%Y/%m/%d %H:%M")
                dend = dt.datetime.strptime(coef["end"], "%Y/%m/%d %H:%M")
                chlo[dstr:dend] *= coef["FLUO"][2]
            if force or "CHLO" not in list(self.raws):
                self.raws["CHLO"] = chlo
            else:
                self.raws.loc[self.raws["CHLO"].isnull(), "CHLO"] = chlo[self.raws["CHLO"].isnull()]

    def convert_variables(self):
        """Convert units or types of variables"""

        # Renomme Pression en Profondeur
        self.rename_pres()

        # Conversion de la conductivité en mS/cm
        self.change_units_cond()

        # Calcul de la salinité
        self.convert_cond_to_psal()

        # Conversion de l'O2 % en O2 mg/L
        self.convert_o2_saturation_to_concentration()

        # Conversion de la fluorescence (FFU) en chlorophylle (µg/L)
        self.convert_fluo_to_chloro()

    def reduce_time(self, date_str: dt.datetime, date_end: dt.datetime) -> None:
        """
        Select data within a specified time window.

        Args:
            date_str (dt.datetime): The start date of the time window.
            date_end (dt.datetime): The end date of the time window.

        Returns:
            None. Modifies the dataframes of the SondeData object.
        """
        self.data = self.data.loc[date_str:date_end]
        self.raws = self.raws.loc[date_str:date_end]
        self.flag = self.flag.loc[date_str:date_end]
        self.posi = self.posi.loc[date_str:date_end]

    def resample_data(self, freq=None):
        """
        Resample data at a specified frequency using interpolation and nearest neighbor methods.

        Args:
            freq (int, optional): The frequency at which to resample the data. If not provided, it defaults to the frequency specified in the `conf_site` attribute of the `SondeData` object.

        Returns:
            None. The `raws` attribute of the `SondeData` object is updated with the resampled data.
        """
        if freq is None:
            freq = self.conf_site.get("freq", 15)

        # Determine the start and end times for the resampled data
        start_time = self.raws.index[0].replace(second=0, minute=(self.raws.index[0].minute // freq) * freq)
        end_time = self.raws.index[-1].replace(second=0)
        m_end = end_time.minute
        if m_end % freq != 0:
            end_time = end_time.replace(minute=0)
            end_time += dt.timedelta(minutes=freq * ((m_end // freq) + 1))

        # Reindex the `raws` attribute to the new time vector using the nearest neighbor method to fill missing values
        new_time = pd.date_range(start=start_time, end=end_time, freq=f"{freq}min")
        data_nearest = self.raws.reindex(new_time, method="nearest", tolerance=pd.Timedelta("60s"))

        # Time interpolation raws the missing values in the reindexed `raws` attribute using the time method
        union_time = self.raws.index.union(new_time)
        data_interp = self.raws.reindex(union_time).interpolate(method="time")

        # Mask the interpolated values with the nearest neighbor values within a tolerance of half the specified frequency
        tolerance = pd.Timedelta(f"{freq * 60 / 2}s")
        nearest_values = self.raws.reindex(union_time, method="nearest", tolerance=tolerance)
        data_interp = data_interp.mask(pd.isnull(nearest_values), np.NaN)
        data_interp = data_interp.reindex(new_time)

        # Combine the nearest neighbor and interpolated dataframes, giving priority to the nearest neighbor values
        self.raws = data_nearest.combine_first(data_interp)

        # Remove rows where all variables have NaN values
        self.raws.dropna(axis=0, how="all", inplace=True)

    def filtre_spike_variable(self, var, diff, local=False):
        """Filtrage d'une variable sur détection de pics
        On définit le pic par ordre de priorité suivant:
        1. Valeur dans la config du site
        2. Valeur dans la config de la variable
        3. Pas de filtre sur les pics
        """
        try:
            dVmax = self.conf_site[var]["dVmax"]
        except KeyError:
            try:
                dVmax = self.conf_vars[var]["dVmax"]
            except KeyError:
                dVmax = None

        # Filtre avec moyenne glissante
        if dVmax is not None:
            if local:
                self.data[var][diff[var] > dVmax] = np.NaN
                ind = self.flag.index.isin(diff.index[diff[var] > dVmax])
                self.flag[var][ind] = 3
            else:
                self.data = self.data[diff[var] <= dVmax]
                ind = self.flag.index.isin(diff.index[diff[var] > dVmax])
                self.flag[ind] = np.maximum(3, self.flag[ind])

        # Filtre gradients horaires
        if dVmax is not None:
            # Passage en valeur par heure
            dVmax = dVmax / 3600.0
            nb_mask_avt = 0
            nb_mask_apr = 10
            mask = self.data[var].isnull().values
            dnum = self.data.index.to_series().diff().dt.total_seconds().values
            grad = self.data[var].diff().div(dnum, axis=0).shift(periods=-1).values
            while nb_mask_avt != nb_mask_apr:
                nb_mask_avt = mask.sum()
                mask = self.data[var].isnull().values
                dnum = self.data.index.to_series().diff().dt.total_seconds().values
                grad = self.data[var].diff().div(dnum, axis=0).shift(periods=-1).values
                # mask = temp.isnull().values
                # dnum = temp.index.to_series().diff().dt.total_seconds().values
                # grad = temp.diff().div(dnum, axis=0).slice_shift(periods=-1).values
                # grad = self.data[var].diff().div(dnum, axis=0).slice_shift(periods=-1).values
                mask = qc.mask_spikes(grad, mask, lim=dVmax)

                self.data[var][mask] = np.NaN
                ind = self.flag.index.isin(self.data.index[mask])
                self.flag[var][ind] = np.maximum(3, self.flag[var][ind])

                nb_mask_apr = mask.sum()
                # print(nb_mask_avt,nb_mask_apr)

            # if local:
            #    self.data[var][mask] = np.NaN
            #    ind = self.flag.index.isin(self.data.index[mask])
            #    self.flag[var][ind] = np.maximum(3, self.flag[var][ind])
            # else:
            #    self.data[mask] = np.NaN
            #    ind = self.flag.index.isin(self.data[mask].index)
            #    self.flag[ind] = np.maximum(3, self.flag[ind])


    def get_qc_values(self, variables=None):
        filter_manager = qc.FilterManager(self.conf_site, self.conf_vars)
        if variables is None:
            variables = self.raws.columns
        self.conf_qc = filter_manager.generate_qc(variables)

    def get_filter_value(self, var, filter_name):
        return self.conf_qc.get(var, {}).get(filter_name, None)


    def apply_qc(self, date_start: dt.datetime, date_end: dt.datetime, daily: bool = True) -> None:
        """
        Filter and process the data in the `raws` attribute of the `SondeData` class.

        Args:
            date_start (dt.datetime): The start date for filtering the data.
            date_end (dt.datetime): The end date for filtering the data.
            daily (bool, optional): Flag indicating whether to calculate daily temporal averages. Defaults to True.

        Returns:
            None. The filtered and processed data is stored in the `data` attribute of the `SondeData` object.
        """
        logging.debug(f"Filtrage {self.Type} {self.Name}")

        # Step 1: Filter the raws attribute based on the provided start and end dates
        self.raws = self.raws[date_start:date_end]

        # Step 2: Perform quality control checks and flag missing values in the filtered data
        if self.Type == "MANUEL":
            qc.reset_flags(self, flag=4)
            qc.flag_missing_values(self)
            return
        else:
            qc.reset_flags(self)
            qc.flag_missing_values(self)

        # Step 3: Separate the variables that need to be treated from the untreated variables
        untreated = [
            v for v in self.raws.columns if (v not in conf_vars) or (v in conf_vars and not conf_vars[v]["filtre"])
        ]
        variables = [v for v in self.raws.columns if v not in untreated]
        self.get_qc_values(variables)

        # Step 4: Apply threshold checks on temperature, salinity, and pressure and apply them to all the treated variables
        for var in ["TEMP", "PSAL", "PROF"]:
            if var in variables:
                qc.threshold_min(self, var, local=False)
                qc.threshold_max(self, var, local=False)

        # Step 5: Apply additional filters on all treated variables
        for var in variables:
            qc.threshold_min(self, var)
            qc.threshold_max(self, var)
            qc.tukey53H(self, var)
            qc.climato(self, var)

        # Step 6: Assign flag 0 to the untreated variables and flag 9 to the missing values in the untreated variables
        self.flag.loc[:, untreated] = 0
        for var in untreated:
            self.flag.loc[self.raws.loc[:, var].isnull(), var] = 9

        # Step 7: Define the data attribute based on the filtered data where the flag is less than or equal to 2
        self.define_data()

    def define_position(self, longitude=None, latitude=None):
        """
        Set longitude and latitude data to arbitrary values for each time step
        """
        # Set default values if longitude and latitude are not provided
        if longitude is None:
            longitude = self.conf_site["longitude"]
        if latitude is None:
            latitude = self.conf_site["latitude"]

        lon = np.full(len(self.raws), longitude)
        lat = np.full(len(self.raws), latitude)

        self.posi = pd.DataFrame({"LON": lon, "LAT": lat}, index=self.raws.index)

    def define_data(self):
        """
        Define data DataFrame from raws & flags
        """
        # Combine adjs and raws DataFrames
        self.data = self.adjs.combine_first(self.raws)

        # Set values to NaN where flag is greater than 2
        self.data = self.data.where(self.flag <= 2, np.NaN)

        # Drop rows where all values are NaN
        self.data.dropna(axis=0, how="all", inplace=True)

    def init_read_netcdf(self, rep: str = ".", file_in: Optional[str] = None) -> None:
        """
        Read a SondeData object from a NetCDF file and initialize its attributes.

        Args:
            rep (str, optional): The directory path where the NetCDF file is located. Default is ".".
            file_in (str, optional): The name of the NetCDF file to be read.

        Raises:
            ValueError: If `rep` is not a valid directory path or `file_in` is not a valid file path.
        """
        # Construct the file path if not provided
        if file_in is None:
            file_in = f"{rep}/{self.site}_{self.Type}_{self.Name.replace(' ', '_')}.nc"

        # Check if the directory path is valid
        if not os.path.isdir(rep):
            raise ValueError(f"Invalid directory path: {rep}")

        # Check if the file path is valid
        if not os.path.isfile(file_in):
            raise ValueError(f"Invalid file path: {file_in}")

        # Open the NetCDF file
        logging.debug(f"Reading file: {file_in}")
        with netCDF4.Dataset(file_in, "r") as nc:
            # Read time values and convert to datetime objects
            dates = netCDF4.num2date(
                nc.variables["time"][:],
                nc.variables["time"].units,
                only_use_cftime_datetimes=False,
                only_use_python_datetimes=True,
            )

            # Read latitude and longitude values
            try:
                lon = nc.variables["longitude"][:]
                lat = nc.variables["latitude"][:]
            except IndexError:
                logging.warning(f"No geographic position in file: {file_in}")
                lon = np.full(len(dates), self.conf_site["longitude"])
                lat = np.full(len(dates), self.conf_site["latitude"])
            self.posi = pd.DataFrame(data={"LAT": lat, "LON": lon}, index=dates)

            # Initialize variables defined in the configuration file
            header = []
            variables = [v for v in nc.variables if v + "_flag" in nc.variables]
            for var in variables:
                if var in self.conf_vars:
                    header.append(var)
                    self.raws[var] = nc.variables[var][:]
                    self.flag[var] = nc.variables[var + "_flag"][:]
                else:
                    logging.info(f"Unknown variable in configuration file: {var}")
            self.raws.index = dates
            self.raws.index.name = "Date"
            self.flag.index = dates
            self.flag.index.name = "Date"

            self.adjs = pd.DataFrame(index=self.raws.index, columns=self.raws.columns, dtype="float32")
            for var in self.raws.columns:
                if var + "_adjusted" in nc.variables:
                    self.adjs[var] = nc.variables[var + "_adjusted"][:]

            # Construct correct data
            self.define_data()

    def save_netcdf(self, rep: str = ".", name: Optional[str] = None) -> None:
        """
        Save the data from the SondeData object into a NetCDF file.

        Args:
            rep (str, optional): The directory path where the NetCDF file will be saved. Defaults to ".".
            name (str, optional): The name of the NetCDF file. Defaults to None.

        Returns:
            None
        """
        if self.Type.startswith("MANUEL"):
            return

        os.makedirs(rep, exist_ok=True)

        if name is not None:
            out = f"{rep}/{name}.nc"
        else:
            out = f"{rep}/{self.site}_{self.Type}_{self.Name.replace(' ', '-')}.nc"
        logging.debug(CREATION % out)

        with netCDF4.Dataset(out, "w", format="NETCDF4_CLASSIC") as nc:
            nc.createDimension("time", None)

            time_var = nc.createVariable("time", "d", ("time",), zlib=True)
            time_var.units = "seconds since 1900-01-01T00:00:00Z"
            time_var.long_name = "time in seconds (UT)"
            time_var.time_origin = "01-JAN-1900 00:00:00"

            lon_var = nc.createVariable("longitude", "f", ("time",), zlib=True)
            lon_var.units = "degrees_east"
            lon_var.long_name = "longitude"
            lon_var[:] = self.posi["LON"].values

            lat_var = nc.createVariable("latitude", "f", ("time",), zlib=True)
            lat_var.units = "degrees_east"
            lat_var.long_name = "latitude"
            lat_var[:] = self.posi["LAT"].values

            for var_name in self.raws.columns:
                var = nc.createVariable(var_name, "f", ("time",), fill_value=-999.0, zlib=True)
                try:
                    var.longname = self.conf_vars[var_name]["longname"]
                    var.units = self.conf_vars[var_name]["units"]
                except KeyError:
                    logging.warning(f"{var_name} not in config_sondes.cfg")
                tmp = self.raws[var_name].values
                tmp = np.nan_to_num(tmp, nan=-999.0)
                var[:] = tmp

                flag_var = nc.createVariable(var_name + "_flag", "i", ("time",), zlib=True)
                flag_var[:] = self.flag[var_name].values

                if var_name in self.adjs.columns and self.adjs[var_name].notnull().any():
                    adj_var = nc.createVariable(var_name + "_adjusted", "f", ("time",), fill_value=-999.0, zlib=True)
                    adj_var.longname = self.conf_vars[var_name]["longname"]
                    adj_var.units = self.conf_vars[var_name]["units"]
                    tmp = self.adjs[var_name].values
                    tmp = np.nan_to_num(tmp, copy=False, nan=-999.0)
                    adj_var[:] = tmp

            nc.variables["time"][:] = netCDF4.date2num(
                self.raws.index.to_pydatetime(), "seconds since 1900-01-01T00:00:00Z"
            )
            nc.flags_value = "0: No check; 1: Good data; 2: Probably good data; 3: Probably bad data; \
                              4: Bad data; 5: Changed; 7: Nominal value; 8: Interpolated data; 9: Missing value"

    def save_csv(self, rep: str = "."):
        """
        Save data in csv file for French users

        Args:
            rep (str, optional): The directory path where the CSV file will be saved. If not provided, the current directory will be used.

        Returns:
            None: The method saves the data from the `raws` attribute of the `SondeData` class into a CSV file.
        """

        # Create the output folder if it doesn't exist
        os.makedirs(rep, exist_ok=True)

        out = f"{rep}/{self.site}_{self.Type}_{self.Name.replace(' ', '_')}.csv"
        logging.debug(CREATION % out)

        header = self.get_writable_names()
        self.raws.to_csv(out, sep=";", index_label="Date", header=header, decimal=",", float_format="%.2f")

    def save_OCO_csv(self, rep: str = ".", name: Optional[str] = None):
        """
        Save raws data in csv with CDOCO format

        Args:
            rep (str, optional): The output folder where the CSV file will be saved. If not provided, the current directory will be used.
            name (str, optional): The name of the output file. If not provided, the site name will be used.
        """

        # Create the output folder if it doesn't exist
        os.makedirs(rep, exist_ok=True)

        # Set the output file path based on the output folder, name, and the first and last dates of the raws data
        if name is None:
            name = self.site
        out = f"{rep}/{name}_{self.raws.index[0].strftime('%Y%m%d')}_{self.raws.index[-1].strftime('%Y%m%d')}.csv"
        logging.debug(CREATION % out)

        # Get the CDOCO names (units) for the variables in the raws attribute
        h_tmp = self.get_cdoco_names()

        # Create the header for the CSV file, including the platform, date, latitude, longitude, variable names, and QC
        header = ["PLATFORM", "DATE (yyyy-mm-ddThh:mi:ssZ)", "LATITUDE (degree_north)", "LONGITUDE (degree_east)"]
        header.extend(h_tmp)
        header.append("QC")

        # Add flag for Platform (0), Date (1) & Positions (1,1)
        self.flag = self.flag.astype("int")
        self.flag.insert(0, "OCO", "0111")

        # Gather all flags and create a unique flag row
        flag = self.flag.to_string(index=False, header=False)
        flag = flag.replace(" ", "").split("\n")
        self.raws["QC"] = flag

        # Add Platform, Date & Position data
        self.raws["PLAT"] = self.conf_site.get("platform", "IF000000")
        self.raws["DATE"] = self.raws.index.strftime("%Y-%m-%dT%H:%M:%SZ")
        self.raws["LATI"] = round(self.conf_site["latitude"], 4)
        self.raws["LONG"] = round(self.conf_site["longitude"], 4)

        # Reorder columns
        self.raws = self.raws[["PLAT", "DATE", "LATI", "LONG"] + list(self.raws.columns[:-4])]

        # Export to csv format
        self.raws.to_csv(out, index=False, header=header, float_format="%.3f", na_rep="NaN")

    def save_OCO_csv_new(
        self,
        folder: str = ".",
        filename: Optional[str] = None,
        ict: Optional[str] = None,
        platform: bool = True,
        names: Optional[List[str]] = None,
    ) -> None:
        """
        Save raws data in csv with new CDOCO format

        Args:
            folder (str, optional): The folder where the CSV file will be saved. Defaults to ".".
            filename (str, optional): The name of the CSV file. Defaults to None.
            ict (str, optional): The path to an existing ICT file. Defaults to None.
            platform (bool, optional): A boolean indicating whether to include the platform information in the CSV file. Defaults to True.
            names (list, optional): A list of column names to be used in the CSV file. Defaults to None.
        """

        # Create the folder if it doesn't exist
        os.makedirs(folder, exist_ok=True)

        # Set the output file path
        if filename is None:
            filename = self.site
        out = f"{folder}/{filename}.csv"
        if ict is not None:
            out = ict
        logging.debug(CREATION % out)

        # Concatenate raws and flag & keep only cdoco variables
        header = []
        flag = self.flag.add_suffix("_flag")
        for v in self.raws.columns:
            if v in self.conf_vars and "cdoco" in self.conf_vars[v]:
                header.append(v)
                header.append(v + "_flag")
        raws = self.raws.join(flag)
        raws = raws[header]

        # Define column names
        h_tmp = [self.conf_vars[name]["cdoco"] if name in self.conf_vars else name for name in raws.columns]

        # Add Platform, Date & Position data
        header = ["PLATFORM", "DATE (yyyy-mm-ddThh:mi:ssZ)", "LATITUDE (degree_north)", "LONGITUDE (degree_east)"]
        header.extend(h_tmp)
        raws["PLAT"] = self.conf_site.get("platform", "IF000000")
        raws["DATE"] = raws.index.strftime("%Y-%m-%dT%H:%M:%SZ")
        raws["LATI"] = f'{self.conf_site["latitude"]: .4f}'
        raws["LONG"] = f'{self.conf_site["longitude"]: .4f}'

        # Re-order columns
        raws = raws[["PLAT", "DATE", "LATI", "LONG"] + list(raws.columns[:-4])]

        # Remove platform column if platform is False
        if not platform:
            raws = raws.iloc[:, 1:]
            header = header[1:]

        # Save the raws dataframe as a CSV file
        if names is None:
            raws.to_csv(out, index=False, header=header, float_format="%.3f")
        else:
            raws.to_csv(out, index=False, header=names, float_format="%.3f")

    def plot_raws_flag(self, variables: Optional[List[str]] = None):
        """
        Plot the raw data with flags.

        Args:
            variables (list): List of variables to plot. If None, all variables will be plotted.

        Returns:
            None
        """
        # If variables is None, set it to the list of all variables in the raws attribute
        if variables is None:
            variables = self.raws.columns

        # Create a color map using the "tab10" colormap with 10 colors
        cmap = plt.cm.get_cmap("tab10", 10)

        # Create a figure and a set of subplots with the same x-axis
        fig, ax = plt.subplots(len(variables), 1, sharex=True)

        # Iterate over each variable in the variables list
        for i, var in enumerate(variables):
            # Scatter plot the data points from the raws attribute for the current variable,
            # using the corresponding flag values as the color
            c = ax[i].scatter(self.raws.index, self.raws[var], s=0.6, c=self.flag[var], cmap=cmap, vmin=0, vmax=10)

        # Adjust the layout of the figure to make room for the colorbar
        fig.subplots_adjust(right=0.9)

        # Add a colorbar to the figure, with tick marks corresponding to the flag values
        cbar_ax = fig.add_axes([0.92, 0.1, 0.02, 0.79])
        cbar = fig.colorbar(c, cax=cbar_ax, format="%d")
        cbar.set_ticks([0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5])
        cbar.ax.tick_params(length=0)

        # Customize labels
        ax[-1].set_xlabel("Time")
        fig.suptitle("Raw Data with Flags")

        plt.show()

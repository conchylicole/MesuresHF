import numpy as np
import pandas as pd


def sf_calc(Eint=0.0865, Eext=-0.93, E0int=-0.39, E0ext=-1.46, tempC=16.2, salt=35.6, dE0int=None, dE0ext=None):
    """With calibration coefficients E0int and E0ext, and in-situ temperature and
       salinity, corrects electrode signals and return in-situ pH

    Args :
      Eint    : voltage values of internal electrode
      Eext    : voltage values of external electrode
      E0int   : calibration coefficient for internal electrode (kdf0)
      E0ext   : calibration coefficient for external electrode (k0)
      tempC   : in-situ temperature (°C)
      salt    : in-situ salinity (psu)
      dE0int : calibration slope for internal electrode (kdf2)
      dE0ext : calibration slope for external electrode (k2)

    Returns :
      pHint_tot : total pH according to internal electrode
      pHext_tot : total pH according to external electrode

    Origin :
      Jean-Pierre Gattuso, James Orr, Jean-Marie Epitalon, Kimberlee Baldry, Umihiko Hoshijima, & Abulos. (2021).
      Seacarb v3.2.16 https://doi.org/10.5281/zenodo.4600014
    """

    # Universal gas constant & Faraday constant (https://physics.nist.gov/cuu/Constants/index.html)
    R = 8.314462618
    FF = 96485.33212

    # Temperature dependence of standard potentials, Martz et al. 2010
    dtemp = 0
    if dE0int is None or dE0ext is None:
        # Slope given at 25°C
        dE0int = -0.001101
        dE0ext = -0.001048
        dtemp = 25
        # ==> E0int & E0ext must be estimate at 25°C (like in searcarb)

    # See Martz et al. 2010 for greater details
    tempK = tempC + 273.15  # Convert temp from C to K
    S_T = (R * tempK) / FF * np.log(10)  # Nernst temp dependence

    # Calc pHint from Nernst
    pHint_tot = (Eint - (E0int + dE0int * (tempC - dtemp))) / S_T

    Z = 19.924 * salt / (1000 - 1.005 * salt)  # Ionic strength, Dickson et al. 2007
    SO4_tot = (0.14 / 96.062) * (salt / 1.80655)  # Total conservative sulfate
    cCl = 0.99889 / 35.453 * salt / 1.80655  # Conservative chloride mol/kg_SW
    mCl = cCl * 1000 / (1000 - salt * 35.165 / 35)  # mol/kg_H2O
    K_HSO4 = np.exp(
        -4276.1 / tempK
        + 141.328
        - 23.093 * np.log(tempK)
        + (-13856 / tempK + 324.57 - 47.986 * np.log(tempK)) * Z**0.5
        + (35474 / tempK - 771.54 + 114.723 * np.log(tempK)) * Z
        - 2698 / tempK * Z**1.5
        + 1776 / tempK * Z**2
        + np.log(1 - 0.001005 * salt)
    )  # Bisulfate equilibrium const., Dickson et al. 2007

    #  Debye-Huckel constant for activity of HCl, Khoo et al. 1977
    DHconst = 0.00000343 * tempC**2 + 0.00067524 * tempC + 0.49172143
    log10gamma_HCl = 2 * (-DHconst * np.sqrt(Z) / (1 + 1.394 * np.sqrt(Z)) + (0.08885 - 0.000111 * tempC) * Z)

    pHext_free = (
        -(((E0ext + dE0ext * (tempC - dtemp)) - Eext) - S_T * (np.log10(mCl) + log10gamma_HCl)) / S_T
    )  # mol/kg_H2O
    pHext_free = pHext_free - np.log10((1000 - salt * 35.165 / 35) / 1000)  # mol/kg_SW
    pHext_tot = pHext_free - np.log10(1 + SO4_tot / K_HSO4)

    if isinstance(pHint_tot, float):
        pHint_tot = pd.Series(pHint_tot)
    if isinstance(pHext_tot, float):
        pHext_tot = pd.Series(pHext_tot)

    res = pd.concat([pHint_tot, pHext_tot], axis=1)
    res.rename(columns={0: "pHint_tot", 1: "pHext_tot"}, inplace=True)
    return res


def sf_calib(calEint=0.0865, calEext=-0.93, calpH=8.132, tempC=16.2, salt=35.6, dE0int=None, dE0ext=None):
    """Determine calibration coefficients E0int and E0ext at 25°C based on
       in-situ discrete pH data (temperature and salinity)

    Args :
      calEint : discrete voltage values of internal electrode
      calEext : discrete voltage values of external electrode
      calpH   : discrete spectrophotometric pH
      tempC   : in-situ temperature (°C)
      salt    : in-situ salinity (psu)
      dE0int  : calibration slope for internal electrode (kdf2)
      dE0ext  : calibration slope for external electrode (k2)

    Returns :
      E0int25 : calibration coefficient for internal electrode at 25°C
      E0ext25 : calibration coefficient for external electrode at 25°C

    Origin :
      Jean-Pierre Gattuso, James Orr, Jean-Marie Epitalon, Kimberlee Baldry, Umihiko Hoshijima, & Abulos. (2021).
      Seacarb v3.2.16 https://doi.org/10.5281/zenodo.4600014
    """

    # Universal gas constant & Faraday constant (https://physics.nist.gov/cuu/Constants/index.html)
    R = 8.314462618
    FF = 96485.33212

    # Temperature dependence of standard potentials, Martz et al. 2010
    dtemp = 0
    if dE0int is None or dE0ext is None:
        # Slope given at 25°C
        dE0int = -0.001101
        dE0ext = -0.001048
        dtemp = 25.0
        # ==> E0int & E0ext must be estimate at 25°C (like in searcarb)

    # See Martz et al. 2010 for greater details
    tempK = tempC + 273.15  # Convert temp from C to K
    S_T = (R * tempK) / FF * np.log(10)  # Nernst temp dependence

    # Calc E0int From Nernst & pH at discrete points
    E0int = calEint - S_T * calpH
    E0int25 = E0int + dE0int * (dtemp - tempC)

    Z = 19.924 * salt / (1000 - 1.005 * salt)  # Ionic strength, Dickson et al. 2007
    SO4_tot = (0.14 / 96.062) * (salt / 1.80655)  # Total conservative sulfate
    cCl = 0.99889 / 35.453 * salt / 1.80655  # Conservative chloride mol/kg_SW
    mCl = cCl * 1000 / (1000 - salt * 35.165 / 35)  # mol/kg_H2O
    K_HSO4 = np.exp(
        -4276.1 / tempK
        + 141.328
        - 23.093 * np.log(tempK)
        + (-13856 / tempK + 324.57 - 47.986 * np.log(tempK)) * Z**0.5
        + (35474 / tempK - 771.54 + 114.723 * np.log(tempK)) * Z
        - 2698 / tempK * Z**1.5
        + 1776 / tempK * Z**2
        + np.log(1 - 0.001005 * salt)
    )  # Bisulfate equilibrium const., Dickson et al. 2007

    pHint_free = calpH + np.log10(1 + SO4_tot / K_HSO4)
    pHint_free = pHint_free + np.log10((1000 - salt * 35.165 / 35) / 1000)  # mol/kg-H2O

    #  Debye-Huckel constant for activity of HCl, Khoo et al. 1977
    DHconst = 0.00000343 * tempC**2 + 0.00067524 * tempC + 0.49172143
    log10gamma_HCl = 2 * (-DHconst * np.sqrt(Z) / (1 + 1.394 * np.sqrt(Z)) + (0.08885 - 0.000111 * tempC) * Z)
    mHfree = 10 ** (-pHint_free)  # mol/kg-H2O
    aHfree_aCl = mHfree * mCl * 10 ** (log10gamma_HCl)

    E0ext = calEext + S_T * np.log10(aHfree_aCl)
    E0ext25 = E0ext + dE0ext * (dtemp - tempC)

    if isinstance(E0int25, float):
        E0int25 = pd.Series(E0int25)
    if isinstance(E0ext25, float):
        E0ext25 = pd.Series(E0ext25)

    res = pd.concat([E0int25, E0ext25], axis=1)
    res.rename(columns={0: "E0int25", 1: "E0ext25"}, inplace=True)
    return res


def K0(S=35, T=25, P=0, Patm=1):
    """
    Estimate Henry’s constant mol/(kg/atm)

    Origin :
      Jean-Pierre Gattuso, James Orr, Jean-Marie Epitalon, Kimberlee Baldry, Umihiko Hoshijima, & Abulos. (2021).
      Seacarb v3.2.16 https://doi.org/10.5281/zenodo.4600014
    """
    TK = T + 273.15

    # CO2(g) <-> CO2(aq.)
    # K0      = [CO2]/ p CO2

    # Weiss (1974)   [mol/kg/atm]
    tmp = 9345.17 / TK - 60.2409 + 23.3585 * np.log(TK / 100)
    nK0we74 = tmp + S * (0.023517 - 0.00023656 * TK + 0.0047036e-4 * TK * TK)
    K0 = np.exp(nK0we74)

    # Pressure correction
    Phydro_atm = P / 1.01325  # convert hydrostatic pressure from bar to atm (1.01325 bar / atm)
    Ptot = Patm + Phydro_atm  # total pressure (in atm) = atmospheric pressure + hydrostatic pressure
    R = 82.05736  # (cm3 * atm) / (mol * K)  CODATA (2006)
    vbarCO2 = 32.3  # partial molal volume (cm3 / mol) from Weiss (1974, Appendix, paragraph 3)
    K0 = K0 * np.exp(((1 - Ptot) * vbarCO2) / (R * TK))  # Weiss (1974, equation 5), TK is absolute temperature (K)

    return K0


def rho(S=35, T=25, P=0):
    """
    Convert temperature (C) on today's "ITS 90" scale to older "IPTS 68" scale
    (see Dickson et al., Best Practices Guide, 2007, Chap. 5, p. 7, including footnote)
    IPTS 68 = International Practical Temperature Scale of 1968
    ITS 90  = International Temperature Scale of 1990

    Origin :
      Jean-Pierre Gattuso, James Orr, Jean-Marie Epitalon, Kimberlee Baldry, Umihiko Hoshijima, & Abulos. (2021).
      Seacarb v3.2.16 https://doi.org/10.5281/zenodo.4600014
    """
    # Note: Density calculation below requires temperature on the IPTS 68 scale
    T68 = (T - 0.0002) / 0.99975

    # Density of pure water
    rhow = (
        999.842594
        + 6.793952e-2 * T68
        - 9.095290e-3 * T68**2
        + 1.001685e-4 * T68**3
        - 1.120083e-6 * T68**4
        + 6.536332e-9 * T68**5
    )

    # Density of seawater at 1 atm, P=0
    A = 8.24493e-1 - 4.0899e-3 * T68 + 7.6438e-5 * T68**2 - 8.2467e-7 * T68**3 + 5.3875e-9 * T68**4
    B = -5.72466e-3 + 1.0227e-4 * T68 - 1.6546e-6 * T68**2
    C = 4.8314e-4
    rho0 = rhow + A * S + B * S ** (3 / 2) + C * S**2

    # Secant bulk modulus of pure water
    # The secant bulk modulus is the average change in pressure
    # divided by the total change in volume per unit of initial volume.
    Ksbmw = 19652.21 + 148.4206 * T68 - 2.327105 * T68**2 + 1.360477e-2 * T68**3 - 5.155288e-5 * T68**4
    # Secant bulk modulus of seawater at 1 atm
    Ksbm0 = (
        Ksbmw
        + S * (54.6746 - 0.603459 * T68 + 1.09987e-2 * T68**2 - 6.1670e-5 * T68**3)
        + S ** (3 / 2) * (7.944e-2 + 1.6483e-2 * T68 - 5.3009e-4 * T68**2)
    )

    # Secant bulk modulus of seawater at S,T,P
    Ksbm = (
        Ksbm0
        + P * (3.239908 + 1.43713e-3 * T68 + 1.16092e-4 * T68**2 - 5.77905e-7 * T68**3)
        + P * S * (2.2838e-3 - 1.0981e-5 * T68 - 1.6078e-6 * T68**2)
        + P * S ** (3 / 2) * 1.91075e-4
        + P * P * (8.50935e-5 - 6.12293e-6 * T68 + 5.2787e-8 * T68**2)
        + P**2 * S * (-9.9348e-7 + 2.0816e-8 * T68 + 9.1697e-10 * T68**2)
    )

    # Density of seawater at S,T,P
    rho = rho0 / (1 - P / Ksbm)

    return rho


if __name__ == "main":
    test = pd.DataFrame({"E0int25": [-0.390073], "E0ext25": [-1.438797]})
    pd.testing.assert_frame_equal(sf_calib(), test)

    test = pd.DataFrame({"pHint_tot": [8.130724], "pHext_tot": [8.501309]})
    pd.testing.assert_frame_equal(sf_calc(), test)

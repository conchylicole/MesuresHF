# -*- coding: utf-8 -*-
"""
Created on Tue Oct  1 09:23:07 2013
@author: S Petton
@aim : Get satellite data from CDOCO (don't forget to ask for data)
"""
import datetime
import os
import shutil
import subprocess

import netCDF4
import numpy as np

from . import common

model = 'ODYSSEA-IBI'
oco_dir_best = '/home/oc/sst/best_estimate/'
oco_name = 'PREVIMER_SST-ODYSSEA-IBI'
frequency = 24
verbose = False
machineSFTP = 'datarmor.ifremer.fr'


def get_sst(data, date_start=None, date_end=None):
    f = netCDF4.Dataset(data, 'r')
    lon = f.variables['longitude'][:]
    lat = f.variables['latitude'][:]
    if len(lon.shape) <= 1:
        lon, lat = np.meshgrid(lon, lat)
    if date_start is not None and date_end is not None:
        ind_deb = netCDF4.date2index(date_start, f.variables['time'], select='nearest')
        ind_end = netCDF4.date2index(date_end, f.variables['time'], select='nearest')
        vec_time = np.arange(ind_deb, ind_end)
    else:
        vec_time = np.arange(0, len(f.variables['time'][:]))
        print(len(f.variables['time'][:]))
    tps_sat = f.variables['time'][vec_time]
    tps_sat = netCDF4.num2date(tps_sat, f.variables['time'].units)
    sst_sat = f.variables['analysed_sst'][vec_time, 0, :, :]
    f.close()

    return tps_sat, sst_sat, lon, lat


def file_satellite(date0, inc=0):
    """Renvoie le chemin complet du fichier au CDOCO"""
    date = date0 + datetime.timedelta(hours=inc)
    date_tz = date.strftime('%Y%m%d')
    filename = oco_name + '_' + date_tz + '.nc'
    year = date.strftime('%Y')
    file_from_best_estimate = os.path.join(oco_dir_best, year, filename)
    return file_from_best_estimate


def get_satellite(
    model, date_start, date_end, out='data_satellite.nc', latmin=None, latmax=None, lonmin=None, lonmax=None
):
    # repertoire temporaire pour le modele considere
    tmp_dir = 'tmp_%s' % model
    if os.path.isdir(tmp_dir):
        shutil.rmtree(tmp_dir)
    os.makedirs(tmp_dir)

    step = frequency  # in hours
    last_file = file_satellite(model, date_end)

    # Constitution de la liste des fichiers valides pour le modele et la periode souhaites
    file_liste = open('valid_files.txt', 'w')
    inc = -step
    fic = ''
    nb_mod = 0
    # on boucle sur les fichiers jusqu'au dernier
    while fic != last_file:
        inc += step
        fic = file_satellite(date_start, inc)
        filename = os.path.basename(fic)
        # on teste l'existence du fichier et on le rajoute au fichier valide_files.txt
        if os.path.isfile(fic):
            nb_mod += 1
            file_liste.write(fic + '\n')
        else:
            # on essaie avec une copie distante
            try:
                common.call('scp %(machineSFTP)s:%(file)s %(tmp_dir)s' % vars(), verbose=verbose)
                local_file = '%s/%s' % (tmp_dir, filename)
                if os.path.isfile(local_file):
                    nb_mod += 1
                    file_liste.write(local_file + '\n')
            except subprocess.CalledProcessError:
                print('Could not get file : %s' % (filename))
    file_liste.close()
    # si il n'y a pas de données disponibles on sort
    if nb_mod == 0:
        return nb_mod

    # Concatenation des fichiers meteo listes dans le fichier valid_files.txt
    tmp_file = 'tmp_file.nc'
    nco_cmd = 'ncrcat -h -O -o %s' % (tmp_file)
    name_lon, name_lat = ('longitude', 'latitude')
    if lonmin is not None and lonmax is not None and latmin is not None and latmax is not None:
        nco_cmd += ' -d %s,%.2f,%.2f -d %s,%.2f,%.2f' % (name_lon, lonmin, lonmax, name_lat, latmin, latmax)

    # Appel de ncrcat en deux temps, necessaire si le nombre de fichiers est tres eleve
    # equivalent a : cat valid_files.txt | ncrcat -o out.nc [options]
    p1 = subprocess.Popen('cat valid_files.txt', shell=True, stdout=subprocess.PIPE)
    common.call(nco_cmd, verbose=verbose, stdin=p1.stdout)

    os.rename(tmp_file, out)
    shutil.rmtree(tmp_dir)
    return nb_mod

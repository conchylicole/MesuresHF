import logging
import pathlib
import pickle

import numpy as np
import pandas as pd

rep_lib = pathlib.Path(__file__).parent


def runavg(ts, w=31):
    """
    Performs a running average of an input time series using uniform window
    of width w. This function assumes that the input time series is periodic.

    Inputs:
      ts            Time series [1D numpy array]
      w             Integer length (must be odd) of running average window

    Outputs:
      ts_smooth     Smoothed time series

    Written by Eric Oliver, Institue for Marine and Antarctic Studies, University of Tasmania, Feb-Mar 2015
    """
    if len(ts) == 0:
        raise ValueError("Input time series is empty.")
    # Original length of ts
    N = len(ts)
    # Check if w is larger than N
    if w > N:
        raise ValueError("Window size is larger than the length of the time series.")
    # make ts three-fold periodic
    ts = np.append(ts, np.append(ts, ts))
    # smooth by convolution with a window of equal weights
    ts_smooth = np.convolve(ts, np.ones(w) / w, mode="same")
    # Only output central section, of length equal to the original length of ts
    ts_smooth = ts_smooth[N : 2 * N]
    return ts_smooth



    def estimate_daily_Teau(self, threshold=12):
        self.Teau_D = self.Teau.resample('D').mean()
        # Threshold expressed in hours
        tmp = self.Teau.resample('H').mean()
        nbr = tmp.resample('D').count()
        self.Teau_D[nbr < threshold] = np.NaN
         

    def estimate_monthly_Teau(self, threshold=25):
        if self.Teau_D.empty:
            # First estimate daily data
            self.estimate_daily_Teau()
        self.Teau_M = self.Teau_D.resample('M').mean()
        # Threshold expressed in days
        nbr = self.Teau_D.resample('M').count()
        self.Teau_M[nbr < threshold] = np.NaN



def estimate_daily_means(df: pd.Series | pd.DataFrame, threshold: int = 12) -> pd.DataFrame:
    """
    Estimate daily means of df DataFrame (at least 12 hourly data needed)

    Args:
        df (pd.DataFrame or pd.Series): The input DataFrame containing hourly data points.
        threshold (int, optional): The minimum number of hourly data points needed to calculate the daily average. Defaults to 12.

    Returns:
        pd.DataFrame: The DataFrame containing the estimated daily means.
    """
    if isinstance(df, pd.Series):
        df = pd.DataFrame(df)

    hourly_avg = df.resample("H").mean()
    daily_avg = hourly_avg.resample("D").mean()
    hourly_count = hourly_avg.resample("D").count()

    daily_avg[hourly_count < threshold] = pd.NA

    return daily_avg


def estimate_monthly_means(df: pd.DataFrame, days: int = 25, means : bool = True) -> pd.DataFrame:
    """
    Calculate monthly means from a daily mean dataframe.

    Args:
        df (pd.DataFrame): A daily mean dataframe.
        days (int, optional): The minimum number of days required for a month to be included in the calculation of monthly means. Default is 25.

    Returns:
        pd.DataFrame: A new dataframe with monthly means.
    """
    if isinstance(df, pd.Series):
        df = pd.DataFrame(df)
    else:
        df = df.copy()
    if means:
        df = df.resample("M").sum(min_count=days) / df.resample("M").count()
    else:
        df = df.resample("M").sum(min_count=days)
    return df


def estimate_yearly_means(df: pd.DataFrame) -> pd.DataFrame:
    """
    Calculates the yearly means of a given DataFrame by applying weighted means based on the number of days in each month.

    Args:
        df (pd.DataFrame): A DataFrame containing monthly means with a datetime index.

    Returns:
        pd.DataFrame: A DataFrame containing the yearly means of the input data, aligned with the middle of each year.
    """
    df = df.copy()  # Use shallow copy

    # Calculate the number of days in each month
    df["days"] = df.index.days_in_month

    # Calculate the total number of days in each year
    df["year"] = df["days"].resample("Y").transform("sum")

    # Check if there are complete years
    df.loc[df["year"] < 365, :] = pd.NA

    # Calculate the weights for each month
    df["weights"] = df["days"] / df["year"]

    # Apply weighted means to each variable
    df = df.mul(df["weights"], axis=0)

    # Calculate the yearly means
    df = df.resample("Y").agg(pd.Series.sum, skipna=False)

    # Shift the index to align with the middle of each year
    df = df.shift(-182, freq="D")

    df.drop(["days", "year", "weights"], axis=1, inplace=True)

    return df


def estimate_climatology(df):
    clim = Climato()
    clim.estimate(df)
    return clim


def read_climatology(site):
    """
    Read climatology data for a given site.

    Args:
        site (str): The site name.

    Returns:
        Climato: The climatology data for the site, or None if the file is not found.
    """
    clim = Climato()
    try:
        file_path = pathlib.Path(rep_lib) / "clim" / f"climato_{site}.pkl"
        with open(file_path, "rb") as f:
            clim = pickle.load(f)
    except FileNotFoundError:
        logging.debug("No climatological file found. It needs to be create at first")
        return None
    return clim


class Climato:
    """Daily climatic time series"""

    def __init__(self):
        self.data = pd.DataFrame()
        self.nstd = pd.DataFrame()
        self.nobs = pd.DataFrame()
        self.d_str = None
        self.d_end = None

    def estimate_climatology_gross(self, df: pd.Series | pd.DataFrame) -> None:
        """
        Estimate the daily climatology based on the input data using a brute force method.

        Args:
            df (pd.Series or pd.DataFrame): The input data.

        Returns:
            None. The estimated climatology data is stored in the `data` attribute of the `Climato` object.
        """
        if isinstance(df, pd.Series):
            df = pd.DataFrame(df)

        # Calculate the number of observations and standard deviation for each day
        self.nobs = df.resample("D").count()
        self.nstd = df.resample("D").std()

        # Calculate the mean for each day of the year and group the data by day of the year
        df = df.resample("D").mean()
        df["DOY"] = df.index.dayofyear
        df = df.groupby("DOY").mean()

        # Apply a running average to smooth the data
        for v in df.columns:
            self.data[v] = runavg(df[v])

    def estimate(self, df: pd.Series | pd.DataFrame, threshold: int = 12) -> None:
        """
        Estimate the daily climatology based on the input data.

        Args:
            df (Union[pd.Series, pd.DataFrame]): The input data.
            threshold (int, optional): The minimum number of hourly data points needed to calculate the daily average. Defaults to 12.
        """
        if isinstance(df, pd.Series):
            df = pd.DataFrame(df)

        self.d_str = df.index[0]
        self.d_end = df.index[-1]

        tmp = df.copy()
        tmp["DOY"] = tmp.index.dayofyear
        tmp.loc[(tmp.DOY >= 60) & (~tmp.index.is_leap_year), "DOY"] += 1
        self.nstd = tmp.groupby("DOY").std()
        if len(self.nstd) != 366:
            self.nstd = self.nstd.reindex(index=np.arange(1, 367))
            self.nstd.interpolate(inplace=True)

        df = estimate_daily_means(df, threshold=threshold)
        df["DOY"] = df.index.dayofyear
        df.loc[(df.DOY >= 60) & (~df.index.is_leap_year), "DOY"] += 1
        self.nobs = df.groupby("DOY").count()
        df = df.groupby("DOY").mean().interpolate()
        for v in df.columns:
            self.data[v] = runavg(df[v])
        self.data.index = df.index

        # TODO: check number of observations
        logging.debug(self.nobs.iloc[59:62,])

    def save(self, site):
        pickle.dump(self, open(f"{rep_lib}/clim/climato_{site}.pkl", "wb"))


def month_to_daily_clim(df: pd.Series | pd.DataFrame) -> pd.DataFrame:
    """
    Estimate daily climatology from monthly mean data using cubic spline interpolation.

    Args:
        df (pd.DataFrame): A pandas DataFrame containing monthly mean data.

    Returns:
        pd.DataFrame: A pandas DataFrame containing the estimated daily climatology.
    """
    if isinstance(df, pd.Series):
        df = pd.DataFrame(df)

    # Group by month and calculate mean
    df = df.groupby(df.index.month).mean()

    # Extrapolate first and last two months for smooth interpolation
    df = pd.concat([df.iloc[-2:], df, df.iloc[:2]])

    # Create new index with day numbers
    d_month = np.array([-59, -29, 1, 32, 61, 92, 122, 153, 183, 214, 245, 275, 306, 336, 367, 398]) + 14
    df.set_index(d_month, inplace=True)

    # Interpolate using cubic spline
    clim = df.reindex(index=range(-45, 413)).interpolate(method="cubic")

    # Trim extrapolated rows
    clim = clim.iloc[46:-46]

    return clim


def fill_with_daily_clim(df: pd.Series | pd.DataFrame, clim: pd.DataFrame) -> pd.DataFrame:
    """
    Fills missing values in specific columns of a DataFrame with corresponding values from a climatology DataFrame.

    Args:
        df (pd.DataFrame): The input DataFrame containing the data to be filled.
        clim (pd.DataFrame): The climatology DataFrame containing the reference values for filling missing data.

    Returns:
        pd.DataFrame: The input DataFrame with missing values filled using the climatology values.
    """
    # Convert Series to DataFrame if necessary
    if isinstance(df, pd.Series):
        df = pd.DataFrame(df)

    # Create column 'DOY' which represents the day of the year for each index value
    df["DOY"] = df.index.dayofyear

    # Adjust 'DOY' values for non-leap years
    df.loc[(df["DOY"] >= 60) & (~df.index.is_leap_year), "DOY"] += 1

    # Iterate over each column in the climatology DataFrame
    for col in clim.columns:
        # Fill missing values in the corresponding column of df with the values from clim based on the 'DOY' values in df
        df[col] = df.apply(lambda row: clim[col].loc[row["DOY"].astype(int)] if pd.isna(row[col]) else row[col], axis=1)
        #df[col] = df[col].fillna(clim[col].values[df["DOY"].astype(int)])

    return df


def cyclical_encoding(data: pd.Series, cycle_length: int) -> pd.DataFrame:
    """
    Encode a cyclical feature with two new features sine and cosine.
    The minimum value of the feature is assumed to be 0. The maximum value
    of the feature is passed as an argument.
      
    Parameters
    ----------
    data : pd.Series
        Series with the feature to encode.
    cycle_length : int
        The length of the cycle. For example, 12 for months, 24 for hours, etc.
        This value is used to calculate the angle of the sin and cos.

    Returns
    -------
    result : pd.DataFrame
        Dataframe with the two new features sin and cos.

    """

    sin = np.sin(2 * np.pi * data/cycle_length)
    cos = np.cos(2 * np.pi * data/cycle_length)
    result =  pd.DataFrame({
                  f"{data.name}_sin": sin,
                  f"{data.name}_cos": cos
              })

    return result

import logging as log

import numpy as np
import pandas as pd

from . import common

conf_vars = common.data_config('variables')

###########################################
# Sauvegarde des fichiers au format EXCEL #
###########################################

conf_OTT = {
    '45859': {'COEF': 33.599},
    '46531': {'COEF': 60.302},
    '49226': {'COEF': 62.025},
    '61086': {'COEF': 54.932},
    '62596': {'COEF': 49.750},
    '62597': {'COEF': 48.188},
    '64693': {'COEF': 47.519},
    '67297': {'COEF': 36.485},
}


def hf_excel(file_out, sonde, data, site='None', version='xlsx'):

    file_out += version
    if version == 'xlsx':
        writer = pd.ExcelWriter(file_out, engine='xlsxwriter')
    else:
        writer = pd.ExcelWriter(file_out, engine='xlwt')

    cols = data.columns.tolist()
    # Changement du format des entêtes en utf-8 (Python 2)
    # cols = [str(c, encoding="utf-8") for c in data.columns.tolist()]

    # Vérification des entêtes
    for i, v in enumerate(cols):
        if v in conf_vars:
            cols[i] = conf_vars[v]['longname'] + ' (' + conf_vars[v]['units'] + ')'
    data.columns = cols
    # Ecriture des données
    data.to_excel(writer, sheet_name=sonde, startrow=1, index=False)
    workbook = writer.book
    worksheet = writer.sheets[sonde]
    # Formats
    if version == 'xlsx':
        date_format = workbook.add_format({'num_format': 'dd/mm/yyyy', 'align': 'center'})
        hour_format = workbook.add_format({'num_format': '[hh]:mm:ss', 'align': 'center'})
        nbre_format = workbook.add_format({'num_format': '0.000', 'align': 'center'})
    else:
        import xlwt  # type: ignore

        align = 'align: horiz center'
        date_format = xlwt.easyxf(align, num_format_str='dd/mm/yyyy')
        hour_format = xlwt.easyxf(align, num_format_str='[hh]:mm:ss')
        nbre_format = xlwt.easyxf(align, num_format_str='0.000')
        workbook.add_style(date_format)
        workbook.add_style(hour_format)
        workbook.add_style(nbre_format)
    # Ecriture de l'entête
    worksheet.write(0, 0, sonde)
    worksheet.write(0, 1, site)
    worksheet.write(0, 2, data.iloc[0, 0], date_format)
    worksheet.write(0, 3, data.iloc[-1, 0], date_format)
    # Formatage
    if version == 'xlsx':
        worksheet.set_column(0, 0, 10, date_format)
        worksheet.set_column(1, 1, 10, hour_format)
        worksheet.set_column(2, data.shape[1], 13)
    # Convert Chlo(V) to Chlo(µg/L)
    try:
        from xlsxwriter.utility import xl_rowcol_to_cell

        ott = sonde.split('_')[1]
        indChlo = [i for i, x in enumerate(list(data)) if x == 'Chlorophylle (V)'][0]
        indPsal = [i for i, x in enumerate(list(data)) if x == 'Salinité (PSU)'][0]
        nb_rows = data.shape[0] + 2
        # Chlo (V) during air exposure / Offset Chloro (V) / Chlorophylle (µg/L)
        irow = data.shape[1]
        worksheet.write(1, indChlo, 'Mesure Chloro (V)')
        worksheet.write(1, irow + 0, 'Air Chloro (V)')
        worksheet.write(1, irow + 1, 'Offset Chloro (V)')
        worksheet.write(1, irow + 2, 'Chlorophylle (µg/L)')
        # Write min, max & factor
        worksheet.write(2, irow + 3, 'Min Chloro')
        worksheet.write(2, irow + 4, 0.2)
        worksheet.write(3, irow + 3, 'Max Chloro')
        worksheet.write(3, irow + 4, 40)
        worksheet.write(4, irow + 3, 'Coef V -> µg/L')
        worksheet.write(4, irow + 4, conf_OTT[ott]['COEF'])
        min_c = xl_rowcol_to_cell(2, irow + 4)
        max_c = xl_rowcol_to_cell(3, irow + 4)
        coefc = xl_rowcol_to_cell(4, irow + 4)
        # Write formulas
        for i in np.arange(2, nb_rows):
            s_loc = xl_rowcol_to_cell(i, indPsal)
            c_loc = xl_rowcol_to_cell(i, indChlo)
            formula = 'IF(AND(%s>0.002,%s<0.03),IF(%s<5,%s,#N/A),#N/A)' % (c_loc, c_loc, s_loc, c_loc)
            worksheet.write_formula(i, irow + 0, formula)
            str_range = xl_rowcol_to_cell(2, irow + 0)
            end_range = xl_rowcol_to_cell(nb_rows - 1, irow + 0)
            formula = 'SUMIF({0}:{1},"<>#N/A",{0}:{1}) / COUNTIF({0}:{1},"<>#N/A")'.format(str_range, end_range)
            worksheet.write_formula(i, irow + 1, formula, nbre_format)
            a_loc = xl_rowcol_to_cell(i, irow + 1)
            formula = 'IF(AND({0}*({3}-{4})>{1},{0}*({3}-{4})<{2}),{0}*({3}-{4}),#N/A)'.format(
                coefc, min_c, max_c, c_loc, a_loc
            )
            worksheet.write_formula(i, irow + 2, formula, nbre_format)
        if version == 'xlsx':
            worksheet.set_column(2, irow + 3, 13)
    except IndexError:
        pass
    # Ecriture
    writer.save()


############################################################
# Sauvegarde des données journalières (max d'informations) #
############################################################


def daily_excel(writer, sheet_name, SONDES, date_start, date_end, ind=0):
    """
    Save daily mean data coming from all probes to sum-up.xlsx
         if ind == 0, mean of all probes
         if ind != 0, one probe

    Pour le moment, on regroupe toutes les données haute-fréquence dans une DataFrame
    puis on vient faire une moyenne journalière. S'il y a des doublons de date, la valeur
    prise est la première non nulle dans l'ordre des sondes

    Faire le regroupement sur les moyennes journalières de chaque sonde ? Non car donne trop
    d'importance dans le cas où une sonde avec une seule donnée dans la journée
    """

    if hasattr(SONDES, '__len__') and len(SONDES) == 0:
        log.debug('Pas de données pour ce site')
        return ind
    else:
        log.debug('Ajout des données journalières au format Excel')

    if ind != 0 and SONDES.Type.startswith('MANUEL'):
        return ind

    if ind == 0:
        # Regroupement des données de sonde dans une dataframe commune
        df = SONDES[0].data
        for s in SONDES[1:]:
            if not s.Type.startswith('MANUEL'):
                df = df.combine_first(s.data)
        # Moyenne journalière
        df = df.resample('D').mean()
    else:
        df = SONDES.data.resample('D').mean()

    # Construction du vecteur journalier de sortie
    tps = pd.date_range(date_start, date_end, freq='D', inclusive='left')
    df = df.reindex(tps)

    # Suppression de certaines variables
    for v in ['BATT', 'CHLOR']:
        df.drop(v, axis=1, inplace=True, errors='ignore')

    # Ecriture
    header = []
    for name in list(df):
        try:
            header.append(conf_vars[name]['longname'] + ' (' + conf_vars[name]['units'] + ')')
        except KeyError:
            header.append(name)
    df.to_excel(
        writer, sheet_name=sheet_name, startrow=1, startcol=ind, index_label='Date', header=header, float_format='%.2f'
    )

    # 1ère ligne
    ws = writer.sheets[sheet_name]
    if ind == 0:
        ws.write(0, ind, 'Moyenne sur le site %s' % SONDES[0].site)
    else:
        ws.write(0, ind, SONDES.Type)
        ws.write(0, ind + 1, SONDES.Name)

    # Mise en forme
    ws.set_column(ind, ind + df.shape[1], 13)

    # Offset de colonne pour écriture future
    return ind + len(list(df)) + 2

import datetime as dt
import logging as log
import os
import shutil
import subprocess

from . import common, sonde

MACHINE_SFTP = 'datarmor.ifremer.fr'
config = common.data_config('sondes')
conf_cdoco = config['CDOCO']
conf_sites = common.data_config('sites')


def filesonde(name, site, date=None):
    """Renvoie le chemin complet du fichier au CDOCO
    Arguments:
        name -- Nom de la Sonde
        date -- Objet date à laquelle on veut récupérer le fichier
    """
    fic = ''
    try:
        file_suffix = conf_sites[site][name]['platform']
        dir_type = conf_sites[site][name]['type']
    except KeyError:
        raise KeyError('Unknown platform for (%s). Please verify your config_sonde configuration...' % (name))
    if date is None:
        fic = (
            conf_cdoco['files']['oco_dir_latest']
            + dir_type
            + '/'
            + conf_cdoco['files']['oco_name_latest']
            + dir_type
            + '_'
            + file_suffix
            + '.csv'
        )
    else:
        fic = (
            conf_cdoco['files']['oco_dir_monthly']
            + dir_type
            + '/'
            + conf_cdoco['files']['oco_name_monthly']
            + dir_type
            + '_'
            + file_suffix
            + '.csv'
        )
        fic = fic.replace('AAAAMM', date.strftime('%Y%m'))
    return fic


def inc_datetime_month(d):
    """Incrémente le mois d'un datetime
    Arguments:
        d -- Le datetime pour lequel le mois doit être incrémenté
    """
    ret = d
    y = ret.year
    m = ret.month
    if m == 12:
        m = 1
        y += 1
    else:
        m += 1
    ret = ret.replace(year=y, month=m)
    return ret


def get_file_list(date_start, date_end, sondename, site):
    """Récupère la liste des fichiers d'observation à lire contenant des données pour un interval de dates donné
    Arguments:
        date_start -- La date de début de l'interval
        date_end -- La date de fin de l'interval
        sondeName -- Le nom de la Sonde pour laquelle récupérer les données
    """
    filelist = []
    date_now = dt.datetime.utcnow()
    date_to_get = date_start
    date_to_get = date_to_get.replace(day=1)
    while date_to_get <= date_end:
        if date_to_get.year == date_now.year and date_to_get.month == date_now.month:
            # Latest
            filelist.append(filesonde(sondename, site))
        elif date_to_get <= date_now:
            # Monthly
            filelist.append(filesonde(sondename, site, date_to_get))
        date_to_get = inc_datetime_month(date_to_get)
    return filelist


def get_files(sonde, filelist, site, download, cleanfirst=False):
    """Verification de l'existence des fichiers
    Recuperation par scp sur MACHINE_SFTP si necessaire
    """
    platform = conf_sites[site][sonde]['platform']

    tmp_dir = 'tmp_%s' % platform
    if not os.path.isdir(tmp_dir) or cleanfirst:
        shutil.rmtree(tmp_dir, ignore_errors=True)
        os.makedirs(tmp_dir)

    nb_files = 0
    valid_files = []
    f = open('valid_files_' + platform + '.txt', 'w')
    for file in filelist:
        filename = os.path.basename(file)
        if os.path.isfile(file):
            valid_files.append(file)
            f.write(file + '\n')
            nb_files += 1
        else:
            # on essaie avec une copie distante
            try:
                if download:
                    common.call('scp -O -q -p %s:%s %s 2> /dev/null' % (MACHINE_SFTP, file, tmp_dir), verbose=False)
                local_file = '%s/%s' % (tmp_dir, filename)
                if os.path.isfile(local_file):
                    valid_files.append(local_file)
                    f.write(local_file + '\n')
                    nb_files += 1
            except subprocess.CalledProcessError:
                log.warning('Could not get file : %s', filename)
    f.close()
    if nb_files == 0:
        shutil.rmtree(tmp_dir)
        os.remove('valid_files_' + platform + '.txt')
    return nb_files, valid_files


def clean_files(sonde, site):
    """Suppression du dossier temporaire local (Récupération du CDOCO)"""
    platform = conf_sites[site][sonde]['platform']
    tmp_dir = 'tmp_%s' % platform
    shutil.rmtree(tmp_dir)
    valid_files = 'valid_files_' + platform + '.txt'
    os.remove(valid_files)


def get_data(site, date_start, date_end, download, cleanFirst, name='SMATCH', flags=False):
    """Get probe data from CDOCO database"""
    SONDE = []
    n = 0
    for s, probe in enumerate(conf_sites[site][name]):
        log.info(' -> CDOCO : %s' % probe)
        filelist = get_file_list(date_start, date_end, probe, site)
        nb_files, valid_files = get_files(probe, filelist, site, download, cleanFirst)
        if nb_files == 0:
            log.warning('Pas de fichiers')
        else:
            SONDE.append(sonde.SondeData(probe, name, site))
            for f in valid_files:
                SONDE[n].read_cdoco_file(f, level=conf_sites[site][probe]['level'], flags=flags)
            n += 1
    return SONDE

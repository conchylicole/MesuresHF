import os
from typing import List
import datetime as dt
import glob
import logging

import numpy as np
import pandas as pd

from . import common, reader, sonde

conf_vars = common.data_config("variables")
conf_sites = common.data_config("sites")
conf_sondes = common.data_config("sondes", name="SONDES")
conf_EcoFLNTU = common.data_config("sondes", "EcoFLNTU")


def read_local_dir(Type: str, site: str, directory: str = "./tmp_SONDES/", unique_sonde: bool = False) -> List:
    """
    Read data from a local directory
      - 1st generate liste_file
      - 2nd call read_data_files
    """
    logging.info(" -> Sonde type %s" % Type)
    name = conf_sites[site]["name"]

    logging.debug("Détection de sonde %s dans le dossier %s" % (Type, directory))
    liste_file = sorted(glob.glob(f"{directory}/*{Type}*"))

    SONDE = []
    if len(liste_file) == 0:
        logging.warning("Pas de fichier %s pour le site de %s" % (Type, name))
    else:
        SONDE = read_data_files(
            Type,
            site,
            liste_file,
            unique_sonde=unique_sonde,
        )
    return SONDE


def read_data_files(
    probe_type: str,
    site: str,
    liste_file: List[str],
    unique_sonde: bool = False,
) -> List:
    """
    Iterate over liste_file to read raw data
    Arguments:
        probe_type: The type of data to read.
        site: The site to read data from.
        liste_file: A list of file names to read data from.
        unique_sonde: A flag indicating whether to return a single SondeData object or multiple objects. Default is False.
    Returns:
        A list of SondeData objects containing the raw data read from the files.
    """
    S = []
    if unique_sonde:
        name = conf_sites[site]["name"]
        S.append(sonde.SondeData(name, probe_type, site))
    else:
        n_sonde = []

    for fic in liste_file:
        try:
            ext = os.path.splitext(fic)[1]
            if ext.lower() == ".xls" or ext.lower() == ".xlsx":
                probe, data = reader.read_excel(
                    fic,
                    variables=conf_sondes[probe_type]["variables"],
                    unique_sonde=unique_sonde,
                )
            else:
                probe, data = reader.read_raw_file(fic, probe_type)
        except (IndexError, ValueError, AttributeError):
            logging.error("Impossible de lire le fichier %s" % fic)
            probe, data = reader.read_raw_file(fic, probe_type)
            data = pd.DataFrame()
            continue

        # Vérification du bon format des dates
        if not isinstance(data.index, pd.DatetimeIndex):
            logging.error("Impossible de lire les dates du fichier %s" % fic)
            data = pd.DataFrame()
            continue

        # Concaténation des données lues
        if unique_sonde:
            s = 0
        else:
            if probe.lower() not in set(n_sonde):
                n_sonde.append(probe.lower())
                S.append(
                    sonde.SondeData(
                        probe.split("_")[1].upper(),
                        probe_type,
                        site,
                    )
                )
            s = n_sonde.index(probe.lower())
        S[s].raws = pd.concat([S[s].raws, data], axis=0, sort=False)

    return S


def read_excel(filename: str, variables: list = None, unique_sonde: bool = False) -> tuple:
    """
    Read data from an Excel file and extract necessary information such as the probe type, date, and hour.

    Args:
        filename (str): The path to the Excel file.
        variables (list): A list of variable names to extract from the Excel file. Default is None (all variables).
        unique_sonde (bool): A flag indicating whether to return a single SondeData object or multiple objects. Default is False.

    Returns:
        tuple: A tuple containing the probe type and the cleaned DataFrame.
    """
    logging.debug("%s" % filename)

    # Read the entire file
    wb = pd.read_excel(filename, header=None)

    # Extract necessary information
    val = wb.iloc[0, 0]
    try:
        val = " ".join(val.split())  # Replace double spaces with single spaces
        probe = val.replace(" ", "_")  # Replace spaces with underscores
        probe.split("_")[1]  # Check if the format is Type_Num
    except IndexError as e:
        if not unique_sonde:
            logging.error("Incorrect sonde number format for file %s" % filename)
            logging.exception(e)
        probe = "STPS"

    # Find the index of 'Date' and 'Hour' in the header
    header_row = wb.iloc[1, :]

    try:
       indDatetime = header_row[header_row.notna()].tolist().index("Datetime")
    except ValueError:
        indDatetime = None

    if indDatetime is not None:
      names = ["Date",]
      cols = [indDatetime,]
    else:
      try:
        indDate = header_row[header_row.notna()].tolist().index("Date")
      except ValueError as e:
        logging.error("Unable to find 'Date' field in %s" % filename)
        logging.exception(e)
        return probe, pd.DataFrame()

      try:
        indHour = header_row[header_row.notna()].tolist().index("Heure")
      except ValueError as e:
        logging.error("Unable to find 'Hour' field in %s" % filename)
        logging.exception(e)
        return probe, pd.DataFrame()

      names = ["Date", "Hour"]
      cols = [indDate, indHour]

    if variables is None:
        variables = header_row[header_row.notna()].tolist()[2:]
    for v in variables:
        for i, y in enumerate(header_row):
            try:
                var = y.split(" ")[0][:4].lower()
                uni = y.split("(")[1].split(")")[0].lower()
            except (IndexError, AttributeError):
                continue

            if var in conf_vars[v]["list_header"] and uni in conf_vars[v]["list_units"]:
                names.append(v)
                cols.append(i)
                break

    # Extract the data
    data = wb.iloc[2:, cols].copy()
    data.columns = names
    num_rows = data.shape[0]

    # Conversion des dates
    if indDatetime is not None:
      data = data.set_index("Date")
    else:
      type_mapping = {
          (dt.datetime, dt.time): lambda row: dt.datetime.combine(row["Date"].date(), row["Hour"]),
          (str, str): lambda row: dt.datetime.strptime(row["Date"] + " " + row["Hour"], "%d/%m/%Y %H:%M:%S"),
          (dt.datetime, str): lambda row: dt.datetime.combine(
              row["Date"], dt.datetime.strptime(row["Hour"], "%H:%M:%S").time()
          ),
      }

      parse_date = type_mapping.get((type(data["Date"].iloc[0]), type(data["Hour"].iloc[0])), None)
      if parse_date is None:
          logging.error("Unable to convert date in file %s" % filename)
          logging.error("%s - %s" % (type(data["Date"][0]), type(data["Hour"][0])))
          return probe, pd.DataFrame()

      data["Date"] = data[["Date", "Hour"]].apply(parse_date, axis=1)
      data = data.set_index(data["Date"]).drop(["Date", "Hour"], axis=1)

    data = data.loc[pd.notnull(data.index)]

    # Conversion en float
    data = data.replace(",", ".", regex=True)
    data = data.apply(lambda x: pd.to_numeric(x, errors="coerce") if x.dtype == "O" else x)

    if float(len(data)) / num_rows < 0.1:
        logging.warning("%s %s : less than 10%% of data read %d / %d" % (probe, filename, len(data), num_rows - 1))

    return probe, data


def get_serialnumber(fic: str, probe_type: str) -> str:
    """
    Get the serial number of a probe based on the file name and probe type.

    Args:
        fic (str): The file name.
        probe_type (str): The probe type.

    Returns:
        str: The serial number of the probe.
    """
    if probe_type not in fic:
        raise ValueError("`fic` does not contain `probe_type`")

    probe = f'{probe_type}{fic.split(".")[-2].split(probe_type)[-1]}'.replace("-", "_")
    return probe


def read_raw_file(fic: str, probe_type: str) -> tuple:
    """
    Read raw data files from different types of probes.

    Args:
        fic (str): The file name of the raw data file.
        probe_type (str): The type of the probe.

    Returns:
        tuple: The probe serial number and the data read from the file.
    """
    probe = get_serialnumber(fic, probe_type)
    mapping = {
        "SMATCH": read_raw_nke,
        "MPx": read_raw_nke,
        "STPS": read_raw_nke,
        "SP2T": read_raw_nke,
        "SAMBA": read_raw_nke,
        "WiSens": read_raw_nke_wiprobe,
        "WiMo": read_raw_nke_wiprobe,
        "SeapHox": read_raw_seaphox,
        "HydroCAT": read_raw_hydrocat,
        "SBE": read_raw_seabird,
        "OTT": read_raw_ott,
        "CTD-Diver": read_raw_ctd_diver,
        "FLNTU": read_raw_eco_flntu,
        "RBR": read_raw_rbr,
    }

    if probe_type in mapping:
        read_function = mapping[probe_type]
        probe_tmp, data = read_function(fic, variables=conf_sondes[probe_type]["variables"])
        if probe_tmp is not None:
            probe = probe_tmp
    else:
        logging.error("No raw function to read the file %s" % fic)
        return None, None

    return probe, data


def read_raw_nke_wiprobe(filename: str, variables: list = conf_vars.keys()) -> tuple:
    """
    Read raw files from NKE WiProbe probe - WiSens or WiMo

    Args:
        filename (str): The name of the raw data file to be read.
        variables (list): A list of variables to be extracted from the file.

    Returns:
        tuple: The serial number of the probe and the parsed data as a DataFrame.
    """
    logging.debug("%s" % filename)
    try:
        with open(filename, "r", encoding="iso-8859-1") as f:
            lines = f.readlines()
    except OSError:
        with open(filename, "r") as f:
            lines = f.readlines()

    nrows = None
    for i, line in enumerate(lines):
        if line.startswith("#") and nrows is None:
            nrows = i - 2
        sep = '"'
        if line.startswith('# ;"<'):
            sep += '"'
        if "eH0" in line:
            probe = line.split("eH0=" + sep)[1].split(sep)[0].split()[0]
        if "eH2" in line:
            num = line.split("eH2=" + sep)[1].split(sep)[0].split("x")[1].upper()
        if "EXPORT" in line:
            date_fmt = line.split("eX0=" + sep)[1].split("(")[1].split(")")[0]
            if date_fmt == "yyyy-mm-dd hh:mm:ss":
                date_fmt = "%Y-%m-%d %H:%M:%S"
            decim = line.split("eX1=" + sep)[1].split(sep)[0]
            delim = line.split("eX2=" + sep)[1].split(sep)[0]
            if len(delim) > 1:
                delim = ";"
            elif delim == "t":
                delim = "\t"

    probe = f"{probe}_{num}"

    # Détection des voies (type, unité et index)
    header = ["Date"]
    cols = [0]
    for ind, l in enumerate(lines[0].split(delim)[1:]):
        var = l.split(":")[1].split("(")[0].lower()[:4]
        uni = l.split(":")[1].split("(")[1].split(")")[0].lower()
        for v in variables:
            if "list_header" not in conf_vars[v]:
                continue
            if var in conf_vars[v]["list_header"] and uni in conf_vars[v]["list_units"]:
                header.append(v)
                cols.append(ind + 1)
                break

    headtmp = {}
    for idx, h in enumerate(header):
        if h not in headtmp:
            headtmp[h] = idx
    if len(header) != len(headtmp):
        logging.warning(f"Duplicate columns in {filename}")
        header = list(headtmp.keys())
        indices = list(headtmp.values())
        cols = [cols[ind] for ind in indices]

    data = pd.read_csv(
        filename,
        skiprows=1,
        delimiter=delim,
        decimal=decim,
        header=None,
        names=header,
        usecols=cols,
        nrows=nrows,
    )

    data.dropna(axis=0, how="all", inplace=True)
    ind = ~data.iloc[:, 0].str.startswith("#")
    data = data[ind]

    data["Date"] = pd.to_datetime(data["Date"], format=date_fmt, errors="raise")
    data = data.set_index(data["Date"]).drop(["Date"], axis=1)

    data = data.apply(pd.to_numeric, errors="coerce")

    return probe, data


################################################################


def read_raw_nke(filename: str, variables: list = conf_vars.keys()) -> tuple:
    """
    Read raw files from NKE probe (downloaded from Windows)
    Several cases :
        1) Smatch sent files : '_mesures_' must be in the name
        2) WinMemo 2 ';' separator
        3) WinMemo 2 '\t' separator
    Args:
        filename (str): The name of the raw data file to be read.
        variables (list): A list of variables to be extracted from the file.
    Returns:
        tuple: A tuple containing None (as there is no probe serial number for NKE probes) and the parsed data as a DataFrame.
    """
    logging.debug("%s" % filename)
    with open(filename, "r", encoding="iso-8859-1") as f:
        lines = f.readlines()

    # Detect probe type
    if "_mesures_" in filename:
        names = ["Date"]
        cols = [0]
        ind = 1
        while "V:" in lines[ind]:
            var = lines[ind].split(",")[1][:4].lower()
            uni = lines[ind].split(",")[2].strip().lower()
            for v in variables:
                if var in conf_vars[v]["list_header"] and uni in conf_vars[v]["list_units"]:
                    names.append(v)
                    cols.append(ind)
                    break
            ind += 1
    else:
        names = []
        cols = []
        ind = 2
        while "VOIE:" in lines[ind]:
            var = lines[ind].split()[1][:4].lower()
            uni = lines[ind].split("=")[1].strip().lower()
            for v in variables:
                if var in conf_vars[v].get("list_header", '') and uni in conf_vars[v].get("list_units"):
                    names.append(v)
                    cols.append(ind - 2)
                    break
            ind += 1
        names.append("Date")
        cols.append(ind - 2)

    # Read data
    if "_mesures_" in filename:
        names = [names[i] for i in sorted(range(len(cols)), key=lambda k: cols[k])]
        data = pd.read_csv(
            filename,
            skiprows=ind,
            delimiter=" ",
            decimal=".",
            header=None,
            names=names,
            usecols=cols,
            dtype={"Date": "str"},
        )
        data["Date"] = pd.to_datetime(
            data["Date"],
            format="%d%m%y%H%M%S",
            errors="ignore",
        )
    else:
        # Detect separator
        if ";" in lines[ind]:
            names = [names[i] for i in sorted(range(len(cols)), key=lambda k: cols[k])]
            data = pd.read_csv(
                filename,
                skiprows=ind,
                delimiter=";",
                decimal=",",
                header=None,
                names=names,
                usecols=cols,
                encoding="iso-8859-1",
            )
            data["Date"] = pd.to_datetime(
                data["Date"],
                format="%d/%m/%Y\t%H:%M:%S",
                errors="raise",
            )
        else:
            names.append("Heure")
            cols.append(ind - 1)
            names = [names[i] for i in sorted(range(len(cols)), key=lambda k: cols[k])]
            data = pd.read_csv(
                filename,
                skiprows=ind,
                delimiter=r"\s+",
                decimal=",",
                header=None,
                names=names,
                usecols=cols,
                encoding="iso-8859-1",
            )
            try:
                data["Date"] = pd.to_datetime(
                    data["Date"] + " " + data["Heure"],
                    format="%m/%d/%y %H:%M:%S",
                    errors="raise",
                )
            except ValueError:
                data["Date"] = pd.to_datetime(
                    data["Date"] + " " + data["Heure"],
                    format="%d/%m/%Y %H:%M:%S",
                    errors="raise",
                )
            data = data.drop(["Heure"], axis=1)

    # Set Date as index
    data = data.set_index(data["Date"]).drop(["Date"], axis=1)

    # Convert to float (in case decimal separator is '.')
    data = data.apply(pd.to_numeric, errors="coerce")
    return None, data


################################################################


def read_raw_ott(filename, variables=conf_vars.keys()):
    """Read raw files from Hydrolab probe (downloaded from Windows)"""
    logging.debug("%s" % filename)
    f = open(filename, "r", encoding="iso-8859-1")
    lines = f.readlines()
    f.close()

    # Détection des voies (type, unité et index)
    vari = lines[12].split(",")
    unit = lines[13].split(",")
    ind = 2
    cols = [0, 1]
    names = ["Date", "Hour"]
    for i in range(len(vari) - 2):
        var = vari[ind].strip('"').lower()
        uni = unit[ind].strip('"').lower()
        for v in variables:
            if var in conf_vars[v]["list_header"] and uni in conf_vars[v]["list_units"]:
                names.append(v)
                cols.append(i + 2)
                break
        ind += 1

    # Lecture des données
    data = pd.read_csv(
        filename,
        skiprows=16,
        delimiter=",",
        header=None,
        names=names,
        usecols=cols,
    )

    # Détection des pannes piles
    ind = ~(data.Date.str.contains("Power loss") | data.Date.str.contains("Late probe"))
    data = data[ind]

    # Création de l'index Date
    def parse_date(row):
        return dt.datetime.strptime(
            row["Date"] + " " + row["Hour"],
            "%d/%m/%Y %H:%M:%S",
        )

    data["Date"] = data[["Date", "Hour"]].apply(parse_date, axis=1)
    data = data.set_index(data["Date"]).drop(["Date", "Hour"], axis=1)

    return None, data


################################################################


def read_raw_seaphox(filename: str, variables: list = conf_vars.keys()) -> tuple:
    """
    Read raw files from SeapHox probe (SeaBird)

    Args:
        filename (str): The name of the raw data file.
        variables (list): Optional. The list of variables to be extracted from the file. Defaults to all variables defined in `conf_vars`.

    Returns:
        tuple: The probe serial number and the data read from the file, with the "Date" column set as the index.
    """

    probe = "SeapHox_" + filename.split("-")[1][-4:]

    logging.debug(filename)
    with open(filename, "r", encoding="iso-8859-1") as f:
        lines = f.readlines()

    header = ["Date"]
    cols = [1]
    for ind, l in enumerate(lines[0].split(",")[1:]):
        var = l.split("(")[0].lower()[:4]
        uni = l.split("(")[1].split(")")[0].lower()
        for v in variables:
            if var in conf_vars[v]["list_header"] and uni in conf_vars[v]["list_units"]:
                header.append(v)
                cols.append(ind + 1)
                break

    data = pd.read_csv(
        lines,
        skiprows=1,
        delimiter=",",
        decimal=".",
        header=None,
        names=header,
        usecols=cols,
    )

    data["Date"] = pd.to_datetime(data["Date"], format="%m/%d/%Y %H:%M:%S", errors="raise")
    data.set_index("Date", inplace=True)

    return probe, data


################################################################


def read_raw_hydrocat(filename: str, variables: list = conf_vars.keys()) -> tuple:
    """
    Read raw files from Hydrocat probe (SeaBird)

    Args:
        filename (str): The name of the raw data file.
        variables (list): Optional. The list of variables to be extracted from the file. Defaults to all variables defined in `conf_vars`.

    Returns:
        tuple: The probe serial number and the data read from the file, with the "Date" column set as the index.
    """

    if "HydroCAT-EP" in filename:
        probe = "HydroCAT_" + filename.split("-")[2][-4:]

    logging.debug(filename)
    with open(filename, "r", encoding="iso-8859-1") as f:
        lines = f.readlines()

    header = ["Date"]
    cols = [1]
    for ind, l in enumerate(lines[0].split(",")[1:]):
        if '(' not in l:
            break
        var = l.split("(")[0].lower()[:4]
        uni = l.split("(")[1].split(")")[0].lower()
        for v in variables:
            if var in conf_vars[v]["list_header"] and uni in conf_vars[v]["list_units"]:
                if 'std' in l.lower(): v += '_std'
                header.append(v)
                cols.append(ind + 1)
                break
    
    data = pd.read_csv(
        # lines,
        filename,
        encoding="iso-8859-1",
        skiprows=1,
        delimiter=",",
        decimal=".",
        header=None,
        names=header,
        usecols=cols,
    )

    data["Date"] = pd.to_datetime(data["Date"], format="%m/%d/%Y %H:%M:%S", errors="raise")
    data.set_index("Date", inplace=True)

    data = data.apply(lambda x: pd.to_numeric(x, errors="coerce") if x.dtype == "O" else x)

    return probe, data
################################################################


def read_raw_seabird(filename: str, variables=conf_vars.keys()) -> tuple:
    """
    Reads a raw seabird file and returns the data and probe name.

    Args:
        filename (str): The name of the file to read.

    Returns:
        tuple: A tuple containing the data (pandas DataFrame) and probe name (str).
    """
    # Extract the probe name from the filename
    probe = f"SBE_{filename.split('_')[-4]}"

    logging.debug("%s" % filename)

    # Read all the lines from the file
    with open(filename, "r", encoding="us-ascii") as f:
        lines = f.readlines()

    # Define a dictionary to map column names in the file to desired column names
    names = {
        "depSM": "PROF",
        "tv290C": "TEMP",
        "cond0mS/cm": "COND",
        "sal00": "PSAL",
        "sbeopoxPS": "OSAT",
        "sbeopoxML/L": "DOX1",
        "timeK": "timeK",
    }

    cols = []
    header = []
    for ind, line in enumerate(lines, 1):
        if line.startswith("# name"):
            name = line.split("= ")[1].split(":")[0]
            if name in names:
                cols.append(int(line[7]))
                header.append(names[name])
        if line.startswith("*END*"):
            break

    # Read the data from the file into a DataFrame
    with open(filename, "r", encoding="us-ascii") as f:
        data = pd.read_table(
            f,
            skiprows=ind,
            header=None,
            names=header,
            usecols=cols,
            sep='\s+',
        )

    # Set the index of the DataFrame to be a datetime object
    data.index = [dt.datetime(2000, 1, 1) + dt.timedelta(seconds=s) for s in data.timeK]
    data.drop("timeK", axis=1, inplace=True)

    return probe, data


################################################################


def read_raw_eco_flntu(filename: str, conf_eco: dict = conf_EcoFLNTU, variables=conf_vars.keys()) -> pd.DataFrame:
    """
    Read raw files from Wetlab ECO FLNTU probe and process the data.

    Args:
        filename (str): The name of the raw data file to be read.
        conf_eco (dict): Configuration values for the specific serial number.

    Returns:
        pd.DataFrame: The cleaned and processed data from the raw file.
    """
    logging.debug("%s" % filename)

    # Get serial number
    SerialNum = filename[-8:-4]

    # Define header names and which columns need to be read
    header = ["Date", "Heure", "CHLO", "TURB"]
    usecol = [0, 1, 3, 5]

    # Read raw file
    data = pd.read_csv(
        filename,
        skiprows=1,
        header=None,
        usecols=usecol,
        names=header,
    )

    # Delete bad values ('ext','mvs 0',...)
    data = data.dropna(subset=["CHLO"])

    # Convert to physical units
    for v in ("CHLO", "TURB"):
        data[v] = conf_eco[SerialNum][v][0] * (data[v] - conf_eco[SerialNum][v][1]) * conf_eco[SerialNum][v][2]

    # Creation of Date index
    data["Date"] = pd.to_datetime(
        data["Date"] + " " + data["Heure"],
        format="%m/%d/%y %H:%M:%S",
        errors="raise",
    )
    data = data.set_index(data["Date"]).drop(["Date", "Heure"], axis=1)

    # Averaging of bursts within a frame
    data.index = data.index + dt.timedelta(seconds=30)
    avg = data.resample("1Min").mean()
    std = data.resample("1Min").std()
    std.add_suffix("_std", inplace=True)

    # Keep standard deviation values and drop NaN values
    data = pd.merge(avg, std, left_index=True, right_index=True).dropna(axis=0, how="all")

    return None, data


################################################################


def read_raw_ctd_diver(filename):
    probe = "CTD-Diver_" + filename.split("_")[-1][:-4]

    logging.debug("%s" % filename)
    f = open(filename, "r", encoding="iso-8859-1")
    lines = f.readlines()
    f.close()

    spec_cond = False
    ind = 1
    for lin in lines:
        if "SPEC.COND." in lin:
            spec_cond = True
        if lin.startswith("[Data]"):
            ind += 1
            break
        ind += 1

    header = ["Date", "Heure", "PROF", "TEMP", "COND"]
    data = pd.read_csv(
        filename,
        skiprows=ind,
        header=None,
        usecols=[0, 1, 2, 3, 4],
        names=header,
        sep=r"\s+",
        encoding="iso-8859-1",
        decimal=".",
    )

    data["Date"] = pd.to_datetime(
        data["Date"] + " " + data["Heure"],
        format="%Y/%m/%d %H:%M:%S.0",
        errors="coerce",
    )
    data = data[data.Date.notnull()]
    data = data.set_index(data["Date"]).drop(["Date", "Heure"], axis=1)

    data = data.apply(pd.to_numeric, errors="coerce")
    data.PROF = data.PROF / 100.0 - 10.13
    if spec_cond:
        data.COND = data.COND * (1.0 + 0.0191 * (data.TEMP - 25.0))

    return probe, data


################################################################


from pyrsktools import RSK

def read_raw_rbr(filename: str, variables=conf_vars.keys()) -> tuple:

    rsk = RSK(filename)
    rsk.open()
    rsk.readdata()

    probe = rsk.instrument.model + '_%d'%rsk.instrument.serialID
    probe = probe.replace('³','')

    if 'pressure' in rsk.channelNames:
        rsk.deriveseapressure(patm=10.1325)
        rsk.derivedepth()

    if 'conductivity' in rsk.channelNames: 
        rsk.derivesalinity()

    data = pd.DataFrame(rsk.data)
    rsk.close()

    column_mapping = {
        'timestamp': 'Date',
        'temperature': 'TEMP',
        'sea_pressure': 'PRES',
        'depth': 'PROF',
        'conductivity': 'COND',
        'salinity': 'PSAL'
    }
    columns_to_keep = [column_mapping[col] for col in column_mapping if col in data.columns]
    data.rename(columns=column_mapping, inplace=True)
    data = data[columns_to_keep]
    #for old_name, new_name in column_mapping.items():
    #    if old_name in data.columns:
    #        data.rename(columns={old_name: new_name}, inplace=True)
    data.set_index('Date', inplace=True)

    return probe, data

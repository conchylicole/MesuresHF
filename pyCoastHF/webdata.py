# -*- coding: utf-8 -*-
"""
Module de préparation et de transformation des données pour le Web

Created on Wed Apr 09 10:00:00 2014
@author: S. Petton

=> Permet la création des fichiers json à mettre sur
    \\tera5/conchy/velyger/donnees/YYYY/
    \\tera5/conchy/resco/donnees/YYYY/
   1. donnees journalières (regroupement du maximum de source d'informations)
   2. donnees ponctuelles lors des Passsages/Prélèvement/Echantillons pour VELYGER
   3. donnees Haute fréquence (partie TECHNIQUE)

"""

import datetime as dt
import logging as log
import os

import numpy as np
import pandas as pd
from xlrd import open_workbook

from . import common

# ---------------------------------------
#          Variables locales
# ---------------------------------------
config = common.data_config("sondes")
conf_sites = common.data_config("sites")
conf_vars = common.data_config("variables")
machineSFTP = 'datarmor.ifremer.fr'

path_data_local = './donnees_siteweb_local'
# Disque où seront stockées les données Web = \\tera5\conchy -> \\iota1\conchy
path_data_web = '/home/wwwinter/www/htdocs/depot/observatoire_conchylicole'


def loop_year(date_start, date_end, year=None):
    """Retourne les années de départ et de fin
    sauf si 'year' est défini
    """
    if year is None:
        return date_start.year, date_end.year
    else:
        return year, year


def merge_data(SONDES, variables, date_start, date_end, year=None):
    """A partir d'un groupe de sonde, renvoie une valeur journalière
       year : forcer l'année de mise à jour
    Retourne une liste de tableau par années où la 1ère colonne est le jour julien
    """
    y1, y2 = loop_year(date_start, date_end, year=year)

    data = []
    if len(SONDES) > 0:

        # Regroupement des données de sonde dans une dataframe commune
        df = SONDES[0].data
        for s in SONDES[1:]:
            if s.Type != 'MANUEL':
                df = df.combine_first(s.data)
        # Moyenne journalière
        df = df.resample('D').mean()

        # On ne garde que les variables nécessaires
        for v in list(df):
            if v not in variables:
                df.drop(v, axis=1, inplace=True, errors='ignore')

        # On ré-oriente "TEMP" puis "PSAL"
        try:
            df = df[['TEMP', 'PSAL', 'FLUO', 'TURB']]
        except:
            df = df[['TEMP', 'PSAL']]

        # Un fichier par année
        for y in np.arange(y1, y2 + 1):
            if y == y1:
                d_deb = date_start
                d_fin = dt.datetime(date_start.year, 12, 31)
            if y == y2:
                d_deb = dt.datetime(date_end.year, 1, 1)
                # Tdays = np.arange(1, date_end.timetuple()[7] + 1)
            if y1 == y2:
                d_deb = date_start
                d_fin = date_end
            if year is not None:
                d_deb = max(date_start, dt.datetime(year, 1, 1))
                d_fin = min(date_end, dt.datetime(year, 12, 31))

            # sauvegarde
            df[d_deb:d_fin].round(2).to_pickle('%s/%s_%d.pkl'%(path_data_local, SONDES[0].site, d_deb.year))

            # Vecteur temps en numéro du jour de l'année
            # Tdays = np.arange(d_deb.timetuple()[7], d_fin.timetuple()[7] + 1)
            tps = pd.date_range(d_deb, d_fin, freq='D', inclusive='left')

            df_y = df.reindex(tps)
            data.append(df_y.set_index(df_y.index.dayofyear).reset_index().values)

    else:

        # Initialisation obligatoire pour les graphes
        for y in np.arange(y1, y2 + 1):
            data.append(np.zeros((1, len(variables) + 1)))

    return data


def read_normale_temp_xls(fic, site):
    try:
        wb = open_workbook(fic)
    except IOError:
        log.error("Le fichier %s n'est pas au format Excel ou n'est pas present" % fic)
        return np.zeros((1, 2))
    sh = wb.sheet_by_index(0)  # sheet_by_name()
    num_rows = sh.nrows - 1

    try:
        day = np.arange(1, sh.nrows)
        tmp = np.empty((sh.nrows - 1,))
        tmp[:] = np.NAN
        ind = [i for i, y in enumerate(sh.row_values(0)) if conf_sites[site]['code_quadrige'] in y][0]
        cur_row = 0
        while cur_row < num_rows:
            try:
                tmp[cur_row] = float(sh.cell_value(cur_row + 1, ind).replace(',', '.'))
            except AttributeError:
                tmp[cur_row] = float(sh.cell_value(cur_row + 1, ind))
            except ValueError:
                pass
            cur_row += 1
    except IndexError:
        log.info('Pas de fichier normale pour %s' % site)
        return np.zeros((1, 2))

    return np.concatenate((day[:, np.newaxis], tmp[:, np.newaxis]), axis=1)


def create_entete_json(f, var, date_start='', date_end='', description='', google_charts=False):
    f.write('"entete" : {\n')
    f.write('"titre" : "Résultats d\'observations des stations SMATCH / STPS" ,\n')
    f.write('"description" : "%s" ,\n' % description)
    f.write('"institution" : "IFREMER" ,\n')
    f.write('"projet" : "RESCO / VELYGER" ,\n')
    f.write('"date_debut_suivi" : "%s" ,\n' % date_start)
    f.write('"date_fin_suivi" : "%s" ,\n' % date_end)
    f.write('"licence" : "" ,\n')
    f.write('"nom_contributeur" : "" ,\n')
    f.write('"nom_responsable_editorial" : "Stéphane Pouvreau" ,\n')
    f.write('"email_responsable_editorial" : "Stephane.Pouvreau@ifremer.fr" ,\n')
    f.write('"nom_parametre_court" : "%s" ,\n' % var)
    f.write('"nom_parametre" : "%s (%s) de l\'eau"\n' % (conf_vars[var]['longname'], conf_vars[var]['units']))
    f.write('}\n\n')
    if not google_charts:
        f.write(', "donnees" : {\n')
    else:
        f.write(',\n')


def write_json_element(f, x, y, units):
    if np.isnan(y).all():
        f.write('{"x":0, "y":0, "tip": "#val#%s"}\n' % units)
    else:
        ind = 0
        for i in range(len(x)):
            if not np.isnan(y[i]):
                ind += 1
                f.write('{"x":%d, "y":%.1f, "tip": "#val#%s"}' % (x[i], y[i], units))
            if ind > 0 and i != len(x) - 1 and not np.isnan(y[i + 1]):
                f.write(', ')
            if i == len(x) - 1:
                f.write('\n')


def get_entete_dates(y, y1, y2, date_start, date_end):
    deb = dt.datetime(y, 1, 1, 0, 0, 0)
    fin = dt.datetime(y, 12, 31, 0, 0, 0)
    if y == y1:
        deb = date_start.replace(hour=0, minute=0, second=0)
        fin = date_start.replace(month=12, day=31, hour=0, minute=0, second=0)
    if y == y2:
        deb = date_end.replace(month=1, day=1, hour=0, minute=0, second=0)
        fin = date_end.replace(hour=0, minute=0, second=0)
    if y1 == y2:
        deb = date_start.replace(hour=0, minute=0, second=0)
        fin = date_end.replace(hour=0, minute=0, second=0)
    return deb, fin


def data2json_daily(sites, variables_web, webdata, date_start, date_end, year=None, transfert=''):
    """Ecrit le fichier json
    -> pour chaque annee
     -> pour chaque variable
        1. creation du fichier et de l'entete
        2. -> pour chaque site
           a) lecture de la normale de température
           b) Ecriture des valeurs
        3. Transfert le fichier sur 'path_data_web'
    """
    y1, y2 = loop_year(date_start, date_end, year=year)

    # Recuperation des normales de temperature
    if 'TEMP' in variables_web:
        normale_temp = []
        for s, site in enumerate(sites):
            normale_temp.append(read_normale_temp_xls(path_data_local + '/Temperature_Normales.xls', site))

    for y in np.arange(y1, y2 + 1):
        if not os.path.exists(path_data_local + '/%s' % y):
            os.makedirs(path_data_local + '/' + '%s' % y)

    description = 'Moyenne journalière des données haute fréquence'
    for i, v in enumerate(variables_web):
        json_daily = common.supprime_accent(conf_vars[v]['longname']) + 'HF'
        for j, y in enumerate(np.arange(y1, y2 + 1)):
            fic = path_data_local + '/' + '%s/%s.json' % (y, json_daily)
            log.debug('Creation du fichier %s' % fic)

            f = open(fic, 'w')
            f.write('{ \n\n')

            deb, fin = get_entete_dates(y, y1, y2, date_start, date_end)
            create_entete_json(f, v, date_start=deb, date_end=fin, description=description)

            for s, site in enumerate(sites):
                f.write('"%s" :{\n' % conf_sites[site]['code_quadrige'])
                if v == 'TEMP':
                    f.write('"normale":[\n')
                    write_json_element(f, normale_temp[s][:, 0], normale_temp[s][:, 1], conf_vars[v]['units'])
                    f.write('],\n')
                f.write('"elements":[\n')
                write_json_element(f, webdata[s][j][:, 0], webdata[s][j][:, i + 1], conf_vars[v]['units'])
                f.write(']\n')
                if s == len(sites) - 1:
                    f.write('}\n')
                else:
                    f.write('},\n')
            f.write('\n}\n}')
            f.close()

            if len(transfert) > 0:
                cmd = 'scp %s %s:%s/%s/donnees/%d/' % (fic, machineSFTP, path_data_web, transfert, y)
                common.call(cmd, verbose=False)


def create_cols_json(f, list_sondes):
    f.write('"cols": [')
    f.write('{"id":"","label":"Date","type":"date"},')
    ind = 0
    for i, s in enumerate(list_sondes):
        ind += 1
        f.write('{"id":"","label":"%s","type":"number"}' % s)
        if ind > 0 and i != len(list_sondes) - 1:
            f.write(', ')
    f.write('],\n')


def write_rows_json(f, dates, vars):
    v = vars.shape[1]
    for t, d in enumerate(dates):
        f.write('{"c":[{"v":"Date(%d,%d,%d,%d,%d,%d)"},' % (d.year, d.month - 1, d.day, d.hour, d.minute, d.second))
        for i in range(v):
            if vars[t, i] == 'NAN' or np.isnan(vars[t, i]):
                f.write('{"v":null}')
            else:
                f.write('{"v":%.2f}' % vars[t, i])
            if i != v - 1:
                f.write(',')
        if t != len(dates) - 1:
            f.write(']},')
        else:
            f.write(']}\n')


def data2json_hf(SONDES, variables_web, date_start, date_end, year=None, latest=False, transfert=''):
    """Ecrit le fichier json
     -> pour chaque annee
      -> pour chaque variable
         1. creation du fichier et de l'entete
         2. -> pour chaque sonde
            a) Ecriture des valeurs
    latest : 30 derniers jours toutes les 15 minutes
    """
    if len(SONDES) == 0:
        return
    site = SONDES[0].site
    description = 'Données haute fréquence'

    y1, y2 = loop_year(date_start, date_end, year=year)

    # Création des répertoires
    for y in np.arange(y1, y2 + 1):
        for v in variables_web:
            json_daily = common.supprime_accent(conf_vars[v]['longname']) + 'HF'
            if not os.path.exists(path_data_local + '/%d/%s' % (y, json_daily)):
                os.makedirs(path_data_local + '/%s/%s' % (y, json_daily))
            if latest and not os.path.exists(path_data_local + '/%s/%s/latest' % (y, json_daily)):
                os.makedirs(path_data_local + '/%s/%s/latest' % (y, json_daily))

    # Moyenne sur X minutes
    if latest:
        tps = pd.date_range(date_start, date_end, freq='15min', closed='left')
    else:
        tps = pd.date_range(date_start, date_end, freq='H', closed='left')

    # Pour chacune des variables
    for i, v in enumerate(variables_web):
        json_daily = common.supprime_accent(conf_vars[v]['longname']) + 'HF'
        hasData = np.empty(
            (
                len(
                    SONDES,
                )
            ),
            dtype=bool,
        )
        hasData[:] = True

        # Concaténation des valeurs de toutes les sondes ré-indexées
        res = pd.DataFrame()
        list_sondes = []
        for r, s in enumerate(SONDES):
            list_sondes.append(s.Type + ' ' + s.Name)
            if v in list(s.data):
                if s.data[v].isnull().all():
                    hasData[r] = False
                else:
                    tmp = s.data[v].reindex(tps, method='nearest', limit=1)
                    res = pd.concat([res, tmp], axis=1)
        list_sondes = np.array(list_sondes)

        list_sondes = list_sondes[hasData]
        hasData = hasData[hasData]

        for j, y in enumerate(np.arange(y1, y2 + 1)):
            if not latest:
                tmp = res[dt.datetime(y, 1, 1) : dt.datetime(y + 1, 1, 1)]  # noqa : E203
            else:
                tmp = res.copy()

            # Suppression des pas de temps où toutes les variables sont NaN
            tmp = tmp.dropna(axis=0, how='all')

            # Suppression des sondes où toutes les variables sont NaN
            for r in range(tmp.shape[1]):
                if tmp.iloc[:, r].isnull().all():
                    hasData[r] = False
            tmp.dropna(axis=1, how='all', inplace=True)
            list_sondes = list_sondes[hasData]

            if tmp.empty:
                log.info('Pas de données pour %s pour le site %s' % (v, site))
                continue

            # Passage par dates et vars...
            dates = tmp.index
            vari = tmp.values

            # Création du fichier de manière "archaïque"
            if latest:
                fic = path_data_local + '/%d/%s/latest/%s.json' % (y, json_daily, conf_sites[site]['code_quadrige'])
            else:
                fic = path_data_local + '/%d/%s/%s.json' % (y, json_daily, conf_sites[site]['code_quadrige'])
            log.debug('Création du fichier %s' % fic)

            f = open(fic, 'w')
            f.write('{ \n\n')
            deb, fin = get_entete_dates(y, y1, y2, date_start, date_end)
            create_entete_json(f, v, date_start=deb, date_end=fin, description=description, google_charts=True)
            create_cols_json(f, list_sondes)
            f.write('"rows": [\n')
            write_rows_json(f, dates, vari)
            f.write(']\n}')
            f.close()

            if len(transfert) > 0:
                if latest:
                    cmd = 'scp %s %s:%s/%s/%s/' % (fic, machineSFTP, path_data_web, transfert, json_daily)
                else:
                    cmd = 'scp %s %s:%s/%s/donnees/%d/%s/' % (fic, machineSFTP, path_data_web, transfert, y, json_daily)
                common.call(cmd, verbose=False)


def sonde2json_hf(SONDES, variables, date_start, date_end, y=None, filename=None):
    """
    Ecrit le fichier json
    -> pour chaque annee
      -> pour chaque sonde
      Créer un fichier json avec les variables voulues
    """
    # TODO : servait pour la sonde YSI de Saint-Vincent !!!
    if len(SONDES) == 0:
        return

    y1, y2 = loop_year(date_start, date_end, year=y)

    for y in np.arange(y1, y2 + 1):
        if not os.path.exists(path_data_local + '/%d' % y):
            os.makedirs(path_data_local + '/%s' % y)

    description = 'Données haute fréquence'
    for j, y in enumerate(np.arange(y1, y2 + 1)):
        for r, s in enumerate(SONDES):
            mask = (s.dates >= dt.datetime(y, 1, 1)) & (s.dates < dt.datetime(y + 1, 1, 1))

            datetmp = s.dates[mask]
            varstmp = s.varsf[mask, :]
            ind = []
            for v in variables:
                ind.append(s.vars_name.index(v))
            varstmp = varstmp[:, ind]

            if filename is None:
                filename = s.site + '_' + s.Type + '_' + s.Name.replace(' ', '_')

            fic = path_data_local + '/%d/%s.json' % (y, filename)
            log.debug('Creation du fichier %s' % fic)

            f = open(fic, 'w')
            f.write('{ \n\n')
            deb, fin = get_entete_dates(y, y1, y2, date_start, date_end)
            create_entete_json(f, v, date_start=deb, date_end=fin, description=description, google_charts=True)
            f.write('"cols": [')
            f.write('{"id":"","label":"Date","type":"date"},')
            for v in variables:
                f.write('{"id":"","label":"%s","type":"number"}' % (conf_vars[v]['longname']))
                if v != variables[-1]:
                    f.write(', ')
            f.write('],\n')
            f.write('"rows": [\n')
            write_rows_json(f, datetmp, varstmp)
            f.write(']\n}')
            f.close()

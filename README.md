# pyCoastHF
[![pipeline status](https://gitlab.ifremer.fr/conchylicole/MesuresHF/badges/main/pipeline.svg)]
[![coverage report](https://gitlab.ifremer.fr/conchylicole/MesuresHF/badges/main/coverage.svg)]
[![Latest Release](https://gitlab.ifremer.fr/conchylicole/MesuresHF/-/badges/release.svg)]

pyCoastHF is an Open Source Python package to treat time series data from coastal mooring buoys. It was designed to process environmental data from ECOSCOPA and CocoriCO2 coastal networks ([Ifremer](https://www.ifremer.fr)). It provides tools to analyze, filter, and consolidate time series data from various probes deployed at coastal mooring buoys.

## License
``pyCoastHF`` is under CeCILL Free Software License Agreement v2.1 ([Learn more](http://choosealicense.com/licenses/cecill-2.1/))


## Installation
```
git clone https://gitlab.ifremer.fr/conchylicole/MesuresHF.git
cd MesuresHF
pip install pyCoastHF

pip install -e .
```

## Project status
This package is still under development and certainly requires technical improvements: especially, the config_sondes.cfg file is part of the library and cannot be modified (except with development pip -e install pyCoastHF). 

## Authors
Sébastien Petton <<sebastien.petton@ifremer.fr>>

## Support
If you have trouble with some functions, open an issue at [GitLab issues](https://gitlab.ifremer.fr/conchylicole/MesuresHF/issues), and I'll try to help you.

***
---

## Library Purpose

This library processes measurements from various probes and applies filtering. It is based on the `SondeData` class, which handles probe data through methods for reading, transforming, analyzing, and saving. It supports environmental data such as **temperature**, **salinity**, and **oxygen concentration**.

### Key Features

- **Data Storage and Filtering**: Stores raw and flag data in Pandas DataFrames.
- **Probe Consolidation**: Creates a single consolidated dataset for each variable.
- **Manual Validation**: Includes a local GUI for re-qualifying data.

---

## Configuration Files

### `config_variables.cfg`

Defines parameters for detecting fields in raw data and applying general filtering rules. Example configuration for **temperature**:

```yaml
[[TEMP]]
  longname = 'Temperature'
  units  = '°C'
  list_header = ('TEMP', 'temp')
  list_units = ('degree_celsius', '°c', 'øc', 'celsius', 'degc', '∞c')
  cdoco = 'TEMP (degree_Celsius)'
  filtre = True
  V_max = 50
  V_min = 2.5
  dVmax = 0.8
  T53H = 0.25
  CLIM = 5.0
```

### `config_sites.cfg`

Defines configuration for each measurement site. Example configuration for **RO16**:

```yaml
[[RO16]]
  name = 'Anse du Roz'
  longitude = -4.3356
  latitude  = 48.3184
  platform = 'IF000700'
  sondes = ['MPx', 'STPS', 'OTT', 'SMATCH', 'FLNTU', 'SBE', 'WiSens', 'HydroCAT']
  vars = ['PROF', 'TEMP', 'PSAL', 'FLUO', 'TURB', 'OSAT', 'DOX1']
  [[[PSAL]]]
    dVmax = 3.5
```

### `config_sondes.cfg`

Defines variables associated with different probe models. Example:

```yaml
[[SONDES]]
  [[[STPS]]]
    variables = ['TEMP', 'PSAL', 'PROF', 'COND']
  [[[WiSens]]]
    variables = ['TEMP', 'PROF', 'PRES', 'COND', 'PSAL']
```

---

## Example Usage

### 1. Processing Probe Data

```python
site = 'BR08'

# Read local files
SONDES = []
for sonde in ['RBR', 'SMATCH', 'STPS', 'WiSens', 'FLNTU', 'SeapHox', 'SBE']:
    if sonde in conf_sites[site]['sondes']:
        SONDES += pyCoastHF.sonde.read_local_dir(Type=sonde, site=site, directory=f'Fichiers_Bruts/tmp_SONDES/{site}')

# Resampling and Filtering
for s in range(len(SONDES)):
    SONDES[s].sort_index()
    SONDES[s].resample_data()

# Apply Filtering
for s in range(len(SONDES)):
    SONDES[s].convert_variables()
    SONDES[s].apply_qc(date_str, date_end)
```

### 2. Creating a `GLOBAL` Probe

Users select the best probe for each variable, consolidating them into a `GLOBAL` `SondeData` object.

Example configuration for selecting periods:
```python
periods['TEMP'] = [
    ['STPS_33009', '2021/01/01 00:00', '2021/11/17 10:30', .1],
    ['STPS_32019', '2021/11/17 10:45', '2022/01/01 00:00', .1],
    ...
]
```

### 3. Manual Validation

Open the `scripts/qualification/ValidManuel.py` script and adapt the initial part for reading the right netCDF file. Then launch the validation interface using:

```bash
bokeh serve --show ValidManuel.py
```

Access the interface at [http://localhost:5006/ValidManuel](http://localhost:5006/ValidManuel). This is the exemple:

![Image](notebooks/ValidManuelExample.png)

***
---


## Automatic Quality Check as given by G.P. Castelão, 2021 - https://doi.org/10.1016/j.cageo.2021.104803
**Rate of Change**: Evaluates the change with respect to the previous measurement as  

$$
y_{ri} = x_i - x_{i-1}. \tag{1}
$$

**Gradient**: Evaluates the rate of change with respect to the neighboring points, defined as  

$$
y_{gi} = \left| x_i - \frac{x_{i+1} + x_{i-1}}{2} \right|. \tag{2}
$$

**Spike**: Evaluates how different a measurement is in comparison with its neighbors, defined as  
$$
y_{si} = y_{gi} - \left| \frac{x_{i+1} - x_{i-1}}{2} \right|, \tag{3}
$$
where $y_{gi}$ is the gradient given by Eq. (2).

**Tukey 53H**: Evaluates how different a measurement is in comparison to the low-frequency tendency of the data series. This feature takes advantage of the robustness of the median to create a
smoother data series that is used as reference (Goring and
Nikora, 2002), with the following procedure:

1. $x^{(1)}$ is the median of the five points from $x_{i-2}$ to $x_{i+2}$;  
2. $x^{(2)}$ is the median of the three points from $x^{(1)}_{i-1}$ to $x^{(1)}_{i+1}$;  
3. $x^{(3)}$ is defined by the Hanning smoothing filter:  
   $$
   x^{(3)}_i = \frac{1}{4} \left( x^{(2)}_{i-1} + 2x^{(2)}_i + x^{(2)}_{i+1} \right);
   $$
4. Finally:  
   $$
   y_{ti} = \frac{\left| x_i - x^{(3)}_i \right|}{\sigma}, \tag{4}
   $$
   where $\sigma$ is the standard deviation of the low-pass filtered data.

**Climatology**: Evaluates the bias between the observed measurement and a climatology, normalized by the expected variability at that point and time, using the relation:  
$$
y_{ci} = \frac{\left| x_i - \langle x_i \rangle \right|}{\sigma_i}, \tag{5}
$$
where $\langle x_i \rangle$ is the climatology, and $\sigma_i$ is the standard deviation of the observations used to create the climatology at position $i$.


# Development process

## Roadmap
- [x] Generic documentation
- [ ] Specific documentation
- [ ] Treatment process scheme
- [ ] pip or conda package
- [ ] Unit test on commit
- [ ] Include jupyther notebook for showing typical usage
- [x] Enable editing of config_sondes.cfg or add a local one
- [ ] Correct variable names according to Copernicus Marine in situ TAC (physical parameters list) https://doi.org/10.13155/53381
- [ ] Assure netCDF compatibility with http://cfconventions.org/cf-conventions/cf-conventions.html#time-series-data

## Continuous integration
At each commit (or release), several jobs are launched to assure code quality of the library (flake8, isort, unit tests, ...). 
1. Activate CI/CD in Settings -> General menu
2. Add .gitlab-ci.yml file
    - Define the main steps of pipeline (stages)
    - Job **build-dev-env** create 2 dockers (base & dev). It should not be necessary at each commit 
3. Creation of Docker in order to add several packages
    - Based on existing docker *gitlab-registry.ifremer.fr/ifremer-commons/docker/images/python*
    - Creation of correct environment with poetry (pyproject.toml and poetry.lock)

## Quality code requirements
Before each commit, pre-commit launches **autoflake8, isort, pycln, flake8**
1. Install pre-commit ` pip install pre-commit `
2. Get .pre-commit-config.yaml file
3. Register the hook ` pre-commit install `



#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
 Created on 06 Dec 2013
 Modified on 17 Nov 2016
 Adapted on 08 Sep 2017
 @author: S. Petton

 Usage:
./create_global.py FIGUIER
./create_global.py BLOSCON
./create_global.py MAREL
./create_global.py MOLIT
./create_global.py ARCB13
./create_global.py BESS
"""

# ----------------------------------------------------------------------#
#                      Chargement des modules                           #
# ----------------------------------------------------------------------#
import datetime as dt
import glob
import sys
import logging

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import rc

import pyCoastHF

rc("font", **{"size": "7", "family": "serif", "serif": ["Latin Modern Roman"], "weight": "bold"})

# ----------------------------------------------------------------------#
#                    Definition des parametres                          #
# ----------------------------------------------------------------------#

# Dates
date_str = dt.datetime.strptime("01-01-2024", "%d-%m-%Y")
date_end = dt.datetime.strptime("01-01-2025", "%d-%m-%Y")
date_end = dt.datetime.utcnow().replace(second=0, microsecond=0)

# Définition de quelles sondes prendre pour chaque paramètres
site = sys.argv[1]
periods = pyCoastHF.common.load_periods(site)

# Répertoire où sont les netCDF
rep_data = "DATA"

# Lecture du fichier de configuration
conf_sites = pyCoastHF.common.data_config("sites")

# Variables à sauver
var_to_save = conf_sites[site]["vars"]
var_to_plot = ["PROF", "TEMP", "PSAL"]
var_to_plot = [["TEMP", "PSAL"]]
if site == "MAREL":
    var_to_plot = [["TEMP", "PSAL"]]
if site == "BESS":
    var_to_plot = [["TEMP", "PSAL"], "DOX1"]

debug = False
if len(sys.argv) >= 3:
    debug = True
    df = pd.read_pickle("TIDE/predictions_250m.pkl")

# ----------------------------------------------------------------------#
#                   Lecture de tous les NetCDF                          #
# ----------------------------------------------------------------------#
SONDES = []
files_list = sorted(glob.glob("%s/%s*.nc" % (rep_data, site)))
for f in files_list:
    type_sonde = f.split("_")[1]
    if type_sonde == "MANUEL":
        continue
    name = f.split("_")[2].split(".")[0]
    SONDES.append(pyCoastHF.sonde.SondeData(name, type_sonde, site))
    SONDES[-1].init_read_netcdf(rep_data)

# ----------------------------------------------------------------------#
#                      Filtrage sur dates                               #
# ----------------------------------------------------------------------#
ind = []
for s in range(len(SONDES)):
    if SONDES[s].data[date_str:date_end].empty:
        ind.append(s)
SONDES = np.delete(SONDES, ind)

# ----------------------------------------------------------------------#
#       Pour chaque variable, selection couple (date / valeur)          #
# ----------------------------------------------------------------------#
list_sondes = [s.Type + "_" + s.Name for s in SONDES]
RAWS = []
FLAG = []
for v in var_to_save:
    if v == "CHLO":
        period = periods["FLUO"]  # noqa:F821
    elif v == "OSAT":
        period = periods["DOX1"]  # noqa:F821
    else:
        period = periods.get(v)
    d_raws = pd.DataFrame()
    d_flag = pd.DataFrame()
    i = 0
    for p in period:
        if p[0] not in list_sondes:
            continue
        s = list_sondes.index(p[0])
        if v not in list(SONDES[s].data):
            logging.error("Pas de paramètre %s dans la sonde %s %s - %s" % (v, SONDES[s].Type, SONDES[s].Name, p[1]))
            continue

        date_str_p = max(date_str, dt.datetime.strptime(p[1], "%Y/%m/%d %H:%M"))
        date_end_p = min(date_end, dt.datetime.strptime(p[2], "%Y/%m/%d %H:%M")) - dt.timedelta(seconds=1)

        tmp_raws = SONDES[s].raws[date_str_p:date_end_p][v]
        tmp_flag = SONDES[s].flag[date_str_p:date_end_p][v]
        if tmp_raws.empty:
            continue

        if i == 0:
            d_raws = pd.DataFrame(tmp_raws)
            d_flag = pd.DataFrame(tmp_flag)
            i += 1
        else:
            d_raws = pd.concat([d_raws, pd.DataFrame(tmp_raws)], axis=0)
            d_flag = pd.concat([d_flag, pd.DataFrame(tmp_flag)], axis=0)

        if d_raws.index.duplicated(keep=False).any():
            logging.error(d_raws[d_raws.index.duplicated(keep=False)])
            logging.error(v)
            exit(1)

    d_raws.rename(columns={0: v}, inplace=True)
    d_flag.rename(columns={0: v}, inplace=True)
    RAWS.append(d_raws)
    FLAG.append(d_flag)

# ----------------------------------------------------------------------#
#       Regroupement des variables sur un vecteur date commun           #
# ----------------------------------------------------------------------#
raws = pd.DataFrame()
for df_ in RAWS:
    raws = pd.concat([raws, df_], axis=1)
raws.sort_index(inplace=True)

flag = pd.DataFrame()
for df_ in FLAG:
    flag = pd.concat([flag, df_], axis=1)
flag.sort_index(inplace=True)
flag[flag.isnull()] = 9

# ----------------------------------------------------------------------#
#           Ecriture : on créé une sonde fictive GLOBAL                 #
# ----------------------------------------------------------------------#
S = pyCoastHF.sonde.SondeData("%d" % date_str.year, "GLOBAL", site)
S.raws = raws
S.flag = flag
S.define_data()
S.define_position()
if not debug:
    S.save_netcdf(rep="./DATA")

# ----------------------------------------------------------------------#
#                    Visualisation des variables                        #
# ----------------------------------------------------------------------#
fileout = "%s.png" % site
plt.figure(figsize=(15, 7))
sub = None

MANUEL = []
if not debug:
    SONDES = np.concatenate(((S,), MANUEL), axis=0)
else:
    SONDES = np.concatenate((SONDES, (S,), MANUEL), axis=0)

for i, var in enumerate(var_to_plot):
    sub = pyCoastHF.common.plot_var(
        SONDES, var, i, len(var_to_plot), date_str, date_end, raw=False, Dmean=False, sub=sub
    )
plt.show()

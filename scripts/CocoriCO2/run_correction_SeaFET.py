#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
"""

__author__ = "Sébastien Petton"
__version__ = "$Id: launch_sondes.py 60 2017-08-03 14:01:34Z sp1f8b7 $"

import datetime as dt
import glob
import logging as log

# ----------------------------------------------------------------------#
#                      Chargement des modules                          #
# ----------------------------------------------------------------------#
import os
import sys

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import PyCO2SYS as pyco2

opt_pyco2_cst = {"opt_k_carbonic": 10, "opt_k_fluoride": 2, "opt_total_borate": 2}

import pyCoastHF

# import rpy2.robjects as ro
# from rpy2.robjects.packages import importr
# seacarb = importr("seacarb")
# from rpy2.robjects import pandas2ri, r
# pandas2ri.activate()

# ----------------------------------------------------------------------#
#                           Main parameters                            #
# ----------------------------------------------------------------------#
# fmt: off
sites = ('FIGUIER','BLOSCON', 'RO16','MAREL', 'QB02','MOLIT', 'MA21','ROCHL', 'AR11','ARCB13', 'TH03','BESS', 'BOUIN',)  # noqa:E231,E501
sites = ('FIGUIER','BLOSCON', 'RO16','MAREL', 'QB02','MOLIT', 'MA21','ROCHL', 'AR11','ARCB13', 'TH03','BESS',)  # noqa:E231,E501
# fmt: on

interp_CTD_Samples = False  # Interpolate CTD data at sampling dates
coefSeaBird_init = False  # Use SeaBird calibration coefficients
debug = True  # Show difference between spectrophotometric pH and SeaFET

pdf_report = True  # Make final report with all figures
save_figs = True  # Save diagnostic plot in png
show_figs = False  # Display diagnostic plot
save_data = True  # Save treated data as *.RData files
visu = pdf_report | save_figs | show_figs

treat_all = False  # Treat all SeaFET files

if len(sys.argv) >= 2:
    if sys.argv[1] == "from":
        i = sites.index(sys.argv[2])
        sites = sites[i:]
    else:
        sites = [sys.argv[1].upper()]
        if len(sys.argv) >= 3:
            show_figs = True
if len(sites) == 1:
    pdf_report = False
# if len(sites) >= 2: save_figs = False

# Visual dates (only used if defined, otherwise deployement parameters)
# fixed_dates = True # if False, use
# d_str = dt.datetime.strptime('01-01-2021','%d-%m-%Y')
# d_end = dt.datetime.strptime('01-01-2022','%d-%m-%Y')

level = log.INFO  # DEBUG 10 / INFO 20 / WARNING 30 / ERROR 40 / CRITICAL 50
log.basicConfig(format="%(levelname)s\t: %(message)s", level=level)


# ----------------------------------------------------------------------#
#                 SeaFET data reading functions                        #
# ----------------------------------------------------------------------#
def fread_raw_data(directory=".", files=None, probe="SeaFETv2", debug=False, seapHOx=False):
    """Choose right function depending on probe"""

    if probe == "SeaFETv2":
        data = fread_raw_data_seafet_v2(directory, files, debug)
    elif probe == "SeaFETv1":
        data = fread_raw_data_seafet_v1(directory, files, debug)
    elif probe == "SeapHOx":
        data = fread_raw_data_seaphox_v2(directory, files, debug)

    data = data.astype(
        {
            "V_EXT": "float32",
            "V_INT": "float32",
            "Temp": "float32",
        }
    )
    return data.sort_index()


def fread_raw_data_seafet_v1(directory=".", files=None, debug=False):
    col_names = (
        "FrameSync",  # 1 : Satlantic & Serial number
        "Date",  # 2 : Date YYYYDDD
        "Time",  # 3 : Decimal hours
        "pH_INT",  # 4 : original Internal pH calculated by the Instrument
        "pH_EXT",  # 5 : original External pH calculated by the Instrument
        "Temp",  # 6 : original temperature calculated by the Instrument
        "Temp_CTD",  # 7 :
        "Psal_CTD",  # 8 :
        "Oxyg_CTD",  # 9 :
        "Pres_CTD",  # 10 :
        "V_INT",  # 11 : Internal reference electrode voltage (V) - FET|INT
        "V_EXT",  # 12 : External reference electrode voltage (V) - FET|EXT
        "V_THERM",  # 13 : ISFET Thermistor voltage (V)
        "V_Supply",  # 14 : Supply voltage (V)
        "I_Supply",  # 15 : Supply current (mA)
        "Humidity",  # 16 : Electronics compartiment relative humidity (%)
        "V_5V",  # 17 : Internal 5V supply voltage (V) - USB
        "V_MBatt",  # 18 : Main battery pack voltage (V)
        "V_Iso",  # 19 : Internal isolated supply voltage (V)
        "V_IsoBatt",  # 20 : Isolated battery pack voltage (V)
        "I_B",  # 21 :
        "I_K",  # 22 :
        "V_K",  # 23 :
        "Status",  # 24 : Status word (a 16-bit hexadecimal formatted bitmask indicating system status)
        "Check Sum",
    )

    if files is None:
        files = glob.glob("%s/SeaFET_*[0-9].CSV" % directory)

    data = pd.DataFrame()
    for fic in files:
        df = pd.read_table(fic, decimal=".", sep=",", skiprows=8, header=None, names=col_names)
        df["Date"] = pd.to_datetime(df["Date"], format="%Y%j", errors="raise")
        df["Date"] += pd.to_timedelta(df["Time"] * 3600 * 1e9).dt.floor("Min")
        df.set_index("Date", inplace=True)
        data = pd.concat([data, df], axis=0)

    if not debug:
        data = data[["FrameSync", "pH_EXT", "pH_INT", "V_EXT", "V_INT", "Temp", "Humidity"]]

    return data


def fread_raw_data_seafet_v2(directory=".", files=None, debug=False):
    col_names = (
        "FrameSync",  # 1 : Satlantic & Serial number
        "Date",  # 2 : Date %m/d%/%Y %H:%M:%S
        "Sample",  # 3 : Sample number
        "Flag",  # 4 : Error flag
        "pH_EXT",  # 5 : original External pH calculated by the Instrument
        "pH_INT",  # 6 : original Internal pH calculated by the Instrument
        "V_EXT",  # 7 : External reference electrode voltage (V) - FET|EXT
        "V_INT",  # 8 : Internal reference electrode voltage (V) - FET|INT
        "Temp",  # 9 : original temperature calculated by the Instrument
        "Humidity",  # 10 : Electronics compartiment relative humidity (%)
        "TEMP_in",  # 11 : Electronics compartiment temperature (°C)
    )

    if files is None:
        files = glob.glob("%s/SeaFET2-*[0-9].csv" % directory)

    data = pd.DataFrame()
    for fic in files:
        df = pd.read_table(fic, decimal=".", sep=",", skiprows=1, header=None, names=col_names)
        df["Date"] = pd.to_datetime(df["Date"], format="%m/%d/%Y %H:%M:%S", errors="raise")
        df.set_index("Date", inplace=True)
        data = pd.concat([data, df], axis=0)

    if not debug:
        data = data[["FrameSync", "pH_EXT", "pH_INT", "V_EXT", "V_INT", "Temp", "Humidity"]]

    return data


def fread_raw_data_seaphox_v2(directory=".", files=None, debug=False):
    col_names = (
        "FrameSync",  # 1 : Satlantic & Serial number
        "Date",  # 2 : Date %m/d%/%Y %H:%M:%S
        "Sample",  # 3 : Sample number
        "Flag",  # 4 : Error flag
        "TEMP",  # 5 : SBE 37 Temperature (°C)
        "pH_EXT",  # 6 : original External pH calculated by the Instrument
        "pH_INT",  # 7 : original Internal pH calculated by the Instrument
        "V_EXT",  # 8 : External reference electrode voltage (V) - FET|EXT
        "V_INT",  # 9 : Internal reference electrode voltage (V) - FET|INT
        "Temp",  # 10 : original temperature calculated by the Instrument
        "PROF",  # 11 : SBE 37 Pressure (dbar)
        "PSAL",  # 12 : SBE 37 Salinity (psu)
        "COND",  # 13 : SBE 37 Conductivity (S/m)
        "OXYG",  # 14 : SBE 37 Oxygene (ml/L)
        "Humidity",  # 15 : Electronics compartiment relative humidity (%)
        "TEMP_in",  # 16 : Electronics compartiment temperature (°C)
    )

    if files is None:
        files = glob.glob("%s/*SeapHox2-*[0-9].csv" % directory)

    data = pd.DataFrame()
    for fic in files:
        df = pd.read_table(fic, decimal=".", sep=",", skiprows=1, header=None, names=col_names)
        df["Date"] = pd.to_datetime(df["Date"], format="%m/%d/%Y %H:%M:%S", errors="raise")
        df.set_index("Date", inplace=True)
        data = pd.concat([data, df], axis=0)

    if not debug:
        data = data[["FrameSync", "pH_EXT", "pH_INT", "V_EXT", "V_INT", "Temp", "Humidity"]]

    return data


rasterized = False
if pdf_report:
    from matplotlib.backends.backend_pdf import PdfPages

    pdf = PdfPages("rapport_CocoriCO2.pdf")
    rasterized = True


def read_dplmt_date(strdate):
    try:
        date = dt.datetime.strptime(strdate, "%d/%m/%Y")
    except ValueError:
        date = dt.datetime.strptime(strdate, "%d/%m/%Y %H:%M")
    return date


# ----------------------------------------------------------------------#
#                 Program start : Treating site by site                #
# ----------------------------------------------------------------------#
config = pyCoastHF.common.data_config("sites", "CocoriCO2")
conf_seafet = pyCoastHF.common.data_config("sondes", "SeaFET")


for site in sites:
    conf_site = config["SITES"][site]

    log.info("-" * 60)
    log.info("Processing data for %s - %s" % (site, conf_site["name"]))
    log.info("-" * 60)

    RawData_dir = "./Fichiers_Bruts/tmp_SONDES/%s/" % site
    Deplmts_fic = "./Suivi/SeaFET_Deployments_%s.xlsx" % site

    # Dates
    if not treat_all:
        d_str = read_dplmt_date(conf_site["dplmts"][conf_site["num"] - 1]["str"])
        d_end = read_dplmt_date(conf_site["dplmts"][conf_site["num"] - 1]["end"])
        d_end = min(d_end, dt.datetime.utcnow())
    else:
        d_str = read_dplmt_date(conf_site["dplmts"][0]["str"])
        d_end = read_dplmt_date(conf_site["dplmts"][-1]["end"])
        d_end = min(d_end, dt.datetime.utcnow())

    # ----------------------------------------------------------------------#
    #                 Read SeaFET data and Select water data               #
    # ----------------------------------------------------------------------#
    files = None
    if treat_all:
        files = glob.glob("./Fichiers_Bruts/2*/%s/*SeaFET2*[0-9].csv" % site)
        SeaFET_raws = fread_raw_data(files=files, probe="SeaFETv2")
        files = glob.glob("./Fichiers_Bruts/2*/%s/*SeapHox2*[0-9].csv" % site)
        if len(files) > 0:
            SeaFET_raws = pd.concat([SeaFET_raws, fread_raw_data(files=files, probe="SeapHOx")], axis=0)
        files = glob.glob("./Fichiers_Bruts/2*/%s/SeaFET_*" % site)
        if len(files) > 0:
            SeaFET_raws = pd.concat([SeaFET_raws, fread_raw_data(files=files, probe="SeaFETv1")], axis=0)
        SeaFET_raws.sort_index(inplace=True)
    else:
        SeaFET_raws = fread_raw_data(
            directory=RawData_dir, files=files, probe=conf_site["dplmts"][conf_site["num"] - 1]["probe"]
        )
    SeaFET_raws = SeaFET_raws[d_str:d_end]
    if not SeaFET_raws[SeaFET_raws.index.duplicated()].empty:
        log.info("Duplicated SeaFET raw data")
        print(SeaFET_raws[SeaFET_raws.index.duplicated()])
        # ind = ~ SeaFET_raws.index.duplicated(keep='last')
        # SeaFET_raws =  SeaFET_raws[ind]
    log.info("Last available SeaFET data : %s", SeaFET_raws.index[-1])

    Periods = pd.read_excel(Deplmts_fic, sheet_name="SeaFET")
    Periods = Periods[pd.notnull(Periods.Deployments)]
    Periods.IN = pd.to_datetime(Periods.IN, format="%d/%m/%Y %H:%M:%S", errors="raise")
    Periods.OUT = pd.to_datetime(Periods.OUT, format="%d/%m/%Y %H:%M:%S", errors="raise")
    if (
        (Periods.IN.diff().dt.total_seconds() < 0).any()
        or (Periods.OUT.diff().dt.total_seconds() < 0).any()
        or ((Periods.IN.shift(-1) - Periods.OUT).dt.total_seconds() < 0).any()
    ):
        log.warning("Overlap in deployement periods")
        print(Periods)
    last_data = max(Periods.IN.iloc[-1], Periods.OUT.iloc[-1])
    log.info("Last time probe deployemnt : %s", last_data)
    if last_data < SeaFET_raws.index[-1]:
        log.warning("Probe deployemnt information is missing")
    # Periods.to_pickle('Periods_%s.pkl'%site)

    SeaFET = pd.DataFrame()
    for i, p in Periods.iterrows():
        if pd.notnull(p.OUT) and p.OUT > SeaFET_raws.index[0]:
            # df = SeaFET_raws.loc[df['FrameSync'].str.endswith(str(int(p.Num)))]
            # df = df.loc[(p.IN + dt.timedelta(hours=p.DT)):p.OUT]
            df = SeaFET_raws.loc[SeaFET_raws["FrameSync"].str.endswith(str(int(p.Num)))][
                (p.IN + dt.timedelta(hours=p.DT)) : p.OUT
            ]
            df["Deployments"] = p.Deployments
            SeaFET = pd.concat([SeaFET, df], axis=0)

    # ----------------------------------------------------------------------#
    #                       Read external CTD data                         #
    # ----------------------------------------------------------------------#
    if os.path.exists(conf_site["CTD_file"]):
        S = pyCoastHF.SondeData("ALL", "GLOBAL", site)
        S.init_read_netcdf(file_in=conf_site["CTD_file"])
        S.reduce_time(d_str, d_end)
        log.info("Last available CTD data    : %s", S.data.index[-1])

        # Interpolation of CTD data at SeaFET dates
        CTD_variables = ["TEMP", "PSAL"]
        unionTime = S.data.index.union(SeaFET.index)
        data_interp = (
            S.data[CTD_variables]
            .reindex(unionTime)
            .interpolate(method="time", limit_area="inside")
            .reindex(SeaFET.index)
        )

        # Determine mask for each interpolate CTD variable where there are too important gaps
        mask = pd.DataFrame()
        for v in CTD_variables:
            tmp = S.data[[v]].dropna(axis=0, how="all")
            mask[v] = tmp.index.to_series().diff().dt.total_seconds()
        mask = mask.reindex(unionTime).bfill().reindex(SeaFET.index)

        # Apply mask to interpolate dataframe
        for v in CTD_variables:
            data_interp.loc[mask[v] > 3600 * 6, v] = np.NaN
        data_interp.rename(columns={"TEMP": "TEMP_Probe", "PSAL": "PSAL_Probe"}, inplace=True)

        SeaFET = pd.concat([SeaFET, data_interp], axis=1)

        # If no T°C from CTD probe, take DuraFET T°C as reference
        SeaFET.loc[(pd.isnull(SeaFET["TEMP_Probe"])), "TEMP_Probe"] = SeaFET["Temp"]
    else:
        log.warning("No external CTD data available")
        SeaFET["TEMP_Probe"] = SeaFET["Temp"]
        SeaFET["PSAL_Probe"] = np.NaN
        S = pyCoastHF.sonde.SondeData("ALL", "SeaFET", site)
        S.data["TEMP"] = SeaFET["Temp"]
        S.data["PSAL"] = np.NaN

    # ----------------------------------------------------------------------#
    #                     Read discrete sampling data                      #
    # ----------------------------------------------------------------------#
    Samples = pd.read_excel(Deplmts_fic, sheet_name="Sampling", usecols=range(14))
    Samples["Date"] = pd.to_datetime(Samples["Date"], format="%m/%d/%Y %H:%M:%S", errors="coerce")
    Samples = Samples[pd.notnull(Samples.Date)]
    Samples.set_index("Date", inplace=True)
    Samples = Samples[d_str:d_end]
    if Samples.empty:
        log.warning("No discrete samples for this deployement")
    else:
        log.info("Last sample date available : %s", Samples.index[-1])

    # Alkalinity and DIC values from SNAPO CO2 lab
    SNAPO = Samples[["A_T", "C_T"]].copy()
    SNAPO.interpolate(method="time", inplace=True)
    if pd.isnull(SNAPO["A_T"]).all():
        SNAPO["A_T"] = 2250
    if pd.isnull(SNAPO["C_T"]).all():
        SNAPO["C_T"] = 2100
    SNAPO.fillna(2200, inplace=True)

    # Interpolate CTD data at sampling dates
    if interp_CTD_Samples:
        unionTime = Samples.index.union(SeaFET.index)
        tmp = SeaFET[["TEMP_Probe", "PSAL_Probe"]].reindex(unionTime).interpolate(method="time").reindex(Samples.index)
        Samples["Temp"] = tmp["TEMP_Probe"]
        Samples["Psal"] = tmp["PSAL_Probe"]

    # Estimate pH at in-situ T°C
    ind_pH = (
        pd.notnull(Samples["pH_Spectro"])
        & pd.notnull(Samples["Status"])
        & pd.notnull(Samples["Temp"])
        & (Samples["Status"] == "A")
    )
    Samples["pH_Spectro_InSitu"] = np.NaN
    if ind_pH.any():
        # Samples.loc[ind_pH, 'pH_Spectro_InSitu'] = seacarb.pHinsi(pH = Samples.pH_Spectro[ind_pH],
        #                                                          ALK = SNAPO.A_T[ind_pH],
        #                                                          Tinsi = Samples.Temp[ind_pH],
        #                                                          Tlab = Samples.Temp_pH_Spectro[ind_pH],
        #                                                          S = Samples.Psal[ind_pH])
        Samples.loc[ind_pH, "pH_Spectro_InSitu"] = pyco2.sys(
            par1=Samples.pH_Spectro[ind_pH],
            par1_type=3,
            par2=SNAPO.A_T[ind_pH],
            par2_type=1,
            temperature=Samples.Temp_pH_Spectro[ind_pH],
            salinity=Samples.Psal[ind_pH],
            temperature_out=Samples.Temp[ind_pH],
            **opt_pyco2_cst,
        )["pH_total_out"]
        # Samples.loc[ind_pH, 'pH_Spectro_InSitu'] = seacarb.pHinsi(
        #    pH=Samples.pH_Spectro[ind_pH],
        #    ALK=SNAPO.A_T[ind_pH]*1e-6,
        #    Tinsi=Samples.Temp[ind_pH],
        #    Tlab=Samples.Temp_pH_Spectro[ind_pH],
        #    S=Samples.Psal[ind_pH]#, k1k2="s20", kf="dg"
        # )
    else:
        log.warning("No spectrophotometric pH for this deployement")
        coefSeaBird_init = True

    coefSeaBird = [
        coefSeaBird_init,
    ] * len(SeaFET.Deployments.unique())

    # Conversion des pH spectros avec le statut  B
    ind_pH = pd.notnull(Samples["pH_Spectro"]) & (Samples["Status"] == "B")
    if ind_pH.any():
        Samples.loc[ind_pH, "pH_Spectro_InSitu"] = pyco2.sys(
            par1=Samples.pH_Spectro[ind_pH],
            par1_type=3,
            par2=SNAPO.A_T[ind_pH],
            par2_type=1,
            temperature=Samples.Temp_pH_Spectro[ind_pH],
            salinity=Samples.Psal[ind_pH],
            temperature_out=Samples.Temp[ind_pH],
            **opt_pyco2_cst,
        )["pH_total_out"]

    # -----------------------------------------------------------------------------#
    #    Apply processing functions to SeaFET raw data [Bresnahan et al (2014)]   #
    #              http://dx.doi.org/10.1016/j.mio.2014.08.003                    #
    # -----------------------------------------------------------------------------#
    def process_seafet_seabird_coefficient(SeaFET, conf_seafet):
        SeaFET[["E0int", "E0ext", "dE0int", "dE0ext"]] = np.NaN
        for s in SeaFET.FrameSync.unique():
            # ind = SeaFET.FrameSync == s
            SeaFET["E0int"] = conf_seafet[s[-4:]]["coef"]["k0int"]
            SeaFET["E0ext"] = conf_seafet[s[-4:]]["coef"]["k0ext"]
            SeaFET["dE0int"] = conf_seafet[s[-4:]]["coef"]["k2int"]
            SeaFET["dE0ext"] = conf_seafet[s[-4:]]["coef"]["k2ext"]

        res = pyCoastHF.seacarb.sf_calc(
            SeaFET["V_INT"],
            SeaFET["V_EXT"],
            SeaFET["E0int"],
            SeaFET["E0ext"],
            SeaFET["TEMP_Probe"],
            SeaFET["PSAL_Probe"],  # .astype(np.float64),
            SeaFET["dE0int"],
            SeaFET["dE0ext"],
        )

        SeaFET.drop(["E0int", "E0ext", "dE0int", "dE0ext"], axis=1, inplace=True)
        return res

    if all(coefSeaBird):
        log.warning("-> Use SeaBird coefficient for all periods")
        RES = process_seafet_seabird_coefficient(SeaFET, conf_seafet)
    else:
        RES = pd.DataFrame()
        # Determine calibration coefficients E0INT,25 & E0EXT,25
        tmp = SeaFET[["V_EXT", "V_INT"]].reindex(
            Samples.index, method="nearest", tolerance=pd.Timedelta(conf_site["freq"])
        )
        res = pyCoastHF.seacarb.sf_calib(
            calEint=tmp["V_INT"],
            calEext=tmp["V_EXT"],
            calpH=Samples["pH_Spectro_InSitu"],
            tempC=Samples["Temp"],
            salt=Samples["Psal"],
        )
        Samples = pd.concat([Samples, res], axis=1)

        # Get one average coefficient per deployements
        # ind = (Samples["Status"] != "A") | ((Samples.Date_Analysis - Samples.index) / dt.timedelta(days=1) >= 245)
        ind = Samples["Status"] != "A"
        Samples.loc[ind, ["E0int25", "E0ext25"]] = np.NaN
        Samples["E0int25_orig"] = Samples["E0int25"].copy()
        Samples["E0ext25_orig"] = Samples["E0ext25"].copy()
        for ii, i in enumerate(SeaFET.Deployments.unique()):
            ind = Samples.Deployments == i
            inds = SeaFET.Deployments == i
            if ind.empty or all(Samples["E0int25"][ind].isnull()):
                log.warning("-> Use SeaBird coefficient for deployment %d" % i)
                df = SeaFET[inds].copy()
                res = process_seafet_seabird_coefficient(df, conf_seafet)
                coefSeaBird[ii] = i
            else:
                SeaFET[["E0int25", "E0ext25"]] = np.NaN
                Samples.loc[ind, "E0int25"] = Samples["E0int25"][ind].mean()
                Samples.loc[ind, "E0ext25"] = Samples["E0ext25"][ind].mean()
                SeaFET.loc[inds, "E0int25"] = Samples["E0int25"][ind].mean()
                SeaFET.loc[inds, "E0ext25"] = Samples["E0ext25"][ind].mean()

                # Determine calibrated pH based on deployements-averaged E0INT,25 & E0EXT,25
                res = pyCoastHF.seacarb.sf_calc(
                    Eint=SeaFET.loc[inds, "V_INT"],
                    Eext=SeaFET.loc[inds, "V_EXT"],
                    E0int=SeaFET.loc[inds, "E0int25"],
                    E0ext=SeaFET.loc[inds, "E0ext25"],
                    tempC=SeaFET.loc[inds, "TEMP_Probe"],
                    salt=SeaFET.loc[inds, "PSAL_Probe"],  # .astype(np.float64),
                )
                SeaFET.drop(["E0int25", "E0ext25"], axis=1, inplace=True)
            RES = pd.concat([RES, res], axis=0)

    RES.rename(columns={"pHint_tot": "pH_INT_Cor", "pHext_tot": "pH_EXT_Cor"}, inplace=True)
    RES.index = SeaFET.index
    SeaFET = pd.concat([SeaFET, RES], axis=1)

    # Compare corrected pH signals vs spectrophotomectric pH at in-situ temperature
    tmp = SeaFET[["pH_INT_Cor", "pH_EXT_Cor"]].reindex(
        Samples.index, method="nearest", tolerance=pd.Timedelta(conf_site["freq"])
    )
    Samples = pd.concat([Samples, tmp], axis=1)
    if debug:
        log.info("Errors between pH_Corr and pH_Spectro_InSitu")
        for d, row in Samples.iterrows():
            log.info(
                "%s %5d %10.4f %10.4f"
                % (
                    d,
                    row["Deployments"],
                    row["pH_INT_Cor"] - row["pH_Spectro_InSitu"],
                    row["pH_EXT_Cor"] - row["pH_Spectro_InSitu"],
                )
            )

    # ----------------------------------------------------------------------#
    #                                Save data                             #
    # ----------------------------------------------------------------------#
    if save_data:
        if not treat_all:
            SeaFET.to_pickle("Donnees_Traitees/%s/SeaFET_%s_%02d.pkl" % (site, site, conf_site["num"]))
            Samples.to_pickle("Donnees_Traitees/%s/Samples_%s_%02d.pkl" % (site, site, conf_site["num"]))
        else:
            for dplmt in conf_site["dplmts"]:
                dstr = read_dplmt_date(dplmt["str"])
                dend = read_dplmt_date(dplmt["end"])
                SeaFET[dstr:dend].to_pickle("Donnees_Traitees/%s/SeaFET_%s_%02d.pkl" % (site, site, dplmt["num"]))
                Samples[dstr:dend].to_pickle("Donnees_Traitees/%s/Samples_%s_%02d.pkl" % (site, site, dplmt["num"]))

    # ----------------------------------------------------------------------#
    #                              Visualisation                           #
    # ----------------------------------------------------------------------
    if visu:
        fig = pyCoastHF.common.plot_seafet_diag(
            SeaFET, S.data, Samples, Periods, d_str, d_end, conf_site, DuraFET_Temp=False, rasterized=rasterized
        )
        if save_figs:
            fig.savefig("FIGURES/Suivi_%s.png" % site, dpi=300)
        if show_figs:
            plt.show()
        if pdf_report:
            log.debug("Enregistrement dans le pdf")
            # for ax in fig.axes:
            #    ax.set_rasterized(True)
            pdf.savefig(fig)

# Fermeture du rapport.pdf
if pdf_report:
    log.debug("Finalisation du pdf")
    d = pdf.infodict()
    d["Title"] = "Rapport de mesures pH CocoriCO2"
    d["Author"] = "Sébastien Petton"
    d["CreationDate"] = dt.datetime.today()
    pdf.close()

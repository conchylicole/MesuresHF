#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""

in CDOCO repository -> historic file
in DATA repository -> annual file

--> concat both  put in CDOCO repository site_GLOBAL_ALL.nc

@author: S. Petton
"""

import glob

import pandas as pd

import pyCoastHF

liste_sites = ("ARCB13", "BESS")
liste_sites = ("MAREL",)
liste_sites = ("BLOSCON", "FIGUIER", "MAREL", "MOLIT", "ROCHL", "ARCB13", "BESS")

for site in liste_sites:
    f1 = sorted(glob.glob("CDOCO/%s_*_202*.nc" % site))[0]
    s1 = pyCoastHF.sonde.SondeData(site, "", site)
    s1.init_read_netcdf(file_in=f1)
    print(f1)

    s2 = pyCoastHF.sonde.SondeData(site, "", site)
    try:
        file_in = "./DATA/%s_GLOBAL_2024.nc" % site
        s2.init_read_netcdf(file_in=file_in)
    except (IOError, ValueError):
        file_in = "./DATA/%s_CDOCO_GLOBAL.nc" % site
        s2.init_read_netcdf(file_in=file_in)
    print(file_in)

    raws = pd.concat([s1.raws, s2.raws], axis=0)
    flag = pd.concat([s1.flag, s2.flag], axis=0)
    adjs = pd.concat([s1.adjs, s2.adjs], axis=0)

    # if len(raws[raws.index.duplicated()])
    S = pyCoastHF.sonde.SondeData("ALL", "GLOBAL", site)
    S.raws = raws
    S.flag = flag
    S.adjs = adjs
    S.define_position()
    S.save_netcdf(rep="./CDOCO/")

#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The purpose of this programm is to :
 - Easily get data from the RESCO-VELYGER Smatch / STPS / SP2T / or other SONDE
 - Filter and Compare them
 - Save data in a common way in netCDF or in XLS (daily value by default)
 - Make a pdf report for each RESCO-VELYGER Site
 - Optionnally send a mail to observatoire_conchylicole

The only necessary parameters must be date_str,date_end and which sites

"""

__author__ = 'Sébastien Petton'
__version__ = '$Id: launch_sondes.py 60 2017-08-03 14:01:34Z sp1f8b7 $'

# ----------------------------------------------------------------------#
#                      Chargement des modules                          #
# ----------------------------------------------------------------------#
import datetime as dt
import glob
import logging as log
import os
import shutil
import sys

import distro
import numpy as np

import pyCoastHF

# ----------------------------------------------------------------------#
#                    Définition des paramètres                          #
# ----------------------------------------------------------------------#
# Sites
sites = ('BLOSCON', 'FIGUIER', 'MAREL', 'MOLIT', 'ROCHL', 'ARCB13', 'BESS')  # CocoriCO2 -> CDOCO

if len(sys.argv) == 2:
    sites = [sys.argv[1]]

# Dates
date_str = dt.datetime.strptime('01-01-2024', '%d-%m-%Y')
date_end = dt.datetime.utcnow().replace(second=0, microsecond=0)
# date_end = dt.datetime.strptime('01-01-2024','%d-%m-%Y')  # et mettre des dates specifiques

# Variables Figures
variables_plot = ["TEMP", "PSAL", "DOX1", "CHLO"]
variables_plot = ["TEMP", "PSAL", "PROF"]

# Lecture du fichier de configuration
conf_sites = pyCoastHF.common.data_config('sites')
conf_vars = pyCoastHF.common.data_config('variables')

# ------------ Data -------------
download_oc = True  # Téléchargement des données au CDOCO
clean = False  # Suppression des données en local
save_data = True  # Ok en NetCDF  + en xls (journalier)
rep_data = './DATA/'  # Répertoire où les NetCDF seront stockés
# ----------- Rapport -----------
make_report = True  # Création du fichier rapport.pdf
# -------- Développement --------
verbose = False
plot_raw = True  # Trace les données brutes
plot_Dmean = False  # Trace les moyennes journalières
show_figs = True  # Affiche les figures lors de l'exécution du programme
save_fig = False  # Sauvegarde chaque figure en png
# -------------------------------


# ----------------------------------------------------------------------#
#             Visualisation : Matplotlib si nécessaire                 #
# ----------------------------------------------------------------------#
if make_report or save_fig or show_figs:
    import matplotlib.pyplot as plt
    from matplotlib import rc
    rc('font', **{'size': '8', 'family': 'serif', 'serif': ['Latin Modern Roman']})
    from matplotlib.backends.backend_pdf import PdfPages

# ----------------------------------------------------------------------#
#         Définition du suivi (print screen ou fichier log)            #
# ----------------------------------------------------------------------#
level = log.INFO  # DEBUG 10 / INFO 20 / WARNING 30 / ERROR 40 / CRITICAL 50
log.basicConfig(format='%(levelname)s\t: %(message)s', level=level)

# ----------------------------------------------------------------------#
#                       Exécution du run                               #
# ----------------------------------------------------------------------#
log.info('Date de début %s' % date_str)
log.info('Date de fin   %s' % date_end)
log.info(sites)

if make_report:
    pdf = PdfPages('rapport.pdf')

# ---------------------------------------#
# Comparaison des données site par site
# ---------------------------------------#
for _, site in enumerate(sites):

    name = conf_sites[site]['name']
    log.info('-' * 60)
    log.info('Traitement du site : %s - %s' % (site, name))
    log.info('-' * 60)

    # Local files
    SONDES = []
    for sonde in ['SMATCH', 'STPS', 'WiSens', 'WiMo', 'YSI', 'SBE']:
        if sonde in conf_sites[site]['sondes']:
            SONDES += pyCoastHF.sonde.read_local_dir(
                Type=sonde, site=site, directory='Fichiers_Bruts/tmp_SONDES/%s' % site
            )

    # Distant files from CDOCO database
    if 'CDOCO' in conf_sites[site]['sondes']:
        SONDES += pyCoastHF.cdoco.get_data(
            site, date_str, date_end, download_oc, cleanFirst=False, flags=True, name='CDOCO'
        )

    # ============================== #
    # Filtrage des sondes hors dates #
    # ============================== #
    log.info(' => Filtrage des sondes hors période')
    ind = []
    for s in range(len(SONDES)):
        SONDES[s].sort_index()
        if SONDES[s].raws[date_str:date_end].empty:
            ind.append(s)
    SONDES = np.delete(SONDES, ind)

    # =================== #
    # Filtrage spécifique #
    # =================== #
    log.info(' => Filtrage des données')
    ind = []
    for s in range(len(SONDES)):
        SONDES[s].convert_variables()
        SONDES[s].apply_qc(date_str, date_end)
        if SONDES[s].data[date_str:date_end].empty:
            ind.append(s)
        SONDES[s].define_position()
    SONDES = np.delete(SONDES, ind)

    # ============= #
    # Visualisation #
    # ============= #
    if make_report or save_fig or show_figs:
        log.info(' => Visualisation')
        fig = plt.figure(facecolor='w', figsize=(10, 5))
        sub = None
        for i, var in enumerate(variables_plot):
            log.debug('Variable : %s' % conf_vars[var]['longname'])
            sub = pyCoastHF.common.plot_var(
                SONDES,
                var,
                i,
                len(variables_plot),
                date_str,
                date_end,
                raw=plot_raw,
                Dmean=plot_Dmean,
                sub=sub,
                uniqueCol=False,
            )
        fig.suptitle('Site %s : %s \n Période du %s au %s' % (site, name, date_str, date_end))

    # ====================== #
    # Sauvegarde des figures #
    # ====================== #
    if save_fig:
        plt.savefig('%s.png' % site, bbox_inches='tight', dpi=300)
    if make_report:
        log.debug('Enregistrement dans le pdf')
        pdf.savefig(fig)

    # ================================ #
    # Sauvegarde en NetCDF, CSV et XLS #
    # ================================ #
    if save_data:
        log.debug(' => Sauvegarde')
        for s in range(len(SONDES)):
            SONDES[s].save_netcdf(rep_data)

log.debug('-' * 60)

# ========= #
# Nettoyage #
# ========= #
if clean:
    # CDOCO
    tmp = glob.glob('./tmp_*')
    for t in tmp:
        shutil.rmtree(t)
        os.remove('valid_files_' + t.split('_')[1] + '.txt')

# Fermeture du rapport.pdf
if make_report:
    log.debug('Finalisation du pdf')
    d = pdf.infodict()
    d['Title'] = 'Rapport de mesures'
    d['Author'] = u'Sébastien Petton'
    d['CreationDate'] = dt.datetime.today()
    pdf.close()

# Affichages des figures
if show_figs:
    plt.show()

log.info('-' * 60)
log.info('Fin du script')

#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
The purpose of this programm is to :
 - Easily get data from the RESCO-VELYGER Smatch / STPS / SP2T / or other SONDE
 - Filter and Compare them
 - Save data in a common way in netCDF or in XLS (daily value by default)
 - Make a pdf report for each RESCO-VELYGER Site
 - Optionnally send a mail to observatoire_conchylicole

The only necessary parameters must be date_str,date_end and which sites

Sur DATARMOR : ( python launch_rapport_sondes.py ) > & stderr_file
"""

__author__ = 'Sébastien Petton'
__version__ = '$Id: launch_sondes.py 60 2017-08-03 14:01:34Z sp1f8b7 $'

# ----------------------------------------------------------------------#
#                      Chargement des modules                          #
# ----------------------------------------------------------------------#
import datetime as dt
import glob
import logging as log
import os
import shutil
import sys

import distro
import numpy as np

import pyCoastHF

# ----------------------------------------------------------------------#
#                    Définition des paramètres                          #
# ----------------------------------------------------------------------#
# Sites
sites = ('RO16',)

# Dates
date_str = dt.datetime.strptime('01-01-2024', '%d-%m-%Y')
date_end = dt.datetime.utcnow().replace(second=0, microsecond=0)
#date_end   = dt.datetime.strptime('01-01-2023','%d-%m-%Y')  # et mettre des dates specifiques

# Variables Web
web_data = []
variables_web = ['TEMP', 'PSAL']

# Variables Figures
variables_plot = ['TEMP', 'PSAL', 'PROF', 'OSAT', 'DOX1', 'TURB', 'FLUO']
# variables_plot = ['FLUO', 'TURB']
#variables_plot = ['PSAL']

# Lecture du fichier de configuration
conf_sites = pyCoastHF.common.data_config('sites')
conf_vars = pyCoastHF.common.data_config('variables')

# ------------ Data -------------
download_oc = True  # Téléchargement des données au CDOCO
download_q2 = False  # Téléchargement des données de Quadrige 2 sur (M:)\\argenton\ostrea
clean = True  # Suppression des données en local
save_data = True  # Ok en NetCDF  + en xls (journalier)
rep_data = './DATA/'  # Répertoire où les NetCDF seront stockés
# ----------- Rapport -----------
make_report = False  # Création du fichier rapport.pdf
# -------- Données Web ----------
web_daily = False  # Création des données web JSON moyennes journalière
web_data_hf = False  # Création des données web JSON moyennes horaires (extranet)
maj_1_year = None  # Ne fera la mise à jour que cette année
latest = False  # Fichier JSON avec mesures HF toutes les 15mins
transfert = ''  # Active le transfert vers les sites WEB !
# -------- Développement --------
verbose = False
plot_raw = True  # Trace les données brutes
plot_Dmean = False  # Trace les moyennes journalières
show_figs = True  # Affiche les figures lors de l'exécution du programme
save_fig = False  # Sauvegarde chaque figure en png
# -------------------------------

# For display & file access purposes
DATARMOR = False
if distro.linux_distribution() == ('SLES', '12.1', 'x86_64'):
    DATARMOR = True

# ----------------------------------------------------------------------#
#             Visualisation : Matplotlib si nécessaire                 #
# ----------------------------------------------------------------------#
if make_report or save_fig or show_figs:
    if DATARMOR:
        from matplotlib import use

        use('Agg')
    import matplotlib.pyplot as plt
    from matplotlib.backends.backend_pdf import PdfPages

# ----------------------------------------------------------------------#
#         Définition du suivi (print screen ou fichier log)            #
# ----------------------------------------------------------------------#
level = log.INFO  # DEBUG 10 / INFO 20 / WARNING 30 / ERROR 40 / CRITICAL 50
if DATARMOR:
    today = dt.datetime.today()
    filelog = 'log_%d%02d%02d.txt' % (today.year, today.month, today.day)
    log.basicConfig(format='%(levelname)s\t: %(message)s', level=level, filename=filelog)
else:
    log.basicConfig(format='%(levelname)s\t: %(message)s', level=level)

# ----------------------------------------------------------------------#
#                       Exécution du run                               #
# ----------------------------------------------------------------------#
log.info('Date de début %s' % date_str)
log.info('Date de fin   %s' % date_end)
log.info(sites)

if make_report:
    pdf = PdfPages('rapport.pdf')

if save_data:
    writer = pyCoastHF.common.init_workbook(rep_data + './sum-up.xlsx')

# ---------------------------------------------------#
# Téléchargement des fichiers provenant de Quadrige
# --------------------------------------------------#
path = r'/export/home2/commun/ostrea/Observatoire\ Mat\ et\ Meth/Outils_informatiques/Sources_de_Donnees/STPS\ RESCO'
liste_excel = r'Q2_*_Extraction\ Sondes\ RESCO*_FILEMEAS.csv'
liste_manuel = r'Q2_*_Extraction\ Sondes\ RESCO*_MEAS_LIG.csv'
if download_q2:
    pyCoastHF.quadrige.download_from_q2(path, liste_excel, liste_manuel, DATARMOR, verbose)

# ---------------------------------------#
# Comparaison des données site par site
# ---------------------------------------#
for _, site in enumerate(sites):

    name = conf_sites[site]['name']
    log.info('-' * 60)
    log.info('Traitement du site : %s - %s' % (site, name))
    log.info('-' * 60)

    # Local files
    SONDES = []
    for sonde in ['SMATCH', 'STPS', 'WiSens', 'FLNTU', 'SeapHox', 'SBE', 'HydroCAT']:
        if sonde in conf_sites[site]['sondes']:
            SONDES += pyCoastHF.sonde.read_local_dir(
                Type=sonde, site=site, directory='../Fichiers_Bruts/tmp_SONDES/%s' % site
            )

    # Distant files from CDOCO database
    if 'CDOCO' in conf_sites[site]['sondes']:
        SONDES += pyCoastHF.cdoco.get_data(
            site, date_str, date_end, download_oc, cleanFirst=False, flags=True, name='CDOCO'
        )

    # Discrete sampling from Quadrige 2 database
    if 'MANUEL' in conf_sites[site]['sondes']:
        SONDES += pyCoastHF.sonde.read_local_manuel(site, '../Fichiers_Bruts/Mesures_MANUEL.xlsx')
        SONDES += pyCoastHF.sonde.read_local_rephy(site, '../Fichiers_Bruts/Mesures_REPHY.csv')

    # ================= #
    # Ré-échantillonage #
    # ================= #
    log.info(' => Ré-échantillonage des sondes')
    for s in range(len(SONDES)):
        SONDES[s].sort_index()
        SONDES[s].resample_data()

    # ============================== #
    # Filtrage des sondes hors dates #
    # ============================== #
    log.info(' => Filtrage des sondes hors période')
    ind = []
    for s in range(len(SONDES)):
        SONDES[s].sort_index()
        if SONDES[s].raws[date_str:date_end].empty:
            ind.append(s)
    SONDES = np.delete(SONDES, ind)

    # =================== #
    # Filtrage spécifique #
    # =================== #
    log.info(' => Filtrage des données')
    ind = []
    for s in range(len(SONDES)):
        SONDES[s].convert_variables()
        SONDES[s].apply_qc(date_str, date_end)
        if SONDES[s].data[date_str:date_end].empty:
            ind.append(s)
        SONDES[s].define_position()
    SONDES = np.delete(SONDES, ind)

    # ============= #
    # Visualisation #
    # ============= #
    if make_report or save_fig or show_figs:
        log.info(' => Visualisation')
        fig = plt.figure(facecolor='w', figsize=(10, 5))
        sub = None
        for i, var in enumerate(variables_plot):
            log.debug('Variable : %s' % conf_vars[var]['longname'])
            sub = pyCoastHF.common.plot_var(
                SONDES,
                var,
                i,
                len(variables_plot),
                date_str,
                date_end,
                raw=plot_raw,
                Dmean=plot_Dmean,
                sub=sub,
                uniqueCol=False,
            )
        fig.suptitle('Site %s : %s \n Période du %s au %s' % (site, name, date_str, date_end))

    # ====================== #
    # Sauvegarde des figures #
    # ====================== #
    if save_fig:
        plt.savefig('%s.png' % site, bbox_inches='tight', dpi=300)
    if make_report:
        log.debug('Enregistrement dans le pdf')
        pdf.savefig(fig)

    # ================================ #
    # Sauvegarde en NetCDF, CSV et XLS #
    # ================================ #
    if save_data:
        log.debug(' => Sauvegarde')
        sheet_name = '%s - %s' % (site, name)
        ind = pyCoastHF.writer.daily_excel(writer, sheet_name, SONDES, date_str, date_end)
        for s in range(len(SONDES)):
            SONDES[s].save_netcdf(rep_data)
            # SONDES[s].save_csv(rep_data)
            ind = pyCoastHF.writer.daily_excel(writer, sheet_name, SONDES[s], date_str, date_end, ind)

    # ======== #
    # WEB data #
    # ======== #
    if web_daily or web_data_hf:
        log.debug('Transformation pour le site Web')
        # Journaliere
        if web_daily:
            web_data.append(pyCoastHF.webdata.merge_data(SONDES, variables_web, date_str, date_end, year=maj_1_year))
        # Haute fréquence
        if web_data_hf:
            pyCoastHF.webdata.data2JSON_HF(
                SONDES, variables_web, date_str, date_end, year=maj_1_year, transfert=transfert, latest=latest
            )

log.debug('-' * 60)

# ========= #
# Nettoyage #
# ========= #
if clean:
    if download_q2:
        log.debug('Suppression des données de Quadrige en local')
        shutil.rmtree('./tmp_SONDES/')
        os.remove('liste_sondes.csv')
        os.remove('liste_manuel.csv')
    # CDOCO
    tmp = glob.glob('./tmp_*')
    for t in tmp:
        shutil.rmtree(t)
        os.remove('valid_files_' + t.split('_')[1] + '.txt')

# Fermeture du rapport.pdf
if make_report:
    log.debug('Finalisation du pdf')
    d = pdf.infodict()
    d['Title'] = 'Rapport de mesures'
    d['Author'] = u'Sébastien Petton'
    d['CreationDate'] = dt.datetime.today()
    pdf.close()

# Fermeture du fichier xls
if save_data:
    writer.close()

# Ecriture des données Web
if web_daily:
    pyCoastHF.webdata.data2JSON_daily(
        sites, variables_web, web_data, date_str, date_end, year=maj_1_year, transfert=transfert
    )

# Affichages des figures
if show_figs:
    plt.show()

log.info('-' * 60)
log.info('Fin du script')

#!/usr/bin/env python
# -*- coding: utf8 -*-
"""
With command line in a Terminal:
    bokeh serve --show ValidManuel.py
    bokeh serve --show --log-level=debug ValidManuel.py

With Visual Studio Code:
    Launch the script with "Run Current File in Interactive Window" 

'0: No check; 1: Good data; 2: Probably good data; 3: Probably bad data; 4: Bad data; 9: Missing value'
"""
__author__ = "Sébastien Petton"

import datetime as dt
import functools
import os
import subprocess
import time

import colorcet as cc
import numpy as np
import pandas as pd
from bokeh.events import ButtonClick
from bokeh.io import curdoc
from bokeh.layouts import Spacer, column, row
from bokeh.models import (
    BoxZoomTool,
    Button,
    CheckboxButtonGroup,
    CheckboxGroup,
    ColumnDataSource,
    DateRangeSlider,
    DatetimeRangeSlider,
    Dropdown,
    FileInput,
    HoverTool,
    LinearColorMapper,
    Paragraph,
    PreText,
    Row,
    Select,
    Slider,
    Span,
    Spinner,
    TextInput,
    Tooltip,
)
from bokeh.models.widgets import Div
from bokeh.plotting import figure

import pyCoastHF

os.environ["TZ"] = "UTC+0"
time.tzset()


# For more palette, check https://colorcet.holoviz.org/user_guide/index.html
# TODO : si re-sélection d'indices du même paramètre, les indices s'incrémentent (pas de reset -> d'où le bouton !)

file_in = "SEANOE_FINAL/TH03_GLOBAL_2010_2022.nc"
file_in = "CDOCO/BESS_GLOBAL_2020_2022.nc"
file_in = "CDOCO/FIGUIER_GLOBAL_ALL.nc"
file_in = "../OUT/MA21_GLOBAL_2023.nc"
file_in = "OUT/RO16_GLOBAL_2016_2023.nc"


file_in = ""

file_in = "/Users/spetton/PROJECTS/ECOSCOPA/SONDES_HF/OUT/TH03_GLOBAL_2024.nc"

try:
    site = os.path.basename(file_in).split("_")[0]
    Type = os.path.basename(file_in).split("_")[1]
    Name = os.path.basename(file_in).split("_")[2]
    Name = Name.split(".")[0]
except IndexError:
    site = "TEST"
    Type = "GLOBAL"
    Name = "ALL"
s = pyCoastHF.sonde.SondeData(Name, Type, site)

if file_in == "":
    date_range = pd.date_range(start="2020-01-01 01:00:00", end="2021-01-01 00:00:00", freq="H")
    s.raws = pd.DataFrame(index=date_range)
    s.raws.index.name = "Date"
    np.random.seed(42)
    noise = np.random.normal(0, 0.02, size=len(date_range))
    s.raws["TEMP"] = 10 + 5 * np.sin(2 * np.pi * date_range.dayofyear / 365) + noise
    spike_ind = np.random.choice(range(len(date_range)), 30, replace=False)
    s.raws["TEMP"].iloc[spike_ind] += np.random.normal(0, 3, size=len(spike_ind))
    s.flag = s.raws * 0 + 1
else:
    s.init_read_netcdf(file_in=file_in)

s.get_qc_values()

s_extrn = pyCoastHF.sonde.SondeData("", "", "TEST")

# ----------------------------------------------------------------------#
#                        Paramètres locaux                             #
# ----------------------------------------------------------------------#
flags = [
    1,
]
param = ["TEMP"]
df = pd.DataFrame()
str_date = s.raws.index[0]
end_date = s.raws.index[-1]
conf_flags = {
    0: {"status": "No check", "color": "#1f77b4"},
    1: {"status": "Good data", "color": "#2ca02c"},
    2: {"status": "Probably good data", "color": "#0000ff"},
    3: {"status": "Probably bad data", "color": "#ff7f0e"},
    4: {"status": "Bad data", "color": "#d62728"},
    6: {"status": "Other", "color": "red"},
    9: {"status": "Missing value", "color": "#9467bd"},
}

# ----------------------------------------------------------------------#
#                 Définition des boutons de contrôles                  #
# ----------------------------------------------------------------------#
flag_name = Paragraph(text="""Show flags:""")
flag_group = CheckboxGroup(
    labels=[
        "0: No check",
        "1: Good data",
        "2: Probably good data",
        "3: Probably bad data",
        "4: Bad data",
        "6: Other",
        "9: Missing value",
    ],
    active=[1, 2, 3],
)

file_tooltip = Tooltip(content="Indicate relative or absolute path")
file_input_title = Paragraph(text="File in to modify (relative or absolute path):", align="center")
file_input = TextInput(sizing_mode="stretch_width")
file_input.value = file_in
load_in_bt = Button(label="==> Load")

plot_extrn = CheckboxButtonGroup(labels=["Plot extern data"], active=[])
plot_extrn_daily = CheckboxButtonGroup(labels=["Add daily means"], active=[])
file_extrn_title = Paragraph(text="File extern to compare with (relative or absolute path):", align="center")
file_extrn = TextInput(sizing_mode="stretch_width")  # , description=file_tooltip)
type_extrn = Select(
    value="1",
    options=[
        ("1", "classic pyCoastHF netCDF"),
        ("2", "classic Excel file"),
        ("3", "REPHY Q2 extraction"),
        ("4", "SOMLIT extraction"),
    ],
)
load_ex_bt = Button(label="==> Load")

vars_name = Paragraph(text="Variables:")
vars_group = CheckboxGroup(labels=list(s.raws), active=[list(s.raws).index("TEMP")])

flag_slct = Select(value="4", options=["0", "1", "2", "3", "4", "9"])
flag_glob = CheckboxButtonGroup(labels=["Flag all variables ?"], active=[])

bt_set = Button(label="Set flag to:")
bt_rst = Button(label="Reset sel")

plot_rst = Button(label="Reset dates")

welcome = "0 indice sélectionné"
text_banner = Paragraph(text=welcome)

bt_sav = Button(label="Save results")

rslider = DateRangeSlider(start=str_date, end=end_date, value=(str_date, end_date), title="", show_value=True)
rslider_bt = Button(label="Zoom")

d_input = TextInput(title="Year")
m_input = Slider(start=1, end=12, value=1, step=1, title="Month")
j_input2 = Slider(start=11, end=61, value=31, step=10, title="Days")
j_input1 = Slider(start=0, end=20, value=0, step=10, title="Delay from start")

plot_lines = CheckboxButtonGroup(labels=["Plot with line ?"], active=[])
plot_daily = CheckboxButtonGroup(labels=["Plot daily means ?"], active=[])
scatter_hour = CheckboxButtonGroup(labels=["Scatter with hour ?"], active=[])
scatter_with_var = CheckboxButtonGroup(labels=["Scatter with ?"], active=[])
scatter_vars = Select(value="TEMP", options=list(s.raws))

point_size = Spinner(low=0.5, high=4, step=0.1, value=2, width=55)
point_size_extrn = Spinner(low=0.5, high=8, step=0.1, value=2, width=55)
alpha_extrn = Spinner(low=0.2, high=1, step=0.1, value=0.6, width=55)
alpha_extrn_title = Paragraph(text="Opacity", align="center")
point_title = Paragraph(text="Point size", align="center")
plot_height_title = Paragraph(text="Plot height", align="center")
plot_height = Spinner(low=200, high=1200, step=100, value=600, width=75)

hover = HoverTool(tooltips=[("", "@Date{%F %H:%M} - $y")], formatters={"@Date": "datetime"})
TOOLS = [hover, "box_zoom", "wheel_zoom", "lasso_select", "box_select", "reset", "save", "pan"]


# ------------------------------------------------------------------------------------------------- #
#                                    QUALITY CHECK FILTERING
# ------------------------------------------------------------------------------------------------- #
filter_labs = ["Threshold Min", "Threshold Max", "Tukey 53H", "Climatology", "Rate of Change", "Gradient", "Spike"]
filter_keys = ["threshold_min", "threshold_max", "tukey53H", "climatology", "rate_of_change", "gradient", "spike"]
filter_mapping = dict(zip(filter_labs, filter_keys))

filt_menu = CheckboxButtonGroup(labels=["Quality check"], active=[])
rst_flag = Button(label="Reset flag for this variable")

def apply_local_qc(var, filter_name, filter_value):
    print(f"Applying QC: Variable = {var}, Filter = {filter_name}, Value = {filter_value}")
    qc_function = getattr(pyCoastHF.qc, filter_name, None)
    try:
        filter_value = filter_value.replace(',','.')
        filter_value = float(filter_value)
    except ValueError:
        print(f"Could not convert string to float:{filter_value}")
        return

    if callable(qc_function):
        qc_function(s, var, value=filter_value)
    else:
        print(f"No function found for filter '{filter_name}'")
    reset_plot()

checkboxes = []
filt_inputs = []
for label in filter_labs:
    checkboxes.append(CheckboxGroup(labels=[label], active=[]))
    filt_inputs.append(TextInput(value="0.0", width=60))

def update_widgets(attr, old, new):
    param = [vars_group.labels[i] for i in vars_group.active]
    var = param[-1]
    for i, label in enumerate(filter_labs):
        key = filter_mapping[label]
        value = s.get_filter_value(var, key)
        checkboxes[i].active = [0] if value is not None else []  # Cocher si une valeur existe
        filt_inputs[i].value = str(value) if value is not None else ""  # Définir la valeur du champ

update_widgets(None, None, None)

def on_change(attr, old, new, filter_index):
    param = [vars_group.labels[i] for i in vars_group.active]
    # Only apply QC if the corresponding checkbox is active
    if 0 in checkboxes[filter_index].active:  # Checkbox is checked
        filter_name = filter_labs[filter_index]
        key = filter_mapping[filter_name]
        filter_value = filt_inputs[filter_index].value
        apply_local_qc(param[-1], key, filter_value)


for i, (checkbox, input_field) in enumerate(zip(checkboxes, filt_inputs)):
    checkbox.on_change("active", lambda attr, old, new, idx=i: on_change(attr, old, new, idx))
    input_field.on_change("value", lambda attr, old, new, idx=i: on_change(attr, old, new, idx))

# Align checkboxes and inputs in a single row
aligned_items = []
for checkbox, input_field in zip(checkboxes, filt_inputs):
    aligned_items.append(checkbox)
    aligned_items.append(input_field)

# Combine all widgets in a single row
filt_group = Row(*aligned_items)

# ------------------------------------------------------------------------------------------------- #


# ---
#
l_apply_adjust = False
adjs_menu = CheckboxButtonGroup(labels=["Correct drift"], active=[])
# TODO : choix du step ou alors DatetimeRangeSlider sur s.raws.index)
adjs_slider = DatetimeRangeSlider(
    start=str_date, end=end_date, value=(str_date, end_date), title="", show_value=True, step=900 * 1000
)
adjs_vars = Div(text="")
adjs_span1 = Span(
    location=adjs_slider.value[0], dimension="height", line_color="blue", line_dash="dashed", line_width=1
)
adjs_span2 = Span(
    location=adjs_slider.value[1], dimension="height", line_color="blue", line_dash="dashed", line_width=1
)
adjs_slct = Select(
    value="1",
    options=[
        ("1", "Offset: raws * a + b"),
        ("2", "Linear: raws + alpha . x  where  a is the final offset"),
        ("3", "Exponential: raws + a . exp(beta . x) | b:"),
        ("4", "Logarithm: raws + ln(1 + a . x) NOT DONE"),
    ],
)
adjs_bt = Button(label="Adjust data")
adjs_bt_reset = Button(label="Reset adjust data")

adjs_input_a = TextInput(prefix="a = ")
adjs_input_b = TextInput(prefix="b = ")
# ---

# ----------------------------------------------------------------------#
#                 Fonctions de rappels (sur évenements)                #
# ----------------------------------------------------------------------#
ts = []
cds = []


def create_figure(param, flags):
    """Create a list of figures (one or each variable)
    Each couple variable <-> flag has its own ColumnDataSource and the same callback function"""
    global l_apply_adjust
    ts.clear()
    cds.clear()

    for i, var in enumerate(param):
        # Une figure par variables
        ts.append(
            figure(
                tools=TOOLS,
                toolbar_location="above",
                x_axis_type="datetime",
                active_drag="box_select",
                height=plot_height.value,
                sizing_mode="stretch_width",
                x_range=(str_date, end_date),
            )
        )
        if i != 0:
            ts[-1].x_range = ts[0].x_range

        # Ligne pour les données sélectionnées
        if plot_lines.active:
            for i, f in enumerate(flags):
                if i == 0:
                    ind = s.flag[var] == f
                else:
                    ind = ind | (s.flag[var] == f)
                raws_temp = s.raws[ind][str_date:end_date]
            if not raws_temp.empty:
                cds_tmp = ColumnDataSource(raws_temp)
                ts[-1].line(x="Date", y=var, line_width=1, line_alpha=0.8, line_color="#d3d3d3", source=cds_tmp)

        # Pour chaque flag, création d'une source CDS et affichage
        for f in flags:
            ind = s.flag[var] == f
            raws_temp = s.raws[ind][str_date:end_date]
            # if sum(iind) > 0:
            #    cds.append(ColumnDataSource(s.raws[ind]))
            if not raws_temp.empty:
                cds.append(ColumnDataSource(raws_temp))
                ts[-1].circle(
                    x="Date", y=var, size=point_size.value, color=conf_flags[f]["color"], alpha=1, source=cds[-1]
                )

                # Fonction active lors de la sélection
                cds[-1].selected.on_change(
                    "indices",
                    functools.partial(callback, cds[-1], var, f),
                )

        # Affiche les points en fonction de l'heure de la journée
        if scatter_hour.active:
            for i, f in enumerate(flags):
                if i == 0:
                    ind = s.flag[var] == f
                else:
                    ind = ind | (s.flag[var] == f)
                raws = s.raws[ind][str_date:end_date]
            if not raws.empty:
                raws["HOUR"] = raws.index.hour + raws.index.minute / 60
                cds_hour = ColumnDataSource(raws)
                color_mapper = LinearColorMapper(palette=cc.CET_C2s, low=0, high=24)
                ts[-1].circle(
                    x="Date",
                    y=var,
                    size=2,
                    color={"field": "HOUR", "transform": color_mapper},
                    source=cds_hour,
                )

        # Affiche les points en fonction de la profondeur
        if scatter_with_var.active and scatter_vars.value in list(s.raws):
            for i, f in enumerate(flags):
                if i == 0:
                    ind = s.flag[var] == f
                else:
                    ind = ind | (s.flag[var] == f)
                raws = s.raws[ind][str_date:end_date]
            ind = s.flag[scatter_vars.value] == 1
            var_tmp = s.raws.loc[ind, scatter_vars.value][str_date:end_date]
            if not raws.empty and not var_tmp.empty:
                cds_prof = ColumnDataSource(raws)
                color_mapper = LinearColorMapper(palette="Plasma256", low=var_tmp.min(), high=var_tmp.max())
                ts[-1].circle(
                    x="Date",
                    y=var,
                    size=2,
                    color={"field": scatter_vars.value, "transform": color_mapper},
                    source=cds_prof,
                )

        # Données ajustées
        if 1 in flags and var in list(s.adjs):
            adjs_temp = s.adjs.loc[str_date:end_date, :]
            if not adjs_temp.empty:
                cds_adjs = ColumnDataSource(adjs_temp)
                ts[-1].circle(x="Date", y=var, size=point_size.value, color="#d473d4", source=cds_adjs)

        # Données externe
        if plot_extrn.active:
            if var in list(s_extrn.data) and not s_extrn.data[str_date:end_date].empty:
                cds_tmp0 = ColumnDataSource(s_extrn.data[str_date:end_date])
                ts[-1].circle(
                    x="Date",
                    y=var,
                    size=point_size_extrn.value,
                    line_alpha=alpha_extrn.value,
                    fill_alpha=alpha_extrn.value,
                    color="black",
                    source=cds_tmp0,
                )
                if plot_extrn_daily.active:
                    raws_temp = s_extrn.data[str_date:end_date].resample("D").mean()
                    raws_temp.index += dt.timedelta(hours=12)
                    cds_day = ColumnDataSource(raws_temp)
                    ts[-1].line(x="Date", y=var, line_width=2, line_alpha=1, line_color="#1493ff", source=cds_day)

        # Moyenne journalière
        if plot_daily.active:
            for i, f in enumerate(flags):
                if i == 0:
                    ind = s.flag[var] == f
                else:
                    ind = ind | (s.flag[var] == f)
                raws_temp = s.raws[ind][str_date:end_date]
            if not raws_temp.empty:
                raws_temp = raws_temp.resample("D").mean()
                raws_temp.index += dt.timedelta(hours=12)
                cds_day = ColumnDataSource(raws_temp)
                ts[-1].line(x="Date", y=var, line_width=2, line_alpha=1, line_color="#DC143C", source=cds_day)

        # Encadrement de la zone à ajuster
        if adjs_menu.active:
            ts[-1].add_layout(adjs_span1)
            ts[-1].add_layout(adjs_span2)

    for t in ts:
        t.x_range.on_change("start", zoom_update)
        t.x_range.on_change("end", zoom_update)

    # Apply Quality Check
    ts.append(filt_menu)
    if filt_menu.active and len(param) >= 1:
        ts.append(filtqc)

    # Correct drift
    ts.append(adjs_menu)
    if adjs_menu.active and len(param) >= 1:
        ts.append(adjust)
        d_str = dt.datetime.fromtimestamp(adjs_slider.value[0] / 1e3)
        d_end = dt.datetime.fromtimestamp(adjs_slider.value[1] / 1e3)
        if l_apply_adjust:
            l_apply_adjust = False
        else:
            adjs_slider.start = str_date
            adjs_slider.end = end_date
            adjs_slider.value = (str_date, end_date)
        var = param[-1]
        raws_temp = s.raws[var][str_date:end_date]
        if not raws_temp.empty:
            adjs_vars.text = "%s <br /> %s = %.3f <br /> %s = %.3f" % (
                var,
                raws_temp.index[0],
                raws_temp.iloc[0],
                raws_temp.index[-1],
                raws_temp.iloc[-1],
            )

    ts.append(extern)

    return column(ts, sizing_mode="stretch_width")


def update(attr, old, new):
    """Update all graphs if modification on variables or flags
    Delete the ongoing selection"""
    global df

    df = pd.DataFrame()
    text_banner.text = welcome

    flags = [int(flag_group.labels[i].split(":")[0]) for i in flag_group.active]
    param = [vars_group.labels[i] for i in vars_group.active]

    layout.children[1].children[1] = create_figure(param, flags)


def callback(source, var, flag, attrname, old, new):
    """Add selected indices to DataFrame df"""
    global df

    # Indices sélectionnés
    ind = source.selected.indices

    # Valeurs sélectionnées
    tp = pd.DataFrame(data={"%s" % var: source.data[var][ind]}, index=source.data["Date"][ind])

    # Regroupement (et/ou remplacement si pré-sélection)
    # TODO : si re-sélection d'indices du même paramètre, les indices s'incrémentent (pas de reset -> d'où le bouton !)
    if var in list(df):
        # df.drop([var], axis=1, inplace=True)
        # df.dropna(axis=0, how='all', inplace=True)
        df = pd.concat([df, tp], axis=0, sort=True)
        df = df.loc[~df.index.duplicated(keep="first")]
    else:
        df = pd.concat([df, tp], axis=1, sort=True)
    text_banner.text = "%d indices sélectionnées" % len(df)


def date_range_correct(attr, old, new):
    """Update data values upon slider"""
    param = [vars_group.labels[i] for i in vars_group.active]
    if len(param) >= 1:
        var = param[-1]
        d_str = dt.datetime.fromtimestamp(adjs_slider.value[0] / 1e3)
        d_end = dt.datetime.fromtimestamp(adjs_slider.value[1] / 1e3)
        raws_temp = s.raws[var][d_str:d_end]
        if not raws_temp.empty:
            adjs_vars.text = "%s <br /> %s = %.3f <br /> %s = %.3f" % (
                var,
                raws_temp.index[0],
                raws_temp.iloc[0],
                raws_temp.index[-1],
                raws_temp.iloc[-1],
            )
        adjs_span1.location = adjs_slider.value[0]
        adjs_span2.location = adjs_slider.value[1]


def zoom_update(attr, old, new):
    """Update Slider value after a box_zoom or wheel_zoom event"""
    rslider.value = (ts[0].x_range.start, ts[0].x_range.end)


def date_update(attr, old, new):
    """Update date plot range upon Year input"""
    global str_date, end_date
    try:
        year = int(new)
        str_date = dt.datetime(year, 1, 1)
        end_date = dt.datetime(year + 1, 1, 1)

        reset_plot()
    except ValueError:
        print("Mauvais format d'année")


def date_update_month(attr, old, new):
    """Update date plot range upon Month input"""
    global str_date, end_date

    try:
        year = str_date.year
        month = m_input.value
        str_date = dt.datetime(year, month, 1) + dt.timedelta(days=j_input1.value)
        end_date = str_date + dt.timedelta(days=j_input2.value)

        reset_plot()
    except ValueError:
        print("Mauvais format d'année")
    except AttributeError:
        print("Zoom en cours -> obligation de repasser par une année")


def reset_plot():
    global df
    df = pd.DataFrame()
    text_banner.text = welcome
    update(0, 0, 0)


def cb_zoom_update(event):
    """Set zoom when click upon rslider values"""
    global df, str_date, end_date
    str_date = dt.datetime.fromtimestamp(rslider.value[0] / 1e3)
    end_date = dt.datetime.fromtimestamp(rslider.value[1] / 1e3)
    reset_plot()


def cb_setflag(event):
    """Set flag to selected value (global or local)"""
    global df, str_date, end_date

    # Keep zoom
    str_date = ts[0].x_range.start
    end_date = ts[0].x_range.end

    if flag_glob.active:
        s.flag.loc[df.index] = int(flag_slct.value)
    else:
        for v in list(df):
            ind = df[df[v].notnull()].index
            s.flag[v][ind] = int(flag_slct.value)

    reset_plot()


def cb_reset(event):
    """Reset selected points"""
    reset_plot()


def cb_plot(event):
    """Reset selected dates"""
    global df, str_date, end_date

    str_date = s.raws.index[0]
    end_date = s.raws.index[-1]

    reset_plot()


def cb_save_ncdf(event):
    s.save_netcdf("OUT_MANUEL")
    print("Fichier sauvé : ok")


def apply_offset(var, d_str, d_end):
    try:
        a = float(adjs_input_a.value)
    except ValueError:
        print("Mauvais format d'offset (paramètre a)")
        return 0

    try:
        b = float(adjs_input_b.value)
    except ValueError:
        print("Mauvais format d'offset (paramètre b)")
        return 0

    ind = s.flag.loc[d_str:d_end, var] == 1
    print(a * s.raws.loc[d_str:d_end, var][ind] + b)
    s.adjs.loc[d_str:d_end, var][ind] = a * s.raws.loc[d_str:d_end, var][ind] + b
    print(s.adjs.loc[d_str:d_end, var][ind])


def apply_linear(var, d_str, d_end):
    try:
        y = float(adjs_input_a.value)
    except ValueError:
        print("Mauvais format de valeur de biais (paramètre a)")
        return 0

    ind = s.flag.loc[d_str:d_end, var] == 1
    tmp = s.raws.loc[d_str:d_end, var][ind]

    a = (y - tmp[-1]) / (d_end - d_str).total_seconds()
    X = (tmp.index - tmp.index[0]).total_seconds().astype("float32")

    print(X)
    adjs_values = s.raws.loc[d_str:d_end, var][ind] + a * X
    print(adjs_values)
    s.adjs.loc[d_str:d_end, var][ind] = s.raws.loc[d_str:d_end, var][ind] + a * X
    print(s.adjs.loc[d_str:d_end, var][ind])
    s.adjs.loc[d_str:d_end, var][ind] = adjs_values
    print(s.adjs.loc[d_str:d_end, var][ind])


def apply_exponential(var, d_str, d_end):
    try:
        a = float(adjs_input_a.value)
    except ValueError:
        print("Mauvais format de valeur initiale (paramètre a)")
        return 0

    try:
        y = float(adjs_input_b.value)
    except ValueError:
        print("Mauvais format de valeur finale (paramètre b)")
        return 0

    ind = s.flag.loc[d_str:d_end, var] == 1
    tmp = s.raws.loc[d_str:d_end, var][ind]

    b = np.log((y - tmp[-1]) / a) / (d_end - d_str).total_seconds()
    X = (tmp.index - tmp.index[0]).total_seconds()
    s.adjs.loc[d_str:d_end, var][ind] = s.raws.loc[d_str:d_end, var][ind] + a * np.exp(b * X)


def cb_adjust(event):
    """Apply selected adjustment method"""
    global l_apply_adjust

    param = [vars_group.labels[i] for i in vars_group.active]
    if len(param) >= 1:
        var = param[-1]
        d_str = dt.datetime.fromtimestamp(adjs_slider.value[0] / 1e3)
        d_end = dt.datetime.fromtimestamp(adjs_slider.value[1] / 1e3)

        if adjs_slct.value == "1":
            l_apply_adjust = True
            apply_offset(var, d_str, d_end)
        elif adjs_slct.value == "2":
            l_apply_adjust = True
            apply_linear(var, d_str, d_end)
        elif adjs_slct.value == "3":
            l_apply_adjust = True
            apply_exponential(var, d_str, d_end)
        reset_plot()


def cb_reset_adjust(event):
    """Reset adjusted values"""
    global l_apply_adjust

    param = [vars_group.labels[i] for i in vars_group.active]
    if len(param) >= 1:
        var = param[-1]
        d_str = dt.datetime.fromtimestamp(adjs_slider.value[0] / 1e3)
        d_end = dt.datetime.fromtimestamp(adjs_slider.value[1] / 1e3)

        l_apply_adjust = True

        s.adjs.loc[d_str:d_end, var] = np.NaN

        reset_plot()


def cb_reset_flag(event):
    """Reset flag values"""
    d_str = ts[0].x_range.start
    d_end = ts[0].x_range.end
    param = [vars_group.labels[i] for i in vars_group.active]
    for var in param:
        s.flag.loc[d_str:d_end, var] = 1
    pyCoastHF.qc.flag_missing_values(s)
    reset_plot()


def cb_load_in(event):
    """Load new file"""
    global s
    try:
        site = os.path.basename(file_input.value).split("_")[0]
        Type = os.path.basename(file_input.value).split("_")[1]
        Name = os.path.basename(file_input.value).split("_")[2]
        Name = Name.split(".")[0]
    except IndexError:
        site = "TEST"
        Type = "GLOBAL"
        Name = "ALL"
    try:
        s = pyCoastHF.sonde.SondeData(Name, Type, site)
        s.init_read_netcdf(file_in=file_input.value)
        vars_group.labels = list(s.raws)
        vars_group.active = [list(s.raws).index("TEMP")]
        reset_plot()
    except:
        print("Unable to load file")


def cb_load_ex(event):
    """Load external file"""
    global s_extrn
    try:
        s_extrn = pyCoastHF.sonde.SondeData("", "", "TEST")
        if type_extrn.value == "1":
            s_extrn.init_read_netcdf(file_in=file_extrn.value)
        elif type_extrn.value == "2":
            data = pd.read_excel(file_in=file_extrn.value)
            data = data[pd.notnull(data.Date)]
            s_extrn.data = data.set_index(data["Date"]).drop(["Date"], axis=1)
        elif type_extrn.value == "3":
            s_extrn = pyCoastHF.sonde.read_local_rephy(s.site, fileIN=file_extrn.value)[0]
        elif type_extrn.value == "4":
            s_extrn = pyCoastHF.sonde.read_local_somlit(s.site, fileIN=file_extrn.value)[0]
        reset_plot()
        print("External file loaded")
    except FileNotFoundError:
        print("Unable to load file")


# ----------------------------------------------------------------------#
#            Mise en place du GUI et appels aux fonctions              #
# ----------------------------------------------------------------------#
controls = column(
    flag_name,
    flag_group,
    vars_name,
    vars_group,
    row(point_title, point_size),
    row(plot_height_title, plot_height),
    Spacer(height=20),
    rslider,
    rslider_bt,
    plot_rst,
    d_input,
    m_input,
    j_input2,
    j_input1,
    Spacer(height=20),
    column(plot_lines, plot_daily, scatter_hour, row(scatter_with_var, scatter_vars), background="#F0F8FF"),
    Spacer(height=20),
    text_banner,
    bt_rst,
    row(bt_set, flag_slct),
    flag_glob,
    Spacer(height=40),
    bt_sav,
    width=200,
)


filtqc = column(
    row(rst_flag),
    Row(*aligned_items),
    background="coral",
    sizing_mode="stretch_width",
)

adjust = row(
    column(adjs_slider, adjs_vars),
    column(adjs_slct, adjs_input_a, adjs_input_b),
    column(adjs_bt, adjs_bt_reset),
)

inputs = row(file_input_title, file_tooltip, file_input, load_in_bt, sizing_mode="stretch_width", background="beige")
extern = row(
    column(
        row(plot_extrn, point_title, point_size_extrn, alpha_extrn_title, alpha_extrn, plot_extrn_daily),
        row(file_extrn_title, file_extrn, type_extrn, load_ex_bt, sizing_mode="stretch_width"),
        sizing_mode="stretch_width",
    ),
    sizing_mode="stretch_width",
    background="beige",
)

layout = column(
    inputs,
    row(controls, column(create_figure(param, flags), sizing_mode="stretch_width"), sizing_mode="stretch_width"),
    sizing_mode="stretch_width",
)


rst_flag.on_event(ButtonClick, cb_reset_flag)

load_in_bt.on_event(ButtonClick, cb_load_in)
load_ex_bt.on_event(ButtonClick, cb_load_ex)

flag_group.on_change("active", update)
vars_group.on_change("active", update)
plot_lines.on_change("active", update)
plot_daily.on_change("active", update)
plot_extrn_daily.on_change("active", update)
scatter_hour.on_change("active", update)
scatter_with_var.on_change("active", update)
plot_extrn.on_change("active", update)
adjs_menu.on_change("active", update)
filt_menu.on_change("active", update)

plot_rst.on_event(ButtonClick, cb_plot)

rslider_bt.on_event(ButtonClick, cb_zoom_update)
bt_rst.on_event(ButtonClick, cb_reset)
bt_set.on_event(ButtonClick, cb_setflag)
bt_sav.on_event(ButtonClick, cb_save_ncdf)
adjs_bt.on_event(ButtonClick, cb_adjust)
adjs_bt_reset.on_event(ButtonClick, cb_reset_adjust)

alpha_extrn.on_change("value", update)
point_size.on_change("value", update)
point_size_extrn.on_change("value", update)
plot_height.on_change("value", update)
adjs_slider.on_change("value", date_range_correct)
d_input.on_change("value", date_update)
m_input.on_change("value", date_update_month)
j_input1.on_change("value", date_update_month)
j_input2.on_change("value", date_update_month)

curdoc().add_root(layout)
curdoc().title = "Validation manuelle version 2"


if __name__ == "__main__":
    subprocess.call(["bokeh", "serve", "--show", "ValidManuel.py"])

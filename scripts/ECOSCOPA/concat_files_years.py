#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on 06 Dec 2013
Modified on 17 Nov 2016
Adapted on 08 Sep 2017
@author: S. Petton
"""

import glob

import pandas as pd

import pyCoastHF

liste_sites = ('KERA', 'ROSC')
liste_sites = ('BV02', 'CA02', 'BR08', 'PF02', 'QB02', 'BO02', 'MA03', 'MA21', 'AR03', 'AR11', 'TH03')
liste_sites = ('BR08', 'QB02', 'MA21', 'AR11', 'TH03')

for site in liste_sites:
    f1 = sorted(glob.glob('SEANOE_FINAL/%s*.nc' % site))[0]
    print(f1)
    s1 = pyCoastHF.sonde.SondeData(site, '', site)
    s1.init_read_netcdf(file_in=f1)

    s2 = pyCoastHF.sonde.SondeData(site, '', site)
    try:
        s2.init_read_netcdf(file_in='./OUT/%s_GLOBAL_2024.nc' % site)
    except IOError:
        s2.init_read_netcdf(file_in='./DATA/%s_CDOCO_GLOBAL.nc' % site)

    raws = pd.concat([s1.raws, s2.raws], axis=0)
    flag = pd.concat([s1.flag, s2.flag], axis=0)

    S = pyCoastHF.sonde.SondeData('ALL', 'GLOBAL', site)
    S.raws = raws
    S.flag = flag
    S.define_position()
    S.save_netcdf(rep='./OUT/')

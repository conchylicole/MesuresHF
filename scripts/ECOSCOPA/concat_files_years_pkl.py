#!/usr/bin/env python
# -*- coding: utf-8 -*-

import glob
import pandas as pd

sites=('BV02','CA02','BR08','PF02','BO02','MA03','AR03','TH03',) # ECOSCOPA

for site in sites:
    df = pd.DataFrame()
    files = sorted(glob.glob('donnees_siteweb_local/%s_????*.pkl'%site))
    for fic in files:
        tp = pd.read_pickle(fic)
        df = pd.concat([df, tp], axis=0)
    df = df[~df.index.duplicated(keep='first')]
    df.to_pickle('donnees_siteweb_local/sondes/%s_all.pkl'%site)


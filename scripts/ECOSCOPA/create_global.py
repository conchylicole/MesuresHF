#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
 Created on 06 Dec 2013
 Modified on 17 Nov 2016
 Adapted on 08 Sep 2017
 @author: S. Petton

 Usage:
./create_global.py BR08
./create_global.py QB02
./create_global.py MA21
./create_global.py AR11
./create_global.py TH03
"""

# ----------------------------------------------------------------------#
#                      Chargement des modules                           #
# ----------------------------------------------------------------------#
import datetime as dt
import glob
import sys
import logging

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import rc

import pyCoastHF

rc("font", **{"size": "7", "family": "serif", "serif": ["Latin Modern Roman"], "weight": "bold"})

# ----------------------------------------------------------------------#
#                    Definition des parametres                          #
# ----------------------------------------------------------------------#

# Dates
date_str = dt.datetime.strptime("01-01-2024", "%d-%m-%Y")
date_end = dt.datetime.strptime("01-01-2025", "%d-%m-%Y")
date_end = dt.datetime.utcnow().replace(second=0, microsecond=0)

# Définition de quelles sondes prendre pour chaque paramètres
site = sys.argv[1]
periods = pyCoastHF.common.load_periods(site)

# Répertoire où sont les netCDF
rep_data = "DATA"

# Lecture du fichier de configuration
conf_sites = pyCoastHF.common.data_config("sites")

# Variables à sauver
var_to_save = conf_sites[site]["vars"]
var_to_plot = ["PROF", "TEMP", "PSAL"]
# fmt : off
if site == "AR11":
    var_to_plot = [["TEMP", "PSAL"]]
if site == "BR08":
    var_to_plot = [["TEMP", "PSAL", "PROF"], ["FLUO", "TURB"]]
if site == "MA03":
    var_to_plot = [["TEMP", "PSAL", "PROF"], ["FLUO", "TURB"]]
if site == "AR03":
    var_to_plot = [["TEMP", "PSAL", "PROF"], ["FLUO", "TURB"]]
if site == "MA21":
    var_to_plot = [["TEMP", "PSAL"], ["FLUO", "TURB"]]
if site == "TH03":
    var_to_plot = [["TEMP", "PSAL"], ["FLUO", "TURB"]]
if site == "QB02":
    var_to_plot = [["PROF", "TEMP", "PSAL"], ["FLUO", "TURB", "DOX1"]]
if site == "KERA":
    var_to_plot = [["TEMP", "PSAL", "PROF"], ["FLUO", "TURB", "OSAT"]]
if site == "ROSC":
    var_to_plot = [["TEMP", "PSAL", "PROF"], ["FLUO", "TURB", "OSAT"]]
# fmt : on

debug = False
if len(sys.argv) >= 3:
    debug = True
    df = pd.read_pickle("TIDE/predictions_250m.pkl")

# ----------------------------------------------------------------------#
#                   Lecture de tous les NetCDF                          #
# ----------------------------------------------------------------------#
SONDES = []
files_list = sorted(glob.glob("%s/%s*.nc" % (rep_data, site)))
for f in files_list:
    type_sonde = f.split("_")[1]
    if type_sonde == "MANUEL":
        continue
    name = f.split("_")[2].split(".")[0]
    SONDES.append(pyCoastHF.sonde.SondeData(name, type_sonde, site))
    SONDES[-1].init_read_netcdf(rep_data)

# ----------------------------------------------------------------------#
#                      Filtrage sur dates                               #
# ----------------------------------------------------------------------#
ind = []
for s in range(len(SONDES)):
    if SONDES[s].data[date_str:date_end].empty:
        ind.append(s)
SONDES = np.delete(SONDES, ind)

# ----------------------------------------------------------------------#
#       Pour chaque variable, selection couple (date / valeur)          #
# ----------------------------------------------------------------------#
list_sondes = [s.Type + "_" + s.Name for s in SONDES]
RAWS = []
FLAG = []
for v in var_to_save:
    if v == "CHLO":
        period = periods["FLUO"]  # noqa:F821
    elif v == "OSAT":
        period = periods["DOX1"]  # noqa:F821
    else:
        period = periods.get(v)
    d_raws = pd.DataFrame()
    d_flag = pd.DataFrame()
    i = 0
    for p in period:
        if p[0] not in list_sondes:
            continue
        s = list_sondes.index(p[0])
        if v not in list(SONDES[s].data):
            logging.error("Pas de paramètre %s dans la sonde %s %s - %s" % (v, SONDES[s].Type, SONDES[s].Name, p[1]))
            continue

        date_str_p = max(date_str, dt.datetime.strptime(p[1], "%Y/%m/%d %H:%M"))
        date_end_p = min(date_end, dt.datetime.strptime(p[2], "%Y/%m/%d %H:%M")) - dt.timedelta(seconds=1)

        tmp_raws = SONDES[s].raws[date_str_p:date_end_p][v]
        tmp_flag = SONDES[s].flag[date_str_p:date_end_p][v]
        if tmp_raws.empty:
            continue

        if i == 0:
            d_raws = pd.DataFrame(tmp_raws)
            d_flag = pd.DataFrame(tmp_flag)
            i+= 1
        else:
            d_raws = pd.concat([d_raws, pd.DataFrame(tmp_raws)], axis=0)
            d_flag = pd.concat([d_flag, pd.DataFrame(tmp_flag)], axis=0)

        if d_raws.index.duplicated(keep=False).any():
            logging.error(d_raws[d_raws.index.duplicated(keep=False)])
            logging.error(v)
            exit(1)

    d_raws.rename(columns={0: v}, inplace=True)
    d_flag.rename(columns={0: v}, inplace=True)
    RAWS.append(d_raws)
    FLAG.append(d_flag)


# ----------------------------------------------------------------------#
#       Regroupement des variables sur un vecteur date commun           #
# ----------------------------------------------------------------------#
raws = pd.DataFrame()
for df_ in RAWS:
    raws = pd.concat([raws, df_], axis=1)
raws.sort_index(inplace=True)

flag = pd.DataFrame()
for df_ in FLAG:
    flag = pd.concat([flag, df_], axis=1)
flag.sort_index(inplace=True)
flag[flag.isnull()] = 9

# ----------------------------------------------------------------------#
#           Ecriture : on créé une sonde fictive GLOBAL                 #
# ----------------------------------------------------------------------#
S = pyCoastHF.sonde.SondeData("%d" % date_str.year, "GLOBAL", site)
S.raws = raws
S.flag = flag
S.define_data()
S.define_position()
if not debug:
    S.save_netcdf(rep="./OUT")

# ----------------------------------------------------------------------#
#                    Visualisation des variables                        #
# ----------------------------------------------------------------------#
fileout = "%s.png" % site
plt.figure(figsize=(15, 7))
sub = None

MANUEL = []
try:
    MANUEL += pyCoastHF.sonde.read_local_manuel(site, "Samplings/Synthese_Environnementale_"+site+".xlsx", skiprows=8)
except FileNotFoundError:
    pass
try:
    MANUEL += pyCoastHF.sonde.read_local_rephy(site, date_str=date_str, date_end=date_end)
except KeyError:
    print('AIE')
    pass
MANUEL += pyCoastHF.sonde.read_local_manuel(site, 'Fichiers_Bruts/Mesures_MANUEL.xlsx')
if not debug:
    SONDES = np.concatenate(((S,), MANUEL), axis=0)
else:
    SONDES = np.concatenate((SONDES, (S,), MANUEL), axis=0)

for i, var in enumerate(var_to_plot):
    sub = pyCoastHF.common.plot_var(
        SONDES, var, i, len(var_to_plot), date_str, date_end, raw=False, Dmean=False, sub=sub
    )
plt.savefig("FIGURES/%s_%d.png" % (site, date_str.year), bbox_inches="tight", dpi=300)
# dtide = dtide[date_str:date_end]
# sub[0][0].plot(dtide.SSH - thrs_tide)
"""
if site == "TH03" and not debug:
    # Température
    df = pd.read_csv("THAU/valid_temp_table_ifremer_.txt",     skiprows=1, usecols=(0,1), names=["Date","TEMP"],
                     delimiter=";", decimal=".")
    df["Date"] = pd.to_datetime(df["Date"], format="%d/%m/%Y %H:%M", errors="raise")
    df = df.set_index(df["Date"]).drop(["Date"], axis=1)
    # Salinité
    ps = pd.read_csv("THAU/valid_salinity_table_ifremer_.txt", skiprows=1, usecols=(0,1), names=["Date","PSAL"],
                     delimiter=";", decimal=".")
    ps["Date"] = pd.to_datetime(ps["Date"], format="%d/%m/%Y %H:%M", errors="raise")
    ps = ps.set_index(ps["Date"]).drop(["Date"], axis=1)
    ind = ~ ps.index.duplicated(keep="last")
    ps = ps[ind]
    data = pd.concat([df, ps], axis=1, sort=False)

    sub[1][0].plot(data["TEMP"],".g", markersize=.4, alpha=.5)
    sub[2][0].plot(data["PSAL"],".g", markersize=.4, alpha=.5)
"""
# SONDES[0].save_OCO_csv()
plt.show()

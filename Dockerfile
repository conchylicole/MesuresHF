# syntax=docker/dockerfile:experimental

ARG DOCKER_PYTHON_NAME="gitlab-registry.ifremer.fr/ifremer-commons/docker/images/python"
ARG DOCKER_PYTHON_VERSION=3.9-slim

FROM $DOCKER_PYTHON_NAME:$DOCKER_PYTHON_VERSION as python-base
ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_HOME="/opt/poetry" \
    POETRY_VIRTUALENVS_IN_PROJECT=true \
    POETRY_NO_INTERACTION=1 \
    PYSETUP_PATH="/opt/pysetup" \
    VENV_PATH="/opt/pysetup/.venv"

ENV PATH="$POETRY_HOME/bin:$VENV_PATH/bin:$PATH"

# ======================================================================================================================
# builder-base is used to build dependencies
# ======================================================================================================================
FROM python-base as builder-base

# Install dependencies (curl, git)
RUN buildDeps="build-essential" \
	&& apt-get -y update \
    && apt-get -y upgrade \
    && apt-get install --no-install-recommends --no-install-suggests -y curl git  \
    && apt-get -y purge --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
    && rm -rf /var/lib/apt/lists/*

# Install Poetry - respects $POETRY_VERSION & $POETRY_HOME
#ENV POETRY_VERSION=1.1.13
#SHELL ["/bin/bash", "-o", "pipefail", "-c"]
##RUN curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python && \
#    chmod a+x /opt/poetry/bin/poetry
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
ENV POETRY_VERSION=1.2.2
RUN curl -sSL https://install.python-poetry.org | python3 - --version 1.2.2 && \
    chmod a+x /opt/poetry/bin/poetry

# We copy our Python requirements here to cache them
# and install only runtime deps using poetry
WORKDIR $PYSETUP_PATH
COPY ./poetry.lock ./pyproject.toml ./
RUN git init && \
    poetry add poetry-dynamic-versioning==0.14.0 && \
    poetry install --no-dev --no-root

# ======================================================================================================================
# 'development' stage installs all dev deps and can be used to develop code.
# For example using docker-compose to mount local volume under /app
# ======================================================================================================================
FROM builder-base as development

# venv already has runtime deps installed we get a quicker install
WORKDIR $PYSETUP_PATH
RUN poetry install --no-root

# ======================================================================================================================
# 'production' stage uses the clean 'python-base' stage and copyies
# in only our runtime deps that were installed in the 'builder-base'
# ======================================================================================================================
FROM python-base as runtime

# Install dependencies (curl)
RUN buildDeps="build-essential" \
	&& apt-get -y update \
    && apt-get -y upgrade \
    && apt-get install --no-install-recommends --no-install-suggests -y curl  \
    && apt-get -y purge --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
    && rm -rf /var/lib/apt/lists/*

COPY --from=builder-base $VENV_PATH $VENV_PATH
